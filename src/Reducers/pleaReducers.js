import { COUPOM_REDEEM_PLEA_SUCCESS } from "../Constants/pleaConstants";
import { COUPOM_REDEEM_PLEA_RESET } from "../Constants/pleaConstants";
import { COUPOM_REDEEM_PLEA_FAIL } from "../Constants/pleaConstants";
import { COUPOM_REDEEM_PLEA_REQUEST } from "../Constants/pleaConstants";
import {
  ALL_PLEA_LIST_FAIL,
  ALL_PLEA_LIST_REQUEST,
  ALL_PLEA_LIST_SUCCESS,
  ALL_PLEA_LIST_BY_USER_FAIL,
  ALL_PLEA_LIST_BY_USER_REQUEST,
  ALL_PLEA_LIST_BY_USER_SUCCESS,
  CAMPAIGN_ALLY_REJECT_REQUEST,
  CAMPAIGN_ALLY_REJECT_SUCCESS,
  CAMPAIGN_ALLY_REJECT_FAIL,
  CAMPAIGN_ALLY_REJECT_RESET,
  CAMPAIGN_ALLY_GRANT_REQUEST,
  CAMPAIGN_ALLY_GRANT_SUCCESS,
  CAMPAIGN_ALLY_GRANT_FAIL,
  CAMPAIGN_ALLY_GRANT_RESET,
} from "../Constants/pleaConstants";

export function allPleasListReducer(state = { allPleas: [] }, action) {
  switch (action.type) {
    case ALL_PLEA_LIST_REQUEST:
      return { loading: true };
    case ALL_PLEA_LIST_SUCCESS:
      return { loading: false, success: true, allPleas: action.payload };
    case ALL_PLEA_LIST_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}
export function allPleasListByUserReducer(state = { allPleas: [] }, action) {
  switch (action.type) {
    case ALL_PLEA_LIST_BY_USER_REQUEST:
      return { loading: true };
    case ALL_PLEA_LIST_BY_USER_SUCCESS:
      return { loading: false, success: true, allPleas: action.payload };
    case ALL_PLEA_LIST_BY_USER_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

export function campaignAllyPleaRejectReducer(state = {}, action) {
  switch (action.type) {
    case CAMPAIGN_ALLY_REJECT_REQUEST:
      return { loading: true };
    case CAMPAIGN_ALLY_REJECT_SUCCESS:
      return { loading: false, success: true, plea: action.payload };
    case CAMPAIGN_ALLY_REJECT_FAIL:
      return { loading: false, error: action.payload };
    case CAMPAIGN_ALLY_REJECT_RESET:
      return {};
    default:
      return state;
  }
}

export function campaignAllyPleaGrantReducer(state = {}, action) {
  switch (action.type) {
    case CAMPAIGN_ALLY_GRANT_REQUEST:
      return { loading: true };
    case CAMPAIGN_ALLY_GRANT_SUCCESS:
      return { loading: false, success: true, plea: action.payload };
    case CAMPAIGN_ALLY_GRANT_FAIL:
      return { loading: false, error: action.payload };
    case CAMPAIGN_ALLY_GRANT_RESET:
      return {};
    default:
      return state;
  }
}

export function coupomRedeemUserPleaReducer(state = {}, action) {
  switch (action.type) {
    case COUPOM_REDEEM_PLEA_REQUEST:
      return { loading: true };
    case COUPOM_REDEEM_PLEA_SUCCESS:
      return { loading: false, success: true, plea: action.payload };
    case COUPOM_REDEEM_PLEA_FAIL:
      return { loading: false, error: action.payload };
    case COUPOM_REDEEM_PLEA_RESET:
      return {};
    default:
      return state;
  }
}
