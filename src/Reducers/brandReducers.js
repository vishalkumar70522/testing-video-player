import {
  BRAND_CREATE_PROFILE_REQUEST,
  BRAND_CREATE_PROFILE_SUCCESS,
  BRAND_CREATE_PROFILE_FAIL,
  BRAND_CREATE_PROFILE_RESET,
  BRAND_UPDATE_PROFILE_REQUEST,
  BRAND_UPDATE_PROFILE_SUCCESS,
  BRAND_UPDATE_PROFILE_FAIL,
  BRAND_UPDATE_PROFILE_RESET,
  BRAND_DETAILS_REQUEST,
  BRAND_DETAILS_SUCCESS,
  BRAND_DETAILS_FAIL,
} from "../Constants/brandConstants";

export function brandCreateProfileReducer(state = {}, action) {
  switch (action.type) {
    case BRAND_CREATE_PROFILE_REQUEST:
      return {
        loading: true,
      };
    case BRAND_CREATE_PROFILE_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload,
      };
    case BRAND_CREATE_PROFILE_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    case BRAND_CREATE_PROFILE_RESET:
      return {};
    default:
      return state;
  }
}

export function brandUpdateProfileReducer(state = {}, action) {
  switch (action.type) {
    case BRAND_UPDATE_PROFILE_REQUEST:
      return {
        loading: true,
      };
    case BRAND_UPDATE_PROFILE_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload,
      };
    case BRAND_UPDATE_PROFILE_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    case BRAND_UPDATE_PROFILE_RESET:
      return {};
    default:
      return state;
  }
}
export function brandDetailsGetReducer(state = {}, action) {
  switch (action.type) {
    case BRAND_DETAILS_REQUEST:
      return {
        loading: true,
      };
    case BRAND_DETAILS_SUCCESS:
      return {
        loading: false,
        brandDetails: action.payload,
      };
    case BRAND_DETAILS_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}
