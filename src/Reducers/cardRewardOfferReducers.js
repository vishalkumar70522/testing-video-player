import {
  CARD_REWARD_CREATE_FAIL,
  CARD_REWARD_CREATE_REQUEST,
  CARD_REWARD_CREATE_RESET,
  CARD_REWARD_CREATE_SUCCESS,
  CARD_REWARD_DETAILS_FAIL,
  CARD_REWARD_DETAILS_REQUEST,
  CARD_REWARD_DETAILS_SUCCESS,
  CARD_REWARD_LIST_FOR_BRAND_FAIL,
  CARD_REWARD_LIST_FOR_BRAND_REQUEST,
  CARD_REWARD_LIST_FOR_BRAND_SUCCESS,
  UPDATE_CARD_REWARD_FAIL,
  UPDATE_CARD_REWARD_REQUEST,
  UPDATE_CARD_REWARD_RESET,
  UPDATE_CARD_REWARD_SUCCESS,
} from "../Constants/cardRewardOfferConstants";

export function cardRewardProgramCreateReducer(state = {}, action) {
  switch (action.type) {
    case CARD_REWARD_CREATE_REQUEST:
      return {
        loading: true,
      };
    case CARD_REWARD_CREATE_SUCCESS:
      return {
        loading: false,
        success: true,
        rewardProgram: action.payload,
      };
    case CARD_REWARD_CREATE_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    case CARD_REWARD_CREATE_RESET:
      return {};
    default:
      return state;
  }
}

export function cardRewardOfferListForBrandGetReducer(state = {}, action) {
  switch (action.type) {
    case CARD_REWARD_LIST_FOR_BRAND_REQUEST:
      return {
        loading: true,
      };
    case CARD_REWARD_LIST_FOR_BRAND_SUCCESS:
      return {
        loading: false,
        success: true,
        myRewardPrograms: action.payload,
      };
    case CARD_REWARD_LIST_FOR_BRAND_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}

export function cardRewardOfferDetailsGetReducer(state = {}, action) {
  switch (action.type) {
    case CARD_REWARD_DETAILS_REQUEST:
      return {
        loading: true,
      };
    case CARD_REWARD_DETAILS_SUCCESS:
      return {
        loading: false,
        success: true,
        rewardProgramDetails: action.payload,
      };
    case CARD_REWARD_DETAILS_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}

export function cardRewardProgramUpdateReducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_CARD_REWARD_REQUEST:
      return {
        loading: true,
      };
    case UPDATE_CARD_REWARD_SUCCESS:
      return {
        loading: false,
        success: true,
        updatedRewardProgram: action.payload,
      };
    case UPDATE_CARD_REWARD_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    case UPDATE_CARD_REWARD_RESET:
      return {};
    default:
      return state;
  }
}
