import {
  ADD_REDEEMER_TO_COUPON_REWARD_FAIL,
  ADD_REDEEMER_TO_COUPON_REWARD_REQUEST,
  ADD_REDEEMER_TO_COUPON_REWARD_RESET,
  ADD_REDEEMER_TO_COUPON_REWARD_SUCCESS,
  COUPON_REWARD_CREATE_FAIL,
  COUPON_REWARD_CREATE_REQUEST,
  COUPON_REWARD_CREATE_RESET,
  COUPON_REWARD_CREATE_SUCCESS,
  COUPON_REWARD_DETAILS_FAIL,
  COUPON_REWARD_DETAILS_REQUEST,
  COUPON_REWARD_DETAILS_SUCCESS,
  COUPON_REWARD_LIST_FOR_BRAND_FAIL,
  COUPON_REWARD_LIST_FOR_BRAND_REQUEST,
  COUPON_REWARD_LIST_FOR_BRAND_SUCCESS,
  COUPON_REWARD_OFFER_PARTNER_LIST_FAIL,
  COUPON_REWARD_OFFER_PARTNER_LIST_REQUEST,
  COUPON_REWARD_OFFER_PARTNER_LIST_SUCCESS,
  INCREMENT_COUPON_REDEEMER_FREQUENCY_FAIL,
  INCREMENT_COUPON_REDEEMER_FREQUENCY_REQUEST,
  INCREMENT_COUPON_REDEEMER_FREQUENCY_RESET,
  INCREMENT_COUPON_REDEEMER_FREQUENCY_SUCCESS,
  UPDATE_COUPON_REWARD_FAIL,
  UPDATE_COUPON_REWARD_REQUEST,
  UPDATE_COUPON_REWARD_RESET,
  UPDATE_COUPON_REWARD_SUCCESS,
} from "../Constants/couponRewardOfferConstants";

export function couponRewardProgramCreateReducer(state = {}, action) {
  switch (action.type) {
    case COUPON_REWARD_CREATE_REQUEST:
      return {
        loading: true,
      };
    case COUPON_REWARD_CREATE_SUCCESS:
      return {
        loading: false,
        success: true,
        rewardProgram: action.payload,
      };
    case COUPON_REWARD_CREATE_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    case COUPON_REWARD_CREATE_RESET:
      return {};
    default:
      return state;
  }
}

export function couponRewardOfferListForBrandGetReducer(state = {}, action) {
  switch (action.type) {
    case COUPON_REWARD_LIST_FOR_BRAND_REQUEST:
      return {
        loading: true,
      };
    case COUPON_REWARD_LIST_FOR_BRAND_SUCCESS:
      return {
        loading: false,
        success: true,
        myRewardPrograms: action.payload,
      };
    case COUPON_REWARD_LIST_FOR_BRAND_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}

export function couponRewardOfferDetailsGetReducer(state = {}, action) {
  switch (action.type) {
    case COUPON_REWARD_DETAILS_REQUEST:
      return {
        loading: true,
      };
    case COUPON_REWARD_DETAILS_SUCCESS:
      return {
        loading: false,
        success: true,
        rewardProgramDetails: action.payload,
      };
    case COUPON_REWARD_DETAILS_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}

export function couponRewardProgramUpdateReducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_COUPON_REWARD_REQUEST:
      return {
        loading: true,
      };
    case UPDATE_COUPON_REWARD_SUCCESS:
      return {
        loading: false,
        success: true,
        updatedRewardProgram: action.payload,
      };
    case UPDATE_COUPON_REWARD_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    case UPDATE_COUPON_REWARD_RESET:
      return {};
    default:
      return state;
  }
}

export function addRedeemerToCouponRewardReducer(state = {}, action) {
  switch (action.type) {
    case ADD_REDEEMER_TO_COUPON_REWARD_REQUEST:
      return {
        loading: true,
      };
    case ADD_REDEEMER_TO_COUPON_REWARD_SUCCESS:
      return {
        loading: false,
        success: true,
        updatedRewardProgram: action.payload,
        couponReward: action.payload.couponReward,
        userCouponDetails: action.payload.userCouponDetails,
      };
    case ADD_REDEEMER_TO_COUPON_REWARD_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    case ADD_REDEEMER_TO_COUPON_REWARD_RESET:
      return {};
    default:
      return state;
  }
}

export function incrementUserRedeemFrequencyReducer(state = {}, action) {
  switch (action.type) {
    case INCREMENT_COUPON_REDEEMER_FREQUENCY_REQUEST:
      return {
        loading: true,
      };
    case INCREMENT_COUPON_REDEEMER_FREQUENCY_SUCCESS:
      return {
        loading: false,
        success: true,
        couponReward: action.payload,
      };
    case INCREMENT_COUPON_REDEEMER_FREQUENCY_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    case INCREMENT_COUPON_REDEEMER_FREQUENCY_RESET:
      return {};
    default:
      return state;
  }
}

export function couponRewardOfferPartnerListReducer(state = {}, action) {
  switch (action.type) {
    case COUPON_REWARD_OFFER_PARTNER_LIST_REQUEST:
      return {
        loading: true,
      };
    case COUPON_REWARD_OFFER_PARTNER_LIST_SUCCESS:
      return {
        loading: false,
        success: true,
        couponOfferPartnerList: action.payload,
      };
    case COUPON_REWARD_OFFER_PARTNER_LIST_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}
