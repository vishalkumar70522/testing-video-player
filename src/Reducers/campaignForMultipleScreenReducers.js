import {
  CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_FAIL,
  CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_REQUEST,
  CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_RESET,
  CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_SUCCESS,
} from "../Constants/campaignForMultipleScreen";

export function createCampaignForMultipleScreenReducer(state = {}, action) {
  switch (action.type) {
    case CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_REQUEST:
      return { loading: true };
    case CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_SUCCESS:
      return {
        loading: false,
        uploadedCampaign: action.payload,
        success: true,
      };
    case CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_FAIL:
      return { loading: false, error: action.payload };
    case CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_RESET:
      return {};
    default:
      return state;
  }
}
