import {
  REWARD_CREATE_FAIL,
  REWARD_CREATE_REQUEST,
  REWARD_CREATE_RESET,
  REWARD_CREATE_SUCCESS,
  MY_CREATED_REWARD_REQUEST,
  MY_CREATED_REWARD_SUCCESS,
  MY_CREATED_REWARD_FAIL,
  REWARD_DETAILS_REQUEST,
  REWARD_DETAILS_SUCCESS,
  REWARD_DETAILS_FAIL,
  UPDATE_REWARD_REQUEST,
  UPDATE_REWARD_SUCCESS,
  UPDATE_REWARD_FAIL,
  UPDATE_REWARD_RESET,
} from "../Constants/rewardConstants";

export function rewardProgramCreateReducer(state = {}, action) {
  switch (action.type) {
    case REWARD_CREATE_REQUEST:
      return {
        loading: true,
      };
    case REWARD_CREATE_SUCCESS:
      return {
        loading: false,
        success: true,
        rewardProgram: action.payload,
      };
    case REWARD_CREATE_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    case REWARD_CREATE_RESET:
      return {};
    default:
      return state;
  }
}

export function myRewardProgramsCreatedReducer(state = {}, action) {
  switch (action.type) {
    case MY_CREATED_REWARD_REQUEST:
      return {
        loading: true,
      };
    case MY_CREATED_REWARD_SUCCESS:
      return {
        loading: false,
        success: true,
        myRewardPrograms: action.payload,
      };
    case MY_CREATED_REWARD_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}

export function rewardProgramDetailsGetReducer(state = {}, action) {
  switch (action.type) {
    case REWARD_DETAILS_REQUEST:
      return {
        loading: true,
      };
    case REWARD_DETAILS_SUCCESS:
      return {
        loading: false,
        success: true,
        rewardProgramDetails: action.payload,
      };
    case REWARD_DETAILS_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}

export function rewardProgramUpdateReducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_REWARD_REQUEST:
      return {
        loading: true,
      };
    case UPDATE_REWARD_SUCCESS:
      return {
        loading: false,
        success: true,
        updatedRewardProgram: action.payload,
      };
    case UPDATE_REWARD_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    case UPDATE_REWARD_RESET:
      return {};
    default:
      return state;
  }
}
