import { Text, Center, Image } from "@chakra-ui/react";
import logo from "../../../assets/logo1024.png";

export function SingleCard(props: any) {
  const reward = props?.reward;
  return (
    <>
      <Center borderRadius={"100%"} py="5">
        <Image src={logo} height={"100px"} width="100px" alt=""></Image>
      </Center>
      <Text color="#000000" fontSize="md" fontWeight={"bold"}>
        {reward?.offerName}
      </Text>
      <Text color="#000000" fontSize="sm">
        {`Valid till ${reward?.rewardOfferType?.cardReward?.validity?.to}`}
      </Text>
      <Text color="#000000" fontSize="sm">
        {`Card Type ${reward?.rewardOfferType?.cardReward?.cardType}`}
      </Text>
    </>
  );
}
