import {
  Box,
  Flex,
  SimpleGrid,
  Text,
  Tooltip,
  Stack,
  Button,
} from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import { AdsPlaying } from "../AdsPlaying";
import { AiFillEdit } from "react-icons/ai";
import { useDispatch, useSelector } from "react-redux";
import { incrementUserRedeemFrequency } from "../../../Actions/couponRewardOfferActions";
import HLoading from "../../atoms/HLoading";
import MessageBox from "../../atoms/MessageBox";
import { useEffect } from "react";
import { INCREMENT_COUPON_REDEEMER_FREQUENCY_RESET } from "../../../Constants/couponRewardOfferConstants";
// import { convertIntoDateAndTime } from "../../../utils/dateAndTime";

export function SingleRewardCouponDetail(props: any) {
  const dispatch = useDispatch<any>();
  const reward = props.reward;
  const navigate = useNavigate();
  //incrementUserRedeemFrequencyByBrand
  const incrementUserRedeemFrequencyByBrand = useSelector(
    (state: any) => state.incrementUserRedeemFrequencyByBrand
  );
  const {
    loading: loadingRewardProgramCreate,
    error: errorRewardProgramCreate,
    success,
    couponReward,
  } = incrementUserRedeemFrequencyByBrand;

  const handelRedeem = (couponId: any, couponCode: any) => {
    console.log("coupons : ", couponId, couponCode);
    dispatch(incrementUserRedeemFrequency(couponId, couponCode));
  };

  useEffect(() => {
    if (success) {
      alert("user has redeemed this coupon");
      dispatch({ type: INCREMENT_COUPON_REDEEMER_FREQUENCY_RESET });
    }
    if (errorRewardProgramCreate) {
      setTimeout(() => {
        dispatch({ type: INCREMENT_COUPON_REDEEMER_FREQUENCY_RESET });
      }, 4000);
    }
  }, [success, errorRewardProgramCreate]);

  return (
    <Box>
      {loadingRewardProgramCreate ? (
        <HLoading loading={loadingRewardProgramCreate} />
      ) : errorRewardProgramCreate ? (
        <MessageBox variant="danger">{errorRewardProgramCreate}</MessageBox>
      ) : (
        <SimpleGrid columns={[1, 2, 1]} spacing="4" boxShadow={"2xl"} p="5">
          <Box boxShadow={"2xl"} p="10">
            {props?.editAllow ? (
              <Stack align="flex-end">
                <Tooltip
                  label="Click for edit reward details"
                  // bg="gray.300"
                  // color="black"
                  // arrowSize={15}
                  placement="right-end"
                >
                  <AiFillEdit
                    color="#000000"
                    size="30px"
                    onClick={() =>
                      navigate(`/editCouponRewardAndOffer/${reward?._id}`)
                    }
                  />
                </Tooltip>
              </Stack>
            ) : null}

            <Text
              align="left"
              fontSize="2xl"
              fontWeight={"bold"}
              color="orange"
              py="5"
            >
              Basic Details
            </Text>
            <Box>
              <Flex gap={5}>
                <Text color="#000000" fontSize="md" align="left">
                  Reward program name:
                </Text>
                <Text color="#000000" fontSize="md">
                  {reward?.offerName}
                </Text>
              </Flex>
              <Flex gap={5}>
                <Text color="#000000" fontSize="md" align="left">
                  Rewarding Brand:
                </Text>
                <Text color="#000000" fontSize="md">
                  {reward?.brandName}
                </Text>
              </Flex>
              <Flex gap={5}>
                <Text color="#000000" fontSize="md" align="left">
                  Rewarding Quantity:
                </Text>
                <Text color="#000000" fontSize="md">
                  {reward?.quantity || 0}
                </Text>
              </Flex>
              <Flex gap={5}>
                <Text color="#000000" fontSize="md" align="left">
                  Ratting of Coupon:
                </Text>
                <Text color="#000000" fontSize="md">
                  {reward?.ratings || 0}
                </Text>
              </Flex>
              <Flex gap={5}>
                <Text color="#000000" fontSize="md" align="left">
                  Coupon item:
                </Text>
                <Text color="#000000" fontSize="md">
                  {reward?.couponRewardInfo?.couponItem}
                </Text>
              </Flex>
              <Flex gap={5}>
                <Text color="#000000" fontSize="md" align="left">
                  Coupon valid till:
                </Text>
                <Text color="#000000" fontSize="md">
                  {reward?.couponRewardInfo?.validity?.from} to{" "}
                  {reward?.couponRewardInfo?.validity?.to}
                </Text>
              </Flex>
              <Flex gap={5}>
                <Text color="#000000" fontSize="md" align="left">
                  No. of times coupon can be redeemed
                </Text>
                <Text color="#000000" fontSize="md">
                  {reward?.couponRewardInfo?.redeemFrequency} times
                </Text>
              </Flex>
              <Flex gap={5}>
                <Text color="#000000" fontSize="md" align="left">
                  Coupon type:
                </Text>
                <Text color="#000000" fontSize="md">
                  {reward?.couponRewardInfo?.couponType}
                </Text>
              </Flex>
              {reward?.couponRewardInfo?.couponType === "discount" ? (
                <>
                  <Flex gap={5}>
                    <Text color="#000000" fontSize="md" align="left">
                      Discount Type:
                    </Text>
                    <Text color="#000000" fontSize="md">
                      {reward?.couponRewardInfo?.couponInfo?.type}
                    </Text>
                  </Flex>
                  <Flex gap={5}>
                    <Text color="#000000" fontSize="md" align="left">
                      Discount Value:
                    </Text>
                    <Text color="#000000" fontSize="md">
                      {reward?.couponRewardInfo?.couponInfo?.type === "%"
                        ? `${reward?.couponRewardInfo?.couponInfo?.value} %`
                        : `₹ ${reward?.couponRewardInfo?.couponInfo?.value}`}
                    </Text>
                  </Flex>
                </>
              ) : reward?.couponRewardInfo?.couponType === "free" ? (
                <>
                  <Flex gap={5}>
                    <Text color="#000000" fontSize="md" align="left">
                      Free item Quantity :
                    </Text>
                    <Text color="#000000" fontSize="md">
                      {reward?.couponRewardInfo?.couponInfo?.freeItemQuantity}
                    </Text>
                  </Flex>
                </>
              ) : null}
            </Box>
          </Box>
          {/* //displaying campaign list attached to this coupon */}
          <Box boxShadow={"2xl"} p="10" mt="5">
            <Text
              align="left"
              fontSize="2xl"
              fontWeight={"bold"}
              color="orange"
              py="5"
            >
              See All Campaign List attached to this coupon
            </Text>
            <SimpleGrid columns={[2, 2, 4]} spacing="5" pt="5">
              {reward?.campaigns &&
                reward?.campaigns?.map((video: any, index: any) => (
                  <AdsPlaying key={index} campaignId={video} />
                ))}
            </SimpleGrid>
          </Box>
          {/* //displaying user offer partner list attached to this coupon */}
          <Box boxShadow={"2xl"} p="10" mt="5">
            <Text
              align="left"
              fontSize="2xl"
              fontWeight={"bold"}
              color="orange"
              py="5"
            >
              See All offer patner list
            </Text>
            <SimpleGrid columns={[2, 2, 4]} spacing="5" pt="5">
              {reward?.rewardOfferPartners &&
                reward?.rewardOfferPartners?.map((user: any, index: any) => (
                  <Text color="#000000" fontSize="sm" key="index">
                    {user}
                  </Text>
                ))}
            </SimpleGrid>
          </Box>
          <Box boxShadow={"2xl"} p="10" mt="5">
            <Text
              align="left"
              fontSize="2xl"
              fontWeight={"bold"}
              color="orange"
              py="5"
            >
              {props.editAllow
                ? `See All redeemer user list`
                : `See redeerer user details`}
            </Text>
            {props.editAllow ? (
              <SimpleGrid columns={[2, 2, 4]} spacing="5" pt="5">
                {reward?.rewardCoupons &&
                  reward?.rewardCoupons?.map((user: any, index: any) => (
                    <Box key={index}>
                      <Text color="#000000" fontSize="sm" align="left">
                        Coupon Code : {user.couponCode}
                      </Text>
                      <Text color="#000000" fontSize="sm" align="left">
                        Redeemer id : {user.redeemer}
                      </Text>
                      <Text color="#000000" fontSize="md" align="left">
                        No. of times coupon can be redeemed:
                        {reward?.couponRewardInfo?.redeemFrequency} times
                      </Text>
                      <Text color="#000000" fontSize="sm" align="left">
                        Used that coupon : {user.redeemedFrequency} times
                      </Text>
                    </Box>
                  ))}
              </SimpleGrid>
            ) : (
              <Box>
                {reward?.rewardCoupons &&
                  reward?.rewardCoupons?.map((user: any, index: any) => {
                    if (user.couponCode === props?.couponCode) {
                      return (
                        <Box key={index} pt="5">
                          <Text color="#000000" fontSize="sm" align="left">
                            Coupon Code : {user.couponCode}
                          </Text>
                          <Text color="#000000" fontSize="sm" align="left">
                            Redeemer id : {user.redeemer}
                          </Text>
                          <Text color="#000000" fontSize="md" align="left">
                            No. of times coupon can be redeemed:
                            {reward?.couponRewardInfo?.redeemFrequency} times
                          </Text>
                          <Text color="#000000" fontSize="sm" align="left">
                            Used that coupon : {user.redeemedFrequency} times
                          </Text>
                        </Box>
                      );
                    }
                  })}
                <Button
                  onClick={() => handelRedeem(reward?._id, props?.couponCode)}
                  py="3"
                >
                  Redeems
                </Button>
              </Box>
            )}
          </Box>
        </SimpleGrid>
      )}
    </Box>
  );
}
