import { useSelector, useDispatch } from "react-redux";
import * as React from "react";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import {
  Box,
  Stack,
  Image,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Tooltip,
  Center,
  Button,
  useDisclosure,
  HStack,
} from "@chakra-ui/react";
import { AiOutlineBell, AiOutlineUser, AiOutlineLogout } from "react-icons/ai";

import { BiChevronDown } from "react-icons/bi";
import { TbUser } from "react-icons/tb";
import Logo from "../../../assets/logo.png";
import Name from "../../../assets/name.png";
import { signout } from "../../../Actions/userActions";
import { RiCoupon4Line } from "react-icons/ri";

export const Nav = () => {
  const navigate = useNavigate();
  const { isOpen, onToggle } = useDisclosure();
  const style = {
    borderTop: "1px solid #E7E7E7",
    textAlign: "center",
    // position: "fixed",
    left: "0",
    top: "0",
    width: "100%",
    zIndex: "1",
    backgroundColor: "#ffffff80",
  };
  const [walletBalance, setWalletBalance] = React.useState({
    ar: 0,
    koii: 0,
    ratData: 0,
  });
  const btnRef = React.useRef(null);

  const dispatch = useDispatch<any>();
  const userSignin = useSelector((state: any) => state.userSignin);
  const {
    loading: loadingUserInfo,
    error: errorUserInfo,
    userInfo,
  } = userSignin;

  const signoutHandler = () => {
    dispatch(signout());
    navigate("/");
  };

  return (
    <Box
      __css={style}
      bgGradient="linear(to-r, rgba(244,196,196,1.00) 0%,  rgba(175,198,226,0.50) 75%)"
    >
      <Center>
        <Stack
          align="center"
          direction="row"
          justifyContent="space-between"
          width="100%"
          px={{ base: "10", lg: "20" }}
          py="3"
        >
          <Stack as={RouterLink} to="/" direction="row" align="center">
            <Image width={{ base: 30, lg: "45px" }} src={Logo} />
            <Image width={{ base: 70, lg: "100px" }} src={Name} />
          </Stack>
          {!userInfo ? (
            <Stack direction="row">
              <Button
                as={RouterLink}
                to={`/signin`}
                size="sm"
                fontSize="sm"
                ref={btnRef}
                py="2"
                color="blue"
                bgColor="#FFFFFF"
                _hover={{ bgColor: "blue", color: "#FFFFFF" }}
                borderRadius="53px"
                variant="outline"
              >
                Sign in
              </Button>
              <Button
                as={RouterLink}
                to={`/signup`}
                size="sm"
                fontSize="sm"
                py="2"
                color="green"
                bgColor="#FFFFFF"
                _hover={{ bgColor: "green", color: "#FFFFFF" }}
                variant="outline"
                borderRadius="53px"
              >
                Create Account
              </Button>
            </Stack>
          ) : (
            <Stack
              align="center"
              justifyContent="flex-end"
              direction="row"
              width="80%"
              mt="1px"
              display={{ base: "none", md: "flex" }}
            >
              <Menu>
                <MenuButton>
                  <Tooltip
                    bg="violet.500"
                    color="white"
                    hasArrow
                    placement="bottom"
                    label="Click for Menu"
                  >
                    <Center as={RouterLink} to="/">
                      <HStack>
                        <Box
                          borderRadius="100%"
                          border="2px"
                          height="40px"
                          width="40px"
                          borderColor="#403F49"
                          color="#403F49"
                          _hover={{
                            color: "teal.600",
                            borderColor: "teal.600",
                          }}
                        >
                          <Stack mt="2" ml="2">
                            <TbUser
                              size="20px"
                              fontWeight="1"
                              onClick={() => navigate("/userprofile")}
                            />
                          </Stack>
                        </Box>
                        <BiChevronDown
                          size="30px"
                          fontWeight="1"
                          color="#403F49"
                        />
                      </HStack>
                    </Center>
                  </Tooltip>
                </MenuButton>
                <MenuList>
                  <MenuItem
                    as={RouterLink}
                    to={`/notification`}
                    color="black"
                    icon={<AiOutlineBell size="20px" />}
                  >
                    Notifications
                  </MenuItem>
                  <MenuItem
                    as={RouterLink}
                    to={`/userprofile`}
                    color="black"
                    icon={<AiOutlineUser size="20px" />}
                  >
                    Profile
                  </MenuItem>
                  <MenuItem
                    as={RouterLink}
                    to={`/couponDetils`}
                    color="black"
                    icon={<RiCoupon4Line size="20px" />}
                  >
                    Coupon
                  </MenuItem>
                  <MenuItem
                    onClick={signoutHandler}
                    color="black"
                    icon={<AiOutlineLogout size="20px" />}
                  >
                    Logout
                  </MenuItem>
                </MenuList>
              </Menu>
            </Stack>
          )}
        </Stack>
      </Center>
    </Box>
  );
};
