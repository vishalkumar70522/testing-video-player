import { Text, Center, Image } from "@chakra-ui/react";
import logo from "../../../assets/logo1024.png";

export function SingleCardForBrand(props: any) {
  const reward = props?.reward;
  return (
    <>
      <Center borderRadius={"100%"} py="5">
        <Image src={logo} height={"100px"} width="100px" alt=""></Image>
      </Center>
      <Text color="#000000" fontSize="md" fontWeight={"bold"}>
        Offer Name {reward?.offerName}
      </Text>
      <Text color="#000000" fontSize="md" fontWeight={"bold"}>
        Total card created {reward?.quantity}
      </Text>
      <Text color="#000000" fontSize="sm">
        {`Valid till ${reward?.validity?.to}`}
      </Text>
      <Text color="#000000" fontSize="sm">
        {`Card Type ${reward?.cardType}`}
      </Text>
      <Text color="#000000" fontSize="sm" fontWeight={"semibold"}>
        {`Gidt Coupons on this card  ${reward?.cardMilestonesReward?.length}`}
      </Text>
    </>
  );
}
