import React from "react";
import { Link as RouterLink } from "react-router-dom";
import {
  Box,
  Image,
  Flex,
  Text,
  IconButton,
  Stack,
  Tooltip,
} from "@chakra-ui/react";
import { BiRupee } from "react-icons/bi";

export function Screen(props: any) {
  const { eachScreen: screen } = props;
  return (
    <Box
      bgColor="#F7F7F7"
      borderColor="#DFDFDF"
      border="1.5px"
      width="100%"
      height="100%"
      borderRadius="lg"
      boxShadow="2xl"
      key={screen._id}
      as={RouterLink}
      to={`/screen/${screen._id}`}
    >
      {/* image */}
      <Box p="2" height={{ height: 50, lg: "200px" }}>
        <Image
          width="100%"
          height={{ base: "150px", lg: "240px" }}
          borderRadius="10px"
          src={screen?.image}
          // onLoad={() => triggerPort(screen?.image?.split("/").slice(-1)[0])}
        />
      </Box>
      {/* details of screem */}
      <Stack p="2" pb="4">
        {/* Name */}
        <Text
          color="#403F49"
          fontSize={{ base: "md", lg: "lg" }}
          fontWeight="bold"
          align="left"
          width="85%"
        >
          {screen.name}
        </Text>
        <Tooltip
          label={`${screen.screenAddress}, ${screen.districtCity}, ${screen.country}`}
          aria-label="A tooltip"
        >
          <Stack align="left">
            <Text
              color="#666666"
              fontSize="xs"
              fontWeight="semibold"
              align="left"
            >
              {`${screen.districtCity}, ${screen.country}`}
            </Text>
          </Stack>
        </Tooltip>
        {/* Cost */}
        <Flex align="center" justify="space-between">
          <Flex align="center">
            <IconButton
              bg="none"
              icon={<BiRupee size="20px" color="#403F49" />}
              aria-label="Star"
            ></IconButton>
            <Text
              color="#403F49"
              fontSize={{ base: "md", lg: "xl" }}
              fontWeight="semibold"
              align="left"
            >
              {`${screen.rentPerSlot}/slot`}
            </Text>
          </Flex>
          <Flex align="center" as="s">
            <Text
              color="#787878"
              fontSize={{ base: "md", lg: "xl" }}
              fontWeight="semibold"
              align="left"
            >
              ₹
              {screen.rentPerSlot - screen.rentOffInPercent
                ? (screen.rentPerSlot * 100) / screen.rentOffInPercent
                : 0}{" "}
              per slot
            </Text>
          </Flex>
          <Text
            pl="1"
            color="#F86E6E"
            fontSize={{ base: "md", lg: "xl" }}
            fontWeight="semibold"
            align="left"
          >
            ( {screen.rentOffInPercent}% OFF)
          </Text>
        </Flex>
        {/* Address */}
      </Stack>
    </Box>
  );
}
