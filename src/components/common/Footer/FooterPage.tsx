import React from "react";
import {
  Box,
  Image,
  Stack,
  Text,
  Flex,
  Input,
  Button,
  Divider,
  SimpleGrid,
  Hide,
} from "@chakra-ui/react";
import { TbBrandDiscord, TbBrandFacebook } from "react-icons/tb";
import Logo from "../../../assets/logo.png";
import Name from "../../../assets/name.png";
import { FaInstagram } from "react-icons/fa";
import { FiTwitter } from "react-icons/fi";
import { AiOutlineYoutube } from "react-icons/ai";

export function FooterPage() {
  return (
    <>
      {window.location.pathname.split("/")[1] !== "playlist" ? (
        <Box pt="5" px={{ base: "5", lg: "0" }}>
          <Box
            pl={{ base: "2", lg: "20" }}
            pr={{ base: "2", lg: "20" }}
            pt={{ base: "5", lg: "5" }}
          >
            <Divider borderColor="black.500" />
          </Box>
          <SimpleGrid
            columns={[1, null, 2]}
            spacing={{ base: "5", lg: "20" }}
            pl={{ base: "2", lg: "20" }}
            pr={{ base: "2", lg: "20" }}
            pt="5"
          >
            <Box>
              <Stack direction="row" align="center">
                <Image width={{ base: 30, lg: "50px" }} src={Logo} />
                <Image width={{ base: 70, lg: "100px" }} src={Name} />
              </Stack>
              <Box alignItems="left">
                <Text
                  fontSize="sm"
                  color="#333333"
                  noOfLines={[4, 2, 2]}
                  align="left"
                  pt="5"
                >
                  <strong>Registered Office Address: </strong>
                  Vinciis Creations Private Limited, D 65/319 C, Lahartara, B
                  Shivdaspur, Varanasi, UP, 221002
                </Text>
              </Box>
              <Flex mt={{ base: "2", lg: "10" }}>
                <Box
                  borderRadius="100%"
                  border="2px"
                  height="40px"
                  width="40px"
                  borderColor="#9A9A9A"
                >
                  <Stack mt="2" ml="2">
                    <TbBrandFacebook
                      color="#9A9A9A"
                      size="20px"
                      onClick={() =>
                        window.location.replace(
                          `https://www.facebook.com/vinciisadtech`
                        )
                      }
                    />
                  </Stack>
                </Box>
                <Box
                  ml="2"
                  borderRadius="100%"
                  border="2px"
                  height="40px"
                  width="40px"
                  borderColor="#9A9A9A"
                >
                  <Stack mt="2" ml="2">
                    <FaInstagram
                      color="#9A9A9A"
                      size="20px"
                      onClick={() =>
                        window.location.replace(
                          `https://www.instagram.com/vinciis_itself`
                        )
                      }
                    />
                  </Stack>
                </Box>
                <Box
                  ml="2"
                  borderRadius="100%"
                  border="2px"
                  height="40px"
                  width="40px"
                  borderColor="#9A9A9A"
                  alignItems="center"
                  justifyContent="center"
                >
                  <Stack mt="2" ml="2">
                    <FiTwitter
                      color="#9A9A9A"
                      size="20px"
                      onClick={() =>
                        window.location.replace(`https://twitter.com/vinciis_`)
                      }
                    />
                  </Stack>
                </Box>
                <Box
                  ml="2"
                  borderRadius="100%"
                  border="2px"
                  height="40px"
                  width="40px"
                  borderColor="#9A9A9A"
                >
                  <Stack mt="2" ml="2">
                    <TbBrandDiscord
                      color="#9A9A9A"
                      size="20px"
                      onClick={() =>
                        window.location.replace(`https://discord.gg/rxNUvBh5`)
                      }
                    />
                  </Stack>
                </Box>
                <Box
                  ml="2"
                  borderRadius="100%"
                  border="2px"
                  height="40px"
                  width="40px"
                  borderColor="#9A9A9A"
                >
                  <Stack mt="2" ml="2">
                    <AiOutlineYoutube
                      color="#9A9A9A"
                      size="20px"
                      onClick={() =>
                        window.location.replace(
                          `https://www.youtube.com/channel/UCn0ycOFFkT5T9w8fSVwsHOg/featured`
                        )
                      }
                    />
                  </Stack>
                </Box>
              </Flex>
            </Box>
            <Hide below="md">
              <Box>
                <Stack>
                  <Text
                    align="left"
                    color="#403F49"
                    fontSize="sm"
                    fontWeight="semibold"
                  >
                    Exclusive offers
                  </Text>
                  <Text color="#666666" fontSize="sm" align="left">
                    Sign up to the newsletter to receive our latest offers
                  </Text>
                  <Stack pt="5">
                    <Input
                      py="3"
                      borderColor="#888888"
                      borderRadius="5px"
                      width="70%"
                      placeholder="hello@example.com"
                      color="#888888"
                    ></Input>
                  </Stack>
                  <Stack pt="5">
                    <Button
                      color="#403F49"
                      borderColor="#403F49"
                      variant="outline"
                      width="40%"
                      fontSize="xl"
                      fontWeight="semibold"
                      py="3"
                    >
                      Subscribe
                    </Button>
                  </Stack>
                </Stack>
              </Box>
            </Hide>
          </SimpleGrid>
          <Flex></Flex>
          <Text
            color="#5C5C5C"
            fontSize="sm"
            align="left"
            pt={{ base: "3", lg: "10" }}
            pl={{ base: "2", lg: "20" }}
            pr={{ base: "2", lg: "20" }}
          >
            Write to us <strong>ad@vinciis.in</strong> and/or Call{" "}
            <strong>+917250283664</strong>
          </Text>

          <Text
            color="#5C5C5C"
            fontSize="sm"
            align="left"
            pt={{ base: "1", lg: "2" }}
            pb="10"
            pl={{ base: "2", lg: "20" }}
            pr={{ base: "2", lg: "20" }}
          >
            Copyright @ VINCIIS CREATIONS PRIVATE LIMITED, 2022. All rights
            reserved.
          </Text>
        </Box>
      ) : null}
    </>
  );
}
