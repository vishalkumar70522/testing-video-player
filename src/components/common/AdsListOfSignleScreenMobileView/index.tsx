import {
  Card,
  CardBody,
  Stack,
  Text,
  HStack,
  Flex,
  Button,
} from "@chakra-ui/react";
import { convertIntoDateAndTime } from "../../../utils/dateAndTime";
import { BsDot } from "react-icons/bs";
import { useState } from "react";

export function AdsListOfSignleScreenMobileView(props: any) {
  const { videos } = props;
  const [selectedIndex, setSelectedIndex] = useState<any>(-1);
  return (
    <Stack spacing={3}>
      {videos?.length > 0
        ? videos.map((video: any, index: any) => (
            <Card
              bg="#F9F9F9"
              onClick={() =>
                setSelectedIndex(index === selectedIndex ? -1 : index)
              }
            >
              <CardBody>
                <HStack justifyContent="space-between">
                  <Stack>
                    <Text
                      color="#403F49"
                      fontWeight="semibold"
                      fontSize="md"
                      align="left"
                    >
                      {video.campaignName}
                    </Text>
                    <Text color="#575757" fontSize="sm" align="left">
                      {convertIntoDateAndTime(video.startDate)}
                    </Text>
                    {selectedIndex === index ? (
                      <>
                        <Text
                          color="#403F49"
                          fontWeight="semibold"
                          fontSize="sm"
                          align="left"
                        >
                          {video.totalSlotBooked} slots
                        </Text>
                        <HStack justifyContent="space-between">
                          <Text
                            color="#403F49"
                            fontWeight="semibold"
                            fontSize="sm"
                            align="left"
                          >
                            ₹ {video.totalAmount}
                          </Text>
                        </HStack>
                        {props?.show ? (
                          <HStack justifyContent="space-between">
                            <Button
                              color="#FFFFFF"
                              _hover={{ bg: "#82CD47", color: "#FFFFFF" }}
                              fontSize={{ base: "xs", lg: "sm" }}
                              boxShadow="xl"
                              alignContent="center"
                              bgColor="red"
                              py="2"
                              onClick={() =>
                                props?.handleDeteteCampaign(video._id)
                              }
                            >
                              Delete
                            </Button>
                          </HStack>
                        ) : null}
                      </>
                    ) : null}
                  </Stack>
                  <Flex mt="0" pt="0">
                    <BsDot
                      size="20px"
                      color={video.status === "Deleted" ? "#E93A03" : "#00D615"}
                    />
                    <Text color="#403F45" fontSize="sm" pl="2">
                      {video.status}
                    </Text>
                  </Flex>
                </HStack>
              </CardBody>
            </Card>
          ))
        : null}
    </Stack>
  );
}
