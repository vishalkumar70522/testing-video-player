import React from "react";
import { Box, Stack, Text } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import { useMedia } from "../../../hooks";
import { MediaContainer } from "../MediaContainer";
// import HLoading from "../../../components/atoms/HLoading";
// import MessageBox from "../../../components/atoms/MessageBox";

export function UserCollections(props: any) {
  const {
    data: media,
    isLoading,
    isError,
  } = useMedia({ id: props.props.media.split("/").slice(4)[0] });
  //console.log(props.props);

  const navigate = useNavigate();

  return (
    <Box
      width="100%"
      height="100%"
      key={props._id}
      // onClick={() => navigate(`/campaignDetails/${video._id}`)}
    >
      <Stack p="1" borderRadius="lg" onClick={() => {}} boxShadow="2xl">
        <MediaContainer media={media} />
        <Text color="#000000" align="left" fontWeight="semibold">
          {props.props.title}
        </Text>
      </Stack>
    </Box>
  );
}
