import { Text, Center, Image } from "@chakra-ui/react";
import logo from "../../../assets/logo1024.png";

export function SingleCoupon(props: any) {
  const reward = props?.reward;
  return (
    <>
      <Center borderRadius={"100%"} py="5">
        <Image src={logo} height={"100px"} width="100px" alt=""></Image>
      </Center>
      <Text color="#000000" fontSize="sm" fontWeight={"bold"}>
        {reward?.offerName}
      </Text>
      {reward?.rewardOfferType?.couponReward?.couponType === "discount" ? (
        <Text color="#000000" fontSize="sm" fontWeight="bold">
          {reward?.rewardOfferType?.couponReward?.couponInfo?.type === "%"
            ? `${reward?.rewardOfferType?.couponReward?.couponInfo?.value} % discount on ${reward?.rewardOfferType?.couponReward?.couponItem}`
            : `₹ ${reward?.rewardOfferType?.couponReward?.couponInfo?.value} discount on ${reward?.rewardOfferType?.couponReward?.couponItem}`}
        </Text>
      ) : reward?.rewardOfferType?.couponReward?.couponType === "free" ? (
        <Text color="#000000" fontSize="lg" fontWeight={"bold"}>
          {` Free ${reward?.rewardOfferType?.couponReward?.couponInfo?.freeItemQuantity} ${reward?.rewardOfferType?.couponReward?.couponItem}`}
        </Text>
      ) : null}
      <Text color="#000000" fontSize="sm">
        {" "}
        {`Valid till ${reward?.rewardOfferType?.couponReward?.validity?.to}`}
      </Text>
      <Text color="#000000" fontSize="sm">
        {`you can redeem only ${reward?.rewardOfferType?.couponReward?.redeemFrequency} times`}
      </Text>
    </>
  );
}
