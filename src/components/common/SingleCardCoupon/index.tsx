import { Text, Box, Center, Image } from "@chakra-ui/react";
import logo from "../../../assets/logo1024.png";

export function SingleCardCoupon(props: any) {
  const reward = props?.reward;
  const singleCoupon = props?.singleCoupon;
  return (
    <Box
      boxShadow={"2xl"}
      width="250px"
      bgColor="#FFB84C"
      borderRadius="lg"
      color="#FFFFFF"
      py="3"
    >
      <Center borderRadius={"100%"} py="5">
        <Image src={logo} height={"100px"} width="100px" alt=""></Image>
      </Center>
      <Text color="#000000" fontSize="sm" fontWeight={"bold"}>
        {reward?.offerName}
      </Text>
      <Text color="#000000" fontSize="sm" fontWeight={"bold"}>
        {`MileStone points ${singleCoupon?.milestonePoint}`}
      </Text>
      {singleCoupon?.couponRewardInfo?.couponType === "discount" ? (
        <Text color="#000000" fontSize="sm" fontWeight="bold">
          {singleCoupon?.couponRewardInfo?.couponInfo?.type === "%"
            ? `${singleCoupon?.couponRewardInfo?.couponInfo?.value} % discount on ${singleCoupon?.couponRewardInfo?.couponItem}`
            : `₹ ${singleCoupon?.couponRewardInfo?.couponInfo?.value} discount on ${singleCoupon?.couponRewardInfo?.couponItem}`}
        </Text>
      ) : singleCoupon?.couponRewardInfo?.couponType === "free" ? (
        <Text color="#000000" fontSize="lg" fontWeight={"bold"}>
          {` Free ${singleCoupon?.couponRewardInfo?.couponInfo?.freeItemQuantity} ${singleCoupon?.couponRewardInfo?.couponItem}`}
        </Text>
      ) : null}
      <Text color="#000000" fontSize="sm">
        {" "}
        {`Valid till ${singleCoupon?.couponRewardInfo?.validity?.to}`}
      </Text>
      <Text color="#000000" fontSize="sm">
        {`you can redeem only ${singleCoupon?.couponRewardInfo?.redeemFrequency} times`}
      </Text>
    </Box>
  );
}
