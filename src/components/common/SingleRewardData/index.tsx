import { Box } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import { SingleCoupon } from "../SingleCoupon";
import { SingleCard } from "../SingleCard";

export function SingleRewardData(props: any) {
  const reward = props.reward;
  const navigate = useNavigate();

  return (
    <>
      {reward?.rewardOfferType?.type === "coupon" ? (
        <Box
          boxShadow={"2xl"}
          width="250px"
          bgColor="#FFB84C"
          borderRadius="lg"
          color="#FFFFFF"
          py="3"
          onClick={() => props.onClick(reward)}
        >
          <SingleCoupon reward={reward} />
        </Box>
      ) : reward?.rewardOfferType?.type === "card" ? (
        <Box
          boxShadow={"2xl"}
          width="400px"
          bgColor="#10A19D"
          borderRadius="lg"
          color="#FFFFFF"
          py="3"
          //   onClick={() => navigate(`/editRewardAndOffer/${reward._id}`)}
          onClick={() => props.onClick(reward)}
        >
          <SingleCard reward={reward} />
        </Box>
      ) : null}
    </>
  );
}
