import { Text, Center, Image } from "@chakra-ui/react";
import logo from "../../../assets/logo1024.png";

export function SingleCouponForBrand(props: any) {
  const reward = props?.reward;
  return (
    <>
      <Center borderRadius={"100%"} py="5">
        <Image src={logo} height={"100px"} width="100px" alt=""></Image>
      </Center>
      <Text color="#000000" fontSize="sm" fontWeight={"bold"}>
        {reward?.offerName}
      </Text>
      <Text color="#000000" fontSize="sm" fontWeight={"bold"}>
        Brand : {reward?.brandName}
      </Text>
      <Text color="#000000" fontSize="sm" fontWeight={"bold"}>
        Total Quantity of coupon : {reward?.quantity}
      </Text>
      {reward?.couponRewardInfo?.couponType === "discount" ? (
        <Text color="#000000" fontSize="sm" fontWeight="bold">
          {reward?.rewardOfferType?.couponReward?.couponInfo?.type === "%"
            ? `${reward?.couponRewardInfo?.couponInfo?.value} % discount on ${reward?.couponRewardInfo?.couponItem}`
            : `₹ ${reward?.couponRewardInfo?.couponInfo?.value} OFF on ${reward?.couponRewardInfo?.couponItem}`}
        </Text>
      ) : reward?.rewardOfferType?.couponReward?.couponType === "free" ? (
        <Text color="#000000" fontSize="lg" fontWeight={"bold"}>
          {` Free ${reward?.couponRewardInfo?.couponInfo?.freeItemQuantity} ${reward?.couponRewardInfo?.couponItem}`}
        </Text>
      ) : null}
      <Text color="#000000" fontSize="sm">
        {" "}
        {`Valid till ${reward?.couponRewardInfo?.validity?.to}`}
      </Text>
      <Text color="#000000" fontSize="sm">
        {`you can redeem only ${reward?.couponRewardInfo?.redeemFrequency} times`}
      </Text>
    </>
  );
}
