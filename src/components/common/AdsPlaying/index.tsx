import React, { useEffect, useState } from "react";
// import { Link as RouterLink } from "react-router-dom";
import { Box, Flex, Image, Text } from "@chakra-ui/react";
import { consvertSecondToHrAndMinutes } from "../../../utils/dateAndTime";
import Axios from "axios";
export function AdsPlaying(props: any) {
  const [video, setVideo] = useState<any>(null);
  const [errroVideo, setErrorVideo] = useState<any>("");
  const { campaignId } = props;

  const playingTime = consvertSecondToHrAndMinutes(
    video?.duration * video?.totalSlotBooked
  ); // sending time in seconds only and get in hr, mn,sec
  const getCampaignDetail = async () => {
    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/${campaignId}`
      );
      setVideo(data);
    } catch (error: any) {
      setErrorVideo(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      );
    }
  };
  useEffect(() => {
    if (props.video) {
      setVideo(props.video);
    } else {
      getCampaignDetail();
    }
  });

  return (
    <Box
      p="2"
      width="100%"
      bgColor="#F6F5F5"
      borderRadius="12px"
      boxShadow="2xl"
    >
      <Image width="100%" src={video?.thumbnail} alt=""></Image>
      <Text
        color="#403F49"
        pt="2"
        fontSize="sm"
        fontWeight="semibold"
        align="left"
      >
        {video?.campaignName}
      </Text>
      <Flex justifyContent="space-between" pt="2">
        <Text color="#403F49" fontSize="sm" align="left">
          Playtime
        </Text>
        <Text color="#403F49" fontSize="sm" align="left">
          {/* {`${video.hrsToComplete} hours`} */}
          {`${playingTime}`}
        </Text>
      </Flex>
      <Flex justifyContent="space-between" pt="2" align="center">
        <Text color="#403F49" fontSize="sm" align="left">
          Total no of slots
        </Text>
        <Text color="#403F49" fontSize="sm" align="left">
          {video?.totalSlotBooked}
        </Text>
      </Flex>
    </Box>
  );
}
