import { Text, Center, Image, Button, Box } from "@chakra-ui/react";
import logo from "../../../assets/logo1024.png";
import ScratchCard from "react-scratchcard-v3";
import Coupon from "../../../assets/couponimage.jpg";
import { useState } from "react";

export function UserCoupon(props: any) {
  const reward = props?.reward;

  const couponCode = props?.reward?.rewardCoupons
    ?.filter((coupon: any) => coupon?.redeemer === props?.userCouponDetail?._id)
    .map((c: any) => c.couponCode)[0];
  const [showButton, setShowButton] = useState<any>(false);

  return (
    <Box width={280} height={300}>
      <ScratchCard
        width={280}
        height={300}
        image={Coupon}
        finishPercent={70}
        onComplete={() => setShowButton(true)}
      >
        <Center borderRadius={"100%"} py="5">
          <Image src={logo} height={"100px"} width="100px" alt=""></Image>
        </Center>
        <Text color="#000000" fontSize="sm" fontWeight={"bold"}>
          {reward?.offerName}
        </Text>
        <Text color="#000000" fontSize="sm" fontWeight={"bold"}>
          Brand : {reward?.brandName}
        </Text>
        <Text color="#000000" fontSize="sm" fontWeight={"bold"}>
          CouponCode : {couponCode}
        </Text>
        {reward?.couponRewardInfo?.couponType === "discount" ? (
          <Text color="#000000" fontSize="sm" fontWeight="bold">
            {reward?.rewardOfferType?.couponReward?.couponInfo?.type === "%"
              ? `${reward?.couponRewardInfo?.couponInfo?.value} % discount on ${reward?.couponRewardInfo?.couponItem}`
              : `₹ ${reward?.couponRewardInfo?.couponInfo?.value} OFF on ${reward?.couponRewardInfo?.couponItem}`}
          </Text>
        ) : reward?.rewardOfferType?.couponReward?.couponType === "free" ? (
          <Text color="#000000" fontSize="lg" fontWeight={"bold"}>
            {` Free ${reward?.couponRewardInfo?.couponInfo?.freeItemQuantity} ${reward?.couponRewardInfo?.couponItem}`}
          </Text>
        ) : null}
        <Text color="#000000" fontSize="sm">
          {" "}
          {`Valid till ${reward?.couponRewardInfo?.validity?.to}`}
        </Text>
        <Text color="#000000" fontSize="sm">
          {`you can redeem only ${reward?.couponRewardInfo?.redeemFrequency} times`}
        </Text>
      </ScratchCard>
      {showButton ? (
        <Button
          fontSize="sm"
          py="2"
          _hover={{ bg: "red", color: "#FFFFFF" }}
          onClick={() => {
            console.log("clicked!");
            props?.handleOpenCouponOfferPartner(reward?._id, couponCode);
          }}
        >
          See all rewad offer partner List
        </Button>
      ) : null}
    </Box>
  );
}
