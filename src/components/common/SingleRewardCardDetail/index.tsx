import { Box, Flex, SimpleGrid, Text, Tooltip, Stack } from "@chakra-ui/react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { AdsPlaying } from "../AdsPlaying";
import { getCampaignListByScreenId } from "../../../Actions/campaignAction";
import { AiFillEdit } from "react-icons/ai";
import { SingleCardCoupon } from "../SingleCardCoupon";
// import { convertIntoDateAndTime } from "../../../utils/dateAndTime";

export function SingleRewardCardDetail(props: any) {
  const reward = props.reward;
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const activeCampaignListByScreenID = useSelector(
    (state: any) => state.activeCampaignListByScreenID
  );
  const {
    loading: loadingCampaign,
    error: errorCampaign,
    campaigns: videosList,
  } = activeCampaignListByScreenID;

  useEffect(() => {
    dispatch(getCampaignListByScreenId("64227457412d4d0571459576"));
  }, []);

  return (
    <Box>
      <SimpleGrid columns={[1, 2, 1]} spacing="4" boxShadow={"2xl"} p="5">
        <Box boxShadow={"2xl"} p="10">
          <Stack align="flex-end">
            <Tooltip
              label="Click for edit reward details"
              // bg="gray.300"
              // color="black"
              // arrowSize={15}
              placement="right-end"
            >
              <AiFillEdit
                color="#000000"
                size="30px"
                onClick={() =>
                  navigate(`/editCardRewardAndOffer/${reward?._id}`)
                }
              />
            </Tooltip>
          </Stack>

          <Text
            align="left"
            fontSize="2xl"
            fontWeight={"bold"}
            color="orange"
            py="5"
          >
            Basic Details of Card
          </Text>
          <Box>
            <Flex gap={5}>
              <Text color="#000000" fontSize="md" align="left">
                Reward program name:
              </Text>
              <Text color="#000000" fontSize="md">
                {reward?.offerName}
              </Text>
            </Flex>
            <Flex gap={5}>
              <Text color="#000000" fontSize="md" align="left">
                Rewarding Brand:
              </Text>
              <Text color="#000000" fontSize="md">
                {reward?.brandName}
              </Text>
            </Flex>
            <Flex gap={5}>
              <Text color="#000000" fontSize="md" align="left">
                Rewarding Quantity:
              </Text>
              <Text color="#000000" fontSize="md">
                {reward?.quantity}
              </Text>
            </Flex>
            <Flex gap={5}>
              <Text color="#000000" fontSize="md" align="left">
                Ratting of Coupon:
              </Text>
              <Text color="#000000" fontSize="md">
                {reward?.ratings || 0}
              </Text>
            </Flex>
            <Flex gap={5}>
              <Text color="#000000" fontSize="md" align="left">
                Card valid till:
              </Text>
              <Text color="#000000" fontSize="md">
                {reward?.validity?.from} to {reward?.validity?.to}
              </Text>
            </Flex>
            <Flex gap={5}>
              <Text color="#000000" fontSize="md" align="left">
                Card Type:
              </Text>
              <Text color="#000000" fontSize="md">
                {reward?.cardType}
              </Text>
            </Flex>
          </Box>
        </Box>
        <Box boxShadow={"2xl"} p="10" mt="5">
          <Text
            align="left"
            fontSize="2xl"
            fontWeight={"bold"}
            color="orange"
            py="5"
          >
            See All Coupon List related to this card
          </Text>
          <SimpleGrid columns={[1, null, 5]} spacing="5" pt="5">
            {reward?.cardMilestonesRewards?.map((eachCoupon: any) => (
              <Stack>
                <SingleCardCoupon reward={reward} singleCoupon={eachCoupon} />
              </Stack>
            ))}
          </SimpleGrid>
        </Box>
        <Box boxShadow={"2xl"} p="10" mt="5">
          <Text
            align="left"
            fontSize="2xl"
            fontWeight={"bold"}
            color="orange"
            py="5"
          >
            See All Campaign List attached to this coupon
          </Text>
          <SimpleGrid columns={[2, 2, 4]} spacing="5" pt="5">
            {videosList &&
              videosList?.map((video: any, index: any) => (
                <AdsPlaying video={video} key={index} />
              ))}
          </SimpleGrid>
        </Box>
        <Box boxShadow={"2xl"} p="10" mt="5">
          <Text
            align="left"
            fontSize="2xl"
            fontWeight={"bold"}
            color="orange"
            py="5"
          >
            See All User/patner list
          </Text>
          <SimpleGrid columns={[2, 2, 4]} spacing="5" pt="5">
            {videosList &&
              videosList?.map((video: any, index: any) => (
                <AdsPlaying video={video} key={index} />
              ))}
          </SimpleGrid>
        </Box>
      </SimpleGrid>
    </Box>
  );
}
