import { createStore, compose, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";

import {
  createCampaignReducer,
  campaignListAllReducer,
  campaignDetailsReducer,
  campaignListByScreenIDReducer,
  activeCampaignListByScreenIDReducer,
  campaignDeleteReducer,
  filteredCampaignListDateWiseReducer,
} from "../Reducers/campaignReducers";

import {
  mailSendReducer,
  userDeleteReducer,
  userDetailsReducer,
  userListReducer,
  userScreensReducer,
  userSigninReducer,
  userSignupReducer,
  userUpdateProfileReducer,
  userCampaignReducer,
  userMediaReducer,
  userUpdatePasswordReducer,
  userCouponRewardListReducer,
} from "../Reducers/userReducers";
import {
  walletCreateReducer,
  walletEditReducer,
  getWalletDataReducer,
  getTranjectionDatatReducer,
} from "../Reducers/walletReducers";
import {
  allMediaReducer,
  mediaGetReducer,
  videoFromImagesReducer,
  mediaUploadReducer,
  myMediaReducer,
} from "../Reducers/mediaReducers";
import {
  allScreensGetReducer,
  filterScreenListByAudianceReducer,
  filterScreenListReducer,
  playlistCheckReducer,
  screenAllyPleaGrantReducer,
  screenAllyPleaRejectReducer,
  screenAllyPleaRequestReducer,
  screenCreateReducer,
  screenDeleteReducer,
  screenDetailsReducer,
  screenFlagReducer,
  screenLikeReducer,
  screenListReducer,
  screenLogsReducer,
  screenParamsReducer,
  screenPinDetailsReducer,
  screenReviewCreateReducer,
  screenSubscribeReducer,
  screenUnlikeReducer,
  screenUnsubscribeReducer,
  screenUpdateReducer,
  screenVideoDeleteReducer,
  screenVideosReducer,
} from "../Reducers/screenReducers";

import {
  jsonPinsReducer,
  pinDetailsReducer,
  pinUpdateReducer,
} from "../Reducers/pinReducers";
import {
  allPleasListByUserReducer,
  allPleasListReducer,
  campaignAllyPleaGrantReducer,
  campaignAllyPleaRejectReducer,
  coupomRedeemUserPleaReducer,
} from "../Reducers/pleaReducers";
import {
  calenderDataAddReducer,
  calenderDaySlotBookReducer,
  dayBookingReducer,
  screenCalenderReducer,
  slotBookingReducer,
} from "../Reducers/calendarReducers";
import {
  advertGameCreateReducer,
  advertGameDetailsReducer,
  advertGameRemoveReducer,
  screenGameCreateReducer,
  screenGameDetailsReducer,
  screenGameRemoveReducer,
} from "../Reducers/gameReducers";

import { getSingleLogDetailsReducer } from "../Reducers/paymentReducers";
import { createCampaignForMultipleScreenReducer } from "../Reducers/campaignForMultipleScreenReducers";
import {
  myRewardProgramsCreatedReducer,
  rewardProgramCreateReducer,
  rewardProgramDetailsGetReducer,
  rewardProgramUpdateReducer,
} from "../Reducers/rewardReducer";
import {
  addRedeemerToCouponRewardReducer,
  couponRewardOfferDetailsGetReducer,
  couponRewardOfferListForBrandGetReducer,
  couponRewardOfferPartnerListReducer,
  couponRewardProgramCreateReducer,
  couponRewardProgramUpdateReducer,
  incrementUserRedeemFrequencyReducer,
} from "../Reducers/couponRewardOfferReducers";
import {
  cardRewardOfferDetailsGetReducer,
  cardRewardOfferListForBrandGetReducer,
  cardRewardProgramCreateReducer,
  cardRewardProgramUpdateReducer,
} from "../Reducers/cardRewardOfferReducers";

const initialState = {
  userSignin: {
    userInfo: localStorage.getItem("userInfo")
      ? JSON.parse(localStorage.getItem("userInfo"))
      : null,
  },
};

const reducer = combineReducers({
  //campaign reducer
  createCampaign: createCampaignReducer,
  campaignListAll: campaignListAllReducer,
  campaignDetail: campaignDetailsReducer,
  campaignListByScreenId: campaignListByScreenIDReducer,
  activeCampaignListByScreenID: activeCampaignListByScreenIDReducer,
  campaignDelete: campaignDeleteReducer,
  campaignListDateWise: filteredCampaignListDateWiseReducer,

  //campaign for multiple screens reducers
  createCampaignForMultipleScreen: createCampaignForMultipleScreenReducer,

  //plea reducers
  playlistCheck: playlistCheckReducer,

  // user reducer
  userSignin: userSigninReducer,
  userSignup: userSignupReducer,
  userDetails: userDetailsReducer,
  userUpdateProfile: userUpdateProfileReducer,
  userUpdatePassword: userUpdatePasswordReducer,
  userList: userListReducer,
  userDelete: userDeleteReducer,
  userScreens: userScreensReducer,
  userCampaign: userCampaignReducer,
  userMedia: userMediaReducer,
  mailSend: mailSendReducer,
  userCouponRewardList: userCouponRewardListReducer,

  //wallet reducer
  walletCreate: walletCreateReducer,
  walletEdit: walletEditReducer,
  getWalletData: getWalletDataReducer,
  getTranjectionData: getTranjectionDatatReducer,

  // media reducers
  mediaUpload: mediaUploadReducer,
  mediaGet: mediaGetReducer,
  allMedia: allMediaReducer,
  myMedia: myMediaReducer,
  videoFromImages: videoFromImagesReducer,

  //screen reducer
  allScreensGet: allScreensGetReducer,
  screenList: screenListReducer,
  filterScreenList: filterScreenListReducer,
  filterScreenListByAudiance: filterScreenListByAudianceReducer,
  screenLogs: screenLogsReducer,
  screenDetails: screenDetailsReducer,
  screenCreate: screenCreateReducer,
  screenUpdate: screenUpdateReducer,
  screenDelete: screenDeleteReducer,
  screenVideos: screenVideosReducer,
  screenPinDetails: screenPinDetailsReducer,
  screenAllyPleaRequest: screenAllyPleaRequestReducer,
  screenParams: screenParamsReducer,
  screenVideoDelete: screenVideoDeleteReducer,
  screenReviewCreate: screenReviewCreateReducer,
  screenLike: screenLikeReducer,
  screenUnlike: screenUnlikeReducer,
  screenSubscribe: screenSubscribeReducer,
  screenUnsubscribe: screenUnsubscribeReducer,
  screenFlag: screenFlagReducer,
  screenAllyPleaReject: screenAllyPleaRejectReducer,
  screenAllyPleaGrant: screenAllyPleaGrantReducer,

  //plea reducer
  allPleasList: allPleasListReducer,
  allPleasListByUser: allPleasListByUserReducer,
  campaignAllyPleaReject: campaignAllyPleaRejectReducer,
  campaignAllyPleaGrant: campaignAllyPleaGrantReducer,
  coupomRedeemUserPlea: coupomRedeemUserPleaReducer,

  //calender reducer
  screenCalender: screenCalenderReducer,
  calenderDataAdd: calenderDataAddReducer,
  calenderDaySlotBook: calenderDaySlotBookReducer,
  slotBooking: slotBookingReducer,
  dayBooking: dayBookingReducer,

  // screen game reducer
  screenGameDetails: screenGameDetailsReducer,
  screenGameCreate: screenGameCreateReducer,
  screenGameRemove: screenGameRemoveReducer,
  advertGameDetails: advertGameDetailsReducer,
  advertGameCreate: advertGameCreateReducer,
  advertGameRemove: advertGameRemoveReducer,

  // pin reducer
  jsonPins: jsonPinsReducer,
  pinDetails: pinDetailsReducer,
  pinUpdate: pinUpdateReducer,
  // scrign logs reducers
  getSingleLogDetails: getSingleLogDetailsReducer,

  //coupon reward offer reducer
  couponRewardOfferCreate: couponRewardProgramCreateReducer,
  couponRewardOfferUpdate: couponRewardProgramUpdateReducer,
  couponRewardOfferGetDetails: couponRewardOfferDetailsGetReducer,
  couponRewardOfferListForBrand: couponRewardOfferListForBrandGetReducer,
  addRedeemerToCouponReward: addRedeemerToCouponRewardReducer,
  couponRewardOfferPartnerList: couponRewardOfferPartnerListReducer,

  incrementUserRedeemFrequencyByBrand: incrementUserRedeemFrequencyReducer,

  //  Card reward offer reducers
  cardRewardOfferCreate: cardRewardProgramCreateReducer,
  cardRewardOfferUpdate: cardRewardProgramUpdateReducer,
  cardRewardOfferGetDetails: cardRewardOfferDetailsGetReducer,
  cardRewardOfferListForBrand: cardRewardOfferListForBrandGetReducer,

  // reward and offer reducers
  rewardProgramCreate: rewardProgramCreateReducer,
  myRewardProgramsCreated: myRewardProgramsCreatedReducer,
  rewardProgramDetailsGet: rewardProgramDetailsGetReducer,
  rewardProgramUpdate: rewardProgramUpdateReducer,
});

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  reducer,
  initialState,
  composeEnhancer(applyMiddleware(thunk))
);

export default store;
