import { Box, SimpleGrid, Stack } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { userCouponRewardOfferList } from "../../Actions/userActions";
import HLoading from "../../components/atoms/HLoading";
import MessageBox from "../../components/atoms/MessageBox";
import { useNavigate } from "react-router-dom";
import { UserCoupon } from "../../components/common/UserCoupon";
import { CouponRewardOfferPartnerListModel } from "./CouponRewardOfferPartnerListModel";

export function MyRewardsList(props: any) {
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();
  const [showCouponOfferPartnerModel, setShowCouponOfferPartnerModel] =
    useState<any>(false);
  const [selectedCouponId, setSelectedCouponId] = useState<any>(null);
  const [scratchedCouponList, setScratchedCouponList] = useState<any>([]);
  const [couponCode, setCouponCode] = useState<any>(null);

  const userCouponRewardList = useSelector(
    (state: any) => state.userCouponRewardList
  );
  const {
    loading: loadingUserCouponList,
    error: erroruserCouponList,
    userCouponsList,
  } = userCouponRewardList;

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo: user } = userSignin;

  const handleOpenCouponOfferPartner = async (
    couponId: any,
    couonCode: any
  ) => {
    console.log("handleOpenCouponOfferPartner called!");
    setCouponCode(couonCode);
    setSelectedCouponId(couponId);
    setShowCouponOfferPartnerModel(true);
  };

  useEffect(() => {
    if (!user) {
      navigate("/signin");
    }
    dispatch(userCouponRewardOfferList(user));
    // console.log(userCouponsList.map((userCoupon: any) => userCoupon));
  }, []);
  return (
    <Box>
      {loadingUserCouponList ? (
        <HLoading loading={loadingUserCouponList} />
      ) : erroruserCouponList ? (
        <MessageBox variant="danger">{erroruserCouponList}</MessageBox>
      ) : (
        <Box p="10">
          {selectedCouponId ? (
            <CouponRewardOfferPartnerListModel
              show={showCouponOfferPartnerModel}
              onHide={() => {
                setShowCouponOfferPartnerModel(false);
                setSelectedCouponId(null);
                setCouponCode(null);
              }}
              couponId={selectedCouponId}
              couponCode={couponCode}
            />
          ) : null}
          <SimpleGrid columns={[1, 3, 4]} spacing="5" pt="5">
            {userCouponsList?.map((userCoupon: any, index: any) => {
              return (
                <Stack key={index}>
                  <UserCoupon
                    reward={userCoupon}
                    userCouponDetail={user}
                    handleOpenCouponOfferPartner={handleOpenCouponOfferPartner}
                  />
                </Stack>
              );
            })}
          </SimpleGrid>
        </Box>
      )}
    </Box>
  );
}
