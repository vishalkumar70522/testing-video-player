import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import {
  Stack,
  IconButton,
  Text,
  Box,
  FormControl,
  FormLabel,
  Select,
  SimpleGrid,
  Button,
} from "@chakra-ui/react";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { useNavigate } from "react-router-dom";
import { getCouponRewardOfferPartnerList } from "../../Actions/couponRewardOfferActions";
import { useDispatch, useSelector } from "react-redux";
import HLoading from "../../components/atoms/HLoading";
import MessageBox from "../../components/atoms/MessageBox";
import { addNewPleaForUserRedeemCouponOffer } from "../../Actions/pleaActions";
import { COUPOM_REDEEM_PLEA_RESET } from "../../Constants/pleaConstants";

export function CouponRewardOfferPartnerListModel(props: any) {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [offerPartnerId, setOfferPartnerId] = useState<any>([]);
  const handleSelectRewadOfferpartner = (userId: any) => {
    // first check this user id present of not
    if (offerPartnerId?.find((id: any) => id == userId)) {
      setOfferPartnerId(offerPartnerId?.filter((id: any) => id != userId));
    } else {
      setOfferPartnerId([...offerPartnerId, userId]);
    }
  };
  const couponRewardOfferPartnerList = useSelector(
    (state: any) => state.couponRewardOfferPartnerList
  );
  const { loading, error, couponOfferPartnerList } =
    couponRewardOfferPartnerList;

  const coupomRedeemUserPlea = useSelector(
    (state: any) => state.coupomRedeemUserPlea
  );
  const {
    loading: loadingCoupomRedeemUserPlea,
    error: errroCoupomRedeemUserPlea,
    success: successCoupomRedeemUserPlea,
    plea,
  } = coupomRedeemUserPlea;
  const handleCoupomRedeemUserPlea = (toUser: any) => {
    dispatch(
      addNewPleaForUserRedeemCouponOffer({
        couponId: props?.couponId,
        toUser,
        couponCode: props.couponCode,
      })
    );
  };

  useEffect(() => {
    if (successCoupomRedeemUserPlea) {
      alert("Coupon reward offer plea added!");
      dispatch({ type: COUPOM_REDEEM_PLEA_RESET });
    }
    if (errroCoupomRedeemUserPlea) {
      setTimeout(() => {
        dispatch({ type: COUPOM_REDEEM_PLEA_RESET });
      }, 5000);
    }
  }, [successCoupomRedeemUserPlea, errroCoupomRedeemUserPlea]);

  //coupomRedeemUserPlea

  useEffect(() => {
    dispatch(getCouponRewardOfferPartnerList(props?.couponId));
  }, []);

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      backdrop="static"
      keyboard={false}
      dialogClassName="modal-90w"
    >
      <Modal.Body>
        <Box bgColor="#FFFFFF">
          <Stack align="end" justifyContent="flex-end">
            <IconButton
              bg="none"
              icon={
                <AiOutlineCloseCircle
                  size="40px"
                  fontWeight="10"
                  color="black"
                  onClick={() => props.onHide()}
                />
              }
              aria-label="Close"
            />
          </Stack>
          {loading || loadingCoupomRedeemUserPlea ? (
            <HLoading loading={loading || loadingCoupomRedeemUserPlea} />
          ) : error || errroCoupomRedeemUserPlea ? (
            <MessageBox variant="danger">
              {error || errroCoupomRedeemUserPlea}
            </MessageBox>
          ) : (
            <Stack pl="20" pr="20">
              <Text color="#000000" fontSize="lg" fontWeight="bold">
                Coupon Reward Offer Partner List
              </Text>

              <FormControl id="name" pt="5">
                <FormLabel fontSize="sm" color="#333333">
                  Select Coupon reward Offer Partner
                </FormLabel>
                <Stack direction="column" align="left">
                  <Select
                    height="36px"
                    variant="outline"
                    borderColor="#0EBCF5"
                    bgColor="#FFFFFF"
                    color="#403F49"
                    fontSize="sm"
                    onChange={(e: any) => {
                      if (e.target.value !== "")
                        handleSelectRewadOfferpartner(e.target.value);
                    }}
                  >
                    <option value="">Select users</option>

                    {couponOfferPartnerList?.length > 0 ? (
                      couponOfferPartnerList?.map((user: any, index: any) => (
                        <option value={user._id} key={index}>
                          {user.name}
                        </option>
                      ))
                    ) : (
                      <option value="">No user found with this name</option>
                    )}
                  </Select>
                </Stack>
              </FormControl>
              <SimpleGrid columns={[1, 2, 2]} spacing="5" py="5">
                {offerPartnerId?.map((id: any, index: any) => {
                  const offerPartner = couponOfferPartnerList?.find(
                    (user: any) => user._id === id
                  );
                  return (
                    <Stack key={index} bgColor="yellow" p="2">
                      <Text>Name : {offerPartner.name} </Text>
                      <Text>Phone : {offerPartner.phone} </Text>
                      <Text>Email : {offerPartner.email}</Text>
                      <Text>Address : {offerPartner.address} </Text>
                      <Button
                        py="3"
                        bgColor="red"
                        onClick={() =>
                          handleCoupomRedeemUserPlea(offerPartner._id)
                        }
                      >
                        Send plea for Coupon
                      </Button>
                    </Stack>
                  );
                })}
              </SimpleGrid>
            </Stack>
          )}
        </Box>
      </Modal.Body>
    </Modal>
  );
}
