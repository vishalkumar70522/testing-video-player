import { useState } from "react";
import {
  Box,
  Text,
  Button,
  Stack,
  Flex,
  FormControl,
  FormLabel,
  Input,
  FormHelperText,
  FormErrorMessage,
  InputGroup,
  InputRightElement,
} from "@chakra-ui/react";
import { useDispatch, useSelector } from "react-redux";
import { AiOutlineEye, AiOutlineEyeInvisible } from "react-icons/ai";
import {
  updateUserPassword,
  updateUserProfile,
} from "../../Actions/userActions";
import {
  USER_UPDATE_PASSWORD_RESET,
  USER_UPDATE_PROFILE_RESET,
} from "../../Constants/userConstants";

export function EditUserProfile() {
  const dispatch = useDispatch<any>();

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo: user } = userSignin;
  const [activity, setActivity] = useState<any>("Profile");
  const [name, setName] = useState<any>(user?.name);
  const [nameError, setNameError] = useState<any>("");
  const [email, setEmail] = useState<any>(user?.email);
  const [phone, setPhone] = useState<any>(user?.phone);
  const [address, setAddress] = useState<any>(user?.address);
  const [stateUt, setState] = useState<any>(user?.stateUt);
  const [districtCity, setDistrictCity] = useState<any>(user?.districtCity);
  const [pincode, setPincode] = useState<any>(user?.pincode);
  const [currentPassword, setCurrentPassword] = useState<any>("");
  const [password, setPassword] = useState<any>("");
  const [confirmPassword, setConfirmPassword] = useState<any>("");
  const [showPassword, setShowPassword] = useState<any>(false);
  const [showConformPassword, setShowConformPassword] = useState<any>(false);
  const [error, setError] = useState<any>("");
  const [passwordError, setPasswordError] = useState<any>("");
  const [currentPasswordError, setCurrentPasswordError] = useState<any>("");

  const userUpdatePassword = useSelector(
    (state: any) => state.userUpdatePassword
  );
  const {
    loadingUpdatePassword,
    error: errorUpdatePassword,
    success: passwordUpdateSuccess,
  } = userUpdatePassword;
  const userUpdateProfile = useSelector(
    (state: any) => state.userUpdateProfile
  );
  const { loading, error: errorUpdateProfile, success } = userUpdateProfile;
  if (success) {
    dispatch({ type: USER_UPDATE_PROFILE_RESET });
    alert("User updated successFull!");
  }
  if (passwordUpdateSuccess) {
    dispatch({ type: USER_UPDATE_PASSWORD_RESET });
    alert("Password updated successFull!");
    setConfirmPassword("");
    setPassword("");
    setCurrentPassword("");
  }
  if (errorUpdatePassword) {
    dispatch({ type: USER_UPDATE_PASSWORD_RESET });
    alert("old Password wrong!");
    setCurrentPasswordError("old Password wrong!");
  }
  const updateUserInfo = () => {
    dispatch(
      updateUserProfile({
        name,
        email,
        phone,
        address,
        districtCity,
        stateUt,
        pincode,
      })
    );
  };

  const updatePassword = () => {
    if (!currentPassword) {
      setCurrentPasswordError("Please enter your old password");
      return;
    }
    if (!(password === confirmPassword)) {
      setPasswordError("Password mismatch");
      return;
    }
    dispatch(
      updateUserPassword({
        oldPassword: currentPassword,
        newPassword: password,
      })
    );
  };

  return (
    <>
      <Box px={{ base: "2", lg: "20" }} py={{ base: "2", lg: "30" }}>
        <Text color="#000000" fontSize="3xl" align="left" fontWeight="bold">
          Account
        </Text>
        <Text color="#4E4E4E" fontSize="sm" align="left">
          Set your account details from below
        </Text>
        <Stack pt="10">
          <Flex
            color="#000000"
            fontSize="xl"
            fontWeight="semibold"
            bg="#F2F2F2"
          >
            <Button
              py="3"
              px="14"
              bg={activity === "Profile" ? "#FFFFFF" : "#F2F2F2"}
              color="#000000"
              boxShadow={activity === "Profile" ? "xl" : ""}
              onClick={() => setActivity("Profile")}
            >
              Profile
            </Button>
            <Button
              py="3"
              px="14"
              bg={activity === "Address" ? "#FFFFFF" : "#F2F2F2"}
              color="#000000"
              boxShadow={activity === "Address" ? "xl" : ""}
              onClick={() => setActivity("Address")}
            >
              Address
            </Button>
            <Button
              py="3"
              px="14"
              bg={activity === "Password" ? "#FFFFFF" : "#F2F2F2"}
              color="#000000"
              boxShadow={activity === "Password" ? "xl" : ""}
              onClick={() => setActivity("Password")}
            >
              Password
            </Button>
          </Flex>
        </Stack>

        <Stack pt="20" width={{ base: "100%", lg: "30%" }}>
          {activity === "Profile" ? (
            <Stack>
              <FormControl id="name" isInvalid={nameError ? true : false}>
                <FormLabel fontSize="sm" color="#333333">
                  Name
                </FormLabel>
                <Stack direction="column" align="left">
                  <Input
                    color="#333333"
                    id="name"
                    onChange={(e) => setName(e?.target?.value)}
                    placeholder="Enter name"
                    value={name}
                    required
                    type="text"
                    py="2"
                    rounded="md"
                    size="md"
                    borderColor="#888888"
                  />
                  {!nameError ? (
                    <FormHelperText></FormHelperText>
                  ) : (
                    <FormErrorMessage>{nameError}</FormErrorMessage>
                  )}
                </Stack>
              </FormControl>
              <FormControl id="email" isInvalid={nameError ? true : false}>
                <FormLabel fontSize="sm" color="#333333">
                  Email
                </FormLabel>
                <Stack direction="column" align="left">
                  <Input
                    color="#333333"
                    id="email"
                    onChange={(e) => setEmail(e?.target?.value)}
                    placeholder="Enter email"
                    value={email}
                    required
                    type="email"
                    py="2"
                    rounded="md"
                    size="md"
                    borderColor="#888888"
                  />
                  {!nameError ? (
                    <FormHelperText></FormHelperText>
                  ) : (
                    <FormErrorMessage>{nameError}</FormErrorMessage>
                  )}
                </Stack>
              </FormControl>
              <FormControl id="phone" isInvalid={nameError ? true : false}>
                <FormLabel fontSize="sm" color="#333333">
                  Phone
                </FormLabel>
                <Stack direction="column" align="left">
                  <Input
                    color="#333333"
                    id="phone"
                    onChange={(e) => setPhone(e?.target?.value)}
                    placeholder="Enter phone"
                    value={phone}
                    required
                    type="number"
                    py="2"
                    rounded="md"
                    size="md"
                    borderColor="#888888"
                  />
                  {!nameError ? (
                    <FormHelperText></FormHelperText>
                  ) : (
                    <FormErrorMessage>{nameError}</FormErrorMessage>
                  )}
                </Stack>
              </FormControl>
            </Stack>
          ) : activity === "Address" ? (
            <Stack>
              <FormControl id="name" isInvalid={nameError ? true : false}>
                <FormLabel fontSize="sm" color="#333333">
                  Home/Office
                </FormLabel>
                <Stack direction="column" align="left">
                  <Input
                    color="#333333"
                    id="name"
                    onChange={(e) => setAddress(e?.target?.value)}
                    placeholder="Home/office"
                    value={address}
                    required
                    type="text"
                    py="2"
                    rounded="md"
                    size="md"
                    borderColor="#888888"
                  />
                  {!nameError ? (
                    <FormHelperText></FormHelperText>
                  ) : (
                    <FormErrorMessage>{nameError}</FormErrorMessage>
                  )}
                </Stack>
              </FormControl>
              <FormControl id="email" isInvalid={nameError ? true : false}>
                <FormLabel fontSize="sm" color="#333333">
                  State
                </FormLabel>
                <Stack direction="column" align="left">
                  <Input
                    id="email"
                    onChange={(e) => setState(e?.target?.value)}
                    placeholder="Enter email"
                    value={stateUt}
                    required
                    type="text"
                    py="2"
                    rounded="md"
                    size="md"
                    borderColor="#888888"
                    color="#333333"
                  />
                  {!nameError ? (
                    <FormHelperText></FormHelperText>
                  ) : (
                    <FormErrorMessage>{nameError}</FormErrorMessage>
                  )}
                </Stack>
              </FormControl>
              <FormControl id="email" isInvalid={nameError ? true : false}>
                <FormLabel fontSize="sm" color="#333333">
                  districtCity
                </FormLabel>
                <Stack direction="column" align="left">
                  <Input
                    id="email"
                    onChange={(e) => setDistrictCity(e?.target?.value)}
                    placeholder="District/City"
                    value={districtCity}
                    required
                    type="text"
                    py="2"
                    rounded="md"
                    size="md"
                    borderColor="#888888"
                    color="#333333"
                  />
                  {!nameError ? (
                    <FormHelperText></FormHelperText>
                  ) : (
                    <FormErrorMessage>{nameError}</FormErrorMessage>
                  )}
                </Stack>
              </FormControl>

              <FormControl id="phone" isInvalid={nameError ? true : false}>
                <FormLabel fontSize="sm" color="#333333">
                  Pincode
                </FormLabel>
                <Stack direction="column" align="left">
                  <Input
                    id="phone"
                    onChange={(e) => setPincode(e?.target?.value)}
                    placeholder="Enter pincode"
                    value={pincode}
                    required
                    type="number"
                    py="2"
                    rounded="md"
                    size="md"
                    borderColor="#888888"
                    color="#333333"
                  />
                  {!nameError ? (
                    <FormHelperText></FormHelperText>
                  ) : (
                    <FormErrorMessage>{nameError}</FormErrorMessage>
                  )}
                </Stack>
              </FormControl>
            </Stack>
          ) : (
            <Stack>
              <FormControl
                id="name"
                isInvalid={currentPasswordError ? true : false}
              >
                <FormLabel fontSize="sm" color="#333333">
                  Current password
                </FormLabel>
                <Stack direction="column" align="left">
                  <Input
                    id="name"
                    onChange={(e) => {
                      setCurrentPassword(e?.target?.value);
                      setCurrentPasswordError("");
                    }}
                    placeholder="old password"
                    value={currentPassword}
                    required
                    type="password"
                    py="2"
                    rounded="md"
                    size="md"
                    borderColor="#888888"
                    color="#333333"
                  />
                  {!currentPasswordError ? (
                    <FormHelperText></FormHelperText>
                  ) : (
                    <FormErrorMessage>{currentPasswordError}</FormErrorMessage>
                  )}
                </Stack>
              </FormControl>
              <FormControl id="name" isInvalid={passwordError ? true : false}>
                <FormLabel px="1" fontSize="s" color="#333333">
                  New Password
                </FormLabel>
                <Stack direction="column" align="left">
                  <Stack direction="row" align="center">
                    <InputGroup size="md" alignItems="center">
                      <Input
                        id="password"
                        py="3"
                        onChange={(e) => {
                          setPassword(e?.target?.value);
                          setPasswordError("");
                        }}
                        type={showPassword ? "text" : "password"}
                        placeholder="Password"
                        value={password}
                        required
                        borderColor="#888888"
                        color="#333333"
                      />
                      <InputRightElement width="4.5rem" alignItems="center">
                        {showPassword ? (
                          <Stack mt="3">
                            <AiOutlineEye
                              size="20px"
                              color="black"
                              onClick={() => setShowPassword(!showPassword)}
                            />
                          </Stack>
                        ) : (
                          <Stack mt="3">
                            <AiOutlineEyeInvisible
                              size="20px"
                              color="black"
                              onClick={() => setShowPassword(!showPassword)}
                            />
                          </Stack>
                        )}
                      </InputRightElement>
                    </InputGroup>
                  </Stack>
                  {!passwordError ? (
                    <FormHelperText></FormHelperText>
                  ) : (
                    <FormErrorMessage>{passwordError}</FormErrorMessage>
                  )}
                </Stack>
              </FormControl>
              <FormControl
                p="1"
                id="confirmPassword"
                isInvalid={passwordError ? true : false}
              >
                <FormLabel px="1" fontSize="s" color="#333333">
                  Confirm New Password
                </FormLabel>
                <Stack direction="column" align="left">
                  <Stack direction="row" align="center">
                    <InputGroup size="md">
                      <Input
                        py="3"
                        id="confirmPassword"
                        onChange={(e) => {
                          setConfirmPassword(e?.target?.value);
                          setPasswordError("");
                        }}
                        value={confirmPassword}
                        required
                        type={showConformPassword ? "text" : "password"}
                        placeholder="Conform password"
                        borderColor="#888888"
                        color="#333333"
                      />
                      <InputRightElement width="4.5rem">
                        {showConformPassword ? (
                          <Stack mt="3">
                            <AiOutlineEye
                              size="20px"
                              color="black"
                              onClick={() =>
                                setShowConformPassword(!showConformPassword)
                              }
                            />
                          </Stack>
                        ) : (
                          <Stack mt="3">
                            <AiOutlineEyeInvisible
                              size="20px"
                              color="black"
                              onClick={() =>
                                setShowConformPassword(!showConformPassword)
                              }
                            />
                          </Stack>
                        )}
                      </InputRightElement>
                    </InputGroup>
                  </Stack>
                  {!passwordError ? (
                    <FormHelperText></FormHelperText>
                  ) : (
                    <FormErrorMessage>{passwordError}</FormErrorMessage>
                  )}
                </Stack>
              </FormControl>
            </Stack>
          )}
          <Stack align="left">
            {activity !== "Password" ? (
              <Button
                variant="outline"
                borderColor="#403F49"
                color="#D7380E"
                py="3"
                fontSize="lg"
                onClick={updateUserInfo}
              >
                Save
              </Button>
            ) : (
              <Button
                variant="outline"
                borderColor="#403F49"
                color="#D7380E"
                py="3"
                fontSize="lg"
                onClick={updatePassword}
              >
                Change Password
              </Button>
            )}
          </Stack>
        </Stack>
      </Box>
    </>
  );
}
