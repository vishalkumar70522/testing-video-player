import {
  Flex,
  Box,
  Text,
  Stack,
  HStack,
  VStack,
  InputGroup,
  Input,
  Button,
  FormControl,
  Tooltip,
  Image,
  SimpleGrid,
  FormLabel,
} from "@chakra-ui/react";
import { AiOutlineArrowLeft, AiOutlinePlusCircle } from "react-icons/ai";
import { MyMap } from "../MyMap";
import React, { useEffect, useState } from "react";
import DateFnsUtils from "@date-io/date-fns"; // choose your lib
import { MuiPickersUtilsProvider, TimePicker } from "@material-ui/pickers";
import {
  IconButton as MiuiIconButton,
  InputAdornment,
  Switch,
} from "@material-ui/core";
import { BsClock } from "react-icons/bs";
import { detailsScreen, updateScreen } from "../../Actions/screenActions";
import { SCREEN_UPDATE_RESET } from "../../Constants/screenConstants";

import MessageBox from "../../components/atoms/MessageBox";
import HLoading from "../../components/atoms/HLoading";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { BiDownArrow, BiUpArrow } from "react-icons/bi";
import { addFileOnWeb3 } from "../../utils/web3";

export const EditScreen = (props: any) => {
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();

  const screenId = window.location.pathname.split("/")[2];
  const screenSizeUnit = ["CM", "M", "Ft", "Inch"];
  const [tag, setTag] = useState<any>("");
  const [highlight, setHighLight] = useState<any>("");
  const [screenHighlights, setHighLights] = useState<any>([]);
  const [startTime, setStartTime] = useState<any>();
  const [endTime, setEndTime] = useState<any>();
  const [isUploading, setIsUploading] = useState(false);
  const [screenImage, setScreenImage] = useState<any>();
  const [file, setFile] = useState<any>(null);
  const [screenSizeUnitIndex, setScreenSizeUnitIndex] = useState<any>(0);

  const [name, setName] = useState<any>("");
  const [rentPerSlot, setRentPerSlot] = useState<any>("");
  const [image, setImage] = useState<any>("");
  const [screenCategory, setScreenCategory] = useState<any>("");
  const [screenType, setScreenType] = useState<any>("");
  const [screenWorth, setScreenWorth] = useState<any>("");
  const [slotsTimePeriod, setSlotsTimePeriod] = useState<any>("");
  const [description, setDescription] = useState<any>("");
  const [screenTags, setScreenTags] = useState<any>([]);
  const [screenAddress, setScreenAddress] = useState<any>("");
  const [districtCity, setDistrictCity] = useState<any>("");
  const [stateUT, setStateUT] = useState<any>("");
  const [country, setCountry] = useState<any>("India");
  const [screenLength, setScreenLength] = useState<any>("");
  const [screenWidth, setScreenWidth] = useState<any>("");
  const [measurementUnit, setMeasurementUnit] = useState<any>("ft");
  const [geometry, setGeometry] = useState<any>();
  const [jsonData, setJsonData] = useState<any>(null);
  const [syncCode, setSyncCode] = useState<any>("");

  const [sexRatio, setSexRatio] = useState<any>({});
  const [averagePurchasePower, setAveragePurchasePower] = useState<any>({});
  const [averageAgeGroup, setAverageAgeGroup] = useState<any>({});
  const [employmentStatus, setEmploymentStatus] = useState<any>([]);
  const [regularAudiencePercentage, setRegularAudiencePercentage] =
    useState<any>();
  const [maritalStatus, setMaritalStatus] = useState<any>([]);
  const [workType, setWorkType] = useState<any>([]);
  const [kidsFriendly, setKidsFriendly] = useState<any>("");
  const [crowdMobilityType, setCrowdMobilityType] = useState<any>([]);
  const [averageDailyFootfall, setAverageDailyFootfall] = useState<any>();
  const [selectCurrentLocation, setSelectCurrentLocation] =
    useState<any>(false);
  // console.log("averagePurchasePower : ", JSON.stringify(averagePurchasePower));

  const [lng, setLng] = useState<number | undefined>(25.52);
  const [lat, setLat] = useState<number | undefined>(82.33);
  const [mapProps, setMapProps] = useState<any>({
    lng: lng,
    lat: lat,
    zoom: 18,
    height: "360px",
  });

  const [activeStep, setActiveState] = useState<any>(0);
  const addOrRemobeEmploymentStatus = (lable: any) => {
    if (employmentStatus.includes(lable)) {
      setEmploymentStatus([
        ...employmentStatus.filter((data: any) => data !== lable),
      ]);
    } else {
      setEmploymentStatus([...employmentStatus, lable]);
    }
  };
  const addOrRemobeCrowdMobilityType = (lable: any) => {
    if (crowdMobilityType.includes(lable)) {
      setCrowdMobilityType([
        ...crowdMobilityType.filter((data: any) => data !== lable),
      ]);
    } else {
      setCrowdMobilityType([...crowdMobilityType, lable]);
    }
  };
  const addOrRemobeMaritalStatue = (lable: any) => {
    if (maritalStatus.includes(lable)) {
      setMaritalStatus([
        ...maritalStatus.filter((data: any) => data !== lable),
      ]);
    } else {
      setMaritalStatus([...maritalStatus, lable]);
    }
  };

  let hiddenInput: any = null;
  async function handlePhotoSelect(file: any) {
    const fileThumbnail = URL.createObjectURL(file);
    setScreenImage(fileThumbnail);
    setFile(file);
  }
  const hancleSelectSizeTypeUp = () => {
    if (screenSizeUnitIndex < screenSizeUnit.length - 1) {
      setScreenSizeUnitIndex(screenSizeUnitIndex + 1);
      setMeasurementUnit(screenSizeUnit[screenSizeUnitIndex]);
    }
  };
  const hancleSelectSizeTypeDown = () => {
    if (screenSizeUnitIndex > 0) {
      setScreenSizeUnitIndex(screenSizeUnitIndex - 1);
      setMeasurementUnit(screenSizeUnit[screenSizeUnitIndex]);
    }
  };

  const userSignin = useSelector((state: any) => state.userSignin);
  const { loading: loadingUser, error: errorUser, userInfo } = userSignin;

  const screenDetails = useSelector((state: any) => state.screenDetails);
  const { loading: loadingScreen, error: errorScreen, screen } = screenDetails;
  // console.log("screen : ", JSON.stringify(screen));
  // console.log("screenCategory: ", screenCategory);
  // console.log("calender : ", JSON.stringify(calender));

  const screenUpdate = useSelector((state: any) => state.screenUpdate);
  const {
    loading: loadingUpdate,
    error: errorUpdate,
    success: successUpdate,
  } = screenUpdate;

  const redirect = props?.location?.search
    ? props?.location?.search.split("=")[1]
    : "/";

  useEffect(() => {
    if (userInfo) {
      props?.history?.push(redirect);
    } else {
      navigate("/signin");
    }
    if (successUpdate) {
      alert("Screen updated successfully");
      navigate("/screen-owner");
    }
    if (!screen || screen._id !== screenId || successUpdate) {
      dispatch({
        type: SCREEN_UPDATE_RESET,
      });
      dispatch(detailsScreen(screenId));
    } else {
      setName(screen.name);
      setRentPerSlot(screen.rentPerSlot);
      setImage(image || screen.image);
      setScreenCategory(screen.category);
      setScreenType(screen.screenType);
      setScreenWorth(screen.scWorth);
      setSlotsTimePeriod(screen.slotsTimePeriod);
      setDescription(screen.description);
      setScreenAddress(screen.screenAddress);
      setDistrictCity(screen.districtCity);
      setStateUT(screen.stateUT);
      setCountry(screen.country);
      setScreenTags(screen.screenTags);
      setScreenLength(screen.size.length);
      setScreenWidth(screen.size.width);
      setMeasurementUnit(screen.size.measurementUnit);
      setHighLights(screen.screenHighlights);
      setStartTime(screen.startTime);
      setEndTime(screen.endTime);
      setLng(screen.lng);
      setLat(screen.lat);
      setGeometry({
        coordinates: [screen.lat, screen.lng],
      });
      setAverageDailyFootfall(screen?.additionalData?.averageDailyFootfall);
      setSexRatio(screen?.additionalData?.footfallClassification?.sexRatio);
      setAveragePurchasePower(
        screen?.additionalData?.footfallClassification?.averagePurchasePower
      );
      setEmploymentStatus(
        screen?.additionalData?.footfallClassification?.employmentStatus
      );
      setRegularAudiencePercentage(
        screen?.additionalData?.footfallClassification
          ?.regularAudiencePercentage
      );
      setMaritalStatus(
        screen?.additionalData?.footfallClassification?.maritalStatus
      );
      setWorkType(screen?.additionalData?.footfallClassification?.workType);
      setKidsFriendly(
        screen?.additionalData?.footfallClassification?.kidsFriendly
      );
      setAverageAgeGroup(
        screen?.additionalData?.footfallClassification?.averageAgeGroup
      );
      setCrowdMobilityType(
        screen?.additionalData?.footfallClassification?.crowdMobilityType
      );
      setJsonData({
        features: [
          {
            type: "Feature",
            properties: {
              pin: screen._id,
              screen: screen._id,
            },
            geometry: {
              coordinates: [screen.lat, screen.lng],
              type: "Point",
            },
          },
        ],
      });
      setSyncCode(screen.screenCode);
    }
  }, [dispatch, userInfo, successUpdate, navigate, redirect, screen]);

  const submitScreenHandler = (e: any) => {
    setLoading(true);
    if (file) {
      addFileOnWeb3(file).then((cid) => {
        dispatch(
          updateScreen({
            _id: screenId,
            name,
            rentPerSlot,
            image: `https://ipfs.io/ipfs/${cid}`,
            screenCategory,
            screenType,
            screenWorth,
            slotsTimePeriod,
            description,
            screenAddress,
            districtCity,
            stateUT,
            country,
            screenTags,
            screenHighlights,
            screenLength,
            measurementUnit,
            screenWidth,
            startTime,
            endTime,
            lng,
            lat,
            syncCode,
            additionalData: {
              averageDailyFootfall,
              footfallClassification: {
                sexRatio,
                averagePurchasePower,
                regularAudiencePercentage,
                employmentStatus,
                maritalStatus,
                workType,
                kidsFriendly: "yes",
                crowdMobilityType,
                averageAgeGroup,
              },
            },
          })
        );
      });
    } else {
      dispatch(
        updateScreen({
          _id: screenId,
          name,
          rentPerSlot,
          image,
          screenCategory,
          screenType,
          screenWorth,
          slotsTimePeriod,
          description,
          screenAddress,
          districtCity,
          stateUT,
          country,
          screenTags,
          screenHighlights,
          screenLength,
          measurementUnit,
          screenWidth,
          startTime,
          endTime,
          lng,
          lat,
          syncCode,
          additionalData: {
            averageDailyFootfall,
            footfallClassification: {
              sexRatio,
              averagePurchasePower,
              regularAudiencePercentage,
              employmentStatus,
              maritalStatus,
              workType,
              kidsFriendly: "yes",
              crowdMobilityType,
              averageAgeGroup,
            },
          },
        })
      );
    }
    setLoading(false);
  };

  const handleAddPinClick = (location: any) => {
    const { lng, lat } = location;
    setLng(lng);
    setLat(lat);
    setGeometry({
      coordinates: [lat, lng],
    });
    setJsonData({
      features: [
        {
          type: "Feature",
          properties: {
            pin: screen._id,
            screen: screen._id,
          },
          geometry: {
            coordinates: [lat, lng],
            type: "Point",
          },
        },
      ],
    });
    setMapProps({
      lng: lng,
      lat: lat,
    });
  };

  const handleAddTags = (event: any) => {
    if (event.which === 13) {
      setScreenTags([...screenTags, tag]);
      setTag("");
    }
  };
  const deleteTags = (tag: any) => {
    const newTags = screenTags.filter((eachtag: any) => eachtag !== tag);
    setScreenTags(newTags);
  };
  const handleMapLocation = (e: any) => {
    let geometry = e.target.value.split(",").map((data: any) => data.trim());
    setLng(geometry[1]);
    setLat(geometry[0]);
    setGeometry({
      coordinates: [lng, lat],
    });
  };
  const handleEditHighlites = (selectedIndex: any) => {
    let newHighlights = [...screenHighlights];
    newHighlights = newHighlights.filter(
      (value, index) => index != selectedIndex
    );
    setHighLight(screenHighlights[selectedIndex]);
    setHighLights([...newHighlights]);
  };
  const handleAddHighlites = () => {
    setHighLights([...screenHighlights, highlight]);
    setHighLight("");
  };
  const handleEndTime = (value: any) => {
    // let time = value.toString().split(" "); // Wed Jan 04 2023 08:22:28 GMT+0530
    // time = time[4];
    // console.log("time : ", time);
    setEndTime(value);
  };
  const handleStartTime = (value: any) => {
    // let time = value.toString().split(" "); // Wed Jan 04 2023 08:22:28 GMT+0530
    // time = time[4];
    setStartTime(value);
  };
  return (
    <Box px={{ base: "2", lg: "20" }} pt={{ base: "3", lg: "5" }}>
      {loadingScreen ? (
        <HLoading loading={loadingScreen} />
      ) : errorScreen || errorUpdate ? (
        <MessageBox message={errorScreen || errorUpdate}></MessageBox>
      ) : (
        <Box>
          <Stack p={{ base: "3", lg: "10" }} boxShadow="xl" borderRadius="lg">
            <Flex align="center">
              <AiOutlineArrowLeft
                size="20px"
                color="#333333"
                onClick={() => navigate("/screen-owner")}
              />
              <Text
                fontSize="lg"
                fontWeight="semibold"
                color="#333333"
                align="left"
                pl="5"
              >
                Edit screen details
              </Text>
            </Flex>
            <Text
              fontSize="lg"
              fontWeight="semibold"
              color="#383838"
              align="left"
              pt="5"
            >
              {name}
            </Text>
            <SimpleGrid columns={[2, 3, 5]} bg="#EDEDED">
              <Button
                py="3"
                px="14"
                bg={activeStep === 0 ? "#FFFFFF" : ""}
                color="#000000"
                border={activeStep === 0 ? "1px" : ""}
                onClick={() => setActiveState(0)}
              >
                Schedule
              </Button>
              <Button
                py="3"
                px="14"
                bg={activeStep === 1 ? "#FFFFFF" : ""}
                color="#000000"
                border={activeStep === 1 ? "1px" : ""}
                onClick={() => setActiveState(1)}
              >
                Slot details
              </Button>
              <Button
                py="3"
                px="14"
                bg={activeStep === 2 ? "#FFFFFF" : ""}
                color="#000000"
                border={activeStep === 2 ? "1px" : ""}
                onClick={() => setActiveState(2)}
              >
                Location
              </Button>
              <Button
                py="3"
                px="14"
                bg={activeStep === 3 ? "#FFFFFF" : ""}
                color="#000000"
                border={activeStep === 3 ? "1px" : ""}
                onClick={() => setActiveState(3)}
              >
                Highlights
              </Button>
              <Button
                py="3"
                px="14"
                bg={activeStep === 4 ? "#FFFFFF" : ""}
                color="#000000"
                border={activeStep === 4 ? "1px" : ""}
                onClick={() => setActiveState(4)}
              >
                Impressions
              </Button>
            </SimpleGrid>
          </Stack>
          {activeStep === 0 ? (
            <Stack boxShadow="2xl" p="5" borderRadius="lg" spacing="5" pt="10">
              <SimpleGrid columns={[1, 1, 2]} spacing={3}>
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    New screen name
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    Enter your screen name here
                  </Text>
                </VStack>
                <InputGroup size="lg" width="100%">
                  <Input
                    placeholder="Screen Name"
                    size="lg"
                    borderRadius="md"
                    fontSize="lg"
                    border="1px"
                    color="#555555"
                    py="2"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  />
                </InputGroup>
              </SimpleGrid>
              <SimpleGrid columns={[1, 1, 2]} spacing={3}>
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    Screen type
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    Enter your screen name here
                  </Text>
                </VStack>
                <SimpleGrid columns={[2, 2, 4]} spacing={3}>
                  <Button
                    variant="outline"
                    color={screenCategory === "InDoors" ? "#4C4C4C" : "#515151"}
                    bgColor={
                      screenCategory === "InDoors" ? "#D6FFFF" : "#FAFAFA"
                    }
                    fontSize="sm"
                    py={{ base: "2", lg: "3" }}
                    _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#4C4C4C" }}
                    onClick={() => setScreenCategory("InDoors")}
                  >
                    Indoors
                  </Button>
                  <Stack>
                    <Button
                      variant="outline"
                      color={
                        screenCategory === "OutDoors" ? "#4C4C4C" : "#515151"
                      }
                      bgColor={
                        screenCategory === "OutDoors" ? "#D6FFFF" : "#FAFAFA"
                      }
                      fontSize="sm"
                      py={{ base: "2", lg: "3" }}
                      _hover={{
                        bg: "rgba(14, 188, 245, 0.3)",
                        color: "#4C4C4C",
                      }}
                      onClick={() => setScreenCategory("OutDoors")}
                    >
                      Outdoors
                    </Button>
                  </Stack>
                  <Stack>
                    <Button
                      variant="outline"
                      color={
                        screenCategory === "RailWay Station"
                          ? "#4C4C4C"
                          : "#515151"
                      }
                      bgColor={
                        screenCategory === "RailWay Station"
                          ? "#D6FFFF"
                          : "#FAFAFA"
                      }
                      fontSize="sm"
                      py={{ base: "2", lg: "3" }}
                      _hover={{
                        bg: "rgba(14, 188, 245, 0.3)",
                        color: "#4C4C4C",
                      }}
                      onClick={() => setScreenCategory("RailWay Station")}
                    >
                      RailWays
                    </Button>
                  </Stack>
                  <Stack>
                    <Button
                      variant="outline"
                      color={
                        screenCategory === "Appartment" ? "#4C4C4C" : "#515151"
                      }
                      bgColor={
                        screenCategory === "Appartment" ? "#D6FFFF" : "#FAFAFA"
                      }
                      fontSize="sm"
                      py={{ base: "2", lg: "3" }}
                      _hover={{
                        bg: "rgba(14, 188, 245, 0.3)",
                        color: "#4C4C4C",
                      }}
                      onClick={() => setScreenCategory("Appartment")}
                    >
                      Appartments
                    </Button>
                  </Stack>
                </SimpleGrid>
              </SimpleGrid>

              <SimpleGrid columns={[1, 1, 2]} spacing={3}>
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    Start time
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    Enter the screen start time
                  </Text>
                </VStack>
                <Flex>
                  <FormControl
                    id="startDateHere"
                    width={{ base: "100%", lg: "30%" }}
                  >
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <TimePicker
                        inputVariant="outlined"
                        format="hh:mm"
                        value={startTime}
                        onChange={handleStartTime}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="start">
                              <MiuiIconButton>
                                <BsClock />
                              </MiuiIconButton>
                            </InputAdornment>
                          ),
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </FormControl>
                </Flex>
              </SimpleGrid>
              <SimpleGrid columns={[1, 1, 2]} spacing={3}>
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    End time
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    Enter the screen end time
                  </Text>
                </VStack>
                <Flex>
                  <FormControl
                    id="startDateHere"
                    width={{ base: "100%", lg: "30%" }}
                  >
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <TimePicker
                        inputVariant="outlined"
                        format="hh:mm"
                        value={endTime}
                        onChange={handleEndTime}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="start">
                              <MiuiIconButton>
                                <BsClock />
                              </MiuiIconButton>
                            </InputAdornment>
                          ),
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </FormControl>
                </Flex>
              </SimpleGrid>
              <HStack justifyContent="flex-end" pr="30" pb="30" spacing={5}>
                <Button
                  variant="outline"
                  color="#515151"
                  bgColor="#FAFAFA"
                  fontSize="sm"
                  borderColor="#0EBCF5"
                  p="4"
                  isLoading={loadingUpdate || loading}
                  loadingText="Screen data saving"
                  _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
                  onClick={submitScreenHandler}
                >
                  Finish
                </Button>
                <Button
                  variant="outline"
                  color="#515151"
                  bgColor="#FAFAFA"
                  fontSize="sm"
                  p="4"
                  borderColor="#0EBCF5"
                  _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
                  onClick={() => setActiveState(1)}
                >
                  Save & next
                </Button>
              </HStack>
            </Stack>
          ) : activeStep === 1 ? (
            <Stack boxShadow="2xl" p="5" borderRadius="lg" spacing="5" pt="20">
              <SimpleGrid columns={[1, 1, 2]} spacing={3}>
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    Slot time (in seconds)
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    Enter the slot time that the user may play per ad
                  </Text>
                </VStack>
                <InputGroup size="lg" width={{ base: "50%", lg: "50%" }}>
                  <Input
                    placeholder="20 Second"
                    size="lg"
                    fontSize="md"
                    borderRadius="md"
                    border="1px"
                    color="#555555"
                    py="2"
                    type="number"
                    value={slotsTimePeriod}
                    onChange={(e) => setSlotsTimePeriod(e.target.value)}
                  />
                </InputGroup>
              </SimpleGrid>
              <SimpleGrid columns={[1, 1, 2]} spacing={3}>
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    Screen dimentions (height/width)
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    Enter your screen dimensions
                  </Text>
                </VStack>
                <InputGroup size="lg" width="50%">
                  <HStack spacing="3">
                    <Input
                      placeholder="Width"
                      size="lg"
                      fontSize="md"
                      borderRadius="md"
                      border="1px"
                      color="#555555"
                      py="2"
                      type="number"
                      value={screenWidth}
                      onChange={(e) => setScreenWidth(e.target.value)}
                    />
                    <Input
                      placeholder="Hight"
                      size="lg"
                      fontSize="md"
                      borderRadius="md"
                      border="1px"
                      color="#555555"
                      py="2"
                      type="number"
                      value={screenLength}
                      onChange={(e) => setScreenLength(e.target.value)}
                    />
                    <VStack>
                      <BiUpArrow
                        color="#555555"
                        size="16px"
                        onClick={hancleSelectSizeTypeUp}
                      />
                      <Text color="#555555" fontSize="sm" fontWeight="bold">
                        {measurementUnit}
                      </Text>
                      <BiDownArrow
                        color="#555555"
                        size="16px"
                        onClick={hancleSelectSizeTypeDown}
                      />
                    </VStack>
                  </HStack>
                </InputGroup>
              </SimpleGrid>
              <SimpleGrid columns={[1, 1, 2]} spacing={3}>
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    Enter price for slot (in credits)
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    Enter price for each slot
                  </Text>
                </VStack>
                <InputGroup size="lg" width="50%">
                  <Input
                    placeholder="4000"
                    size="lg"
                    fontSize="md"
                    borderRadius="md"
                    border="1px"
                    color="#555555"
                    py="2"
                    type="number"
                    value={rentPerSlot}
                    onChange={(e) => setRentPerSlot(e.target.value)}
                  />
                </InputGroup>
              </SimpleGrid>
              <HStack justifyContent="flex-end" pr="30" pb="30" spacing={5}>
                <Button
                  variant="outline"
                  color="#515151"
                  bgColor="#FAFAFA"
                  fontSize="sm"
                  borderColor="#0EBCF5"
                  p="4"
                  isLoading={loadingUpdate || loading}
                  loadingText="Screen data saving"
                  _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
                  onClick={submitScreenHandler}
                >
                  Finish
                </Button>
                <Button
                  variant="outline"
                  color="#515151"
                  bgColor="#FAFAFA"
                  fontSize="sm"
                  p="4"
                  borderColor="#0EBCF5"
                  _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
                  onClick={() => setActiveState(2)}
                >
                  Save & next
                </Button>
              </HStack>
            </Stack>
          ) : activeStep === 2 ? (
            <Stack boxShadow="2xl" borderRadius="lg" pt="3">
              <SimpleGrid columns={[1, 1, 2]} spacing={3}>
                <Box
                  width="100%"
                  color="black.500"
                  height={{ base: "300px", lg: "700px" }}
                >
                  <MyMap
                    data={jsonData}
                    setLocation={handleAddPinClick}
                    geometry={geometry}
                    zoom="4"
                    selectCurrentLocation={selectCurrentLocation}
                  />
                </Box>
                <VStack
                  fontSize="sm"
                  spacing="2"
                  width="100%"
                  align="left"
                  pt="10"
                  px="5"
                >
                  <Text
                    color="#393939"
                    fontWeight="semibold"
                    align="left"
                    pt="10"
                  >
                    New screen location
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    Enter your screen location here
                  </Text>
                  <InputGroup size="lg" width="100%" pt="2">
                    <VStack>
                      <Input
                        placeholder="DistrictCity"
                        size="lg"
                        borderRadius="md"
                        fontSize="lg"
                        border="1px"
                        color="#555555"
                        py="2"
                        value={screenAddress}
                        onChange={(e) => setScreenAddress(e.target.value)}
                      />
                      <Input
                        placeholder="DistrictCity"
                        size="lg"
                        borderRadius="md"
                        fontSize="lg"
                        border="1px"
                        color="#555555"
                        py="2"
                        value={districtCity}
                        onChange={(e) => setDistrictCity(e.target.value)}
                      />
                      <Input
                        placeholder="State"
                        size="lg"
                        borderRadius="md"
                        fontSize="lg"
                        border="1px"
                        color="#555555"
                        py="2"
                        value={stateUT}
                        onChange={(e) => setStateUT(e.target.value)}
                      />

                      <Input
                        placeholder="Country"
                        size="lg"
                        borderRadius="md"
                        fontSize="lg"
                        border="1px"
                        color="#555555"
                        py="2"
                        value={country}
                        onChange={(e) => setCountry(e.target.value)}
                      />
                    </VStack>
                  </InputGroup>
                  <FormControl display="flex" alignItems="center">
                    <FormLabel htmlFor="email-alerts" mb="0" color="#4D4D4D">
                      Set screen location as out current location?
                    </FormLabel>
                    <Switch
                      onChange={(e: any) =>
                        setSelectCurrentLocation(e.target.checked)
                      }
                    />
                  </FormControl>
                  <Text
                    color="#393939"
                    fontWeight="semibold"
                    align="left"
                    pt="10"
                  >
                    Latitudes & Longitudes
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    The latitudes and longitudes of the map is shown
                  </Text>
                  <InputGroup size="lg" width="100%" pt="2">
                    <Input
                      disabled
                      placeholder="Screen name"
                      size="lg"
                      borderRadius="md"
                      fontSize="lg"
                      border="1px"
                      color="#555555"
                      py="2"
                      value={`${lat} , ${lng}`}
                      onChange={handleMapLocation}
                    />
                  </InputGroup>
                  <HStack
                    justifyContent="flex-end"
                    pr="30"
                    pb="30"
                    pt="30"
                    spacing={5}
                  >
                    <Button
                      variant="outline"
                      color="#515151"
                      bgColor="#FAFAFA"
                      fontSize="sm"
                      borderColor="#0EBCF5"
                      p="4"
                      isLoading={loadingUpdate || loading}
                      loadingText="Screen data saving"
                      _hover={{
                        bg: "rgba(14, 188, 245, 0.3)",
                        color: "#674780",
                      }}
                      onClick={submitScreenHandler}
                    >
                      Finish
                    </Button>
                    <Button
                      variant="outline"
                      color="#515151"
                      bgColor="#FAFAFA"
                      fontSize="sm"
                      borderColor="#0EBCF5"
                      p="4"
                      _hover={{
                        bg: "rgba(14, 188, 245, 0.3)",
                        color: "#674780",
                      }}
                      onClick={() => setActiveState(3)}
                    >
                      Save & next
                    </Button>
                  </HStack>
                </VStack>
              </SimpleGrid>
            </Stack>
          ) : activeStep === 3 ? (
            <Stack boxShadow="2xl" borderRadius="lg" pt="3" p="5">
              <SimpleGrid columns={[1, 1, 2]} spacing={3} pt="5">
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    Screen photos
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    Upload the screen details
                  </Text>
                </VStack>
                <HStack>
                  <Box>
                    <Image
                      src={screenImage || image}
                      width="76px"
                      height="86px"
                    />
                  </Box>
                  <Button
                    variant="outline"
                    width="76px"
                    height="86px"
                    isLoading={isUploading}
                    loadingText="Uploading..."
                    onClick={() => hiddenInput.click()}
                    borderColor="#8B8B8B"
                    color="#8B8B8B"
                  >
                    <AiOutlinePlusCircle size="30px" />
                  </Button>
                  <Input
                    hidden
                    type="file"
                    ref={(el) => (hiddenInput = el)}
                    accept="image/png, image/jpeg"
                    onChange={(e: any) => handlePhotoSelect(e.target.files[0])}
                  />
                </HStack>
              </SimpleGrid>
              <SimpleGrid columns={[1, 1, 2]} spacing={3} pt="5">
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    Screen code
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    Enter code to sync the screen
                  </Text>
                </VStack>
                <InputGroup size="lg" width={{ base: "100%", lg: "100%" }}>
                  <Input
                    placeholder={syncCode}
                    size="lg"
                    borderRadius="md"
                    fontSize="lg"
                    border="1px"
                    color="#555555"
                    onChange={(e) => setSyncCode(e.target.value)}
                    py="2"
                  />
                </InputGroup>
              </SimpleGrid>
              <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    Screen tags
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    Enter the hashscreenTags for
                  </Text>
                </VStack>
                <VStack>
                  <InputGroup
                    size="lg"
                    width={{ base: "100%", lg: "100%" }}
                    pl="0"
                  >
                    <Input
                      placeholder="Enter tags"
                      size="lg"
                      borderRadius="md"
                      fontSize="lg"
                      border="1px"
                      color="#555555"
                      value={tag}
                      onChange={(e) => setTag(e.target.value)}
                      onKeyPress={(e) => handleAddTags(e)}
                      py="2"
                    />
                  </InputGroup>
                  <SimpleGrid columns={[2, 3, 4]} spacing={3} width="100%">
                    {screenTags &&
                      screenTags.map((tag: any, index: number) => (
                        <InputGroup key={index + 1} size="lg">
                          <Button
                            variant="outline"
                            color="#515151"
                            bgColor="#FAFAFA"
                            fontSize="sm"
                            p="3"
                            borderColor="#555555"
                            _hover={{
                              bg: "rgba(14, 188, 245, 0.3)",
                              color: "#674780",
                            }}
                            onClick={() => {
                              deleteTags(tag);
                              return true;
                            }}
                          >
                            {`#${tag}`}
                            <Text pl="2">{` X`}</Text>
                          </Button>
                        </InputGroup>
                      ))}
                  </SimpleGrid>
                </VStack>
              </SimpleGrid>
              <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    Enter screen highlights
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    Enter your screen highlights here
                  </Text>
                </VStack>
                <VStack spacing="2" width="100%" align="left">
                  {screenHighlights &&
                    screenHighlights.map((highLight: any, index: any) => (
                      <Tooltip
                        label="Click for edit"
                        fontSize="md"
                        key={index + 1}
                        aria-label="A tooltip"
                        hasArrow
                        bg="red.600"
                      >
                        <Text
                          color="#000000"
                          width="100%"
                          fontSize="sm"
                          p="2"
                          align="left"
                          onClick={() => handleEditHighlites(index)}
                        >
                          {highLight}
                        </Text>
                      </Tooltip>
                    ))}
                  <InputGroup size="lg" width="100%" color="#555555">
                    <Input
                      placeholder="Enter highlights"
                      size="lg"
                      borderRadius="md"
                      fontSize="lg"
                      border="1px"
                      color="#555555"
                      value={highlight}
                      onChange={(e) => setHighLight(e.target.value)}
                      py="2"
                    />
                  </InputGroup>
                  <Button
                    color="#555555"
                    borderColor="#C8C8C8"
                    variant="outline"
                    bgColor="#F5F5F5"
                    width="70%"
                    fontSize="xl"
                    fontWeight="semibold"
                    p="2"
                    onClick={handleAddHighlites}
                  >
                    + Add more
                  </Button>
                </VStack>
              </SimpleGrid>

              <HStack
                justifyContent="flex-end"
                pr="30"
                pb="30"
                pt="30"
                spacing={5}
              >
                <Button
                  variant="outline"
                  color="#515151"
                  bgColor="#FAFAFA"
                  fontSize="sm"
                  borderColor="#0EBCF5"
                  p="4"
                  isLoading={loadingUpdate || loading}
                  loadingText="Screen data saving"
                  _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
                  onClick={submitScreenHandler}
                >
                  Finish
                </Button>
                <Button
                  variant="outline"
                  color="#515151"
                  bgColor="#FAFAFA"
                  fontSize="sm"
                  borderColor="#0EBCF5"
                  p="4"
                  _hover={{
                    bg: "rgba(14, 188, 245, 0.3)",
                    color: "#674780",
                  }}
                  onClick={() => setActiveState(4)}
                >
                  Save & next
                </Button>
              </HStack>
            </Stack>
          ) : activeStep === 4 ? (
            <Stack boxShadow="2xl" borderRadius="lg" pt="3" p="5">
              <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    Average daily footfall
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    The number of people around the screen per day
                  </Text>
                </VStack>
                <InputGroup size="lg" width="100%">
                  <Input
                    placeholder="only number"
                    borderRadius="md"
                    fontSize="lg"
                    border="1px"
                    color="#555555"
                    type="number"
                    value={averageDailyFootfall}
                    onChange={(e: any) =>
                      setAverageDailyFootfall(e.target.value)
                    }
                    py="2"
                  />
                </InputGroup>
              </SimpleGrid>
              <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    Regular audience percentage
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    Enter regular audience percentage
                  </Text>
                </VStack>
                <HStack>
                  <InputGroup size="lg" width="90%">
                    <Input
                      placeholder="only number"
                      size="lg"
                      borderRadius="md"
                      fontSize="lg"
                      border="1px"
                      color="#555555"
                      type="number"
                      value={regularAudiencePercentage}
                      onChange={(e: any) =>
                        setRegularAudiencePercentage(e.target.value)
                      }
                      py="2"
                    />
                  </InputGroup>
                  <Text color="#393939" fontSize="xl" fontWeight="bold">
                    %
                  </Text>
                </HStack>
              </SimpleGrid>
              <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    Sex ration
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    Enter each sex ration which cross to this screen
                  </Text>
                </VStack>
                <HStack>
                  <HStack
                    color="#393939"
                    fontWeight="semibold"
                    align="left"
                    alignItems="center"
                  >
                    <Text>Male</Text>
                    <InputGroup size="lg" width="100px">
                      <Input
                        type="number"
                        placeholder="male footfall per day"
                        size="lg"
                        borderRadius="md"
                        fontSize="lg"
                        border="1px"
                        color="#555555"
                        py="2"
                        value={sexRatio?.male}
                        onChange={(e: any) => {
                          setSexRatio({
                            ...sexRatio,
                            male: Number(e.target.value),
                          });
                        }}
                      />
                    </InputGroup>
                  </HStack>
                  <Text color="#393939" fontSize="xl" fontWeight="bold">
                    :
                  </Text>

                  <HStack
                    color="#393939"
                    fontWeight="semibold"
                    align="left"
                    alignItems="center"
                  >
                    <Text>Female</Text>
                    <InputGroup size="lg" width="100px">
                      <Input
                        placeholder="female footfall per day"
                        size="lg"
                        type="number"
                        borderRadius="md"
                        fontSize="lg"
                        border="1px"
                        color="#555555"
                        py="2"
                        value={sexRatio?.female}
                        onChange={(e: any) => {
                          setSexRatio({
                            ...sexRatio,
                            female: Number(e.target.value),
                          });
                        }}
                      />
                    </InputGroup>
                  </HStack>
                </HStack>
              </SimpleGrid>
              <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    Average purchase power
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    Thew average person comes to your resturant
                  </Text>
                </VStack>
                <SimpleGrid columns={[1, 2, 2]} spacing={3}>
                  <HStack
                    color="#393939"
                    fontWeight="semibold"
                    align="left"
                    alignItems="center"
                    width="100%"
                  >
                    <Text>Start value</Text>
                    <InputGroup size="lg">
                      <Input
                        placeholder=""
                        size="lg"
                        type="number"
                        borderRadius="md"
                        fontSize="lg"
                        border="1px"
                        color="#555555"
                        py="2"
                        value={averagePurchasePower?.start}
                        onChange={(e: any) => {
                          setAveragePurchasePower({
                            ...averagePurchasePower,
                            start: Number(e.target.value),
                          });
                        }}
                      />
                    </InputGroup>
                    <Text color="#000000" fontSize="sm" fontWeight="">
                      Rupees
                    </Text>
                  </HStack>
                  <HStack
                    color="#393939"
                    fontWeight="semibold"
                    align="left"
                    alignItems="center"
                    width="100%"
                    pl={{ base: "0", lg: "20" }}
                  >
                    <Text>End value</Text>
                    <InputGroup size="lg">
                      <Input
                        placeholder=""
                        size="lg"
                        borderRadius="md"
                        fontSize="lg"
                        border="1px"
                        color="#555555"
                        py="2"
                        value={averagePurchasePower?.end}
                        onChange={(e: any) => {
                          setAveragePurchasePower({
                            ...averagePurchasePower,
                            end: Number(e.target.value),
                          });
                        }}
                      />
                    </InputGroup>
                    <Text color="#000000" fontSize="sm" fontWeight="">
                      Rupees
                    </Text>
                  </HStack>
                </SimpleGrid>
              </SimpleGrid>
              <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    Average age group
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    Enter average age group
                  </Text>
                </VStack>
                <HStack>
                  <HStack
                    color="#393939"
                    fontWeight="semibold"
                    align="left"
                    alignItems="center"
                  >
                    <Text>Start age</Text>
                    <InputGroup size="lg" width="100px">
                      <Input
                        placeholder=""
                        size="lg"
                        type="number"
                        borderRadius="md"
                        fontSize="lg"
                        border="1px"
                        color="#555555"
                        py="2"
                        value={averageAgeGroup?.averageStartAge}
                        onChange={(e: any) => {
                          setAverageAgeGroup({
                            ...averageAgeGroup,
                            averageStartAge: Number(e.target.value),
                          });
                        }}
                      />
                    </InputGroup>
                  </HStack>
                  <HStack
                    color="#393939"
                    fontWeight="semibold"
                    align="left"
                    alignItems="center"
                  >
                    <Text>End age</Text>
                    <InputGroup size="lg" width="100px">
                      <Input
                        placeholder="Enter average End Age"
                        size="lg"
                        borderRadius="md"
                        fontSize="lg"
                        border="1px"
                        color="#555555"
                        py="2"
                        value={averageAgeGroup?.averageEndAge}
                        onChange={(e: any) => {
                          setAverageAgeGroup({
                            ...averageAgeGroup,
                            averageEndAge: Number(e.target.value),
                          });
                        }}
                      />
                    </InputGroup>
                  </HStack>
                </HStack>
              </SimpleGrid>
              <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    Employment status
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    Who are most persons visit to your place
                  </Text>
                </VStack>
                <SimpleGrid columns={[2, 2, 4]} spacing={2}>
                  <Button
                    variant="outline"
                    color={
                      employmentStatus?.includes("Salried Employees")
                        ? "#4C4C4C"
                        : "#515151"
                    }
                    bgColor={
                      employmentStatus?.includes("Salried Employees")
                        ? "#D6FFFF"
                        : "#FAFAFA"
                    }
                    fontSize="sm"
                    p="4"
                    _hover={{
                      bg: "rgba(14, 188, 245, 0.3)",
                      color: "#4C4C4C",
                    }}
                    onClick={() =>
                      addOrRemobeEmploymentStatus("Salried Employees")
                    }
                  >
                    Salried Employees
                  </Button>
                  <Button
                    variant="outline"
                    color={
                      employmentStatus?.includes("Unsalaried")
                        ? "#4C4C4C"
                        : "#515151"
                    }
                    bgColor={
                      employmentStatus?.includes("Unsalaried")
                        ? "#D6FFFF"
                        : "#FAFAFA"
                    }
                    fontSize="sm"
                    p="4"
                    onClick={() => addOrRemobeEmploymentStatus("Unsalaried")}
                  >
                    Business
                  </Button>
                  <Button
                    variant="outline"
                    color={
                      employmentStatus?.includes("Student")
                        ? "#4C4C4C"
                        : "#515151"
                    }
                    bgColor={
                      employmentStatus?.includes("Student")
                        ? "#D6FFFF"
                        : "#FAFAFA"
                    }
                    fontSize="sm"
                    p="4"
                    onClick={() => addOrRemobeEmploymentStatus("Student")}
                  >
                    Student
                  </Button>

                  <Button
                    variant="outline"
                    color={
                      employmentStatus?.includes("Labours")
                        ? "#4C4C4C"
                        : "#515151"
                    }
                    bgColor={
                      employmentStatus?.includes("Labours")
                        ? "#D6FFFF"
                        : "#FAFAFA"
                    }
                    fontSize="sm"
                    p="4"
                    onClick={() => addOrRemobeEmploymentStatus("Labours")}
                  >
                    Other
                  </Button>
                </SimpleGrid>
              </SimpleGrid>
              <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    Marital status
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    Enter marital status
                  </Text>
                </VStack>
                <HStack spacing={2}>
                  <Button
                    variant="outline"
                    color={
                      maritalStatus?.includes("Married") ? "#4C4C4C" : "#515151"
                    }
                    bgColor={
                      maritalStatus?.includes("Married") ? "#D6FFFF" : "#FAFAFA"
                    }
                    fontSize="sm"
                    p="4"
                    onClick={() => addOrRemobeMaritalStatue("Married")}
                  >
                    Married
                  </Button>
                  <Button
                    variant="outline"
                    color={
                      maritalStatus?.includes("Unmarried")
                        ? "#4C4C4C"
                        : "#515151"
                    }
                    bgColor={
                      maritalStatus?.includes("Unmarried")
                        ? "#D6FFFF"
                        : "#FAFAFA"
                    }
                    fontSize="sm"
                    p="4"
                    onClick={() => addOrRemobeMaritalStatue("Unmarried")}
                  >
                    Unmarried
                  </Button>
                </HStack>
              </SimpleGrid>
              <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
                <VStack fontSize="sm" spacing="2" width="100%" align="left">
                  <Text color="#393939" fontWeight="semibold" align="left">
                    Crowd mobility
                  </Text>
                  <Text color="#4D4D4D" align="left">
                    How the most crowd comes to your place
                  </Text>
                </VStack>
                <SimpleGrid columns={[2, 2, 4]} spacing={3}>
                  <Button
                    variant="outline"
                    color={
                      crowdMobilityType?.includes("Moving")
                        ? "#4C4C4C"
                        : "#515151"
                    }
                    bgColor={
                      crowdMobilityType?.includes("Moving")
                        ? "#D6FFFF"
                        : "#FAFAFA"
                    }
                    fontSize="sm"
                    p="4"
                    onClick={() => addOrRemobeCrowdMobilityType("Moving")}
                  >
                    Moving
                  </Button>
                  <Button
                    variant="outline"
                    color={
                      crowdMobilityType?.includes("Sitting")
                        ? "#4C4C4C"
                        : "#515151"
                    }
                    bgColor={
                      crowdMobilityType?.includes("Sitting")
                        ? "#D6FFFF"
                        : "#FAFAFA"
                    }
                    fontSize="sm"
                    p="4"
                    onClick={() => addOrRemobeCrowdMobilityType("Sitting")}
                  >
                    Sitting
                  </Button>
                  <Button
                    variant="outline"
                    color={
                      crowdMobilityType?.includes("Walking")
                        ? "#4C4C4C"
                        : "#515151"
                    }
                    bgColor={
                      crowdMobilityType?.includes("Walking")
                        ? "#D6FFFF"
                        : "#FAFAFA"
                    }
                    fontSize="sm"
                    p="4"
                    onClick={() => addOrRemobeCrowdMobilityType("Walking")}
                  >
                    Walking
                  </Button>
                </SimpleGrid>
              </SimpleGrid>
              <HStack justifyContent="flex-end" pr="30" pb="30" pt="30">
                <Button
                  variant="outline"
                  color="#515151"
                  bgColor="#FAFAFA"
                  fontSize="sm"
                  borderColor="#0EBCF5"
                  p="4"
                  isLoading={loadingUpdate || loading}
                  loadingText="Screen data saving"
                  _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
                  onClick={submitScreenHandler}
                >
                  Finish
                </Button>
              </HStack>
            </Stack>
          ) : null}
        </Box>
      )}
    </Box>
  );
};
