import React, { useState } from "react";
import { Link as RouterLink } from "react-router-dom";
import {
  Box,
  Flex,
  Center,
  Stack,
  Text,
  Input,
  InputGroup,
  Button,
  InputRightElement,
  IconButton,
  SimpleGrid,
} from "@chakra-ui/react";

import { IoSearchCircleSharp } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { filteredScreenList, listScreens } from "../../Actions/screenActions";
import HLoading from "../../components/atoms/HLoading";
import MessageBox from "../../components/atoms/MessageBox";
import { ContactUs, Screen } from "../../components/common";
import { AtvertiseBox } from "../../components/common/AtvertiseBox";
import { motion } from "framer-motion";
import { getCampaignList } from "../../Actions/campaignAction";
import { userScreensList } from "../../Actions/userActions";
import { USER_SCREENS_RESET } from "../../Constants/userConstants";

export function HomePage() {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const MotionFlex = motion(Flex);
  const [optionSelected, setOptionSelected] = useState<any>("pop");
  const [searchText, setSearchText] = useState<any>("");
  const userSignin = useSelector((state: any) => state.userSignin);
  const {
    loading: loadingUserInfo,
    error: errorUserInfo,
    userInfo,
  } = userSignin;

  const screenList = useSelector((state: any) => state.screenList);
  const { loading: loadingScreens, error: errorScreens, screens } = screenList;

  const campaignListAll = useSelector((state: any) => state.campaignListAll);

  const {
    loading: loadingVideos,
    error: errorVideos,
    allCampaign,
  } = campaignListAll;

  const userScreens = useSelector((state: any) => state.userScreens);
  const {
    loading: loadingUserScreens,
    error: errorUserScreens,
    screens: listOfUserScreen,
  } = userScreens;

  const getUserScreenList = () => {
    setOptionSelected("myScreen");
    if (!userInfo) {
      navigate("/signin");
    }
    dispatch(userScreensList(userInfo));
  };

  const getScreenListByFilter = async () => {
    dispatch(filteredScreenList(searchText));
    navigate(`/filterScreens/${searchText}`);
  };
  const handleSearch = (e: any) => {
    setSearchText(e.target.value);
  };

  React.useEffect(() => {
    // console.log("userInfo : ", userInfo);
    if (!loadingUserInfo && !userInfo && errorUserInfo) {
      navigate("/signin");
    }
    dispatch(getCampaignList());
    dispatch(listScreens({}));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, navigate, userInfo, loadingUserInfo, errorUserInfo]);

  return (
    <Box color="black.500" alignItems="center" pt={{ base: "3", lg: "5" }}>
      <Center px={{ base: "2", lg: "20" }}>
        {loadingScreens || loadingVideos ? (
          <HLoading loading={loadingScreens || loadingVideos} />
        ) : errorScreens || errorVideos ? (
          <MessageBox variant="danger">
            {errorScreens || errorVideos}
          </MessageBox>
        ) : (
          <Stack>
            <Center>
              <Stack
                zIndex="1"
                align="center"
                fontFamily="sans"
                width={{ base: "90%", lg: "65%" }}
              >
                <InputGroup
                  size="lg"
                  width="100%"
                  mt={{ base: "1px", lg: "10px" }}
                >
                  <Input
                    type="text"
                    py={{ base: "1", lg: "5" }}
                    bgColor="#FCFCFC"
                    borderRadius="80px"
                    fontSize="lg"
                    onChange={handleSearch}
                    onKeyPress={(e: any) => {
                      if (e.which === 13) {
                        getScreenListByFilter();
                      }
                    }}
                    placeholder="Search by place, by location, by screen names"
                  />
                  <InputRightElement
                    width="4.5rem"
                    pr={{ base: "0", lg: "1" }}
                    pt={{ base: "0", lg: "2.5" }}
                  >
                    <IconButton
                      bg="none"
                      icon={
                        <IoSearchCircleSharp
                          size="45px"
                          color="#D7380E"
                          onClick={getScreenListByFilter}
                        />
                      }
                      aria-label="Edit user details"
                    />
                  </InputRightElement>
                </InputGroup>
              </Stack>
            </Center>

            <Flex
              pt={{ base: "5", lg: "5" }}
              pb={{ base: "5", lg: "5" }}
              gap={5}
              align="center"
            >
              <Text
                color="#403F49"
                onClick={() => {
                  dispatch({ type: USER_SCREENS_RESET });
                  setOptionSelected("pop");
                }}
                fontSize={
                  optionSelected === "pop"
                    ? { base: "lg", lg: "3xl" }
                    : { base: "sm", lg: "md" }
                }
                fontWeight="bold"
                align="left"
              >
                Popular screens
              </Text>
              <Text
                color="#403F49"
                fontSize={
                  optionSelected === "myScreen"
                    ? { base: "lg", lg: "3xl" }
                    : { base: "sm", lg: "md" }
                }
                fontWeight="bold"
                align="left"
                onClick={getUserScreenList}
              >
                My screens
              </Text>
            </Flex>
            <SimpleGrid columns={[1, 2, 3]} spacing="4">
              {listOfUserScreen?.length > 0
                ? listOfUserScreen?.map((eachScreen: any) => (
                    <MotionFlex
                      key={eachScreen._id}
                      flexDir="column"
                      w="100%"
                      role="group"
                      rounded="md"
                      // shadow="card"
                      whileHover={{
                        translateY: -3,
                      }}
                      pos="relative"
                      zIndex="0"
                    >
                      <Screen eachScreen={eachScreen} key={eachScreen._id} />
                    </MotionFlex>
                  ))
                : screens?.map((eachScreen: any) => (
                    <MotionFlex
                      key={eachScreen._id}
                      flexDir="column"
                      w="100%"
                      role="group"
                      rounded="md"
                      // shadow="card"
                      whileHover={{
                        translateY: -3,
                      }}
                      pos="relative"
                      zIndex="0"
                    >
                      <Screen eachScreen={eachScreen} key={eachScreen._id} />
                    </MotionFlex>
                  ))}
            </SimpleGrid>
            <Flex align="center" justifyContent="center">
              <Button
                width="250px"
                p="3"
                variant="outline"
                borderColor="black"
                color="#D7380E"
                fontSize="xl"
                fontWeight="semibold"
                mt="20"
                as={RouterLink}
                to={`/all-screens`}
              >
                See All
              </Button>
            </Flex>
            <Text
              color="#403F49"
              pt={{ base: "5", lg: "10" }}
              pb={{ base: "5", lg: "10" }}
              fontSize={{ base: "lg", lg: "4xl" }}
              fontWeight="bold"
              align="left"
            >
              Ads playing
            </Text>
            <SimpleGrid columns={[1, 3]} spacing={{ base: "4", lg: "10" }}>
              {allCampaign &&
                allCampaign
                  ?.slice(allCampaign.length - 3, allCampaign.length)
                  ?.map((campaign: any) => (
                    <AtvertiseBox video={campaign} key={campaign._id} />
                  ))}
            </SimpleGrid>
            <Flex align="center" justifyContent="center">
              <Button
                width="250px"
                p="3"
                variant="outline"
                borderColor="black"
                color="#D7380E"
                fontSize="xl"
                fontWeight="semibold"
                mt="20"
                mb="20"
                as={RouterLink}
                to={`/allads`}
              >
                See All
              </Button>
            </Flex>
          </Stack>
        )}
      </Center>
      <ContactUs />
    </Box>
  );
}
