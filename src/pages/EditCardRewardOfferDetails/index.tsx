import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Box,
  Stack,
  FormControl,
  FormLabel,
  Input,
  Select,
  Text,
  SimpleGrid,
  Button,
  InputGroup,
  Center,
  Flex,
} from "@chakra-ui/react";

import { useNavigate } from "react-router-dom";
// import { AiOutlinePlusCircle } from "react-icons/ai";
import { addFileOnWeb3 } from "../../utils/web3";
import { SingleCardCoupon } from "../../components/common/SingleCardCoupon";
import {
  getCardRewardOfferDetailForBrand,
  updateCardRewardProgram,
} from "../../Actions/cardRewardOfferActions";
import {
  CARD_REWARD_CREATE_RESET,
  UPDATE_CARD_REWARD_RESET,
} from "../../Constants/cardRewardOfferConstants";
import HLoading from "../../components/atoms/HLoading";
import MessageBox from "../../components/atoms/MessageBox";
export const EditCardRewardOfferDetails = () => {
  const rewardId = window.location.pathname.split("/")[2];
  // console.log("rewardId : ", rewardId);
  const navigate = useNavigate();
  const [rewardProgramId, setRewardProgramId] = useState<any>(null);
  const [offerName, setOfferName] = useState<any>("");
  const [brandName, setBrand] = useState<any>("");
  const [quantity, setQuantity] = useState<any>(0);

  const [cardValidityTo, setCardValidityTo] = useState<any>(null);
  const [cardValidityFrom, setCardValidityFrom] = useState<any>(null);
  const [rewardCardType, setRewardCardType] = useState<any>(null);

  const [milestone, setMilestone] = useState<any>("");

  const [couponValidityTo, setCouponValidityTo] = useState<any>(null);
  const [couponValidityFrom, setCouponValidityFrom] = useState<any>(null);
  const [rewardCouponType, setRewardCouponType] = useState<any>(null);
  const [couponItem, setCouponItem] = useState<any>(null);
  const [couponRedeemFrequency, setCouponRedeemFrequency] = useState<any>(null);
  const [cardMilestonesRewards, setCardMilestonesReward] = useState<any>([]);

  const [discountCouponType, setDiscountCouponType] = useState<any>();
  const [discountCouponValue, setDiscountCouponValue] = useState<any>();

  const [freeItemQuantity, setFreeItemQuantity] = useState<any>(null);

  const [programModal, setProgramModal] = useState<boolean>(false);
  const [graphicText, setGraphicText] = useState<any>("");
  const [graphicTextArray, setGraphicTextArray] = useState<any>([]);

  const [fontStyle, setFontStyle] = useState<any>("");
  const [backgroundColors, setBackgroundColors] = useState<any>("");
  const [selectedImage, setSelectedImage] = useState<any>([]);
  const [selectedFiles, setSelectedFiles] = useState<any>([]);
  const [viewAddCardCoupon, setViewAddCardCoupon] = useState<any>(false);
  let hiddenInput: any = null;

  // async function handlePhotoSelect(file: any) {
  //   const fileThumbnail = URL.createObjectURL(file);
  //   setSelectedImage([...selectedImage, fileThumbnail]);
  //   setSelectedFiles([...selectedFiles, file]);
  // }

  // function handelGraphicText(event: any) {
  //   if (event.which === 13) {
  //     setGraphicTextArray([...graphicTextArray, graphicText]);
  //     setGraphicText("");
  //   }
  // }
  const handleDeteleCardCoupon = (singleMileStone: any) => {
    const data = cardMilestonesRewards?.filter(
      (mileStone: any) =>
        singleMileStone?.couponRewardInfo?.couponItem !=
        mileStone?.couponRewardInfo?.couponItem
    );
    setCardMilestonesReward(data);
  };
  const handleEditCardCoupon = (singleMileStone: any) => {
    const data = cardMilestonesRewards?.filter(
      (mileStone: any) =>
        singleMileStone?.couponRewardInfo?.couponItem !=
        mileStone?.couponRewardInfo?.couponItem
    );
    setCardMilestonesReward(data);
    setRewardCouponType(singleMileStone?.couponRewardInfo?.couponType);
    setCouponItem(singleMileStone?.couponRewardInfo?.couponItem);
    setCouponRedeemFrequency(
      singleMileStone?.couponRewardInfo?.couponRedeemFrequency
    );
    setDiscountCouponType(singleMileStone?.couponRewardInfo?.couponInfo?.type);
    setDiscountCouponValue(
      singleMileStone?.couponRewardInfo?.couponInfo?.value
    );
    setFreeItemQuantity(
      singleMileStone?.couponRewardInfo?.couponInfo?.freeItemQuantity
    );
    setCouponValidityFrom(singleMileStone?.couponRewardInfo?.validity?.from);
    setCouponValidityTo(singleMileStone?.couponRewardInfo?.validity?.to);
    setMilestone(singleMileStone?.milestonePoint);
    setViewAddCardCoupon(true);
  };
  const handleAddNewCouponInCardMilestonesReward = () => {
    const data = {
      couponRewardInfo: {
        couponType: rewardCouponType, // discount / free
        couponItem: couponItem,
        redeemFrequency: couponRedeemFrequency,
        couponInfo: {
          // discount
          type: discountCouponType, // percent / rupee
          value: discountCouponValue,

          // free
          freeItemQuantity: freeItemQuantity,
        },
        validity: {
          to: couponValidityTo,
          from: couponValidityFrom,
        },
      },
      milestonePoint: milestone,
    };
    setCardMilestonesReward([...cardMilestonesRewards, data]);
    setRewardCouponType(null);
    setCouponItem(null);
    setCouponRedeemFrequency(null);
    setDiscountCouponType(null);
    setDiscountCouponValue(null);
    setFreeItemQuantity(null);
    setCouponValidityFrom(null);
    setCouponValidityTo(null);
    setMilestone(null);
    setViewAddCardCoupon(false);
  };
  const handelAddNewCoupon = () => {
    setViewAddCardCoupon(true);
  };

  const userSignin = useSelector((state: any) => state.userSignin);
  const { loading: loadingUser, error: errorUser, userInfo } = userSignin;

  const cardRewardOfferCreate = useSelector(
    (state: any) => state.cardRewardOfferCreate
  );
  const {
    loading: loadingRewardProgramCreate,
    error: errorRewardProgramCreate,
    rewardProgram,
  } = cardRewardOfferCreate;

  const cardRewardOfferGetDetails = useSelector(
    (state: any) => state.cardRewardOfferGetDetails
  );
  const {
    loading: loadingRewardProgramDetails,
    error: errorRewardProgramDetails,
    rewardProgramDetails,
  } = cardRewardOfferGetDetails;

  const cardRewardOfferUpdate = useSelector(
    (state: any) => state.cardRewardOfferUpdate
  );
  const {
    loading: loadingRewardProgramUpdate,
    error: errorRewardProgramUpdate,
    updatedRewardProgram,
  } = cardRewardOfferUpdate;

  const dispatch = useDispatch<any>();
  if (updatedRewardProgram) {
    alert("Reward updated successfully");
    dispatch({
      type: UPDATE_CARD_REWARD_RESET,
    });
    dispatch({
      type: CARD_REWARD_CREATE_RESET,
    });
    navigate("/CouponRewardsForBrand");
  }
  useEffect(() => {
    if (rewardProgramDetails) {
      // console.log("rewardProgramDetails useEffect : ", rewardProgramDetails);
      setRewardProgramId(rewardProgramDetails._id);
      setOfferName(rewardProgramDetails?.offerName);
      setBrand(rewardProgramDetails?.brandName);
      setQuantity(rewardProgramDetails?.quantity);

      setCardValidityTo(rewardProgramDetails?.validity?.to);
      setCardValidityFrom(rewardProgramDetails?.validity?.from);
      setRewardCardType(rewardProgramDetails?.cardType);
      setCardMilestonesReward(rewardProgramDetails?.cardMilestonesRewards);
    }
  }, [rewardProgramDetails]);

  useEffect(() => {
    openProgramEditModal();
  }, []);

  const deleteMilestones = (milestone: any) => {};

  const handleChangeCardType = (e: any) => {
    setRewardCardType(e.target.value);
  };

  const handleChangeCouponType = (e: any) => {
    setRewardCouponType(e.target.value);
  };

  const handleChangeDiscountType = (e: any) => {
    setDiscountCouponType(e.target.value);
    // if (discountCouponType !== "" && !couponName) {
    //   setCouponName(name);
    // }
  };

  const openProgramEditModal = () => {
    setProgramModal(!programModal);
    dispatch(getCardRewardOfferDetailForBrand(rewardId));
  };

  const updateRewardProgramHandler = async () => {
    //first get cid of seelcted media
    const cids = [];
    try {
      for (let file of selectedFiles) {
        const cid = await addFileOnWeb3(file);
        cids.push(cid);
      }
      // console.log("cidssssss---", cids);
    } catch (error) {
      console.log("error : ", error);
    }
    dispatch(
      updateCardRewardProgram(rewardProgramId, {
        offerName,
        brandName,
        quantity,
        cardValidityTo,
        cardValidityFrom,
        rewardCardType,
        cardMilestonesRewards,
      })
    );
  };
  return (
    <Box pt={{ base: "3", lg: "5" }}>
      {loadingRewardProgramDetails || loadingRewardProgramUpdate ? (
        <HLoading
          loading={loadingRewardProgramDetails || loadingRewardProgramUpdate}
        />
      ) : errorRewardProgramDetails || errorRewardProgramUpdate ? (
        <MessageBox variant="danger">
          {errorRewardProgramDetails || errorRewardProgramUpdate}
        </MessageBox>
      ) : (
        <Box px={{ base: "3", lg: "20" }}>
          <Center>
            <Stack width="100%" p="2">
              <Text color="red" fontSize="xl" fontWeight="bold">
                Edit Card Reward Details
              </Text>
              {programModal && (
                <Stack>
                  <FormControl id="reward_program_name">
                    <FormLabel fontSize="sm" color="#333333">
                      Reward Program Name
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setOfferName(e?.target?.value)}
                        placeholder="Enter name"
                        value={offerName}
                        required
                        type="text"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                  </FormControl>
                  <FormControl id="rewarding_brandName">
                    <FormLabel fontSize="sm" color="#333333">
                      Rewarding Brand
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setBrand(e?.target?.value)}
                        placeholder="Enter Brand name"
                        value={brandName}
                        required
                        type="text"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                  </FormControl>
                  <FormControl id="reward_quantity">
                    <FormLabel fontSize="sm" color="#333333">
                      Reward Quantity
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setQuantity(e?.target?.value)}
                        placeholder="Enter Reward Quantity"
                        value={quantity}
                        required
                        type="number"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                  </FormControl>

                  {/* Loyalty Reward */}

                  <Box>
                    <FormLabel fontSize="sm" color="#333333">
                      Card valid till
                    </FormLabel>
                    <Stack direction="row" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setCardValidityTo(e?.target?.value)}
                        // placeholder="Your Loyalty Card Name"
                        value={cardValidityTo}
                        required
                        type="text"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setCardValidityFrom(e?.target?.value)}
                        // placeholder="Your Loyalty Card Name"
                        value={cardValidityFrom}
                        required
                        type="text"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                    <FormControl id="name">
                      <FormLabel fontSize="sm" color="#333333">
                        Card Type
                      </FormLabel>
                      <Stack direction="column" align="left">
                        <Select
                          height="36px"
                          variant="outline"
                          borderColor="#0EBCF5"
                          bgColor="#FFFFFF"
                          color="#403F49"
                          fontSize="sm"
                          value={rewardCardType}
                          onChange={(e) => {
                            handleChangeCardType(e);
                          }}
                        >
                          <option value="">Choose One...</option>
                          <option value="loyalty">Loyalty Card</option>
                        </Select>
                      </Stack>
                    </FormControl>
                    <SimpleGrid columns={[1, null, 5]} spacing="5" pt="5">
                      {cardMilestonesRewards?.map((eachCoupon: any) => (
                        <Stack>
                          <SingleCardCoupon
                            reward={rewardProgramDetails}
                            singleCoupon={eachCoupon}
                          />
                          <Flex justifyContent={"space-around"}>
                            <Button
                              py="2"
                              onClick={() => handleEditCardCoupon(eachCoupon)}
                            >
                              Edit
                            </Button>
                            <Button
                              py="2"
                              onClick={() => handleDeteleCardCoupon(eachCoupon)}
                            >
                              Delete
                            </Button>
                          </Flex>
                        </Stack>
                      ))}
                    </SimpleGrid>
                    {viewAddCardCoupon === false && (
                      <Stack pt="10">
                        <Button py="3" onClick={handelAddNewCoupon}>
                          Add new coupon with mileStone
                        </Button>
                      </Stack>
                    )}
                    {viewAddCardCoupon && (
                      <Box
                        border="1px"
                        borderRadius="lg"
                        borderColor="#000000"
                        mt="5"
                        p="10"
                      >
                        <Stack direction="column">
                          <FormLabel fontSize="sm" color="#333333">
                            Add Milestones
                          </FormLabel>
                          <InputGroup
                            size="lg"
                            width={{ base: "100%", lg: "100%" }}
                            pl="0"
                          >
                            <Input
                              placeholder="Enter Milestone"
                              size="lg"
                              borderRadius="md"
                              fontSize="lg"
                              border="1px"
                              color="#555555"
                              value={milestone}
                              onChange={(e) => setMilestone(e.target.value)}
                              py="2"
                            />
                          </InputGroup>
                          <Box>
                            <FormLabel fontSize="sm" color="#333333">
                              Coupon valid till
                            </FormLabel>
                            <Stack direction="row" align="left">
                              <Input
                                color="#333333"
                                id="name"
                                onChange={(e) =>
                                  setCouponValidityTo(e?.target?.value)
                                }
                                // placeholder="Your Loyalty Card Name"
                                value={couponValidityTo}
                                required
                                type="text"
                                py="2"
                                rounded="md"
                                size="md"
                                borderColor="#888888"
                              />
                              <Input
                                color="#333333"
                                id="name"
                                onChange={(e) =>
                                  setCouponValidityFrom(e?.target?.value)
                                }
                                // placeholder="Your Loyalty Card Name"
                                value={couponValidityFrom}
                                required
                                type="text"
                                py="2"
                                rounded="md"
                                size="md"
                                borderColor="#888888"
                              />
                            </Stack>
                            <FormLabel fontSize="sm" color="#333333">
                              Coupon Item
                            </FormLabel>
                            <Stack direction="column" align="left">
                              <Input
                                color="#333333"
                                id="name"
                                onChange={(e) =>
                                  setCouponItem(e?.target?.value)
                                }
                                // placeholder="Your Loyalty Card Name"
                                value={couponItem}
                                required
                                type="text"
                                py="2"
                                rounded="md"
                                size="md"
                                borderColor="#888888"
                              />
                            </Stack>
                            <FormLabel fontSize="sm" color="#333333">
                              No. of times coupon can be redeemed
                            </FormLabel>
                            <Stack direction="column" align="left">
                              <Input
                                color="#333333"
                                id="name"
                                onChange={(e) =>
                                  setCouponRedeemFrequency(e?.target?.value)
                                }
                                // placeholder="Your Loyalty Card Name"
                                value={couponRedeemFrequency}
                                required
                                type="number"
                                py="2"
                                rounded="md"
                                size="md"
                                borderColor="#888888"
                              />
                            </Stack>
                            <FormControl id="name">
                              <FormLabel fontSize="sm" color="#333333">
                                Coupon Type
                              </FormLabel>
                              <Stack direction="column" align="left">
                                <Select
                                  height="36px"
                                  variant="outline"
                                  borderColor="#0EBCF5"
                                  bgColor="#FFFFFF"
                                  color="#403F49"
                                  fontSize="sm"
                                  value={rewardCouponType}
                                  onChange={(e) => {
                                    handleChangeCouponType(e);
                                  }}
                                >
                                  <option value="">Choose One...</option>
                                  <option value="discount">
                                    Discount Coupon
                                  </option>
                                  <option value="free">Free Coupon</option>
                                </Select>
                              </Stack>
                            </FormControl>

                            {rewardCouponType === "discount" && (
                              <Box>
                                <FormControl id="name">
                                  <FormLabel fontSize="sm" color="#333333">
                                    Discount Type
                                  </FormLabel>
                                  <Stack direction="column" align="left">
                                    <Select
                                      height="36px"
                                      variant="outline"
                                      borderColor="#0EBCF5"
                                      bgColor="#FFFFFF"
                                      color="#403F49"
                                      fontSize="sm"
                                      value={discountCouponType}
                                      onChange={(e) => {
                                        handleChangeDiscountType(e);
                                      }}
                                    >
                                      <option value="">Choose One...</option>
                                      <option value="%">
                                        Discount in percentage
                                      </option>
                                      <option value="#">Discount in INR</option>
                                    </Select>
                                  </Stack>
                                </FormControl>
                                <FormLabel fontSize="sm" color="#333333">
                                  Discount Value
                                </FormLabel>
                                <Stack direction="column" align="left">
                                  <Input
                                    color="#333333"
                                    id="name"
                                    onChange={(e) =>
                                      setDiscountCouponValue(e?.target?.value)
                                    }
                                    // placeholder="Your Loyalty Card Name"
                                    value={discountCouponValue}
                                    required
                                    type="text"
                                    py="2"
                                    rounded="md"
                                    size="md"
                                    borderColor="#888888"
                                  />
                                </Stack>
                              </Box>
                            )}

                            {/* free coupon */}
                            {rewardCouponType === "free" && (
                              <Box>
                                <FormLabel fontSize="sm" color="#333333">
                                  Free Quantity
                                </FormLabel>
                                <Stack direction="column" align="left">
                                  <Input
                                    color="#333333"
                                    id="name"
                                    onChange={(e) =>
                                      setFreeItemQuantity(e?.target?.value)
                                    }
                                    // placeholder="Your Loyalty Card Name"
                                    value={freeItemQuantity}
                                    required
                                    type="text"
                                    py="2"
                                    rounded="md"
                                    size="md"
                                    borderColor="#888888"
                                  />
                                </Stack>
                              </Box>
                            )}
                          </Box>
                        </Stack>
                        <Center pt="5">
                          <Button
                            py="3"
                            onClick={handleAddNewCouponInCardMilestonesReward}
                          >
                            Save Coupon to cardMilestonesRewards
                          </Button>
                        </Center>
                      </Box>
                    )}
                  </Box>

                  <Stack pt="10">
                    <Button
                      py="4"
                      fontSize="md"
                      onClick={() => updateRewardProgramHandler()}
                    >
                      Save Reward
                    </Button>
                  </Stack>
                  {/* link to campaign */}
                </Stack>
              )}
            </Stack>
          </Center>
        </Box>
      )}
    </Box>
  );
};
