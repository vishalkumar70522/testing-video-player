import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import {
  Stack,
  IconButton,
  Text,
  FormControl,
  FormLabel,
  Box,
  Image,
  Button,
  Input,
  InputGroup,
} from "@chakra-ui/react";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { useAnimation } from "framer-motion";
import HLoading from "../../components/atoms/HLoading";
import { useSelector } from "react-redux";

export function UplaodCampaignThroughImage(props: any) {
  const controls = useAnimation();
  const [isUploading, setIsUploading] = useState(false);
  const [selectedImages, setSelectedImage] = useState<any>(null);
  const [file, setFile] = useState<any>(null);
  console.log("UplaodCampaignThroughImage ");
  let hiddenInput: any = null;
  const startAnimation = () => controls.start("hover");
  const stopAnimation = () => controls.stop();

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  async function handlePhotoSelect(file: any) {
    const fileThumbnail = URL.createObjectURL(file);
    setSelectedImage(fileThumbnail);
    setFile(file);
  }
  const handleNext = (e: any) => {
    const formData = new FormData();
    formData.append("photo", file);
    props.createVideo(formData);
    setSelectedImage(null);
    setFile(null);
    props.onHide();
  };
  useEffect(() => {}, []);
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      backdrop="static"
      keyboard={false}
    >
      <Modal.Body>
        {isUploading ? (
          <HLoading loading={isUploading} />
        ) : (
          <Box bgColor="#FFFFFF">
            <Stack align="end" justifyContent="flex-end">
              <IconButton
                bg="none"
                icon={
                  <AiOutlineCloseCircle
                    size="40px"
                    fontWeight="10"
                    color="black"
                    onClick={props.onHide}
                  />
                }
                aria-label="Close"
              />
            </Stack>
            <Stack p="5">
              <Stack align="center">
                <Text fontSize="xl" fontWeight="semibold" color="#000000">
                  Add campaign through Images
                </Text>
              </Stack>
              <FormControl pt="5">
                <FormLabel htmlFor="share" fontSize="lg">
                  Upload image for Creating Image
                </FormLabel>
                <InputGroup>
                  <Box
                    height="90px"
                    width="100%"
                    border="1px"
                    alignItems="center"
                    justifyContent="center"
                    onClick={() => hiddenInput.click()}
                  >
                    <Text
                      fontSize="lg"
                      fontWeight="semibold"
                      color="#000000"
                      p="8"
                      align="center"
                    >
                      {selectedImages
                        ? "Change image"
                        : "Drag & Drop Image Only"}
                    </Text>
                    <Input
                      hidden
                      type="file"
                      ref={(el) => (hiddenInput = el)}
                      // accept="image/png, image/jpeg"
                      onDragEnter={startAnimation}
                      onDragLeave={stopAnimation}
                      onChange={(e: any) =>
                        handlePhotoSelect(e.target.files[0])
                      }
                    />
                  </Box>
                </InputGroup>
              </FormControl>
              {selectedImages ? (
                <Stack borderRadius="lg">
                  <Image
                    src={selectedImages}
                    alt=""
                    width="100%"
                    height="100%"
                  ></Image>
                </Stack>
              ) : null}
              <Stack pt="10">
                <Button
                  variant="outline"
                  borderColor="#000000"
                  border="2px"
                  color="#D7380E"
                  fontSize="lg"
                  fontWeight="semiBold"
                  width="40%"
                  p="2"
                  onClick={handleNext}
                >
                  Next
                </Button>
              </Stack>
            </Stack>
          </Box>
        )}
      </Modal.Body>
    </Modal>
  );
}
