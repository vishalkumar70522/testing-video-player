import React, { useState } from "react";
import { Modal } from "react-bootstrap";
import {
  Stack,
  IconButton,
  Text,
  FormControl,
  FormLabel,
  InputGroup,
  Input,
  Box,
  Image,
  SimpleGrid,
  Button,
} from "@chakra-ui/react";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { useAnimation } from "framer-motion";
import MessageBox from "../../components/atoms/MessageBox";

export function UploadCampaign(props: any) {
  const { medias } = props;
  const controls = useAnimation();
  const [selectedMedia, setSelectedMedia] = useState<any>("");
  const [errorMsg, setErrorMsg] = useState("");

  let hiddenInput: any = null;
  const startAnimation = () => controls.start("hover");
  const stopAnimation = () => controls.stop();

  const handleSelectMedia = (media: any) => {
    props.setMediaId(media._id);
    //console.log("media selected : ", media.media);
    setSelectedMedia(media.media);
    props.setIsOldMedia(true);
  };

  const validateSelectedFile = (file: any) => {
    const MIN_FILE_SIZE = 1024; // 1MB
    const MAX_FILE_SIZE = 1024 * 50; // 5MB
    const fileExtension = file.type.split("/")[1];

    const fileSizeKiloBytes = file.size / 1024;

    if (fileSizeKiloBytes < MIN_FILE_SIZE) {
      setErrorMsg("File size is less than minimum limit");
      return false;
    }
    if (fileSizeKiloBytes > MAX_FILE_SIZE) {
      setErrorMsg(
        "File size is greater than maximum limit. File size must be less then 50 MB "
      );
      return false;
    }
    if (fileExtension !== "mp4") {
      setErrorMsg("File format must be .mp4");
      return false;
    }
    console.log("every thing is correct!");
    return true;
  };
  if (errorMsg) {
    setTimeout(() => setErrorMsg(""), 5000);
  }
  function handleFileSelect(file: any) {
    // try {
    //   const data = await generateVideoThumbnails(file, 1);
    //   console.log("response : ", data);
    // } catch (error) {
    //   console.log("error in thumbnail create : ", error);
    // }
    console.log("handleFileSelect : ", file);
    if (validateSelectedFile(file)) {
      const fileURL = URL.createObjectURL(file);
      props?.setFileUrl(file);
      props?.setIsOldMedia(false);
      setSelectedMedia(fileURL);
    }
  }
  const handleNext = (e: any) => {
    if (props?.isCameFromUserProfile) {
      props?.uploadMediaFromUserProfile();
    }
    if (selectedMedia) {
      props.onHide();
      props.videoUploadHandler(e);
    } else {
      setErrorMsg("Please choose a file");
      return;
    }
  };

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      backdrop="static"
      keyboard={false}
    >
      <Modal.Body>
        <Box bgColor="#FFFFFF">
          <Stack align="end" justifyContent="flex-end">
            <IconButton
              bg="none"
              icon={
                <AiOutlineCloseCircle
                  size="40px"
                  fontWeight="10"
                  color="black"
                  onClick={() => {
                    setSelectedMedia("");
                    props.setCampaignName("");
                    props.onHide();
                  }}
                />
              }
              aria-label="Close"
            />
          </Stack>
          <Stack p="10">
            <Stack align="center">
              {errorMsg ? (
                <MessageBox variant="danger">{errorMsg}</MessageBox>
              ) : null}
              <Text fontSize="xl" fontWeight="semibold" color="#000000">
                Add campaign
              </Text>
            </Stack>
            <FormControl pt="5">
              <FormLabel htmlFor="share" fontSize="lg">
                Upload content
              </FormLabel>
              <InputGroup>
                <Box
                  height="90px"
                  width="100%"
                  border="1px"
                  justifyContent="center"
                  onClick={() => hiddenInput.click()}
                >
                  <Text
                    align="center"
                    fontSize="lg"
                    fontWeight="semibold"
                    color="#000000"
                    p="8"
                  >
                    Drag & Drop{" "}
                  </Text>
                  <Input
                    hidden
                    type="file"
                    ref={(el) => (hiddenInput = el)}
                    // accept="image/png, image/jpeg"
                    onDragEnter={startAnimation}
                    onDragLeave={stopAnimation}
                    onChange={(e: any) => handleFileSelect(e.target.files[0])}
                  />
                </Box>
              </InputGroup>
            </FormControl>
            {selectedMedia ? (
              <Box
                as="video"
                src={selectedMedia}
                autoPlay
                loop
                muted
                display="inline-block"
                borderRadius="24px"
                height={{ base: "100%", lg: "50%" }}
              ></Box>
            ) : null}

            <Text
              fontSize="lg"
              fontWeight="light"
              color="#000000"
              align="left"
              pt="5"
            >
              Or select content
            </Text>
            <Stack pt="5">
              <SimpleGrid columns={[1, null, 3]} spacing={3}>
                {medias?.map((media: any) => (
                  <Stack
                    key={media._id}
                    borderRadius="lg"
                    onClick={() => handleSelectMedia(media)}
                  >
                    <Image
                      src={media.thumbnail}
                      alt=""
                      width="100%"
                      height="100%"
                    ></Image>
                  </Stack>
                ))}
              </SimpleGrid>
            </Stack>
            <Stack pt="10">
              <Button
                variant="outline"
                borderColor="#000000"
                border="2px"
                color="#D7380E"
                fontSize="lg"
                fontWeight="semiBold"
                width="40%"
                p="2"
                onClick={handleNext}
              >
                Next
              </Button>
            </Stack>
          </Stack>
        </Box>
      </Modal.Body>
    </Modal>
  );
}
