import React, { useEffect, useState } from "react";
import {
  Box,
  Flex,
  Stack,
  Text,
  Button,
  Image,
  SimpleGrid,
  Progress,
  Textarea,
  HStack,
} from "@chakra-ui/react";

import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import "react-responsive-carousel/lib/styles/carousel.min.css";

import HLoading from "../../components/atoms/HLoading";
import MessageBox from "../../components/atoms/MessageBox";
import Axios from "axios";
import { AiFillStar } from "react-icons/ai";
import { GoPrimitiveDot } from "react-icons/go";
import { MdPhotoSizeSelectSmall } from "react-icons/md";
import { AtvertiseBox } from "../../components/common/AtvertiseBox";
import { MyMap } from "../MyMap";
import { GiRoundStar, GiSandsOfTime } from "react-icons/gi";
import { ContactUs, Review } from "../../components/common";
import { createReview } from "../../Actions/screenActions";
import { userMediasList } from "../../Actions/userActions";
import { BiRupee } from "react-icons/bi";
import { CreateNewCampaign } from "./CreateNewCampaign";
import { UploadCampaign } from "./UploadCampaign";
import { uploadMedia } from "../../Actions/mediaActions";
import { getCampaignListByScreenId } from "../../Actions/campaignAction";
import { UplaodCampaignThroughImage } from "./UplaodCampaignThroughImage";
import { createVideoFromImage } from "../../Actions/videoFromImage";

export function ScreenDetailPage(props: any) {
  const dispatch = useDispatch<any>();
  const screenId = window.location.pathname.split("/")[2];
  const navigate = useNavigate();
  const [jsonData, setJsonData] = useState<any>({});
  const [userScreenRating, setUserScreenRating] = useState<any>(0);
  const [userReview, setUserReview] = useState<any>("");
  const [campaignModal, setCampaignModal] = useState(false);
  const [screen, setScreen] = useState<any>();
  const [loadingScreen, setLoadingScreen] = useState<any>(true);
  const [errorScreen, setErrorScreen] = useState<any>();
  const [geometry, setGeometry] = useState<any>();

  const [uploadCampaignModal, setUploadCampaignModal] = useState(false);
  const [
    uplaodCampaignThroughImageModalShow,
    setUplaodCampaignThroughImageModalShow,
  ] = useState<any>(false);

  const [numberOfCampaignShow, setNumberOfCampaignShow] = useState<Number>(3);
  const [isOldMedia, setIsOldMedia] = useState<any>(true);
  const [mediaId, setMediaId] = useState<any>("");
  const [campaignName, setCampaignName] = useState("");
  const [fileUrl, setFileUrl] = useState<any>();

  //console.log("Type of data buffer ----: ", typeof fileUrl, fileUrl);
  const countEachRating = {
    5: 0,
    4: 0,
    3: 0,
    2: 0,
    1: 0,
  };
  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  // const videoFromImages = useSelector((state: any) => state.videoFromImages);
  // const { loading: loadingVideo, error: errorVideo, video } = videoFromImages;

  const screenReviewCreate = useSelector(
    (state: any) => state.screenReviewCreate
  );
  const {
    loading: loadingCommnet,
    success: screenReviewCreateSuccess,
    error: errorComment,
    review,
  } = screenReviewCreate;
  //console.log("review :::::::::::::", JSON.stringify(review));
  const myMedia = useSelector((state: any) => state.userMedia);
  const {
    loading: loadingMyMedia,
    error: errorMyMedia,
    success,
    medias,
  } = myMedia;
  //console.log("media  : ", JSON.stringify(medias));

  const activeCampaignListByScreenID = useSelector(
    (state: any) => state.activeCampaignListByScreenID
  );
  const {
    loading: loadingCampaign,
    error: errorCampaign,
    campaigns: videosList,
  } = activeCampaignListByScreenID;
  //console.log("videosList :::::::::::::", JSON.stringify(videosList));

  if (!loadingScreen && screen) {
    screen.reviews.forEach((review: any) => {
      if (review.rating === 5) {
        countEachRating["5"] += 1;
      } else if (review.rating === 4) {
        countEachRating["4"] += 1;
      } else if (review.rating === 3) {
        countEachRating["3"] += 1;
      } else if (review.rating === 2) {
        countEachRating["2"] += 1;
      } else {
        countEachRating["1"] += 1;
      }
    });
  }
  const handlePost = () => {
    //console.log("handlepost called!");
    if (userScreenRating > 0 && userReview.length > 10) {
      dispatch(
        createReview(screenId, {
          rating: userScreenRating,
          comment: userReview,
        })
      );
      setUserScreenRating(0);
      setUserReview("");
    }
  };
  const createCampaignFromOldMediaAndThumbnail = () => {
    console.log("media id : ", mediaId);
    const media = medias?.filter((media: any) => media._id === mediaId);
    console.log("selected media :  ::::::: ", media);
    const url = media[0].media.split("/")[4];
    navigate(`/carts/${mediaId}/${screenId}/${campaignName}/${url}`);
  };
  const videoUploadHandler = async (e: any) => {
    e.preventDefault();
    console.log("uplaod video fumction called!");

    if (isOldMedia) {
      console.log("old media");
      createCampaignFromOldMediaAndThumbnail();
    } else {
      console.log("new media ");
      dispatch(
        uploadMedia({
          title: campaignName,
          thumbnail:
            "https://bafybeicduvlghzcrjtuxkro7foazucvuyej25rh3humeujbzt7bmio4hsa.ipfs.w3s.link/raily.png",
          fileUrl,
          media: "",
        })
      );
      navigate(`/carts/${null}/${screenId}/${campaignName}/${null}`);
      setCampaignName("");
      setFileUrl("");
    }
  };
  const getScreenDetail = async (screenId: any) => {
    try {
      setLoadingScreen(true);
      // console.log("getScreenDetail : ", screenId);
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/screens/${screenId}`
      );
      // console.log("screen  : ", JSON.stringify(data));
      setScreen(data);
      setGeometry({
        coordinates: [data.lat, data.lng],
      });
      setJsonData({
        features: [
          {
            type: "Feature",
            properties: {
              pin: data._id,
              screen: data._id,
            },
            geometry: {
              coordinates: [data.lat, data.lng],
              type: "Point",
            },
          },
        ],
      });
      setLoadingScreen(false);
    } catch (error: any) {
      setErrorScreen(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      );
    }
  };
  const handleCreateVideoFromImage = (filedata: any) => {
    dispatch(createVideoFromImage(filedata));
    navigate(`/carts/${null}/${screenId}/${campaignName}/${null}`);
    setCampaignName("");
    setFileUrl("");
  };

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin");
    }

    if (screenReviewCreateSuccess) {
      alert("review Added SuccessFull!");
    }
    getScreenDetail(screenId);
    dispatch(getCampaignListByScreenId(screenId));
    dispatch(userMediasList(userInfo));
  }, [dispatch, navigate, userInfo]);

  return (
    <>
      <CreateNewCampaign
        show={campaignModal}
        onHide={() => setCampaignModal(false)}
        openUploadCampaignModal={() => setUploadCampaignModal(true)}
        setCampaignName={(value: any) => setCampaignName(value)}
        campaignName={campaignName}
        openUploadCampaignThrougnImages={() =>
          setUplaodCampaignThroughImageModalShow(true)
        }
      />
      <UploadCampaign
        show={uploadCampaignModal}
        onHide={() => setUploadCampaignModal(false)}
        setFileUrl={(value: any) => setFileUrl(value)}
        setCampaignName={(value: any) => setCampaignName(value)}
        setIsOldMedia={(value: any) => setIsOldMedia(value)}
        medias={medias}
        setMediaId={(value: any) => setMediaId(value)}
        videoUploadHandler={videoUploadHandler}
      />
      <UplaodCampaignThroughImage
        show={uplaodCampaignThroughImageModalShow}
        onHide={() => setUplaodCampaignThroughImageModalShow(false)}
        createVideo={handleCreateVideoFromImage}
      />

      <Box py={{ base: "3", lg: "5" }}>
        {loadingScreen ||
        loadingCampaign ||
        loadingMyMedia ||
        loadingCommnet ? (
          <HLoading
            loading={
              loadingScreen ||
              loadingMyMedia ||
              loadingCommnet ||
              loadingCampaign
            }
          />
        ) : errorComment || errorCampaign || errorScreen ? (
          <MessageBox variant="danger">
            {errorComment || errorCampaign || errorScreen}
          </MessageBox>
        ) : (
          <Stack>
            <Stack px={{ base: "3", lg: "20" }}>
              <Stack>
                <Image
                  src={screen.image}
                  width="100%"
                  height="500px"
                  position="relative"
                ></Image>
              </Stack>
              <SimpleGrid pt="10" columns={[1, null, 2]}>
                <Stack>
                  <Text
                    fontSize="4xl"
                    fontWeight="bold"
                    color="#403F49"
                    align="left"
                  >
                    {screen.name}
                  </Text>
                  <Flex align="center" fontSize="lg">
                    <AiFillStar size="30px" color="#403F49" />
                    <Text
                      pl="1"
                      color="#403F49"
                      fontWeight="semibold"
                      align="left"
                      mr="5"
                    >
                      {screen.rating}
                    </Text>
                    <GoPrimitiveDot size="16px" color="#403F49" />
                    <Text color="#666666" fontWeight="semibold" align="left">
                      {`${screen.screenAddress} ${screen.districtCity} ${screen.country}`}
                    </Text>
                  </Flex>
                  <Stack pt="5">
                    <Text color="#4B4B4B" fontSize="sm" align="left">
                      Screen Impressions per day: 1245
                    </Text>
                    <Text color="#4B4B4B" fontSize="sm" align="left">
                      Monthly impressions: 1233
                    </Text>
                    <Text color="#4B4B4B" fontSize="sm" align="left">
                      Average footfall: 1532
                    </Text>
                  </Stack>
                  <Text
                    pt="10"
                    color="#403F49"
                    fontSize="sm"
                    align="left"
                    fontWeight="semibold"
                  >
                    Location highlights
                  </Text>
                  <Stack pt="5">
                    {screen.screenHighlights.map(
                      (highlight: any, index: any) => (
                        <Text
                          key={index}
                          fontSize="sm"
                          color="#4B4B4B"
                          align="left"
                        >
                          {highlight}
                        </Text>
                      )
                    )}
                  </Stack>
                </Stack>
                <Stack>
                  <Flex pt="5">
                    <Text fontSize="md" color="#403F49" pr="2">
                      Screen size
                    </Text>
                    <MdPhotoSizeSelectSmall size="20px" color="#3D3D3D" />
                    <Text fontSize="md" color="#403F49" pl="2">
                      {screen.size.length} x {screen.size.width} (16:9)
                    </Text>
                  </Flex>
                  <Flex>
                    <Text fontSize="md" color="#403F49" pr="">
                      Slot time
                    </Text>
                    <GiSandsOfTime size="20px" color="#3D3D3D" />
                    <Text fontSize="md" color="#403F49" pl="2">
                      {screen.slotsTimePeriod} sec
                    </Text>
                  </Flex>
                  <Flex align="center" pt="10">
                    <BiRupee size="26px" color="#403F49" />
                    <Text
                      color="#403F49"
                      fontSize="xl"
                      fontWeight="semibold"
                      align="left"
                    >
                      {`${screen.rentPerSlot} per slot`}
                    </Text>
                    <Text
                      as="s"
                      color="#787878"
                      fontSize="sm"
                      fontWeight="semibold"
                      align="left"
                      pl="2"
                    >
                      ₹
                      {screen.rentPerSlot - screen.rentOffInPercent
                        ? (screen.rentPerSlot * 100) / screen.rentOffInPercent
                        : 0}{" "}
                      per slot
                    </Text>
                    <Text
                      pl="1"
                      color="#F86E6E"
                      fontSize="lg"
                      fontWeight="semibold"
                      align="left"
                    >
                      ( {screen.rentOffInPercent}% OFF)
                    </Text>
                  </Flex>
                  <HStack pt="5">
                    {userInfo._id === screen.master._id ? (
                      <Button
                        width="196px"
                        height="54px"
                        color="#FFFFFF"
                        bgColor="#D7380E"
                        fontWeight="semibold"
                        fontSize="xl"
                        onClick={() => {
                          setCampaignModal(true);
                        }}
                      >
                        Add campaign
                      </Button>
                    ) : null}
                    <Button
                      width="196px"
                      height="54px"
                      color="#FFFFFF"
                      bgColor="#D7380E"
                      fontWeight="semibold"
                      fontSize="xl"
                      onClick={() => navigate(`/playlist/${screenId}`)}
                    >
                      Go to PlayList
                    </Button>
                  </HStack>
                </Stack>
              </SimpleGrid>
              <Text
                color="#403F49"
                fontSize="4xl"
                align="left"
                fontWeight="semibold"
                pt="10"
              >
                Brands playing on this screens
              </Text>
              {videosList.length === 0 ? (
                <Text
                  color="#D7380E"
                  fontSize="lg"
                  align="left"
                  fontWeight="semibold"
                  pt="5"
                >
                  No any campaign running on this screen now
                </Text>
              ) : (
                <SimpleGrid columns={[1, null, 3]} spacing="10" pt="5">
                  {videosList &&
                    videosList
                      .slice(0, numberOfCampaignShow)
                      .map((video: any) => (
                        <AtvertiseBox video={video} key={video._id} />
                      ))}
                </SimpleGrid>
              )}
              <Flex align="center" justifyContent="center">
                {videosList?.length > 3 && numberOfCampaignShow === 3 ? (
                  <Button
                    width="250px"
                    p="3"
                    variant="outline"
                    borderColor="black"
                    color="#D7380E"
                    fontSize="xl"
                    fontWeight="semibold"
                    mt="20"
                    onClick={() => setNumberOfCampaignShow(videosList?.length)}
                  >
                    See All
                  </Button>
                ) : null}
                {numberOfCampaignShow !== 3 ? (
                  <Button
                    width="250px"
                    p="3"
                    variant="outline"
                    borderColor="black"
                    color="#D7380E"
                    fontSize="xl"
                    fontWeight="semibold"
                    mt="20"
                    onClick={() => setNumberOfCampaignShow(3)}
                  >
                    See Less
                  </Button>
                ) : null}
              </Flex>
            </Stack>
            <Box width="100%" height="551px" color="black.500" pb="20" pt="20">
              <MyMap data={jsonData} geometry={geometry} />
            </Box>
            <Stack px={{ base: "3", lg: "20" }}>
              <Text
                fontSize="4xl"
                color="#403F49"
                fontWeight="bold"
                align="left"
              >
                Review
              </Text>
              <Box
                width={{ base: "100%", lg: "25%" }}
                bgColor="#F0F4FC"
                borderRadius="16px"
              >
                <Flex>
                  <Stack p="10" width={{ base: "70%", lg: "60%" }}>
                    <Flex align="center">
                      <Text fontSize="4xl" color="#403F49" fontWeight="bold">
                        {screen.rating}/
                      </Text>
                      <Text fontSize="lg" color="#403F49" fontWeight="bold">
                        5
                      </Text>
                    </Flex>
                    <Text
                      fontSize="sm"
                      color="#403F49"
                      fontWeight="semibold"
                      align="left"
                    >
                      {`Based on ${screen.numReviews} reviews`}
                    </Text>
                    <Flex>
                      <GiRoundStar
                        size="20px"
                        color={screen.rating >= 5 ? "#0EBCF5" : "#E2E2E2"}
                      />
                      <GiRoundStar
                        size="20px"
                        color={screen.rating >= 5 ? "#0EBCF5" : "#E2E2E2"}
                      />
                      <GiRoundStar
                        size="20px"
                        color={screen.rating >= 5 ? "#0EBCF5" : "#E2E2E2"}
                      />
                      <GiRoundStar
                        size="20px"
                        color={screen.rating >= 5 ? "#0EBCF5" : "#E2E2E2"}
                      />
                      <GiRoundStar
                        size="20px"
                        color={screen.rating >= 5 ? "#0EBCF5" : "#E2E2E2"}
                      />
                    </Flex>
                  </Stack>
                  <Stack pt="5" width={{ base: "20%", lg: "30%" }}>
                    <Flex align="center">
                      <Text fontSize="sm" color="#403F49" pr="3">
                        5
                      </Text>
                      <Progress
                        value={(100 * countEachRating["5"]) / screen.numReviews}
                        width="100%"
                        size="sm"
                        colorScheme="#0EBCF5"
                        bgColor="#90F8FF"
                      />
                    </Flex>
                    <Flex align="center">
                      <Text fontSize="sm" color="#403F49" pr="3">
                        4
                      </Text>
                      <Progress
                        value={(100 * countEachRating["4"]) / screen.numReviews}
                        width="100%"
                        size="sm"
                        colorScheme="#0EBCF5"
                        bgColor="#90F8FF"
                      />
                    </Flex>
                    <Flex align="center">
                      <Text fontSize="sm" color="#403F49" pr="3">
                        3
                      </Text>
                      <Progress
                        value={(100 * countEachRating["3"]) / screen.numReviews}
                        width="100%"
                        size="sm"
                        colorScheme="#0EBCF5"
                        bgColor="#90F8FF"
                      />
                    </Flex>
                    <Flex align="center">
                      <Text fontSize="sm" color="#403F49" pr="3">
                        2
                      </Text>
                      <Progress
                        value={(100 * countEachRating["2"]) / screen.numReviews}
                        width="100%"
                        size="sm"
                        colorScheme="#0EBCF5"
                        bgColor="#90F8FF"
                      />
                    </Flex>
                    <Flex align="center">
                      <Text fontSize="sm" color="#403F49" pr="3">
                        1
                      </Text>
                      <Progress
                        value={(100 * countEachRating["1"]) / screen.numReviews}
                        width="100%"
                        size="sm"
                        color="#0EBCF5"
                        bgColor="#90F8FF"
                      />
                    </Flex>
                  </Stack>
                </Flex>
              </Box>
              <Text
                fontSize="xl"
                color="#403F49"
                fontWeight="semibold"
                align="left"
                pt="10"
              >
                Rate your screen
              </Text>
              <Flex pt="5">
                <GiRoundStar
                  size="30px"
                  color={userScreenRating >= 1 ? "#0EBCF5" : "#E2E2E2"}
                  onClick={() => setUserScreenRating(1)}
                />
                <GiRoundStar
                  size="30px"
                  color={userScreenRating >= 2 ? "#0EBCF5" : "#E2E2E2"}
                  onClick={() => setUserScreenRating(2)}
                />
                <GiRoundStar
                  size="30px"
                  color={userScreenRating >= 3 ? "#0EBCF5" : "#E2E2E2"}
                  onClick={() => setUserScreenRating(3)}
                />
                <GiRoundStar
                  size="30px"
                  color={userScreenRating >= 4 ? "#0EBCF5" : "#E2E2E2"}
                  onClick={() => setUserScreenRating(4)}
                />
                <GiRoundStar
                  size="30px"
                  color={userScreenRating >= 5 ? "#0EBCF5" : "#E2E2E2"}
                  onClick={() => setUserScreenRating(5)}
                />
              </Flex>
              <Text
                fontSize="xl"
                color="#403F49"
                fontWeight="semibold"
                align="left"
                pt="10"
              >
                Write a review
              </Text>
              <Textarea
                placeholder="Enter your review here"
                width="30%"
                height="40%"
                color="#000000"
                value={userReview}
                onChange={(e: any) => setUserReview(e.target.value)}
              />
              <Button onClick={handlePost} width="30%" py="3">
                Post
              </Button>

              <Stack pt="10" pb="20">
                {screen?.reviews.length > 0
                  ? screen.reviews.map((review: any, index: any) => (
                      <Review review={review} key={index + 1} />
                    ))
                  : null}
              </Stack>
            </Stack>
            <ContactUs />
          </Stack>
        )}
      </Box>
    </>
  );
}
