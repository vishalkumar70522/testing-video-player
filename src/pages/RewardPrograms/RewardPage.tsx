import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Box,
  Stack,
  FormControl,
  FormLabel,
  Input,
  Select,
  Text,
  SimpleGrid,
  Button,
  InputGroup,
  Divider,
  Center,
  Flex,
} from "@chakra-ui/react";
import {
  createRewardProgram,
  getRewardProgramDetails,
  myCreatedRewardPrograms,
  updateRewardProgram,
} from "../../Actions/rewardAction";
import { UPDATE_REWARD_RESET } from "../../Constants/rewardConstants";
export const RewardPage = () => {
  const [rewardProgramId, setRewardProgramId] = useState<any>(null);
  const [name, setName] = useState<any>("");
  const [brand, setBrand] = useState<any>("");
  const [quantity, setQuantity] = useState<any>(0);
  const [rewardProgramType, setRewardProgramType] = useState<any>(null);

  const [cardValidityTo, setCardValidityTo] = useState<any>(null);
  const [cardValidityFrom, setCardValidityFrom] = useState<any>(null);
  const [rewardCardType, setRewardCardType] = useState<any>(null);

  const [milestone, setMilestone] = useState<any>("");
  const [cardMilestones, setCardMilestones] = useState<any>([]);

  const [couponValidityTo, setCouponValidityTo] = useState<any>(null);
  const [couponValidityFrom, setCouponValidityFrom] = useState<any>(null);
  const [rewardCouponType, setRewardCouponType] = useState<any>(null);
  const [couponItem, setCouponItem] = useState<any>(null);
  const [couponRedeemFrequency, setCouponRedeemFrequency] = useState<any>(null);

  const [discountCouponType, setDiscountCouponType] = useState<any>();
  const [discountCouponValue, setDiscountCouponValue] = useState<any>();

  const [freeItemQuantity, setFreeItemQuantity] = useState<any>(null);

  const [programModal, setProgramModal] = useState<boolean>(false);

  const [campaigns, setCampaigns] = useState<any>([]);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { loading: loadingUser, error: errorUser, userInfo } = userSignin;

  const rewardProgramCreate = useSelector(
    (state: any) => state.rewardProgramCreate
  );
  const {
    loading: loadingRewardProgramCreate,
    error: errorRewardProgramCreate,
    rewardProgram,
  } = rewardProgramCreate;

  const myRewardProgramsCreated = useSelector(
    (state: any) => state.myRewardProgramsCreated
  );
  const {
    loading: loadingMyRewardPrograms,
    error: errorMyRewardPrograms,
    myRewardPrograms,
  } = myRewardProgramsCreated;

  const rewardProgramDetailsGet = useSelector(
    (state: any) => state.rewardProgramDetailsGet
  );
  const {
    loading: loadingRewardProgramDetails,
    error: errorRewardProgramDetails,
    rewardProgramDetails,
  } = rewardProgramDetailsGet;

  const rewardProgramUpdate = useSelector(
    (state: any) => state.rewardProgramUpdate
  );
  const {
    loading: loadingRewardProgramUpdate,
    error: errorRewardProgramUpdate,
    updatedRewardProgram,
  } = rewardProgramUpdate;

  const dispatch = useDispatch<any>();
  useEffect(() => {
    if (updatedRewardProgram) {
      alert("Reward updated successfully");
      dispatch({
        type: UPDATE_REWARD_RESET,
      });
    }
    if (rewardProgramDetails) {
      setRewardProgramId(rewardProgramDetails._id);
      setName(rewardProgramDetails?.programName);
      setBrand(rewardProgramDetails?.brand);
      setRewardProgramType(rewardProgramDetails?.rewardProgramType?.type);
      setQuantity(rewardProgramDetails?.quantity);
    }

    if (rewardProgramDetails?.rewardProgramType?.type === "card") {
      setCardValidityTo(
        rewardProgramDetails?.rewardProgramType?.cardReward?.validity?.to
      );
      setCardValidityFrom(
        rewardProgramDetails?.rewardProgramType?.cardReward?.validity?.from
      );
      setRewardCardType(
        rewardProgramDetails?.rewardProgramType?.cardReward?.cardType
      );
    }

    if (rewardProgramDetails?.rewardProgramType?.type === "coupon") {
      setCardValidityTo(
        rewardProgramDetails?.rewardProgramType?.couponReward?.validity?.to
      );
      setCardValidityFrom(
        rewardProgramDetails?.rewardProgramType?.couponReward?.validity?.from
      );
      setRewardCouponType(
        rewardProgramDetails?.rewardProgramType?.couponReward?.couponType
      );
      setCouponItem(
        rewardProgramDetails?.rewardProgramType?.couponReward?.couponItem
      );
      setCouponRedeemFrequency(
        rewardProgramDetails?.rewardProgramType?.couponReward?.redeemFrequency
      );
      setDiscountCouponType(
        rewardProgramDetails?.rewardProgramType?.couponReward?.couponInfo?.type
      );
      setDiscountCouponValue(
        rewardProgramDetails?.rewardProgramType?.couponReward?.couponInfo?.value
      );
      setFreeItemQuantity(
        rewardProgramDetails?.rewardProgramType?.couponReward?.couponInfo
          ?.freeItemQuantity
      );
    }
    dispatch(myCreatedRewardPrograms());
  }, [
    dispatch,
    // rewardProgramType,
    // setRewardProgramType,
    updatedRewardProgram,
    rewardProgramDetails,
  ]);

  const handleChangeRewardType = (e: any) => {
    setRewardProgramType(e.target.value);
  };

  const handleAddMilestones = (event: any) => {
    if (event.which === 13) {
      setCardMilestones([...cardMilestones, milestone]);
      setMilestone("");
    }
  };
  const deleteMilestones = (milestone: any) => {
    const newMilestones = cardMilestones.filter(
      (cardMilestone: any) => cardMilestone !== milestone
    );
    setCardMilestones(newMilestones);
  };

  const handleChangeCardType = (e: any) => {
    setRewardCardType(e.target.value);
  };

  const handleChangeCouponType = (e: any) => {
    setRewardCouponType(e.target.value);
  };

  const handleChangeDiscountType = (e: any) => {
    setDiscountCouponType(e.target.value);
    // if (discountCouponType !== "" && !couponName) {
    //   setCouponName(name);
    // }
  };

  const createRewardHandler = () => {
    dispatch(createRewardProgram());
  };

  const openProgramEditModal = (rpId: any) => {
    setProgramModal(!programModal);
    dispatch(getRewardProgramDetails(rpId));
  };

  const updateRewardProgramHandler = () => {
    //
    dispatch(
      updateRewardProgram(rewardProgramId, {
        name,
        brand,
        quantity,
        rewardProgramType,
        cardValidityTo,
        cardValidityFrom,
        rewardCardType,
        milestone,
        cardMilestones,
        couponValidityTo,
        couponValidityFrom,
        rewardCouponType,
        couponItem,
        couponRedeemFrequency,
        discountCouponType,
        discountCouponValue,
        freeItemQuantity,
      })
    );
  };
  return (
    <Box pt={{ base: "3", lg: "5" }}>
      {loadingMyRewardPrograms ? (
        <></>
      ) : errorMyRewardPrograms ? (
        <></>
      ) : (
        <Box px={{ base: "3", lg: "20" }}>
          <Center>
            <Stack width="100%" p="2">
              <Flex>
                <Button p="2" onClick={() => createRewardHandler()}>
                  Create Reward
                </Button>
              </Flex>
              <Divider color="black" p="2" />
              <SimpleGrid column={[1, 2, 3]} gap="4">
                {myRewardPrograms?.map((rp: any, index: any) => (
                  <Box
                    //
                    border="1px solid black"
                    rounded="8px"
                    p="2"
                    key={index}
                  >
                    <Flex>
                      <Text color="black">Attached campaign</Text>
                    </Flex>
                    <Divider py="1" />
                    <Flex justify="space-between" align="center">
                      <Text px="1" color="black">
                        {rp.programName}
                      </Text>
                      <Text
                        px="1"
                        color="black"
                        onClick={() => openProgramEditModal(rp._id)}
                      >
                        Edit
                      </Text>
                    </Flex>
                    <Flex justify="space-between" align="center">
                      <Text px="1" color="black">
                        {rp.brand}
                      </Text>
                    </Flex>
                  </Box>
                ))}
              </SimpleGrid>
              <Divider p="2" color="black" />
              {programModal && (
                <Stack>
                  <FormControl id="reward_program_name">
                    <FormLabel fontSize="sm" color="#333333">
                      Reward Program Name
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setName(e?.target?.value)}
                        placeholder="Enter name"
                        value={name}
                        required
                        type="text"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                  </FormControl>
                  <FormControl id="rewarding_brand">
                    <FormLabel fontSize="sm" color="#333333">
                      Rewarding Brand
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setBrand(e?.target?.value)}
                        placeholder="Enter Brand name"
                        value={brand}
                        required
                        type="text"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                  </FormControl>
                  <FormControl id="reward_quantity">
                    <FormLabel fontSize="sm" color="#333333">
                      Reward Quantity
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setQuantity(e?.target?.value)}
                        placeholder="Enter Reward Quantity"
                        value={quantity}
                        required
                        type="number"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                  </FormControl>
                  <FormControl id="reward_type">
                    <FormLabel fontSize="sm" color="#333333">
                      Reward Type
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Select
                        height="36px"
                        variant="outline"
                        borderColor="#0EBCF5"
                        bgColor="#FFFFFF"
                        color="#403F49"
                        fontSize="sm"
                        value={rewardProgramType}
                        onChange={(e) => {
                          handleChangeRewardType(e);
                        }}
                      >
                        <option value="">Choose One...</option>
                        <option value="coupon">Coupon Reward Program</option>
                        <option value="card">Card Reward Program</option>
                      </Select>
                    </Stack>
                  </FormControl>
                  <Divider color="black" p="2" />

                  {/* Loyalty Reward */}
                  {rewardProgramType === "card" && (
                    <Box>
                      <FormLabel fontSize="sm" color="#333333">
                        Card valid till
                      </FormLabel>
                      <Stack direction="row" align="left">
                        <Input
                          color="#333333"
                          id="name"
                          onChange={(e) => setCardValidityTo(e?.target?.value)}
                          // placeholder="Your Loyalty Card Name"
                          value={cardValidityTo}
                          required
                          type="text"
                          py="2"
                          rounded="md"
                          size="md"
                          borderColor="#888888"
                        />
                        <Input
                          color="#333333"
                          id="name"
                          onChange={(e) =>
                            setCardValidityFrom(e?.target?.value)
                          }
                          // placeholder="Your Loyalty Card Name"
                          value={cardValidityFrom}
                          required
                          type="text"
                          py="2"
                          rounded="md"
                          size="md"
                          borderColor="#888888"
                        />
                      </Stack>
                      <FormControl id="name">
                        <FormLabel fontSize="sm" color="#333333">
                          Card Type
                        </FormLabel>
                        <Stack direction="column" align="left">
                          <Select
                            height="36px"
                            variant="outline"
                            borderColor="#0EBCF5"
                            bgColor="#FFFFFF"
                            color="#403F49"
                            fontSize="sm"
                            onChange={(e) => {
                              handleChangeCardType(e);
                            }}
                          >
                            <option value="">Choose One...</option>
                            <option value="loyalty">Loyalty Card</option>
                          </Select>
                        </Stack>
                      </FormControl>
                      {rewardCardType === "loyalty" && (
                        <Box>
                          <Stack direction="column">
                            <FormLabel fontSize="sm" color="#333333">
                              Add Milestones
                            </FormLabel>
                            <InputGroup
                              size="lg"
                              width={{ base: "100%", lg: "100%" }}
                              pl="0"
                            >
                              <Input
                                placeholder="Enter Milestone"
                                size="lg"
                                borderRadius="md"
                                fontSize="lg"
                                border="1px"
                                color="#555555"
                                value={milestone}
                                onChange={(e) => setMilestone(e.target.value)}
                                onKeyPress={(e) => handleAddMilestones(e)}
                                py="2"
                              />
                            </InputGroup>
                            <SimpleGrid
                              columns={[2, 3, 4]}
                              spacing={3}
                              width="100%"
                            >
                              {cardMilestones &&
                                cardMilestones.map(
                                  (cardMilestone: any, index: number) => (
                                    <InputGroup key={index + 1} size="lg">
                                      <Button
                                        variant="outline"
                                        color="#515151"
                                        bgColor="#FAFAFA"
                                        fontSize="sm"
                                        p="3"
                                        borderColor="#555555"
                                        _hover={{
                                          bg: "rgba(14, 188, 245, 0.3)",
                                          color: "#674780",
                                        }}
                                        onClick={() => {
                                          deleteMilestones(cardMilestone);
                                          return true;
                                        }}
                                      >
                                        {`#${cardMilestone}`}
                                        <Text color="black" pl="2">{` X`}</Text>
                                      </Button>
                                    </InputGroup>
                                  )
                                )}
                            </SimpleGrid>
                          </Stack>
                          {cardMilestones.length > 0 && (
                            <Stack>
                              {cardMilestones.map(
                                (cardMilestone: any, index: any) => (
                                  <Box key={index}>
                                    <Text color="black">{cardMilestone}</Text>
                                  </Box>
                                )
                              )}
                            </Stack>
                          )}
                        </Box>
                      )}
                    </Box>
                  )}

                  {/* Discount Reward */}
                  {rewardProgramType === "coupon" && (
                    <Box>
                      <FormLabel fontSize="sm" color="#333333">
                        Coupon valid till
                      </FormLabel>
                      <Stack direction="row" align="left">
                        <Input
                          color="#333333"
                          id="name"
                          onChange={(e) =>
                            setCouponValidityTo(e?.target?.value)
                          }
                          // placeholder="Your Loyalty Card Name"
                          value={couponValidityTo}
                          required
                          type="text"
                          py="2"
                          rounded="md"
                          size="md"
                          borderColor="#888888"
                        />
                        <Input
                          color="#333333"
                          id="name"
                          onChange={(e) =>
                            setCouponValidityFrom(e?.target?.value)
                          }
                          // placeholder="Your Loyalty Card Name"
                          value={couponValidityFrom}
                          required
                          type="text"
                          py="2"
                          rounded="md"
                          size="md"
                          borderColor="#888888"
                        />
                      </Stack>
                      <FormLabel fontSize="sm" color="#333333">
                        Coupon Item
                      </FormLabel>
                      <Stack direction="column" align="left">
                        <Input
                          color="#333333"
                          id="name"
                          onChange={(e) => setCouponItem(e?.target?.value)}
                          // placeholder="Your Loyalty Card Name"
                          value={couponItem}
                          required
                          type="text"
                          py="2"
                          rounded="md"
                          size="md"
                          borderColor="#888888"
                        />
                      </Stack>
                      <FormLabel fontSize="sm" color="#333333">
                        No. of times coupon can be redeemed
                      </FormLabel>
                      <Stack direction="column" align="left">
                        <Input
                          color="#333333"
                          id="name"
                          onChange={(e) =>
                            setCouponRedeemFrequency(e?.target?.value)
                          }
                          // placeholder="Your Loyalty Card Name"
                          value={couponRedeemFrequency}
                          required
                          type="number"
                          py="2"
                          rounded="md"
                          size="md"
                          borderColor="#888888"
                        />
                      </Stack>
                      <FormControl id="name">
                        <FormLabel fontSize="sm" color="#333333">
                          Coupon Type
                        </FormLabel>
                        <Stack direction="column" align="left">
                          <Select
                            height="36px"
                            variant="outline"
                            borderColor="#0EBCF5"
                            bgColor="#FFFFFF"
                            color="#403F49"
                            fontSize="sm"
                            value={rewardCouponType}
                            onChange={(e) => {
                              handleChangeCouponType(e);
                            }}
                          >
                            <option value="">Choose One...</option>
                            <option value="discount">Discount Coupon</option>
                            <option value="free">Free Coupon</option>
                          </Select>
                        </Stack>
                      </FormControl>

                      {rewardCouponType === "discount" && (
                        <Box>
                          <FormControl id="name">
                            <FormLabel fontSize="sm" color="#333333">
                              Discount Type
                            </FormLabel>
                            <Stack direction="column" align="left">
                              <Select
                                height="36px"
                                variant="outline"
                                borderColor="#0EBCF5"
                                bgColor="#FFFFFF"
                                color="#403F49"
                                fontSize="sm"
                                value={discountCouponType}
                                onChange={(e) => {
                                  handleChangeDiscountType(e);
                                }}
                              >
                                <option value="">Choose One...</option>
                                <option value="%">
                                  Discount in percentage
                                </option>
                                <option value="#">Discount in INR</option>
                              </Select>
                            </Stack>
                          </FormControl>
                          <FormLabel fontSize="sm" color="#333333">
                            Discount Value
                          </FormLabel>
                          <Stack direction="column" align="left">
                            <Input
                              color="#333333"
                              id="name"
                              onChange={(e) =>
                                setDiscountCouponValue(e?.target?.value)
                              }
                              // placeholder="Your Loyalty Card Name"
                              value={discountCouponValue}
                              required
                              type="text"
                              py="2"
                              rounded="md"
                              size="md"
                              borderColor="#888888"
                            />
                          </Stack>
                        </Box>
                      )}

                      {/* free coupon */}
                      {rewardCouponType === "free" && (
                        <Box>
                          <FormLabel fontSize="sm" color="#333333">
                            Free Quantity
                          </FormLabel>
                          <Stack direction="column" align="left">
                            <Input
                              color="#333333"
                              id="name"
                              onChange={(e) =>
                                setFreeItemQuantity(e?.target?.value)
                              }
                              // placeholder="Your Loyalty Card Name"
                              value={freeItemQuantity}
                              required
                              type="text"
                              py="2"
                              rounded="md"
                              size="md"
                              borderColor="#888888"
                            />
                          </Stack>
                        </Box>
                      )}
                    </Box>
                  )}
                  <Button p="2" onClick={() => updateRewardProgramHandler()}>
                    Save Reward
                  </Button>
                  {/* link to campaign */}
                </Stack>
              )}
            </Stack>
          </Center>
        </Box>
      )}
    </Box>
  );
};
