import {
  Box,
  Button,
  Center,
  Divider,
  Flex,
  Hide,
  Image,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Show,
  SimpleGrid,
  Stack,
  Text,
} from "@chakra-ui/react";
import {
  FaAngleDown,
  FaAngleLeft,
  FaAngleRight,
  FaMapMarkerAlt,
} from "react-icons/fa";
import { IoNavigate } from "react-icons/io5";
import { caseback, caseoffer, discount, oneplus } from "../../assets/svgs";
import { useState } from "react";
import { OneCoupon } from "./OneCoupon";
import funkfact from "../../assets/funkfeets/funkfact.png";
import omobyke from "../../assets/image/omoByke.jpeg";
const data = [
  {
    image: funkfact,
    brand: "Funk feets",
    address: " Ganesh Nagar, Rawatpur",
    url: oneplus,
    offer: "Flat 25 % off on all footwears",
    couponCode: "FFxMonadx25",
    brandLink: "https://www.funkfeets.com/",
    AboutBrand: "Funkfeet Unisex Licky Sneakers",
    images: [
      "https://www.funkfeets.com/cdn/shop/files/IMG_5299_1080x.jpg?v=1688770119",
      "https://www.funkfeets.com/cdn/shop/files/BeigeMinimalistNewCasualFashionCollectionInstagramPost_FacebookCover_3_1080x.png?v=1688231365",
      "https://www.funkfeets.com/cdn/shop/files/Beige_Minimalist_New_Casual_Fashion_Collection_Instagram_Post_1080x.png?v=1686772282",
      "https://www.funkfeets.com/cdn/shop/files/Summer_Welcome_Beach_Instagram_Post_1080x.png?v=1688291799",
    ],
  },
  {
    image: omobyke,
    brand: "OMO BIKES",
    address: " Ganesh Nagar, Rawatpur",
    url: oneplus,
    offer: "OMONAD 8% OFF FLAT (on any bike above 12,000",
    couponCode: "OMONAD",
    AboutBrand: "Customized Premium Bicycles",
    brandLink: "https://omobikes.com/",
    images: [
      "https://cdn.shopify.com/s/files/1/0026/4110/0861/t/65/assets/hybrid-alloy-1679849042326.jpg?v=1679849043",
      "https://cdn.shopify.com/s/files/1/0026/4110/0861/files/Hampi_1_69790316-2256-4b77-96f4-5fc15a95ff04.jpg?v=1685552861",
      "https://cdn.shopify.com/s/files/1/0026/4110/0861/files/manali_1.jpg?v=1685553484",
    ],
  },
];

export function CouponHomePage(props: any) {
  const [startExistingRewards, setstartExistingRewards] = useState<any>(0);
  const [existingRewards, setExistingRewards] = useState<any>(4);
  const [existingRewardsMobile, setExistingRewardsMobile] = useState<any>(2);
  const [show, setShow] = useState<any>(-1);

  return (
    <Box>
      <Center
        flexDirection="column"
        // bgGradient="linear(to-r, gray.300, yellow.400, pink.200)"
        bgGradient="linear(to-r, rgba(244,196,196,1.00) 0%,  rgba(175,198,226,0.50) 75%)"
        py="5"
      >
        <Text
          fontSize={{ base: "2xl", lg: "40px" }}
          fontWeight="650"
          color="#FF0042"
        >
          Experience a New Era of Incentives
        </Text>
        <Text
          fontSize={{ base: "sm", lg: "md" }}
          fontWeight="400"
          color="#444444"
        >
          Discover a World of Unparalleled Rewards!
        </Text>
        <Stack pt={{ base: "5", lg: "5" }}>
          <InputGroup size="lg" width={{ base: "376px", lg: "584px" }}>
            <Input
              py="3"
              bgColor="#FFFFFF"
              borderRadius="4px"
              fontSize={{ base: "12px", lg: "md" }}
              color="#949494"
              placeholder="Enter your city name"
            />
            <Hide below="md">
              <InputRightElement>
                <Center pt="2" pr="2">
                  <Button
                    py="2"
                    bgColor="#DEDEDE66"
                    color="#808080"
                    fontSize={{ base: "12px", lg: "md" }}
                    leftIcon={<IoNavigate color="#808080" />}
                  >
                    Use My Current Location
                  </Button>
                </Center>
              </InputRightElement>
            </Hide>
            <InputLeftElement>
              <Center pt={{ base: "3", lg: "4" }} pl="3">
                <FaMapMarkerAlt color="#949494" size="20px" />
              </Center>
            </InputLeftElement>
          </InputGroup>
        </Stack>
        <Center>
          <Flex
            pt={{ base: "5", lg: "10" }}
            gap={{ base: "0", lg: "5" }}
            width="100%"
          >
            <Center
              flexDirection="column"
              gap="2"
              width={{ base: "120px", lg: "139px" }}
              height={{ base: "92px", lg: "92px" }}
            >
              <Image
                src={caseoffer}
                alt="case bonus"
                width="60px"
                height="60px"
              />
              <Text
                fontSize={{ base: "8px", lg: "12.53px" }}
                fontWeight="400"
                color="#515151"
                align="left"
              >
                Cash Bonuses
              </Text>
            </Center>
            <Center
              flexDirection="column"
              gap="2"
              width={{ base: "120px", lg: "159px" }}
              height={{ base: "92px", lg: "92px" }}
            >
              <Image
                src={discount}
                alt="case bonus"
                width="60px"
                height="60px"
              />
              <Text
                fontSize={{ base: "8px", lg: "12.53px" }}
                fontWeight="400"
                color="#515151"
                align="left"
              >
                Discounts and Copouns
              </Text>
            </Center>
            <Center
              flexDirection="column"
              gap="2"
              width={{ base: "120px", lg: "139px" }}
              height={{ base: "92px", lg: "92px" }}
            >
              <Image
                src={caseback}
                alt="case bonus"
                width="60px"
                height="60px"
              />
              <Text
                fontSize={{ base: "8px", lg: "12.53px" }}
                fontWeight="400"
                color="#515151"
                align="left"
              >
                Cashback offers
              </Text>
            </Center>
          </Flex>
        </Center>
        <Center flexDirection="column" pt={{ base: "10", lg: "20" }}>
          <Text fontSize="md" fontWeight="400" color="#323232" m="0">
            SCROLL TO EXPLORE
          </Text>
          <Divider color="#323232" />
          <Stack pt={{ base: "5", lg: "5" }}>
            <FaAngleDown color="#323232" size="20px" />
          </Stack>
        </Center>
      </Center>
      <Box px={{ base: "3", lg: "20" }} py={{ base: "0", lg: "0" }}>
        <Text
          pt="5"
          fontSize={{ base: "lg", lg: "2xl" }}
          fontWeight="638"
          color="#403F49"
          m="0"
          align="left"
        >
          Exciting rewards for you
        </Text>
        <Flex gap={{ base: "1", lg: "10" }}>
          <Hide below="md">
            {startExistingRewards != 0 && (
              <Center>
                <FaAngleLeft
                  color="#000000"
                  onClick={() => {
                    setExistingRewards((v: any) => v - 4);
                    setstartExistingRewards((v: any) => v - 4);
                  }}
                />
              </Center>
            )}

            <SimpleGrid
              columns={[2, 2, 4]}
              spacing={{ base: "2", lg: "20" }}
              pt="5"
            >
              {data
                ?.slice(startExistingRewards, existingRewards)
                ?.map((singlData: any, index: any) => (
                  <OneCoupon singlData={singlData} key={index} />
                ))}
            </SimpleGrid>

            {existingRewards <= data.length && (
              <Center>
                <FaAngleRight
                  color="#000000"
                  onClick={() => {
                    if (existingRewards < data?.length) {
                      setExistingRewards((v: any) => v + 4);
                      setstartExistingRewards((v: any) => v + 4);
                    }
                  }}
                />
              </Center>
            )}
          </Hide>
          <Show below="md">
            {startExistingRewards != 0 && (
              <Center>
                <FaAngleLeft
                  color="#000000"
                  onClick={() => {
                    setExistingRewardsMobile((v: any) => v - 2);
                    setstartExistingRewards((v: any) => v - 2);
                  }}
                />
              </Center>
            )}

            <SimpleGrid
              columns={[2, 2, 2]}
              spacing={{ base: "2", lg: "20" }}
              pt="5"
            >
              {data
                ?.slice(startExistingRewards, existingRewardsMobile)
                ?.map((singlData: any, index: any) => (
                  <OneCoupon singlData={singlData} key={index} />
                ))}
            </SimpleGrid>

            {existingRewardsMobile <= data.length && (
              <Center>
                <FaAngleRight
                  color="#000000"
                  onClick={() => {
                    if (existingRewards < data?.length) {
                      setExistingRewardsMobile((v: any) => v + 2);
                      setstartExistingRewards((v: any) => v + 2);
                    }
                  }}
                />
              </Center>
            )}
          </Show>
        </Flex>
        {/* <Text
          pt="5"
          fontSize={{ base: "lg", lg: "2xl" }}
          fontWeight="638"
          color="#403F49"
          align="left"
        >
          Tending stores
        </Text>

        <SimpleGrid
          columns={[3, 3, 6]}
          spacing={{ base: "2", lg: "10" }}
          pt="5"
        >
          {Array.apply(null, Array(1)).map((value: any, index: any) => (
            <Center
              key={index}
              flexDirection="column"
              gap={{ base: "2", lg: "5" }}
              border="1px"
              borderColor="#D9D9D9"
              borderRadius="4px"
              py="5"
              bgColor={show === index ? "#2192FF" : ""}
              color={show === index ? "#FFFFFF" : "#000000"}
              onMouseEnter={() => setShow(index)}
              onMouseLeave={() => setShow(-1)}
            >
              {show === index ? (
                <>
                  <Text fontSize={{ base: "sm", lg: "md" }} fontWeight="565">
                    Wolkswagon
                  </Text>
                  <Text fontSize={{ base: "sm", lg: "md" }} fontWeight="565">
                    12 offers
                  </Text>
                </>
              ) : (
                <>
                  <Image src={ww} alt=" " height="51px" width="76px" />
                  <Text
                    fontSize={{ base: "sm", lg: "md" }}
                    fontWeight="565"
                    color="#000000"
                  >
                    12 offers
                  </Text>
                </>
              )}
            </Center>
          ))}
        </SimpleGrid> */}
        <Text
          pt="5"
          fontSize={{ base: "lg", lg: "2xl" }}
          fontWeight="638"
          color="#403F49"
          align="left"
        >
          Top picks for you!
        </Text>
        <SimpleGrid
          columns={[2, 2, 4]}
          spacing={{ base: "2", lg: "20" }}
          pt="5"
        >
          {data?.slice(0, 4)?.map((singlData: any, index: any) => (
            <OneCoupon singlData={singlData} key={index} />
          ))}
        </SimpleGrid>
      </Box>
    </Box>
  );
}
