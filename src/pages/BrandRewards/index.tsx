import { Box, Button, Stack, Text, SimpleGrid } from "@chakra-ui/react";
// import { SingleRewardData } from "../../components/common/SingleRewardData";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { myCreatedRewardPrograms } from "../../Actions/rewardAndOffer";
import { SingleCoupon } from "../../components/common/SingleCoupon";
import HLoading from "../../components/atoms/HLoading";
import MessageBox from "../../components/atoms/MessageBox";
import { SingleCard } from "../../components/common/SingleCard";
import { SingleRewardCouponDetail } from "../../components/common/SingleRewardCouponDetail";
import { SingleRewardCardDetail } from "../../components/common/SingleRewardCardDetail";

export function BrandRewards() {
  const dispatch = useDispatch<any>();
  const [selectedRewardCoupon, setSelectedRewardCoupon] = useState<any>(null);
  const [selectedRewardCard, setSelectedRewardCard] = useState<any>(null);

  const handleSelectRewardCoupon = (coupon: any) => {
    setSelectedRewardCoupon(coupon);
  };
  const handleSelectRewardCard = (card: any) => {
    setSelectedRewardCard(card);
  };
  const myRewardProgramsCreated = useSelector(
    (state: any) => state.myRewardProgramsCreated
  );
  const {
    loading: loadingMyRewardPrograms,
    error: errorMyRewardPrograms,
    myRewardPrograms,
  } = myRewardProgramsCreated;

  useEffect(() => {
    dispatch(myCreatedRewardPrograms());
  }, []);
  return (
    <Box color="black.500" px={{ base: "5", lg: "20" }}>
      <Text fontSize="lg" fontWeight="bold">
        This is brand Home page
      </Text>
      <Stack width="200px">
        <Button py="3">+ New reward</Button>
      </Stack>
      {loadingMyRewardPrograms ? (
        <HLoading loading={loadingMyRewardPrograms} />
      ) : errorMyRewardPrograms ? (
        <MessageBox variant="danger">{errorMyRewardPrograms}</MessageBox>
      ) : (
        <>
          {selectedRewardCoupon ? (
            <Box>
              <Stack py="3">
                <Button py="3" onClick={() => setSelectedRewardCoupon(null)}>
                  Hide details
                </Button>
              </Stack>
              <SingleRewardCouponDetail reward={selectedRewardCoupon} />
            </Box>
          ) : selectedRewardCard ? (
            <Box>
              <Stack py="3">
                <Button py="3" onClick={() => setSelectedRewardCard(null)}>
                  Hide details
                </Button>
              </Stack>
              <SingleRewardCardDetail reward={selectedRewardCard} />
            </Box>
          ) : (
            <Stack>
              <Text fontSize="xl" fontWeight="bold" align="left" pt="5">
                Coupon List
              </Text>

              <SimpleGrid columns={[1, 3, 6]} spacing="5" pt="5">
                {myRewardPrograms?.length > 0 &&
                  myRewardPrograms?.map((reward: any) => {
                    if (reward?.rewardOfferType?.type === "coupon") {
                      return (
                        <Box
                          boxShadow={"2xl"}
                          width="250px"
                          bgColor="#FFB84C"
                          borderRadius="lg"
                          color="#FFFFFF"
                          py="3"
                          onClick={() => handleSelectRewardCoupon(reward)}
                        >
                          <SingleCoupon reward={reward} />
                        </Box>
                      );
                    }
                  })}
              </SimpleGrid>
              <Text fontSize="xl" fontWeight="bold" align="left" pt="5">
                Card List
              </Text>
              <SimpleGrid columns={[1, 1, 4]} spacing="5" pt="5">
                {myRewardPrograms?.length > 0 &&
                  myRewardPrograms?.map((reward: any) => {
                    if (reward?.rewardOfferType?.type === "card") {
                      return (
                        <Box
                          boxShadow={"2xl"}
                          width="400px"
                          bgColor="#10A19D"
                          borderRadius="lg"
                          color="#FFFFFF"
                          py="3"
                          onClick={() => handleSelectRewardCard(reward)}
                        >
                          <SingleCard reward={reward} />
                        </Box>
                      );
                    }
                  })}
              </SimpleGrid>
            </Stack>
          )}
        </>
      )}
    </Box>
  );
}
