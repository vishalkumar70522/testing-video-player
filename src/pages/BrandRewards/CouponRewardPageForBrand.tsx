import { Box, Button, Stack, Text, SimpleGrid, Flex } from "@chakra-ui/react";
// import { SingleRewardData } from "../../components/common/SingleRewardData";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import HLoading from "../../components/atoms/HLoading";
import MessageBox from "../../components/atoms/MessageBox";
import { SingleRewardCouponDetail } from "../../components/common/SingleRewardCouponDetail";
import { SingleRewardCardDetail } from "../../components/common/SingleRewardCardDetail";
import {
  createCouponRewardProgram,
  getCouponRewardOfferListForBrand,
} from "../../Actions/couponRewardOfferActions";
import { SingleCouponForBrand } from "../../components/common/SingleCouponForBrand";
import { SingleCardForBrand } from "../../components/common/SingleCardForBrand";
import {
  createCardRewardProgram,
  getCardRewardOfferListForBrand,
} from "../../Actions/cardRewardOfferActions";
import { useNavigate } from "react-router-dom";

//cardRewardOfferCreate
export function CouponRewardPageForBrand() {
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();
  const [selectedRewardCoupon, setSelectedRewardCoupon] = useState<any>(null);
  const [selectedRewardCard, setSelectedRewardCard] = useState<any>(null);

  const handleSelectRewardCoupon = (coupon: any) => {
    setSelectedRewardCoupon(coupon);
  };
  const handleSelectRewardCard = (card: any) => {
    setSelectedRewardCard(card);
  };
  const couponRewardOfferListForBrand = useSelector(
    (state: any) => state.couponRewardOfferListForBrand
  );
  const {
    loading: loadingMyRewardPrograms,
    error: errorMyRewardPrograms,
    myRewardPrograms,
  } = couponRewardOfferListForBrand;
  // console.log("myRewardPrograms : ", myRewardPrograms);

  //cardRewardOfferListForBrand
  const cardRewardOfferListForBrand = useSelector(
    (state: any) => state.cardRewardOfferListForBrand
  );
  const {
    loading: loadingCardRewardListPrograms,
    error: errorCardRewardListdPrograms,
    myRewardPrograms: cardRewardList,
  } = cardRewardOfferListForBrand;
  // create coupon reward
  const couponRewardOfferCreate = useSelector(
    (state: any) => state.couponRewardOfferCreate
  );
  const {
    loading: loadingRewardProgramCreate,
    error: errorRewardProgramCreate,
    success: successRewardCreated,
    rewardProgram: couponRewardprogram,
  } = couponRewardOfferCreate;
  // create card dispatcher
  const cardRewardOfferCreate = useSelector(
    (state: any) => state.cardRewardOfferCreate
  );
  const {
    loading: loadingCardRewardProgramCreate,
    error: errorcardRewardProgramCreate,
    success: successCardRewardCreated,
    rewardProgram: cardReadProgram,
  } = cardRewardOfferCreate;

  // console.log("cardRewardList : ", cardRewardList);
  useEffect(() => {
    if (successRewardCreated) {
      navigate(`/editCouponRewardAndOffer/${couponRewardprogram._id}`);
    }
    if (successCardRewardCreated) {
      navigate(`/editCardRewardAndOffer/${cardReadProgram._id}`);
    }
  }, [successRewardCreated, successCardRewardCreated]);

  useEffect(() => {
    dispatch(getCouponRewardOfferListForBrand());
    dispatch(getCardRewardOfferListForBrand());
  }, []);
  return (
    <Box color="black.500" px={{ base: "5", lg: "20" }}>
      <Text fontSize="lg" fontWeight="bold">
        This is brand Home page
      </Text>
      <Flex gap={10}>
        <Stack width="200px">
          <Button py="3" onClick={() => dispatch(createCouponRewardProgram())}>
            + New coupon Reward
          </Button>
        </Stack>
        <Stack width="200px">
          <Button py="3" onClick={() => dispatch(createCardRewardProgram())}>
            + New card reward
          </Button>
        </Stack>
      </Flex>

      {loadingMyRewardPrograms ||
      loadingCardRewardListPrograms ||
      loadingCardRewardProgramCreate ||
      loadingRewardProgramCreate ? (
        <HLoading
          loading={
            loadingMyRewardPrograms ||
            loadingCardRewardListPrograms ||
            loadingCardRewardProgramCreate ||
            loadingRewardProgramCreate
          }
        />
      ) : errorMyRewardPrograms ||
        errorCardRewardListdPrograms ||
        errorRewardProgramCreate ||
        errorcardRewardProgramCreate ? (
        <MessageBox variant="danger">
          {errorMyRewardPrograms ||
            errorCardRewardListdPrograms ||
            errorRewardProgramCreate ||
            errorcardRewardProgramCreate}
        </MessageBox>
      ) : (
        <>
          {selectedRewardCoupon ? (
            <Box>
              <Stack py="3">
                <Button py="3" onClick={() => setSelectedRewardCoupon(null)}>
                  Hide details
                </Button>
              </Stack>
              <SingleRewardCouponDetail
                reward={selectedRewardCoupon}
                editAllow={true}
              />
            </Box>
          ) : selectedRewardCard ? (
            <Box>
              <Stack py="3">
                <Button py="3" onClick={() => setSelectedRewardCard(null)}>
                  Hide details
                </Button>
              </Stack>
              <SingleRewardCardDetail reward={selectedRewardCard} />
            </Box>
          ) : (
            <Stack>
              <Text fontSize="xl" fontWeight="bold" align="left" pt="5">
                Coupon List
              </Text>

              <SimpleGrid columns={[1, 3, 6]} spacing="5" pt="5">
                {myRewardPrograms?.length > 0 &&
                  myRewardPrograms?.map((reward: any, index: any) => {
                    return (
                      <Box
                        key={index}
                        boxShadow={"2xl"}
                        width="250px"
                        bgColor="#FFB84C"
                        borderRadius="lg"
                        color="#FFFFFF"
                        py="3"
                        onClick={() => handleSelectRewardCoupon(reward)}
                      >
                        <SingleCouponForBrand reward={reward} />
                      </Box>
                    );
                  })}
              </SimpleGrid>
              <Text fontSize="xl" fontWeight="bold" align="left" pt="5">
                Card List
              </Text>
              <SimpleGrid columns={[1, 1, 4]} spacing="5" pt="5">
                {cardRewardList?.length > 0 &&
                  cardRewardList?.map((reward: any, index: any) => {
                    return (
                      <Box
                        key={index}
                        boxShadow={"2xl"}
                        width="400px"
                        bgColor="#10A19D"
                        borderRadius="lg"
                        color="#FFFFFF"
                        py="3"
                        onClick={() => handleSelectRewardCard(reward)}
                      >
                        <SingleCardForBrand reward={reward} />
                      </Box>
                    );
                  })}
              </SimpleGrid>
            </Stack>
          )}
        </>
      )}
    </Box>
  );
}
