import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Box,
  Stack,
  FormControl,
  FormLabel,
  Input,
  Select,
  Text,
  SimpleGrid,
  Button,
  InputGroup,
  Divider,
  Center,
  Flex,
} from "@chakra-ui/react";
import {
  getRewardProgramDetails,
  updateRewardProgram,
} from "../../Actions/rewardAndOffer";
import { UPDATE_REWARD_RESET } from "../../Constants/rewardConstants";
import { useNavigate } from "react-router-dom";
// import { AiOutlinePlusCircle } from "react-icons/ai";
import { addFileOnWeb3 } from "../../utils/web3";
import { SingleCardCoupon } from "../../components/common/SingleCardCoupon";
export const EditRewardDetails = () => {
  const rewardId = window.location.pathname.split("/")[2];
  // console.log("rewardId : ", rewardId);
  const navigate = useNavigate();
  const [rewardProgramId, setRewardProgramId] = useState<any>(null);
  const [offerName, setOfferName] = useState<any>("");
  const [brandName, setBrand] = useState<any>("");
  const [quantity, setQuantity] = useState<any>(0);
  const [rewardOfferType, setrewardOfferType] = useState<any>(null);

  const [cardValidityTo, setCardValidityTo] = useState<any>(null);
  const [cardValidityFrom, setCardValidityFrom] = useState<any>(null);
  const [rewardCardType, setRewardCardType] = useState<any>(null);

  const [milestone, setMilestone] = useState<any>("");

  const [couponValidityTo, setCouponValidityTo] = useState<any>(null);
  const [couponValidityFrom, setCouponValidityFrom] = useState<any>(null);
  const [rewardCouponType, setRewardCouponType] = useState<any>(null);
  const [couponItem, setCouponItem] = useState<any>(null);
  const [couponRedeemFrequency, setCouponRedeemFrequency] = useState<any>(null);
  const [cardMilestonesReward, setCardMilestonesReward] = useState<any>([]);

  const [discountCouponType, setDiscountCouponType] = useState<any>();
  const [discountCouponValue, setDiscountCouponValue] = useState<any>();

  const [freeItemQuantity, setFreeItemQuantity] = useState<any>(null);

  const [programModal, setProgramModal] = useState<boolean>(false);
  const [graphicText, setGraphicText] = useState<any>("");
  const [graphicTextArray, setGraphicTextArray] = useState<any>([]);

  const [fontStyle, setFontStyle] = useState<any>("");
  const [backgroundColors, setBackgroundColors] = useState<any>("");
  const [selectedImage, setSelectedImage] = useState<any>([]);
  const [selectedFiles, setSelectedFiles] = useState<any>([]);
  const [viewAddCardCoupon, setViewAddCardCoupon] = useState<any>(false);
  let hiddenInput: any = null;

  // async function handlePhotoSelect(file: any) {
  //   const fileThumbnail = URL.createObjectURL(file);
  //   setSelectedImage([...selectedImage, fileThumbnail]);
  //   setSelectedFiles([...selectedFiles, file]);
  // }

  // function handelGraphicText(event: any) {
  //   if (event.which === 13) {
  //     setGraphicTextArray([...graphicTextArray, graphicText]);
  //     setGraphicText("");
  //   }
  // }
  const handleDeteleCardCoupon = (singleMileStone: any) => {
    const data = cardMilestonesReward?.filter(
      (mileStone: any) =>
        singleMileStone?.couponReward?.couponItem !=
        mileStone?.couponReward?.couponItem
    );
    setCardMilestonesReward(data);
  };
  const handleEditCardCoupon = (singleMileStone: any) => {
    const data = cardMilestonesReward?.filter(
      (mileStone: any) =>
        singleMileStone?.couponReward?.couponItem !=
        mileStone?.couponReward?.couponItem
    );
    setCardMilestonesReward(data);
    setRewardCouponType(singleMileStone?.couponReward?.couponType);
    setCouponItem(singleMileStone?.couponReward?.couponItem);
    setCouponRedeemFrequency(
      singleMileStone?.couponReward?.couponRedeemFrequency
    );
    setDiscountCouponType(singleMileStone?.couponReward?.couponInfo?.type);
    setDiscountCouponValue(singleMileStone?.couponReward?.couponInfo?.value);
    setFreeItemQuantity(
      singleMileStone?.couponReward?.couponInfo?.freeItemQuantity
    );
    setCouponValidityFrom(singleMileStone?.couponReward?.validity?.from);
    setCouponValidityTo(singleMileStone?.couponReward?.validity?.to);
    setMilestone(singleMileStone?.milestonePoint);
    setViewAddCardCoupon(true);
  };
  const handleAddNewCouponInCardMilestonesReward = () => {
    const data = {
      couponReward: {
        couponType: rewardCouponType, // discount / free
        couponItem: couponItem,
        redeemFrequency: couponRedeemFrequency,
        couponInfo: {
          // discount
          type: discountCouponType, // percent / rupee
          value: discountCouponValue,

          // free
          freeItemQuantity: freeItemQuantity,
        },
        validity: {
          to: couponValidityTo,
          from: couponValidityFrom,
        },
      },
      milestonePoint: milestone,
    };
    setCardMilestonesReward([...cardMilestonesReward, data]);
    setRewardCouponType(null);
    setCouponItem(null);
    setCouponRedeemFrequency(null);
    setDiscountCouponType(null);
    setDiscountCouponValue(null);
    setFreeItemQuantity(null);
    setCouponValidityFrom(null);
    setCouponValidityTo(null);
    setMilestone(null);
    setViewAddCardCoupon(false);
  };
  const handelAddNewCoupon = () => {
    setViewAddCardCoupon(true);
  };

  const userSignin = useSelector((state: any) => state.userSignin);
  const { loading: loadingUser, error: errorUser, userInfo } = userSignin;

  const rewardProgramCreate = useSelector(
    (state: any) => state.rewardProgramCreate
  );
  const {
    loading: loadingRewardProgramCreate,
    error: errorRewardProgramCreate,
    rewardProgram,
  } = rewardProgramCreate;

  const myRewardProgramsCreated = useSelector(
    (state: any) => state.myRewardProgramsCreated
  );
  const {
    loading: loadingMyRewardPrograms,
    error: errorMyRewardPrograms,
    myRewardPrograms,
  } = myRewardProgramsCreated;

  const rewardProgramDetailsGet = useSelector(
    (state: any) => state.rewardProgramDetailsGet
  );
  const {
    loading: loadingRewardProgramDetails,
    error: errorRewardProgramDetails,
    rewardProgramDetails,
  } = rewardProgramDetailsGet;

  const rewardProgramUpdate = useSelector(
    (state: any) => state.rewardProgramUpdate
  );
  const {
    loading: loadingRewardProgramUpdate,
    error: errorRewardProgramUpdate,
    updatedRewardProgram,
  } = rewardProgramUpdate;

  const dispatch = useDispatch<any>();
  if (updatedRewardProgram) {
    alert("Reward updated successfully");
    dispatch({
      type: UPDATE_REWARD_RESET,
    });
    navigate("/userprofile");
  }
  useEffect(() => {
    if (rewardProgramDetails) {
      // console.log("rewardProgramDetails useEffect : ", rewardProgramDetails);
      setRewardProgramId(rewardProgramDetails._id);
      setOfferName(rewardProgramDetails?.offerName);
      setBrand(rewardProgramDetails?.brandName);
      setrewardOfferType(rewardProgramDetails?.rewardOfferType?.type);
      setQuantity(rewardProgramDetails?.quantity);
    }

    if (rewardProgramDetails?.rewardOfferType?.type === "card") {
      setCardValidityTo(
        rewardProgramDetails?.rewardOfferType?.cardReward?.validity?.to
      );
      setCardValidityFrom(
        rewardProgramDetails?.rewardOfferType?.cardReward?.validity?.from
      );
      setRewardCardType(
        rewardProgramDetails?.rewardOfferType?.cardReward?.cardType
      );
      setCardMilestonesReward(
        rewardProgramDetails?.rewardOfferType?.cardReward?.cardMilestonesReward
      );
      // console.log(
      //   "rewardProgramDetails?.rewardOfferType?.couponReward?.cardMilestonesReward : ",
      //   rewardProgramDetails?.rewardOfferType?.cardReward?.cardMilestonesReward
      // );
    }

    if (rewardProgramDetails?.rewardOfferType?.type === "coupon") {
      setCouponValidityTo(
        rewardProgramDetails?.rewardOfferType?.couponReward?.validity?.to
      );
      setCouponValidityFrom(
        rewardProgramDetails?.rewardOfferType?.couponReward?.validity?.from
      );
      setRewardCouponType(
        rewardProgramDetails?.rewardOfferType?.couponReward?.couponType
      );
      setCouponItem(
        rewardProgramDetails?.rewardOfferType?.couponReward?.couponItem
      );
      setCouponRedeemFrequency(
        rewardProgramDetails?.rewardOfferType?.couponReward?.redeemFrequency
      );
      setDiscountCouponType(
        rewardProgramDetails?.rewardOfferType?.couponReward?.couponInfo?.type
      );
      setDiscountCouponValue(
        rewardProgramDetails?.rewardOfferType?.couponReward?.couponInfo?.value
      );
      setFreeItemQuantity(
        rewardProgramDetails?.rewardOfferType?.couponReward?.couponInfo
          ?.freeItemQuantity
      );
    }
  }, [rewardProgramDetails]);

  useEffect(() => {
    openProgramEditModal();
  }, []);

  const handleChangeRewardType = (e: any) => {
    setrewardOfferType(e.target.value);
  };

  const deleteMilestones = (milestone: any) => {};

  const handleChangeCardType = (e: any) => {
    setRewardCardType(e.target.value);
  };

  const handleChangeCouponType = (e: any) => {
    setRewardCouponType(e.target.value);
  };

  const handleChangeDiscountType = (e: any) => {
    setDiscountCouponType(e.target.value);
    // if (discountCouponType !== "" && !couponName) {
    //   setCouponName(name);
    // }
  };

  const openProgramEditModal = () => {
    setProgramModal(!programModal);
    dispatch(getRewardProgramDetails(rewardId));
  };

  const updateRewardProgramHandler = async () => {
    //first get cid of seelcted media
    const cids = [];
    try {
      for (let file of selectedFiles) {
        const cid = await addFileOnWeb3(file);
        cids.push(cid);
      }
      // console.log("cidssssss---", cids);
    } catch (error) {
      console.log("error : ", error);
    }
    dispatch(
      updateRewardProgram(rewardProgramId, {
        mediaFiles: cids,
        graphicText: graphicTextArray,
        fontStyle: [fontStyle],
        backgroundColors: [backgroundColors],
        offerName,
        brandName,
        quantity,
        rewardOfferType,
        cardValidityTo,
        cardValidityFrom,
        rewardCardType,
        cardMilestonesReward,
        couponValidityTo,
        couponValidityFrom,
        rewardCouponType,
        couponItem,
        couponRedeemFrequency,
        discountCouponType,
        discountCouponValue,
        freeItemQuantity,
      })
    );
  };
  return (
    <Box pt={{ base: "3", lg: "5" }}>
      {loadingRewardProgramDetails ? (
        <></>
      ) : errorRewardProgramDetails ? (
        <>
          <Text color="red" fontSize="lg">
            {errorRewardProgramDetails}
          </Text>
        </>
      ) : (
        <Box px={{ base: "3", lg: "20" }}>
          <Center>
            <Stack width="100%" p="2">
              <Text color="red" fontSize="xl" fontWeight="bold">
                Edit reward Details
              </Text>
              {programModal && (
                <Stack>
                  <FormControl id="reward_program_name">
                    <FormLabel fontSize="sm" color="#333333">
                      Reward Program Name
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setOfferName(e?.target?.value)}
                        placeholder="Enter name"
                        value={offerName}
                        required
                        type="text"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                  </FormControl>
                  <FormControl id="rewarding_brandName">
                    <FormLabel fontSize="sm" color="#333333">
                      Rewarding Brand
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setBrand(e?.target?.value)}
                        placeholder="Enter Brand name"
                        value={brandName}
                        required
                        type="text"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                  </FormControl>
                  <FormControl id="reward_quantity">
                    <FormLabel fontSize="sm" color="#333333">
                      Reward Quantity
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setQuantity(e?.target?.value)}
                        placeholder="Enter Reward Quantity"
                        value={quantity}
                        required
                        type="number"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                  </FormControl>
                  <FormControl id="reward_type">
                    <FormLabel fontSize="sm" color="#333333">
                      Reward Type
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Select
                        height="36px"
                        variant="outline"
                        borderColor="#0EBCF5"
                        bgColor="#FFFFFF"
                        color="#403F49"
                        fontSize="sm"
                        value={rewardOfferType}
                        onChange={(e) => {
                          handleChangeRewardType(e);
                        }}
                      >
                        <option value="">Choose One...</option>
                        <option value="coupon">Coupon Reward Program</option>
                        <option value="card">Card Reward Program</option>
                      </Select>
                    </Stack>
                  </FormControl>
                  <Divider color="black" p="2" />

                  {/* Loyalty Reward */}
                  {rewardOfferType === "card" && (
                    <Box>
                      <FormLabel fontSize="sm" color="#333333">
                        Card valid till
                      </FormLabel>
                      <Stack direction="row" align="left">
                        <Input
                          color="#333333"
                          id="name"
                          onChange={(e) => setCardValidityTo(e?.target?.value)}
                          // placeholder="Your Loyalty Card Name"
                          value={cardValidityTo}
                          required
                          type="text"
                          py="2"
                          rounded="md"
                          size="md"
                          borderColor="#888888"
                        />
                        <Input
                          color="#333333"
                          id="name"
                          onChange={(e) =>
                            setCardValidityFrom(e?.target?.value)
                          }
                          // placeholder="Your Loyalty Card Name"
                          value={cardValidityFrom}
                          required
                          type="text"
                          py="2"
                          rounded="md"
                          size="md"
                          borderColor="#888888"
                        />
                      </Stack>
                      <FormControl id="name">
                        <FormLabel fontSize="sm" color="#333333">
                          Card Type
                        </FormLabel>
                        <Stack direction="column" align="left">
                          <Select
                            height="36px"
                            variant="outline"
                            borderColor="#0EBCF5"
                            bgColor="#FFFFFF"
                            color="#403F49"
                            fontSize="sm"
                            onChange={(e) => {
                              handleChangeCardType(e);
                            }}
                          >
                            <option value="">Choose One...</option>
                            <option value="loyalty">Loyalty Card</option>
                          </Select>
                        </Stack>
                      </FormControl>
                      <SimpleGrid columns={[1, null, 5]} spacing="5" pt="5">
                        {cardMilestonesReward?.map((eachCoupon: any) => (
                          <Stack>
                            <SingleCardCoupon
                              reward={rewardProgramDetails}
                              couponInfo={eachCoupon}
                            />
                            <Flex justifyContent={"space-around"}>
                              <Button
                                py="2"
                                onClick={() => handleEditCardCoupon(eachCoupon)}
                              >
                                Edit
                              </Button>
                              <Button
                                py="2"
                                onClick={() =>
                                  handleDeteleCardCoupon(eachCoupon)
                                }
                              >
                                Delete
                              </Button>
                            </Flex>
                          </Stack>
                        ))}
                      </SimpleGrid>
                      {viewAddCardCoupon === false && (
                        <Stack pt="10">
                          <Button py="3" onClick={handelAddNewCoupon}>
                            Add new coupon with mileStone
                          </Button>
                        </Stack>
                      )}
                      {viewAddCardCoupon && (
                        <Box
                          border="1px"
                          borderRadius="lg"
                          borderColor="#000000"
                          mt="5"
                          p="10"
                        >
                          <Stack direction="column">
                            <FormLabel fontSize="sm" color="#333333">
                              Add Milestones
                            </FormLabel>
                            <InputGroup
                              size="lg"
                              width={{ base: "100%", lg: "100%" }}
                              pl="0"
                            >
                              <Input
                                placeholder="Enter Milestone"
                                size="lg"
                                borderRadius="md"
                                fontSize="lg"
                                border="1px"
                                color="#555555"
                                value={milestone}
                                onChange={(e) => setMilestone(e.target.value)}
                                py="2"
                              />
                            </InputGroup>
                            <Box>
                              <FormLabel fontSize="sm" color="#333333">
                                Coupon valid till
                              </FormLabel>
                              <Stack direction="row" align="left">
                                <Input
                                  color="#333333"
                                  id="name"
                                  onChange={(e) =>
                                    setCouponValidityTo(e?.target?.value)
                                  }
                                  // placeholder="Your Loyalty Card Name"
                                  value={couponValidityTo}
                                  required
                                  type="text"
                                  py="2"
                                  rounded="md"
                                  size="md"
                                  borderColor="#888888"
                                />
                                <Input
                                  color="#333333"
                                  id="name"
                                  onChange={(e) =>
                                    setCouponValidityFrom(e?.target?.value)
                                  }
                                  // placeholder="Your Loyalty Card Name"
                                  value={couponValidityFrom}
                                  required
                                  type="text"
                                  py="2"
                                  rounded="md"
                                  size="md"
                                  borderColor="#888888"
                                />
                              </Stack>
                              <FormLabel fontSize="sm" color="#333333">
                                Coupon Item
                              </FormLabel>
                              <Stack direction="column" align="left">
                                <Input
                                  color="#333333"
                                  id="name"
                                  onChange={(e) =>
                                    setCouponItem(e?.target?.value)
                                  }
                                  // placeholder="Your Loyalty Card Name"
                                  value={couponItem}
                                  required
                                  type="text"
                                  py="2"
                                  rounded="md"
                                  size="md"
                                  borderColor="#888888"
                                />
                              </Stack>
                              <FormLabel fontSize="sm" color="#333333">
                                No. of times coupon can be redeemed
                              </FormLabel>
                              <Stack direction="column" align="left">
                                <Input
                                  color="#333333"
                                  id="name"
                                  onChange={(e) =>
                                    setCouponRedeemFrequency(e?.target?.value)
                                  }
                                  // placeholder="Your Loyalty Card Name"
                                  value={couponRedeemFrequency}
                                  required
                                  type="number"
                                  py="2"
                                  rounded="md"
                                  size="md"
                                  borderColor="#888888"
                                />
                              </Stack>
                              <FormControl id="name">
                                <FormLabel fontSize="sm" color="#333333">
                                  Coupon Type
                                </FormLabel>
                                <Stack direction="column" align="left">
                                  <Select
                                    height="36px"
                                    variant="outline"
                                    borderColor="#0EBCF5"
                                    bgColor="#FFFFFF"
                                    color="#403F49"
                                    fontSize="sm"
                                    value={rewardCouponType}
                                    onChange={(e) => {
                                      handleChangeCouponType(e);
                                    }}
                                  >
                                    <option value="">Choose One...</option>
                                    <option value="discount">
                                      Discount Coupon
                                    </option>
                                    <option value="free">Free Coupon</option>
                                  </Select>
                                </Stack>
                              </FormControl>

                              {rewardCouponType === "discount" && (
                                <Box>
                                  <FormControl id="name">
                                    <FormLabel fontSize="sm" color="#333333">
                                      Discount Type
                                    </FormLabel>
                                    <Stack direction="column" align="left">
                                      <Select
                                        height="36px"
                                        variant="outline"
                                        borderColor="#0EBCF5"
                                        bgColor="#FFFFFF"
                                        color="#403F49"
                                        fontSize="sm"
                                        value={discountCouponType}
                                        onChange={(e) => {
                                          handleChangeDiscountType(e);
                                        }}
                                      >
                                        <option value="">Choose One...</option>
                                        <option value="%">
                                          Discount in percentage
                                        </option>
                                        <option value="#">
                                          Discount in INR
                                        </option>
                                      </Select>
                                    </Stack>
                                  </FormControl>
                                  <FormLabel fontSize="sm" color="#333333">
                                    Discount Value
                                  </FormLabel>
                                  <Stack direction="column" align="left">
                                    <Input
                                      color="#333333"
                                      id="name"
                                      onChange={(e) =>
                                        setDiscountCouponValue(e?.target?.value)
                                      }
                                      // placeholder="Your Loyalty Card Name"
                                      value={discountCouponValue}
                                      required
                                      type="text"
                                      py="2"
                                      rounded="md"
                                      size="md"
                                      borderColor="#888888"
                                    />
                                  </Stack>
                                </Box>
                              )}

                              {/* free coupon */}
                              {rewardCouponType === "free" && (
                                <Box>
                                  <FormLabel fontSize="sm" color="#333333">
                                    Free Quantity
                                  </FormLabel>
                                  <Stack direction="column" align="left">
                                    <Input
                                      color="#333333"
                                      id="name"
                                      onChange={(e) =>
                                        setFreeItemQuantity(e?.target?.value)
                                      }
                                      // placeholder="Your Loyalty Card Name"
                                      value={freeItemQuantity}
                                      required
                                      type="text"
                                      py="2"
                                      rounded="md"
                                      size="md"
                                      borderColor="#888888"
                                    />
                                  </Stack>
                                </Box>
                              )}
                            </Box>
                          </Stack>
                          <Center pt="5">
                            <Button
                              py="3"
                              onClick={handleAddNewCouponInCardMilestonesReward}
                            >
                              Save Coupon to cardMilestonesReward
                            </Button>
                          </Center>
                        </Box>
                      )}
                    </Box>
                  )}

                  {/* Discount Reward */}
                  {rewardOfferType === "coupon" && (
                    <Box>
                      <FormLabel fontSize="sm" color="#333333">
                        Coupon valid till
                      </FormLabel>
                      <Stack direction="row" align="left">
                        <Input
                          color="#333333"
                          id="name"
                          onChange={(e) =>
                            setCouponValidityTo(e?.target?.value)
                          }
                          // placeholder="Your Loyalty Card Name"
                          value={couponValidityTo}
                          required
                          type="text"
                          py="2"
                          rounded="md"
                          size="md"
                          borderColor="#888888"
                        />
                        <Input
                          color="#333333"
                          id="name"
                          onChange={(e) =>
                            setCouponValidityFrom(e?.target?.value)
                          }
                          // placeholder="Your Loyalty Card Name"
                          value={couponValidityFrom}
                          required
                          type="text"
                          py="2"
                          rounded="md"
                          size="md"
                          borderColor="#888888"
                        />
                      </Stack>
                      <FormLabel fontSize="sm" color="#333333">
                        Coupon Item
                      </FormLabel>
                      <Stack direction="column" align="left">
                        <Input
                          color="#333333"
                          id="name"
                          onChange={(e) => setCouponItem(e?.target?.value)}
                          // placeholder="Your Loyalty Card Name"
                          value={couponItem}
                          required
                          type="text"
                          py="2"
                          rounded="md"
                          size="md"
                          borderColor="#888888"
                        />
                      </Stack>
                      <FormLabel fontSize="sm" color="#333333">
                        No. of times coupon can be redeemed
                      </FormLabel>
                      <Stack direction="column" align="left">
                        <Input
                          color="#333333"
                          id="name"
                          onChange={(e) =>
                            setCouponRedeemFrequency(e?.target?.value)
                          }
                          // placeholder="Your Loyalty Card Name"
                          value={couponRedeemFrequency}
                          required
                          type="number"
                          py="2"
                          rounded="md"
                          size="md"
                          borderColor="#888888"
                        />
                      </Stack>
                      <FormControl id="name">
                        <FormLabel fontSize="sm" color="#333333">
                          Coupon Type
                        </FormLabel>
                        <Stack direction="column" align="left">
                          <Select
                            height="36px"
                            variant="outline"
                            borderColor="#0EBCF5"
                            bgColor="#FFFFFF"
                            color="#403F49"
                            fontSize="sm"
                            value={rewardCouponType}
                            onChange={(e) => {
                              handleChangeCouponType(e);
                            }}
                          >
                            <option value="">Choose One...</option>
                            <option value="discount">Discount Coupon</option>
                            <option value="free">Free Coupon</option>
                          </Select>
                        </Stack>
                      </FormControl>

                      {rewardCouponType === "discount" && (
                        <Box>
                          <FormControl id="name">
                            <FormLabel fontSize="sm" color="#333333">
                              Discount Type
                            </FormLabel>
                            <Stack direction="column" align="left">
                              <Select
                                height="36px"
                                variant="outline"
                                borderColor="#0EBCF5"
                                bgColor="#FFFFFF"
                                color="#403F49"
                                fontSize="sm"
                                value={discountCouponType}
                                onChange={(e) => {
                                  handleChangeDiscountType(e);
                                }}
                              >
                                <option value="">Choose One...</option>
                                <option value="%">
                                  Discount in percentage
                                </option>
                                <option value="#">Discount in INR</option>
                              </Select>
                            </Stack>
                          </FormControl>
                          <FormLabel fontSize="sm" color="#333333">
                            Discount Value
                          </FormLabel>
                          <Stack direction="column" align="left">
                            <Input
                              color="#333333"
                              id="name"
                              onChange={(e) =>
                                setDiscountCouponValue(e?.target?.value)
                              }
                              // placeholder="Your Loyalty Card Name"
                              value={discountCouponValue}
                              required
                              type="text"
                              py="2"
                              rounded="md"
                              size="md"
                              borderColor="#888888"
                            />
                          </Stack>
                        </Box>
                      )}

                      {/* free coupon */}
                      {rewardCouponType === "free" && (
                        <Box>
                          <FormLabel fontSize="sm" color="#333333">
                            Free Quantity
                          </FormLabel>
                          <Stack direction="column" align="left">
                            <Input
                              color="#333333"
                              id="name"
                              onChange={(e) =>
                                setFreeItemQuantity(e?.target?.value)
                              }
                              // placeholder="Your Loyalty Card Name"
                              value={freeItemQuantity}
                              required
                              type="text"
                              py="2"
                              rounded="md"
                              size="md"
                              borderColor="#888888"
                            />
                          </Stack>
                        </Box>
                      )}
                    </Box>
                  )}
                  {/* <Box>
                    {/* <SimpleGrid columns={[1, 1, 2]} spacing={3} pt="5">
                      <VStack
                        fontSize="sm"
                        spacing="2"
                        width="100%"
                        align="left"
                      >
                        <Text
                          color="#393939"
                          fontWeight="semibold"
                          align="left"
                        >
                          Screen Images
                        </Text>
                      </VStack>
                      <HStack>
                        <HStack>
                          {selectedImage?.map((url: any) => (
                            <Image src={url} width="76px" height="86px" />
                          ))}
                        </HStack>
                        <Button
                          variant="outline"
                          width="76px"
                          height="86px"
                          loadingText="Uploading..."
                          onClick={() => hiddenInput.click()}
                          borderColor="#8B8B8B"
                          color="#8B8B8B"
                        >
                          <AiOutlinePlusCircle size="30px" />
                        </Button>
                        <Input
                          hidden
                          type="file"
                          ref={(el) => (hiddenInput = el)}
                          accept="image/png, image/jpeg"
                          onChange={(e: any) =>
                            handlePhotoSelect(e.target.files[0])
                          }
                        />
                      </HStack>
                    </SimpleGrid> 
                    <FormLabel fontSize="sm" color="#333333">
                      GraphicText
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setGraphicText(e?.target?.value)}
                        onKeyPress={(e) => handelGraphicText(e)}
                        placeholder="Enter test which you want to dispaly"
                        value={graphicText}
                        required
                        type="text"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                    <FormLabel fontSize="sm" color="#333333">
                      FontStyle
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setFontStyle(e?.target?.value)}
                        placeholder="Enter font style of test"
                        value={fontStyle}
                        required
                        type="text"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                    <FormLabel fontSize="sm" color="#333333">
                      BackgroundColors
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setBackgroundColors(e?.target?.value)}
                        placeholder="Enter background color card or coupon"
                        value={backgroundColors}
                        required
                        type="text"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                  </Box> */}
                  <Stack pt="10">
                    <Button
                      py="4"
                      fontSize="md"
                      onClick={() => updateRewardProgramHandler()}
                    >
                      Save Reward
                    </Button>
                  </Stack>
                  {/* link to campaign */}
                </Stack>
              )}
            </Stack>
          </Center>
        </Box>
      )}
    </Box>
  );
};
