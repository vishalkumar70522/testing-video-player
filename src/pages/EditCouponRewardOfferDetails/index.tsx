import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Box,
  Stack,
  FormControl,
  FormLabel,
  Input,
  Select,
  Text,
  Button,
  Center,
  Wrap,
} from "@chakra-ui/react";
import Axios from "axios";

import { useNavigate } from "react-router-dom";
// import { AiOutlinePlusCircle } from "react-icons/ai";
import { addFileOnWeb3 } from "../../utils/web3";
import {
  getCouponRewardOfferDetailForBrand,
  updateCouponRewardProgram,
} from "../../Actions/couponRewardOfferActions";
import {
  COUPON_REWARD_CREATE_RESET,
  UPDATE_COUPON_REWARD_RESET,
} from "../../Constants/couponRewardOfferConstants";
import HLoading from "../../components/atoms/HLoading";
import MessageBox from "../../components/atoms/MessageBox";
export const EditCouponRewardOfferDetails = () => {
  const rewardId = window.location.pathname.split("/")[2];
  // console.log("rewardId : ", rewardId);
  const navigate = useNavigate();
  const [rewardProgramId, setRewardProgramId] = useState<any>(null);
  const [offerName, setOfferName] = useState<any>("");
  const [brandName, setBrand] = useState<any>("");
  const [quantity, setQuantity] = useState<any>(0);
  const [couponValidityTo, setCouponValidityTo] = useState<any>(null);
  const [couponValidityFrom, setCouponValidityFrom] = useState<any>(null);
  const [rewardCouponType, setRewardCouponType] = useState<any>(null);
  const [couponItem, setCouponItem] = useState<any>(null);
  const [couponRedeemFrequency, setCouponRedeemFrequency] = useState<any>(null);

  const [discountCouponType, setDiscountCouponType] = useState<any>();
  const [discountCouponValue, setDiscountCouponValue] = useState<any>();
  const [freeItemQuantity, setFreeItemQuantity] = useState<any>(null);

  const [programModal, setProgramModal] = useState<boolean>(false);
  const [graphicText, setGraphicText] = useState<any>("");
  const [graphicTextArray, setGraphicTextArray] = useState<any>([]);
  const [fontStyle, setFontStyle] = useState<any>("");
  const [backgroundColors, setBackgroundColors] = useState<any>("");

  const [selectedImage, setSelectedImage] = useState<any>([]);
  const [selectedFiles, setSelectedFiles] = useState<any>([]);
  const [userName, setUserName] = useState<any>("");
  const [userList, setUserList] = useState<any>([]);
  const [selectedRewadpartner, setSelectedRewadpartner] = useState<any>([]);

  let hiddenInput: any = null;

  // async function handlePhotoSelect(file: any) {
  //   const fileThumbnail = URL.createObjectURL(file);
  //   setSelectedImage([...selectedImage, fileThumbnail]);
  //   setSelectedFiles([...selectedFiles, file]);
  // }

  // function handelGraphicText(event: any) {
  //   if (event.which === 13) {
  //     setGraphicTextArray([...graphicTextArray, graphicText]);
  //     setGraphicText("");
  //   }
  // }

  const userSignin = useSelector((state: any) => state.userSignin);
  const { loading: loadingUser, error: errorUser, userInfo } = userSignin;

  const rewardProgramCreate = useSelector(
    (state: any) => state.rewardProgramCreate
  );
  const {
    loading: loadingRewardProgramCreate,
    error: errorRewardProgramCreate,
    rewardProgram,
  } = rewardProgramCreate;

  const couponRewardOfferGetDetails = useSelector(
    (state: any) => state.couponRewardOfferGetDetails
  );
  const {
    loading: loadingRewardProgramDetails,
    error: errorRewardProgramDetails,
    rewardProgramDetails,
  } = couponRewardOfferGetDetails;

  const couponRewardOfferUpdate = useSelector(
    (state: any) => state.couponRewardOfferUpdate
  );
  const {
    loading: loadingRewardProgramUpdate,
    error: errorRewardProgramUpdate,
    updatedRewardProgram,
  } = couponRewardOfferUpdate;

  const dispatch = useDispatch<any>();
  if (updatedRewardProgram) {
    alert("Reward updated successfully");
    dispatch({
      type: UPDATE_COUPON_REWARD_RESET,
    });
    dispatch({ type: COUPON_REWARD_CREATE_RESET });
    navigate("/CouponRewardsForBrand");
  }
  useEffect(() => {
    if (rewardProgramDetails) {
      // console.log("rewardProgramDetails useEffect : ", rewardProgramDetails);
      setRewardProgramId(rewardProgramDetails._id);
      setOfferName(rewardProgramDetails?.offerName);
      setBrand(rewardProgramDetails?.brandName);
      setQuantity(rewardProgramDetails?.quantity);

      setCouponValidityTo(rewardProgramDetails?.couponRewardInfo?.validity?.to);
      setCouponValidityFrom(
        rewardProgramDetails?.couponRewardInfo?.validity?.from
      );
      setRewardCouponType(rewardProgramDetails?.couponRewardInfo?.couponType);
      setCouponItem(rewardProgramDetails?.couponRewardInfo?.couponItem);
      setCouponRedeemFrequency(
        rewardProgramDetails?.couponRewardInfo?.redeemFrequency
      );
      setDiscountCouponType(
        rewardProgramDetails?.couponRewardInfo?.couponInfo?.type
      );
      setDiscountCouponValue(
        rewardProgramDetails?.couponRewardInfo?.couponInfo?.value
      );
      setFreeItemQuantity(
        rewardProgramDetails?.couponRewardInfo?.couponInfo?.freeItemQuantity
      );
    }
  }, [rewardProgramDetails]);

  useEffect(() => {
    openProgramEditModal();
  }, []);

  const deleteMilestones = (milestone: any) => {};

  const handleChangeCouponType = (e: any) => {
    setRewardCouponType(e.target.value);
  };

  const handleChangeDiscountType = (e: any) => {
    setDiscountCouponType(e.target.value);
    // if (discountCouponType !== "" && !couponName) {
    //   setCouponName(name);
    // }
  };
  const handleSelectedUserList = (id: any) => {
    const filterSameUser = selectedRewadpartner?.filter(
      (user: any) => user._id != id
    );
    if (filterSameUser?.length !== selectedRewadpartner?.length) {
      setSelectedRewadpartner([...filterSameUser]);
    } else {
      const data = userList?.find((user: any) => user._id == id);
      setSelectedRewadpartner([...selectedRewadpartner, data]);
    }
  };
  const handleSearchUser = async () => {
    alert("Do u want to search ?");
    if (userName?.length > 3) {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/users/filterUser/${userName}`,
        {
          headers: { Authorization: "Bearer " + userInfo.token },
        }
      );
      if (data?.length == 0)
        alert(
          "No user founf with given name, try with full name or different name"
        );
      setUserList(data);
    } else {
      alert("Enter atleast 3 charactors for get user list");
    }
  };

  const openProgramEditModal = () => {
    setProgramModal(!programModal);
    dispatch(getCouponRewardOfferDetailForBrand(rewardId));
  };

  const updateRewardProgramHandler = async () => {
    //first get cid of seelcted media
    const cids = [];
    try {
      for (let file of selectedFiles) {
        const cid = await addFileOnWeb3(file);
        cids.push(cid);
      }
      // console.log("cidssssss---", cids);
    } catch (error) {
      console.log("error : ", error);
    }
    dispatch(
      updateCouponRewardProgram(rewardProgramId, {
        offerName,
        brandName,
        quantity,
        couponType: rewardCouponType,
        couponInfoValue: discountCouponValue,
        couponInfoType: discountCouponType,
        couponValidityTo,
        couponValidityFrom,
        couponItem,
        freeItemQuantity,
        couponRedeemFrequency,
        reardPartner: selectedRewadpartner.map((user: any) => user._id),
      })
    );
  };
  return (
    <Box pt={{ base: "3", lg: "5" }}>
      {loadingRewardProgramDetails || loadingRewardProgramUpdate ? (
        <HLoading
          loading={loadingRewardProgramDetails || loadingRewardProgramUpdate}
        />
      ) : errorRewardProgramDetails || errorRewardProgramUpdate ? (
        <MessageBox variant="danger">
          {errorRewardProgramDetails || errorRewardProgramUpdate}
        </MessageBox>
      ) : (
        <Box px={{ base: "3", lg: "20" }}>
          <Center>
            <Stack width="100%" p="2">
              <Text color="red" fontSize="xl" fontWeight="bold">
                Edit reward Details
              </Text>
              {programModal && (
                <Stack>
                  <FormControl id="reward_program_name">
                    <FormLabel fontSize="sm" color="#333333">
                      Reward Program Name
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setOfferName(e?.target?.value)}
                        placeholder="Enter name"
                        value={offerName}
                        required
                        type="text"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                  </FormControl>
                  <FormControl id="rewarding_brandName">
                    <FormLabel fontSize="sm" color="#333333">
                      Rewarding Brand
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setBrand(e?.target?.value)}
                        placeholder="Enter Brand name"
                        value={brandName}
                        required
                        type="text"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                  </FormControl>
                  <FormControl id="reward_quantity">
                    <FormLabel fontSize="sm" color="#333333">
                      Reward Quantity
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setQuantity(e?.target?.value)}
                        placeholder="Enter Reward Quantity"
                        value={quantity}
                        required
                        type="number"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                  </FormControl>

                  {/* Discount Reward */}
                  <Box>
                    <FormLabel fontSize="sm" color="#333333">
                      Coupon valid
                    </FormLabel>
                    <Stack direction="row" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setCouponValidityTo(e?.target?.value)}
                        // placeholder="Your Loyalty Card Name"
                        value={couponValidityTo}
                        required
                        type="text"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) =>
                          setCouponValidityFrom(e?.target?.value)
                        }
                        // placeholder="Your Loyalty Card Name"
                        value={couponValidityFrom}
                        required
                        type="text"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                    <FormLabel fontSize="sm" color="#333333">
                      Coupon Item
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setCouponItem(e?.target?.value)}
                        // placeholder="Your Loyalty Card Name"
                        value={couponItem}
                        required
                        type="text"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                    <FormLabel fontSize="sm" color="#333333">
                      No. of times coupon can be redeemed
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) =>
                          setCouponRedeemFrequency(e?.target?.value)
                        }
                        // placeholder="Your Loyalty Card Name"
                        value={couponRedeemFrequency}
                        required
                        type="number"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                    </Stack>
                    <FormControl id="name">
                      <FormLabel fontSize="sm" color="#333333">
                        Coupon Type
                      </FormLabel>
                      <Stack direction="column" align="left">
                        <Select
                          height="36px"
                          variant="outline"
                          borderColor="#0EBCF5"
                          bgColor="#FFFFFF"
                          color="#403F49"
                          fontSize="sm"
                          value={rewardCouponType}
                          onChange={(e) => {
                            handleChangeCouponType(e);
                          }}
                        >
                          <option value="">Choose One...</option>
                          <option value="discount">Discount Coupon</option>
                          <option value="free">Free Coupon</option>
                        </Select>
                      </Stack>
                    </FormControl>

                    {rewardCouponType === "discount" && (
                      <Box>
                        <FormControl id="name">
                          <FormLabel fontSize="sm" color="#333333">
                            Discount Type
                          </FormLabel>
                          <Stack direction="column" align="left">
                            <Select
                              height="36px"
                              variant="outline"
                              borderColor="#0EBCF5"
                              bgColor="#FFFFFF"
                              color="#403F49"
                              fontSize="sm"
                              value={discountCouponType}
                              onChange={(e) => {
                                handleChangeDiscountType(e);
                              }}
                            >
                              <option value="">Choose One...</option>
                              <option value="%">Discount in percentage</option>
                              <option value="#">Discount in INR</option>
                            </Select>
                          </Stack>
                        </FormControl>
                        <FormLabel fontSize="sm" color="#333333">
                          Discount Value
                        </FormLabel>
                        <Stack direction="column" align="left">
                          <Input
                            color="#333333"
                            id="name"
                            onChange={(e) =>
                              setDiscountCouponValue(e?.target?.value)
                            }
                            // placeholder="Your Loyalty Card Name"
                            value={discountCouponValue}
                            required
                            type="text"
                            py="2"
                            rounded="md"
                            size="md"
                            borderColor="#888888"
                          />
                        </Stack>
                      </Box>
                    )}

                    {/* free coupon */}
                    {rewardCouponType === "free" && (
                      <Box>
                        <FormLabel fontSize="sm" color="#333333">
                          Free Quantity
                        </FormLabel>
                        <Stack direction="column" align="left">
                          <Input
                            color="#333333"
                            id="name"
                            onChange={(e) =>
                              setFreeItemQuantity(e?.target?.value)
                            }
                            // placeholder="Your Loyalty Card Name"
                            value={freeItemQuantity}
                            required
                            type="text"
                            py="2"
                            rounded="md"
                            size="md"
                            borderColor="#888888"
                          />
                        </Stack>
                      </Box>
                    )}
                  </Box>
                  <Box>
                    <FormControl id="name">
                      <FormLabel fontSize="sm" color="#333333">
                        Enter name of reward offer partner
                      </FormLabel>
                      <Stack direction="row" align="left">
                        <Input
                          color="#333333"
                          id="name"
                          onChange={(e) => setUserName(e?.target?.value)}
                          placeholder="Enter name of reward offer partner"
                          value={userName}
                          required
                          type="text"
                          py="2"
                          rounded="md"
                          size="md"
                          borderColor="#888888"
                        />
                        <Button py="4" fontSize="md" onClick={handleSearchUser}>
                          Search
                        </Button>
                      </Stack>
                    </FormControl>

                    <FormControl id="name">
                      <FormLabel fontSize="sm" color="#333333">
                        Select user
                      </FormLabel>
                      <Stack direction="column" align="left">
                        <Select
                          height="36px"
                          variant="outline"
                          borderColor="#0EBCF5"
                          bgColor="#FFFFFF"
                          color="#403F49"
                          fontSize="sm"
                          value={rewardCouponType}
                          onChange={(e: any) => {
                            handleSelectedUserList(e.target.value);
                          }}
                        >
                          <option value="">Select users</option>

                          {userList?.length > 0 ? (
                            userList?.map((user: any, index: any) => (
                              <option value={user._id} key={index}>
                                {user.name}
                              </option>
                            ))
                          ) : (
                            <option value="">
                              No user found with this name
                            </option>
                          )}
                        </Select>
                      </Stack>
                    </FormControl>
                    {selectedRewadpartner?.length > 0 && (
                      <Wrap pt="5" p="10">
                        {selectedRewadpartner?.map((user: any, index: any) => (
                          <Box width="25%" bgColor="yellow" key={index}>
                            <Text color="#000000" fontSize="md">
                              Name : {user.name}
                            </Text>
                          </Box>
                        ))}
                      </Wrap>
                    )}
                  </Box>

                  <Stack pt="10">
                    <Button
                      py="4"
                      fontSize="md"
                      onClick={() => updateRewardProgramHandler()}
                    >
                      Save Reward
                    </Button>
                  </Stack>
                  {/* link to campaign */}
                </Stack>
              )}
            </Stack>
          </Center>
        </Box>
      )}
    </Box>
  );
};
