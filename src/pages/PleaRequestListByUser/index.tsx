import {
  Box,
  Text,
  Stack,
  TableContainer,
  Table,
  Thead,
  Tr,
  Th,
  Td,
  Tbody,
  useToast,
  Badge,
} from "@chakra-ui/react";

import { useDispatch, useSelector } from "react-redux";
import { listAllPleasByUserId } from "../../Actions/pleaActions";
import { useEffect, useState } from "react";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";
import {
  grantCampaignAllyPlea,
  rejectCampaignAllyPlea,
} from "../../Actions/pleaActions";
import {
  grantScreenAllyPlea,
  rejectScreenAllyPlea,
} from "../../Actions/screenActions";
import {
  SCREEN_ALLY_GRANT_RESET,
  SCREEN_ALLY_REJECT_RESET,
} from "../../Constants/screenConstants";
import { VideoPlayerModel } from "./VideoPlayerModel";
import { UserDetailModel } from "./UserDetailModel";
import {
  CAMPAIGN_ALLY_GRANT_RESET,
  CAMPAIGN_ALLY_REJECT_RESET,
} from "../../Constants/pleaConstants";
import { useNavigate } from "react-router-dom";
// import { AlertDialogBox } from "../../components/common/AlertDilogBox/AlertDilogBox";

export function PleaRequestListByUser() {
  const navigate = useNavigate();
  const toast = useToast();
  const dispatch = useDispatch<any>();
  const [selectedPlea, setSelectedPlea] = useState<any>();

  const [videoPlayerModelShow, setVideoPlayerModelShow] =
    useState<boolean>(false);
  const [userDetailModelShow, setUserDetailModelShow] =
    useState<boolean>(false);

  const handelOpenModel = (plea: any) => {
    setSelectedPlea(plea);
    if (plea.pleaType === "CAMPAIGN_ALLY_PLEA") {
      setVideoPlayerModelShow(true);
    } else if (plea.pleaType === "COUPON_REDEEM_PLEA") {
      navigate(
        `/couponDetailsWithRedeemer/${plea?.couponId}/${plea?.couponCode}`
      );
    } else {
      setUserDetailModelShow(true);
    }
  };
  const handelAcceptCampaignPlea = (pleaId: any) => {
    console.log("handelAcceptCampaignPlea called! : ", pleaId);
    dispatch(grantCampaignAllyPlea(pleaId));
  };
  const handelRejectCampaignPlea = (pleaId: any) => {
    console.log("handelRejectCampaignPlea called! : ", pleaId);
    dispatch(rejectCampaignAllyPlea(pleaId));
  };
  const campaignAllyPleaGrant = useSelector(
    (state: any) => state.campaignAllyPleaGrant
  );
  const campaignAllyPleaReject = useSelector(
    (state: any) => state.campaignAllyPleaReject
  );
  const {
    loading: loadingGrantPlea,
    success: successGrantPlea,
    plea: pleaGrant,
    error: errorGrantPlea,
  } = campaignAllyPleaGrant;
  const {
    loading: loadingRejectPlea,
    success: successRejectPlea,
    plea: pleaReject,
    error: errorRejectPlea,
  } = campaignAllyPleaReject;

  const getStatus = (status: boolean, reject: boolean) => {
    if (!status && !reject) {
      return "Pending";
    } else if (status && !reject) {
      return "Accepted";
    } else {
      return "Rejected";
    }
  };

  const screenAllyPleaGrant = useSelector(
    (state: any) => state.screenAllyPleaGrant
  );
  const {
    loading: loadingScreenAllyPleaGrant,
    error: errorScreenAllyPleaGrant,
    success: successScreenAllyPleaGrant,
  } = screenAllyPleaGrant;
  if (successScreenAllyPleaGrant) {
    dispatch({ type: SCREEN_ALLY_GRANT_RESET });
    toast({
      title: "Permition granted!",
      description: "Now you have access to add campaign on this screen.",
      status: "success",
      duration: 4000,
      isClosable: true,
    });
  }

  const screenAllyPleaReject = useSelector(
    (state: any) => state.screenAllyPleaReject
  );
  const {
    loading: loadingScreenAllyPleaReject,
    error: errorScreenAllyPleaReject,
    success: successScreenAllyPleaReject,
  } = screenAllyPleaReject;
  if (successScreenAllyPleaReject) {
    dispatch({ type: SCREEN_ALLY_REJECT_RESET });
    toast({
      title: "Permition rejected!",
      description: "Now you have no access to add campaign on this screen.",
      status: "error",
      duration: 4000,
      isClosable: true,
    });
  }

  const handelAcceptPlea = (pleaId: any) => {
    dispatch(grantScreenAllyPlea(pleaId));
  };
  const handelRejectPlea = (pleaId: any) => {
    dispatch(rejectScreenAllyPlea(pleaId));
  };

  const allPleasListByUser = useSelector(
    (state: any) => state.allPleasListByUser
  );
  const {
    allPleas,
    loading: loadingAllPleas,
    error: errorAllPleas,
  } = allPleasListByUser;
  useEffect(() => {
    if (successGrantPlea) {
      dispatch({ type: CAMPAIGN_ALLY_GRANT_RESET });
      toast({
        title: "Permition granted!",
        description: "Now you have access to add this campaign on this screen.",
        status: "success",
        duration: 4000,
        isClosable: true,
      });
    }
    if (successRejectPlea) {
      dispatch({ type: CAMPAIGN_ALLY_REJECT_RESET });
      toast({
        title: "Permition rejected!",
        description: "Now you have no access to add campaign on this screen.",
        status: "error",
        duration: 4000,
        isClosable: true,
      });
    }
    if (successScreenAllyPleaGrant) {
      dispatch({ type: SCREEN_ALLY_GRANT_RESET });
    }
    if (successScreenAllyPleaReject) {
      dispatch({ type: SCREEN_ALLY_REJECT_RESET });
    }
    dispatch(listAllPleasByUserId());
  }, [
    dispatch,
    successGrantPlea,
    successRejectPlea,
    successScreenAllyPleaGrant,
    successScreenAllyPleaReject,
  ]);

  //   console.log("allPleas : ", JSON.stringify(allPleas));
  return (
    <>
      <VideoPlayerModel
        show={videoPlayerModelShow}
        onHide={() => setVideoPlayerModelShow(false)}
        plea={selectedPlea}
        handelAcceptCampaignPlea={handelAcceptCampaignPlea}
        handelRejectCampaignPlea={handelRejectCampaignPlea}
      />
      <UserDetailModel
        show={userDetailModelShow}
        onHide={() => setUserDetailModelShow(false)}
        plea={selectedPlea}
        handelAcceptPlea={handelAcceptPlea}
        handelRejectPlea={handelRejectPlea}
      />
      <Box>
        <Text fontSize="lg" color="#000000" fontWeight="bold">
          List of plea request came
        </Text>
        <Stack pt="10">
          <TableContainer borderRadius="5px" bgColor="#FFFFFF">
            <Table variant="simple">
              <Thead>
                <Tr>
                  <Th>Plea Date</Th>
                  <Th>Plea type</Th>
                  <Th>Remark</Th>
                  <Th>Status</Th>
                </Tr>
              </Thead>
              <Tbody>
                {allPleas?.map((plea: any, index: any) => (
                  <Tr
                    key={index}
                    onClick={() => handelOpenModel(plea)}
                    _hover={{ bg: "rgba(14, 188, 245, 0.3)" }}
                  >
                    <Td color="#575757" fontSize="sm">
                      {convertIntoDateAndTime(plea.createdAt)}
                    </Td>
                    <Td fontSize="sm" color="#403F49" fontWeight="semibold">
                      {plea.pleaType === "CAMPAIGN_ALLY_PLEA" ? (
                        <Badge colorScheme="red">{plea.pleaType}</Badge>
                      ) : (
                        plea.pleaType
                      )}
                    </Td>
                    <Td fontSize="sm" color="#403F49" fontWeight="semibold">
                      {plea.remarks[plea.remarks.length - 1]}
                    </Td>
                    <Td fontSize="sm" color="#403F49" fontWeight="semibold">
                      <Badge
                        colorScheme={
                          getStatus(plea.status, plea.reject) === "Pending"
                            ? "purple"
                            : getStatus(plea.status, plea.reject) === "Accepted"
                            ? "green"
                            : "red"
                        }
                      >
                        {getStatus(plea.status, plea.reject)}
                      </Badge>
                    </Td>
                  </Tr>
                ))}
              </Tbody>
            </Table>
          </TableContainer>
        </Stack>
      </Box>
    </>
  );
}
