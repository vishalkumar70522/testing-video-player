import { Modal } from "react-bootstrap";
import { Box, Stack, IconButton, HStack, Button } from "@chakra-ui/react";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { useMedia } from "../../hooks";
import { MediaContainer } from "../../components/common";

export function VideoPlayerModel(props: any) {
  const {
    data: media,
    isLoading,
    isError,
  } = useMedia({ id: props.plea?.video?.split("/").slice(4)[0] });

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      backdrop="static"
      keyboard={false}
    >
      <Modal.Body>
        <Box bgColor="#FFFFFF">
          <Stack align="end" justifyContent="flex-end">
            <IconButton
              bg="none"
              icon={
                <AiOutlineCloseCircle
                  size="30px"
                  fontWeight="10"
                  color="#00000090"
                  onClick={props.onHide}
                />
              }
              aria-label="Close"
            />
          </Stack>
          <Box p="5">{media ? <MediaContainer media={media} /> : null}</Box>
          <HStack py="5" px="5" justifyContent="space-around">
            <Button
              py="2"
              onClick={() => {
                props.handelAcceptCampaignPlea(props.plea._id);
                props.onHide();
              }}
              bgColor="green"
            >
              Accept
            </Button>
            <Button
              py="2"
              onClick={() => {
                props.handelRejectCampaignPlea(props.plea._id);
                props.onHide();
              }}
              bgColor="red"
            >
              Reject
            </Button>
          </HStack>
        </Box>
      </Modal.Body>
    </Modal>
  );
}
