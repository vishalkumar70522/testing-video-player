import { Modal } from "react-bootstrap";
import {
  Box,
  Stack,
  IconButton,
  HStack,
  Button,
  FormControl,
  FormLabel,
  Input,
} from "@chakra-ui/react";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { detailsUser } from "../../Actions/userActions";

export function UserDetailModel(props: any) {
  const dispatch = useDispatch<any>();

  const userDetails = useSelector((state: any) => state.userDetails);
  const { loading, user, errro } = userDetails;
  // console.log("user : ", JSON.stringify(user));

  const handelAcceptPlea = (pleaId: any) => {
    props?.handelAcceptPlea(pleaId);
  };
  const handelRejectPlea = (pleaId: any) => {
    props?.handelRejectPlea(pleaId);
  };
  useEffect(() => {
    console.log("useEffect called!");
    dispatch(
      detailsUser({ userId: props.plea?.from, walletAddress: "231132" })
    );
  }, [props]);
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      backdrop="static"
      keyboard={false}
    >
      <Modal.Body>
        <Box bgColor="#FFFFFF">
          <Stack align="end" justifyContent="flex-end">
            <IconButton
              bg="none"
              icon={
                <AiOutlineCloseCircle
                  size="30px"
                  fontWeight="10"
                  color="#00000090"
                  onClick={props.onHide}
                />
              }
              aria-label="Close"
            />
          </Stack>
          {user ? (
            <Box p="5">
              <FormControl id="name">
                <FormLabel fontSize="sm" color="#333333">
                  Name
                </FormLabel>
                <Input
                  color="#333333"
                  value={user.user.name}
                  type="text"
                  disabled
                  py="2"
                  rounded="md"
                  size="md"
                  borderColor="#888888"
                />
              </FormControl>
              <FormControl id="name">
                <FormLabel fontSize="sm" color="#333333">
                  Email
                </FormLabel>
                <Input
                  color="#333333"
                  value={user.user.email}
                  type="text"
                  disabled
                  py="2"
                  rounded="md"
                  size="md"
                  borderColor="#888888"
                />
              </FormControl>
              <FormControl id="name">
                <FormLabel fontSize="sm" color="#333333">
                  Phone
                </FormLabel>
                <Input
                  color="#333333"
                  value={user.user.phone}
                  type="text"
                  disabled
                  py="2"
                  rounded="md"
                  size="md"
                  borderColor="#888888"
                />
              </FormControl>
              <FormControl id="name">
                <FormLabel fontSize="sm" color="#333333">
                  Address
                </FormLabel>
                <Input
                  color="#333333"
                  value={user.user.address}
                  type="text"
                  disabled
                  py="2"
                  rounded="md"
                  size="md"
                  borderColor="#888888"
                />
              </FormControl>
              <FormControl id="name">
                <FormLabel fontSize="sm" color="#333333">
                  State
                </FormLabel>
                <Input
                  color="#333333"
                  value={user.user.stateUt}
                  type="text"
                  disabled
                  py="2"
                  rounded="md"
                  size="md"
                  borderColor="#888888"
                />
              </FormControl>
              <FormControl id="name">
                <FormLabel fontSize="sm" color="#333333">
                  District
                </FormLabel>
                <Input
                  color="#333333"
                  value={user.user.districtCity}
                  type="text"
                  disabled
                  py="2"
                  rounded="md"
                  size="md"
                  borderColor="#888888"
                />
              </FormControl>
              <FormControl id="name">
                <FormLabel fontSize="sm" color="#333333">
                  PinCode
                </FormLabel>
                <Input
                  color="#333333"
                  value={user.user.pincode}
                  type="text"
                  disabled
                  py="2"
                  rounded="md"
                  size="md"
                  borderColor="#888888"
                />
              </FormControl>
            </Box>
          ) : null}
          <HStack py="5" px="5" justifyContent="space-around">
            <Button
              py="2"
              onClick={() => {
                props.handelAcceptPlea(props.plea._id);
                props.onHide();
              }}
              bgColor="green"
            >
              Accept
            </Button>
            <Button
              py="2"
              onClick={() => {
                props.handelRejectPlea(props.plea._id);
                props.onHide();
              }}
              bgColor="red"
            >
              Reject
            </Button>
          </HStack>
        </Box>
      </Modal.Body>
    </Modal>
  );
}
