export { GenerateWallet } from "./OnBrording";

export { Welcome } from "./onboarding/Welcome";
export { KeyManagement } from "./onboarding/KeyManagement";
export { KeyPhraseSave } from "./onboarding/KeyPhraseSave";
export { KeyConfirm } from "./onboarding/KeyConfirm";
export { KeyRecovery } from "./onboarding/KeyRecovery";
export { PinCreate } from "./onboarding/PinCreate";
export { PinSuccess } from "./onboarding/PinSuccess";
export { ViewSecrateKey } from "./onboarding/ViewSecrateKey";

export { WelcomeModal } from "./onboardingmodal/WelcomeModal";
export { LoginModal } from "./onboardingmodal/LoginModal";
export { PinCreateModal } from "./onboardingmodal/PinCreateModal";
export { DisplaySecrateKeyModal } from "./onboardingmodal/DisplaySecrateKeyModal";
export { StartWalletModal } from "./onboardingmodal/StartWalletModal";
export { VerifySecrateKeyModal } from "./onboardingmodal/VerifySecrateKeyModal";
export { ViewSecrateKeyModal } from "./onboardingmodal/ViewSecrateKeyModal";

// uploading

// auth
export { Login } from "./auth/Login";
export { Logout } from "./auth/Logout";
export { Signup } from "./auth/Signup";
export { Signin } from "./auth/Signin";
export { EmailVerification } from "./auth/EmailVerification";
export { AccountSetUp } from "./auth/AccountSetUp";
export { CreateResetPassword } from "./auth/CreateResetPassword";

// auth popup
export { EmailVerificationModal } from "./authPopup/EmailVerificationModal";
export { ReSendEmailModal } from "./authPopup/ResendEmailModal";
export { SignInModal } from "./authPopup/SignInModal";

export { Page404 } from "./404";

export { HomePage } from "./home";
export { AllAds } from "./AllAds";
export { AllScreens, ScreenStatus } from "./AllScreens";
export { ScreenDetailPage } from "./ScreenDetail";
export { ScreenOwner } from "./ScreenOwner";
export { EditScreen } from "./EditScreen";

export { CartAndSummary } from "./CartAndSummary";
export { CartAndSummaryForMultipleScreen } from "./CartAndSummaryForMultipleScreen";

export { CampaignListOfUser } from "./CampaignListOfUser";
export { CampaignDetails } from "./CampaignDetails";

export { WalletPage } from "./WalletPage";
export { PaymentReceipt } from "./WalletPage/PaymentReceipt";
export { SendMoney } from "./Payment/SendMoney";
export { RequestMoney } from "./Payment/RequestMoney";

export { UserProfile } from "./UserProfile";
export { EditUserProfile } from "./EditUserProfile";
export { PlayListByScreen } from "./PlayListByScreen";
export { MyVideoPlayList } from "./PlayListByScreen/MyVideoPlayList";
export { EmailVerificationForForgetPassword } from "./auth/EmailVerificationForForgetPassword";
export { FilterScreens } from "./FilterScreens";
export { AddNewCampaign } from "./AddNewCampaign";
export { AddNewCampaignOnAudianceProfile } from "./AddNewCampaignOnAudianceProfile";
export { PleaRequestListByUser } from "./PleaRequestListByUser";
export { BrandRewards } from "./BrandRewards";
export { CouponRewardPageForBrand } from "./BrandRewards/CouponRewardPageForBrand";
export { EditCouponRewardOfferDetails } from "./EditCouponRewardOfferDetails";
export { EditCardRewardOfferDetails } from "./EditCardRewardOfferDetails";
export { ClameCouponReward } from "./ClameCouponReward";
export { MyRewardsList } from "./MyRewardsList";
export { CouponDetailsWithRedeemer } from "./CouponDetailsWithRedeemer";
// export { RewardPage } from "./RewardPage";

export { CouponHomePage } from "./CouponHomePage";

export { CouponDetails } from "./CouponDetails";
