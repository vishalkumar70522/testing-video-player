import React from "react";
import {
  Box,
  Text,
  Stack,
  IconButton,
  Table,
  TableContainer,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { Modal } from "react-bootstrap";
// import { convertIntoDateAndTime } from "../../utils/dateAndTime";

export function ScreenLogPopup(props: any) {
  const { screenLog } = props;
  console.log(props);
  const getDeviceId = (deviceInfo: string) => {
    const info = deviceInfo ? JSON.parse(deviceInfo) : null;
    return info ? info.deviceId : null;
  };
  const getDeviceIp = (deviceInfo: string) => {
    const info = deviceInfo ? JSON.parse(deviceInfo) : null;
    return info ? info.deviceIp : null;
  };
  const getDeviceDisplay = (deviceInfo: string) => {
    const info = deviceInfo ? JSON.parse(deviceInfo) : null;
    return info ? info.deviceDisplay : null;
  };
  return (
    <Modal {...props} size="xl" aria-labelledby="contained-modal-title-vcenter">
      <Modal.Body>
        <Box bgColor="#FFFFFF">
          {" "}
          <Stack align="end" justifyContent="flex-end">
            <IconButton
              bg="none"
              icon={
                <AiOutlineCloseCircle
                  size="40px"
                  fontWeight="10"
                  color="#00000090"
                  onClick={props.onHide}
                />
              }
              aria-label="Close"
            />
          </Stack>
          <Box color="black.500" p="10" boxShadow="2xl">
            {screenLog ? (
              <TableContainer borderRadius="5px" bgColor="#FFFFFF">
                <Table variant="simple" size="lg">
                  <Thead>
                    <Tr>
                      <Th>Campaign</Th>
                      <Th>Details </Th>
                    </Tr>
                  </Thead>
                  <Tbody>
                    {screenLog?.reverse()?.map((SingleLog: any, index: any) => (
                      <Tr
                        key={index + 1}
                        onClick={() =>
                          props.handleShowCampaignLogs(SingleLog.cid)
                        }
                        _hover={{ bg: "rgba(14, 188, 245, 0.3)" }}
                      >
                        <Td>
                          <Box
                            as="video"
                            // autoPlay
                            loop
                            // controls
                            muted
                            // onLoadedData={() =>
                            //   triggerPort(media.request["responseURL"].split("/").slice()[4])
                            // }
                            boxSize="100%"
                          >
                            <source
                              src={`https://ipfs.io/ipfs/${SingleLog.playVideo
                                .split(".")
                                .slice(0, 1)}`}
                            />
                          </Box>
                          {/* <Text color=" #403F49 " fontSize="sm"></Text> */}
                        </Td>
                        <Td>
                          <Text color=" #403F49 " fontSize="sm">
                            {SingleLog.playVideo.split(".").slice(0, 1)}
                          </Text>
                          <Text color=" #403F49 " fontSize="sm">
                            {new Date(SingleLog.playTime).toLocaleString()}
                          </Text>
                        </Td>
                      </Tr>
                    ))}
                  </Tbody>
                </Table>
              </TableContainer>
            ) : (
              <Text
                color="#D7380E"
                fontSize="lg"
                align="left"
                fontWeight="semibold"
                pt="5"
              >
                No Campaign Logs
              </Text>
            )}
          </Box>
        </Box>
      </Modal.Body>
    </Modal>
  );
}
