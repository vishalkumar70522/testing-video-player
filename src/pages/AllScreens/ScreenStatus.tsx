import React, { useEffect, useState } from "react";
import {
  Box,
  Flex,
  Table,
  TableContainer,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  Stack,
} from "@chakra-ui/react";
import { Form } from "react-bootstrap";

import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import "react-responsive-carousel/lib/styles/carousel.min.css";

import { getAllScreens, getScreenLogs } from "../../Actions/screenActions";

import HLoading from "../../components/atoms/HLoading";
import MessageBox from "../../components/atoms/MessageBox";
import { motion } from "framer-motion";
// import { SCREEN_LIST_RESET } from "../../Constants/screenConstants";
import { ScreenLogPopup } from "./ScreenLogPopup";

export function ScreenStatus() {
  const navigate = useNavigate();
  const MotionFlex = motion<any>(Flex);

  const [showScreenLogs, setShowScreenLogs] = useState<any>(false);
  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const allScreensGet = useSelector((state: any) => state.allScreensGet);
  const {
    loading: loadingScreens,
    error: errorScreens,
    screens,
  } = allScreensGet;

  const screenLogs = useSelector((state: any) => state.screenLogs);
  const {
    loading: loadingScreenLogs,
    error: errorScreenLogs,
    screenLog,
  } = screenLogs;

  const dispatch = useDispatch<any>();

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin");
    }
    dispatch(getAllScreens());
  }, [dispatch, navigate, userInfo]);

  const handleShowScreenLogs = (screen: any) => {
    // const logs = screenLog
    //   .filter((log: any) => log.playVideo.split(".")[0] === cid)
    //   .reverse();
    // setCampaignLogs(logs);
    dispatch(getScreenLogs(screen._id));
    console.log(screen);
    setShowScreenLogs(true);
  };

  return (
    <Box color="black.500" alignItems="center" pt={{ base: "3", lg: "5" }}>
      {loadingScreens ? (
        <HLoading loading={loadingScreens} />
      ) : errorScreens ? (
        <MessageBox variant="danger">{errorScreens}</MessageBox>
      ) : (
        <Stack>
          {loadingScreenLogs ? (
            <Text>Loading...</Text>
          ) : errorScreenLogs ? (
            <Text>{errorScreenLogs}</Text>
          ) : (
            <ScreenLogPopup
              show={showScreenLogs}
              onHide={() => setShowScreenLogs(false)}
              screenLog={screenLog}
            />
          )}
          <Stack py="5" align="center" boxShadow="2xl" bgColor="#FBFBFB">
            <TableContainer borderRadius="5px" bgColor="#FFFFFF">
              <Table variant="simple">
                <Thead>
                  <Tr>
                    <Th>S.NO.</Th>
                    <Th>Status</Th>
                    <Th>Last Played</Th>
                    <Th>Screen Name</Th>
                    <Th>Screen Loaction</Th>
                    <Th>
                      <Flex>
                        <Form.Check
                          aria-label="option 1"
                          // onChange={handleall}
                          // checked={allChecked}
                        />
                        <Text pl="5">Screen Id</Text>
                      </Flex>
                    </Th>
                  </Tr>
                </Thead>
                <Tbody>
                  {screens
                    .filter((sc: any) => sc.lastActive !== undefined || null)
                    .sort((a: any, b: any) => {
                      const aTime = Math.floor(
                        new Date().getTime() - new Date(a.lastActive).getTime()
                      );
                      const bTime = Math.floor(
                        new Date().getTime() - new Date(b.lastActive).getTime()
                      );
                      return aTime - bTime;
                    })
                    .map((screen: any, index: any) => (
                      <Tr
                        key={index}
                        onClick={() => handleShowScreenLogs(screen)}
                      >
                        <Td fontSize="sm" color="#403F49" fontWeight="semibold">
                          {index + 1}
                        </Td>
                        <Td fontSize="sm" color="#403F49" fontWeight="semibold">
                          {!screen.lastActive
                            ? "Screen Not Synced Yet"
                            : Math.floor(
                                new Date().getTime() -
                                  new Date(screen.lastActive).getTime()
                              ) /
                                1000 >
                              200
                            ? "NOT ACTIVE"
                            : "ACTIVE"}
                        </Td>
                        <Td
                          isNumeric
                          fontSize="sm"
                          color="#403F49"
                          fontWeight="semibold"
                        >
                          {new Date(screen.lastActive).toLocaleString()}
                        </Td>
                        <Td color="#575757" fontSize="sm">
                          {screen.name}
                        </Td>
                        <Td color="#575757" fontSize="sm">
                          {screen.screenAddress}, {screen.districtCity}
                        </Td>
                        <Td color="#575757" fontSize="sm">
                          {screen._id}
                        </Td>
                        <Td></Td>
                      </Tr>
                    ))}
                </Tbody>
              </Table>
            </TableContainer>
          </Stack>
        </Stack>
      )}
    </Box>
  );
}
