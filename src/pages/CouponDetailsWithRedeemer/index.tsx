import { Box } from "@chakra-ui/react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCouponRewardOfferDetailForBrand } from "../../Actions/couponRewardOfferActions";
import HLoading from "../../components/atoms/HLoading";
import MessageBox from "../../components/atoms/MessageBox";
import { SingleRewardCouponDetail } from "../../components/common";

export function CouponDetailsWithRedeemer(props: any) {
  const dispatch = useDispatch<any>();
  const couponId = window.location.pathname.split("/")[2];
  const couponCode = window.location.pathname.split("/")[3];

  console.log("couponId : ", couponId);
  //couponRewardOfferGetDetails
  const couponRewardOfferGetDetails = useSelector(
    (state: any) => state.couponRewardOfferGetDetails
  );
  const {
    loading: loadingCouponDetails,
    error: errorCouponDetails,
    rewardProgramDetails,
  } = couponRewardOfferGetDetails;
  useEffect(() => {
    dispatch(getCouponRewardOfferDetailForBrand(couponId));
  }, []);
  return (
    <Box>
      {loadingCouponDetails ? (
        <HLoading loading={loadingCouponDetails} />
      ) : errorCouponDetails ? (
        <MessageBox variant="danger">{errorCouponDetails}</MessageBox>
      ) : (
        <SingleRewardCouponDetail
          reward={rewardProgramDetails}
          editAllow={false}
          couponCode={couponCode}
        />
      )}
    </Box>
  );
}
