import { Box, Text } from "@chakra-ui/react";
import { useDispatch, useSelector } from "react-redux";
import { UserCoupon } from "../../components/common/UserCoupon";
// import ScratchCard from "react-scratchcard-v3";
// import Coupon from "../../assets/couponimage.jpg";
import HLoading from "../../components/atoms/HLoading";
import MessageBox from "../../components/atoms/MessageBox";
// import { ADD_REDEEMER_TO_COUPON_REWARD_RESET } from "../../Constants/couponRewardOfferConstants";

export function ClameCouponReward(props: any) {
  const dispatch = useDispatch<any>();
  const addRedeemerToCouponReward = useSelector(
    (state: any) => state.addRedeemerToCouponReward
  );
  const {
    loading: loadingAddRedeemerToCouponReward,
    error: errorAddRedeemerToCouponReward,
    success: successAddRedeemerToCouponReward,
    couponReward: couponRewardOffer,
    userCouponDetails,
  } = addRedeemerToCouponReward;
  console.log("couponReward : ", couponRewardOffer);
  console.log("userCouponDetails : ", userCouponDetails);

  return (
    <Box>
      <Text color="#000000">Coupon reward page</Text>
      {loadingAddRedeemerToCouponReward ? (
        <HLoading loading={loadingAddRedeemerToCouponReward} />
      ) : errorAddRedeemerToCouponReward ? (
        <MessageBox variant="danger">
          {errorAddRedeemerToCouponReward}
        </MessageBox>
      ) : (
        <UserCoupon
          reward={couponRewardOffer}
          userCouponDetail={userCouponDetails}
        />
      )}
    </Box>
  );
}
