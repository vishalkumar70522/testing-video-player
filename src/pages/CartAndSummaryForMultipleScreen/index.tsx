import React, { useEffect, useState } from "react";
import {
  Box,
  Divider,
  Flex,
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  Slider,
  SliderFilledTrack,
  SliderMark,
  SliderThumb,
  SliderTrack,
  Stack,
  Text,
  Button,
  SimpleGrid,
  Wrap,
  WrapItem,
  Image,
} from "@chakra-ui/react";
import DateFnsUtils from "@date-io/date-fns"; // choose your lib
import {
  MuiPickersUtilsProvider,
  TimePicker,
  DatePicker,
} from "@material-ui/pickers";
import {
  IconButton as MiuiIconButton,
  InputAdornment,
  Switch,
} from "@material-ui/core";
import { BsCalendar2Date } from "react-icons/bs";
import { useDispatch, useSelector } from "react-redux";
// import { createCamapaign } from "../../Actions/campaignAction";
// import { detailsScreen } from "../../Actions/screenActions";
import { useNavigate, useSearchParams } from "react-router-dom";
import HLoading from "../../components/atoms/HLoading";
import MessageBox from "../../components/atoms/MessageBox";
import { CREATE_CAMPAIGN_RESET } from "../../Constants/campaignConstants";
import { MdGraphicEq } from "react-icons/md";
import { createCamapaignForMultipleScreens } from "../../Actions/campaignForMultipleScreenAction";
import Axios from "axios";

export function CartAndSummaryForMultipleScreen(props: any) {
  const [searchParams, setSearchParams] = useSearchParams();
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();

  const [startTime, setStartTime] = useState<any>(new Date());
  const [startTimeError, setStartTimeError] = useState<any>();
  const [screens, setScreens] = useState<any>([]);
  const [screenError, setScreenError] = useState<any>(null);
  const [cid, setCid] = useState<any>(searchParams?.get("cid") || null);
  const [mediaId, setMediaId] = useState<any>(null);

  const [endTime, setEndTime] = useState<any>(new Date());
  const [endTimeError, setEndTimeError] = useState<any>();

  const [startDate, setStartDate] = useState<any>(new Date());
  const [startDateError, setStartDateError] = useState<any>();

  const [endDate, setEndDate] = useState<any>(new Date());
  const [endDateError, setEndDateError] = useState<any>();
  const [dayWise, setDayWise] = useState<any>(false);

  const [totalSlotBooked, setTotalSlotBooked] = useState<any>(50);
  const [campaignName, setCampaignName] = useState<any>(
    searchParams.get("name") || ""
  );

  console.log("screen  : ", screens);

  const handleEndTime = (value: any) => {
    setEndTime(value);
  };
  const handleStartTime = (value: any) => {
    setStartTime(value);
  };
  const handleEndDate = (value: any) => {
    setEndDate(value);
  };
  const handleStartDate = (value: any) => {
    // let time = value.toString().split(" "); // Wed Jan 04 2023 08:22:28 GMT+0530
    // time = time[4];
    setStartDate(value);
  };
  const createCampaignForMultipleScreen = useSelector(
    (state: any) => state.createCampaignForMultipleScreen
  );
  const {
    loading: loadingSlotBooking,
    error: errorSlotBooking,
    success: successSlotBooking,
    uploadedCampaign,
  } = createCampaignForMultipleScreen;

  if (successSlotBooking) {
    alert("Campaign created successfully");
    dispatch({ type: CREATE_CAMPAIGN_RESET });
    navigate("/");
  }
  const mediaUpload = useSelector((state: any) => state.mediaUpload);
  const {
    loading: loadingMedia,
    media: mediaData,
    success,
    error: errorMedia,
  } = mediaUpload;

  const handleCheckConstraint = () => {
    let response = true;
    if (!startDate) {
      setStartDateError("Please select start date");
      alert("Please select start date");
      response = false;
    } else if (!endDate) {
      setEndDateError("Please select end date");
      alert("Please select end date");
      response = false;
    } else if (!startTime) {
      setStartTimeError("please select start time");
      alert("Please select start time");
      response = false;
    } else if (!endTime) {
      setEndTimeError("Please select end time error");
      alert("Please select end time");
      response = false;
    }
    return response;
  };
  const getNunberOfDays = () => {
    return (
      Number(
        (
          (endDate.getTime() - startDate.getTime()) /
          (1000 * 60 * 60 * 24)
        ).toFixed()
      ) * totalSlotBooked
    );
  };

  const getScreensRent = () => {
    const rent = screens?.reduce((accum: number, currentValue: any) => {
      console.log(
        "accum + currentValue.rentPerSlot * totalSlotBooked : ",
        accum + currentValue.rentPerSlot * totalSlotBooked
      );
      return dayWise
        ? accum + currentValue.rentPerSlot * getNunberOfDays()
        : accum + currentValue.rentPerSlot * totalSlotBooked;
    }, 0);
    console.log("Total rent : ", rent);
    return rent;
  };

  const slotBookingHandler = () => {
    dispatch(
      createCamapaignForMultipleScreens({
        campaignName,
        screens,
        mediaId: mediaId !== "null" ? mediaId : mediaData?._id,
        totalSlotBooked: dayWise ? getNunberOfDays() : totalSlotBooked,
        startDate,
        endDate,
        startTime,
        endTime,
      })
    );
  };
  const getScreenDetail = async (screensId: any) => {
    let screensData: any = [];
    for (let screenId of screensId) {
      try {
        console.log("getScreenDetail : ", screenId);
        const { data } = await Axios.get(
          `${process.env.REACT_APP_BLINDS_SERVER}/api/screens/${screenId}`
        );
        console.log("99999999999999");
        screensData.push(data);
        // setScreens([data, ...screens]);
      } catch (error: any) {
        console.log("Errro : ", error);
        setScreenError(
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        );
      }
    }
    console.log("screensData : ", screensData);
    setScreens([...screensData]);
  };

  useEffect(() => {
    const screens1 = searchParams.get("screens")?.split(",");
    setMediaId(searchParams.get("mediaId"));
    getScreenDetail(screens1);
  }, []);

  return (
    <Box px={{ base: "2", lg: "20" }} pt={{ base: "3", lg: "5" }}>
      {errorMedia || errorSlotBooking || screenError ? (
        <MessageBox variant="danger">
          {errorMedia || errorSlotBooking || screenError}
        </MessageBox>
      ) : null}
      {loadingSlotBooking ? (
        <HLoading loading={loadingSlotBooking} />
      ) : (
        <Box>
          <Stack>
            <Text
              color="#403F49"
              fontSize="xl"
              fontWeight="semibold"
              align="left"
              pt="5"
            >
              Order summary
            </Text>
            <Stack pt="10" direction="row" align="center">
              <Text
                color="#333333"
                fontSize="lg"
                fontWeight="semibold"
                align="left"
              >
                Screen selected
              </Text>
              <Flex pl="10" gap={3}>
                {screens?.length > 0
                  ? screens?.map((screen: any) => (
                      <Text
                        key={screen._id}
                        border="1px"
                        borderColor="#0EBCF5"
                        color="#000000"
                        fontSize="md"
                        fontWeight="semibold"
                        p="2"
                      >
                        {screen?.name}
                      </Text>
                    ))
                  : null}
              </Flex>
            </Stack>
          </Stack>
          <Stack direction="row" pt="5">
            <Stack width="60%">
              <Box
                border="1px"
                borderRadius="md"
                borderColor="rgba(0, 0, 0, 0.5)"
              >
                <Stack
                  width=""
                  borderBottom="1px"
                  borderColor="rgba(0, 0, 0, 0.5)"
                >
                  <Text
                    color="#28282E"
                    fontSize="2xl"
                    fontWeight="semibold"
                    align="left"
                    p="3"
                  >
                    Campaign details
                  </Text>
                </Stack>
                <Stack p="3">
                  <Stack direction="row" pt="3">
                    <FormControl width="60%">
                      <FormLabel htmlFor="share" fontSize="lg" color="#333333">
                        Campaign name
                      </FormLabel>
                      <InputGroup pt="2">
                        <Input
                          name="share"
                          id="share"
                          size="lg"
                          py="2"
                          color="#333333"
                          placeholder="Puma snicks"
                          value={campaignName}
                          onChange={(e) => setCampaignName(e.target.value)}
                        />
                        <Stack
                          align="center"
                          justifyContent="center"
                          width="40%"
                        >
                          <Text
                            color="#333333"
                            fontSize="sm"
                            fontWeight="semibold"
                          >
                            Change
                          </Text>
                        </Stack>
                      </InputGroup>
                    </FormControl>
                  </Stack>
                </Stack>
              </Box>
              <Box
                border="1px"
                borderRadius="md"
                borderColor="rgba(0, 0, 0, 0.5)"
              >
                <Stack
                  width=""
                  borderBottom="1px"
                  borderColor="rgba(0, 0, 0, 0.5)"
                >
                  <Text
                    color="#28282E"
                    fontSize="2xl"
                    fontWeight="semibold"
                    align="left"
                    p="3"
                  >
                    Schedule
                  </Text>
                </Stack>
                <Stack p="3">
                  <FormControl width="60%">
                    <FormLabel htmlFor="share" fontSize="lg" color="#333333">
                      Start time
                    </FormLabel>
                    <InputGroup pt="2">
                      <Flex>
                        <FormControl id="startDateHere" width="50%">
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <DatePicker
                              inputVariant="outlined"
                              value={startDate}
                              onChange={handleStartDate}
                              InputProps={{
                                endAdornment: (
                                  <InputAdornment position="start">
                                    <MiuiIconButton>
                                      <BsCalendar2Date />
                                    </MiuiIconButton>
                                  </InputAdornment>
                                ),
                              }}
                            />
                          </MuiPickersUtilsProvider>
                        </FormControl>
                        <FormControl id="startDateHere" width="30%" pl="5">
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <TimePicker
                              inputVariant="outlined"
                              value={startTime}
                              onChange={handleStartTime}
                            />
                          </MuiPickersUtilsProvider>
                        </FormControl>
                      </Flex>
                    </InputGroup>
                  </FormControl>
                  <FormControl width="60%">
                    <FormLabel htmlFor="share" fontSize="lg" color="#333333">
                      End time
                    </FormLabel>
                    <InputGroup pt="2">
                      <Flex>
                        <FormControl id="startDateHere" width="50%">
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <DatePicker
                              inputVariant="outlined"
                              value={endDate}
                              onChange={handleEndDate}
                              InputProps={{
                                endAdornment: (
                                  <InputAdornment position="end">
                                    <MiuiIconButton>
                                      <BsCalendar2Date />
                                    </MiuiIconButton>
                                  </InputAdornment>
                                ),
                              }}
                            />
                          </MuiPickersUtilsProvider>
                        </FormControl>
                        <FormControl id="startDateHere" width="30%" pl="5">
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <TimePicker
                              inputVariant="outlined"
                              value={endTime}
                              onChange={handleEndTime}
                            />
                          </MuiPickersUtilsProvider>
                        </FormControl>
                      </Flex>
                    </InputGroup>
                  </FormControl>
                </Stack>
              </Box>
              <Box
                border="1px"
                borderRadius="md"
                borderColor="rgba(0, 0, 0, 0.5)"
              >
                <Stack
                  width=""
                  borderBottom="1px"
                  borderColor="rgba(0, 0, 0, 0.5)"
                >
                  <SimpleGrid
                    justifyContent="space-between"
                    pt="3"
                    columns={[1, 1, 2]}
                  >
                    <Text
                      color="#28282E"
                      fontSize="2xl"
                      fontWeight="semibold"
                      align="left"
                      p="3"
                    >
                      Schedule and budget
                    </Text>
                    <FormControl display="flex" alignItems="center">
                      <FormLabel htmlFor="email-alerts" mb="0" color="#4D4D4D">
                        {totalSlotBooked} slots will play per day?
                      </FormLabel>
                      <Switch onChange={() => setDayWise(!dayWise)} />
                    </FormControl>
                  </SimpleGrid>
                </Stack>
                <Stack p="3">
                  <Flex justifyContent="space-between" pt="3">
                    <Stack pt="5" width="70%">
                      <Slider
                        aria-label="slider-ex-6"
                        colorScheme="pink"
                        onChange={(val) => setTotalSlotBooked(val * 2)}
                      >
                        <SliderMark
                          value={totalSlotBooked}
                          textAlign="center"
                          bg="blue.500"
                          color="white"
                          mt="-50px"
                          ml="-5"
                          w="20"
                          p="2"
                        >
                          {totalSlotBooked}
                        </SliderMark>
                        <SliderTrack bg="blue">
                          <SliderFilledTrack />
                        </SliderTrack>
                        <SliderThumb boxSize={6}>
                          <Box color="tomato" as={MdGraphicEq} />
                        </SliderThumb>
                      </Slider>
                    </Stack>
                    <Stack pr="5">
                      <Box
                        border="1px"
                        borderColor="#0EBCF5"
                        color="#333333"
                        fontSize="md"
                        fontWeight="semibold"
                      >
                        <Text p="3">
                          Selected:{" "}
                          {dayWise ? getNunberOfDays() : totalSlotBooked}
                        </Text>
                      </Box>
                    </Stack>
                  </Flex>
                  <Divider pt="5" />
                </Stack>
              </Box>
            </Stack>
            <Stack width="40%" pl="5">
              <Box
                border="1px"
                borderRadius="md"
                borderColor="rgba(0, 0, 0, 0.5)"
              >
                <Stack
                  width=""
                  borderBottom="1px"
                  borderColor="rgba(0, 0, 0, 0.5)"
                >
                  <Text
                    color="#28282E"
                    fontSize="2xl"
                    fontWeight="semibold"
                    align="left"
                    p="3"
                  >
                    Preview
                  </Text>
                </Stack>
                <Stack p="3">
                  <Box pt="2" height={{ height: 50, lg: "200px" }}>
                    <Box as="video" autoPlay loop controls muted boxSize="100%">
                      <source
                        src={
                          cid !== "null"
                            ? `https://ipfs.io/ipfs/${cid}`
                            : mediaData?.media.split("/")[4]
                        }
                      />
                    </Box>
                  </Box>
                </Stack>
              </Box>
              <Box
                border="1px"
                borderRadius="md"
                borderColor="rgba(0, 0, 0, 0.5)"
                p="3"
              >
                <Flex justifyContent="space-between" pt="5">
                  <Text color="#28282E" fontSize="lg" fontWeight="semibold">
                    Total no of slots for each screen
                  </Text>
                  <Text
                    color="#28282E"
                    fontSize="lg"
                    fontWeight="semibold"
                    pr="20"
                  >
                    {dayWise ? getNunberOfDays() : totalSlotBooked}
                  </Text>
                </Flex>
                <Divider pt="5" />
                <Flex justifyContent="space-between" pt="5">
                  <Text color="#28282E" fontSize="lg" fontWeight="semibold">
                    Total cost
                  </Text>
                  <Text
                    color="#28282E"
                    fontSize="lg"
                    fontWeight="semibold"
                    pr="20"
                  >
                    {getScreensRent()}
                  </Text>
                </Flex>
                <Divider pt="5" />

                <Stack pt="5">
                  {mediaId !== "null" ? (
                    <Button
                      color="#FFFFFF"
                      fontSize="xl"
                      fontWeight="semibold"
                      bgColor="#D7380E"
                      p="3"
                      onClick={slotBookingHandler}
                    >
                      Proceed to pay
                    </Button>
                  ) : (
                    <Button
                      color="#FFFFFF"
                      fontSize="xl"
                      fontWeight="semibold"
                      bgColor="#D7380E"
                      isLoading={!(!loadingMedia && success)}
                      loadingText="Creating Media"
                      p="3"
                      onClick={slotBookingHandler}
                    >
                      Proceed to pay
                    </Button>
                  )}
                </Stack>
                <Divider pt="5" />
                <Wrap pt="5">
                  {screens?.map((screen: any) => (
                    <WrapItem>
                      <Image src={screen.image} w="150px" h="80px" />
                    </WrapItem>
                  ))}
                </Wrap>
                <Divider pt="5" />
                <Text color="#888888" fontSize="lg" align="left" pt="3">
                  Your available wallet balance:
                </Text>
                <Text color="#888888" fontSize="lg" align="left" pt="3" pb="3">
                  ₹1212
                </Text>
              </Box>
            </Stack>
          </Stack>
        </Box>
      )}
    </Box>
  );
}
