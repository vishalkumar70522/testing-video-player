import {
  Box,
  Text,
  Stack,
  FormControl,
  FormLabel,
  InputGroup,
  Input,
  SimpleGrid,
  Image,
  InputRightElement,
  IconButton,
  Center,
  Flex,
  Tooltip,
  Button,
} from "@chakra-ui/react";
import MessageBox from "../../components/atoms/MessageBox";
import { motion, useAnimation } from "framer-motion";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { IoSearchCircleSharp } from "react-icons/io5";
import { filteredScreenList } from "../../Actions/screenActions";
import { AiFillStar, AiOutlinePlus } from "react-icons/ai";
import { BsDot } from "react-icons/bs";
import { BiRupee } from "react-icons/bi";
import { uploadMedia } from "../../Actions/mediaActions";
import { createCamapaignForMultipleScreens } from "../../Actions/campaignForMultipleScreenAction";
import { userMediasList } from "../../Actions/userActions";
import { useNavigate } from "react-router-dom";

export function AddNewCampaign() {
  const navigate = useNavigate();
  const controls = useAnimation();
  const dispatch = useDispatch<any>();
  const MotionFlex = motion(Flex);
  const [campaignName, setCampaignName] = useState<any>("");
  const [fileUrl, setFileUrl] = useState<any>();
  const [searchText, setSearchText] = useState<any>("");
  const [filteredData, setFilteredData] = useState<any>([]);
  const [selectedMedia, setSelectedMedia] = useState<any>("");
  const [errorMsg, setErrorMsg] = useState("");
  const [mediaId, setMediaId] = useState<any>("");
  const [isOldMedia, setIsOldMedia] = useState<any>(false);
  const [selectedScreens, setSelectedScreens] = useState<any>([]);

  console.log("selected screen  : ", selectedScreens);

  const myMedia = useSelector((state: any) => state.userMedia);
  const {
    loading: loadingMyMedia,
    error: errorMyMedia,
    success: mediaSuccess,
    medias,
  } = myMedia;
  const createCampaignForMultipleScreen = useSelector(
    (state: any) => state.createCampaignForMultipleScreen
  );
  const {
    loading: loadingSlotBooking,
    error: errorSlotBooking,
    success: successSlotBooking,
    uploadedCampaign,
  } = createCampaignForMultipleScreen;
  console.log("uploadedCampaign : ", JSON.stringify(uploadedCampaign));
  console.log("errorSlotBooking : ", errorSlotBooking);

  console.log("mymedia :::: ", medias);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const filterScreenList = useSelector((state: any) => state.filterScreenList);
  const {
    loading: loadingScreens,
    error: errorScreens,
    screens,
  } = filterScreenList;
  const mediaUpload = useSelector((state: any) => state.mediaUpload);
  const {
    loading: loadingMedia,
    media: mediaData,
    success,
    error: errorMedia,
  } = mediaUpload;
  console.log("created media data : ", mediaData);
  console.log("created media data errorMedia: ", errorMedia);

  let hiddenInput: any = null;
  const startAnimation = () => controls.start("hover");
  const stopAnimation = () => controls.stop();

  const handleSelectMedia = (media: any) => {
    setMediaId(media._id);
    //console.log("media selected : ", media.media);
    setSelectedMedia(media.media);
    setIsOldMedia(true);
  };

  const handleSendRequest = () => {
    //case 1: select screen more then 1
    if (selectedScreens.length <= 1) {
      setErrorMsg("Please select screen more then 1");
    }
    dispatch(
      createCamapaignForMultipleScreens({
        campaignName,
        screens: selectedScreens,
        mediaId: mediaData._id,
      })
    );
  };

  const validateSelectedFile = (file: any) => {
    const MIN_FILE_SIZE = 1024; // 1MB
    const MAX_FILE_SIZE = 1024 * 50; // 5MB
    const fileExtension = file.type.split("/")[1];

    const fileSizeKiloBytes = file.size / 1024;

    if (fileSizeKiloBytes < MIN_FILE_SIZE) {
      setErrorMsg("File size is less than minimum limit");
      return false;
    }
    if (fileSizeKiloBytes > MAX_FILE_SIZE) {
      setErrorMsg(
        "File size is greater than maximum limit. File size must be less then 50 MB "
      );
      return false;
    }
    if (fileExtension !== "mp4") {
      setErrorMsg("File format must be .mp4");
      return false;
    }
    console.log("every thing is correct!");
    return true;
  };
  if (errorMsg) {
    setTimeout(() => setErrorMsg(""), 5000);
  }
  const addOrRemoveScreens = (screen: any) => {
    if (selectedScreens.includes(screen)) {
      // console.log("removing........", lable);
      setSelectedScreens([
        ...selectedScreens.filter((data: any) => data !== screen),
      ]);
    } else {
      // console.log("adding........", screen);
      setSelectedScreens([...selectedScreens, screen]);
    }
  };
  function handleFileSelect(file: any) {
    console.log("handleFileSelect : ", file);
    if (validateSelectedFile(file)) {
      const fileURL = URL.createObjectURL(file);
      setFileUrl(file);
      setIsOldMedia(false);
      setSelectedMedia(fileURL);
    }
  }
  const handleContinue = () => {
    if (!selectedScreens) {
      alert("First select media or video or Enmter campaign name");
    } else if (!campaignName) {
      alert("Enmter campaign name");
    } else if (!(selectedScreens?.length >= 1)) {
      alert("Find Screens and select screens");
    } else {
      if (isOldMedia) {
        navigate(
          `/cartsForMultipleScreens?screens=${selectedScreens}&mediaId=${mediaId}&name=${campaignName}&cid=${
            selectedMedia?.split("/")[4]
          }`
        );
      } else {
        dispatch(
          uploadMedia({
            title: campaignName,
            thumbnail:
              "https://bafybeicduvlghzcrjtuxkro7foazucvuyej25rh3humeujbzt7bmio4hsa.ipfs.w3s.link/raily.png",
            fileUrl: fileUrl,
            media: "",
          })
        );
        navigate(
          `/cartsForMultipleScreens?screens=${selectedScreens}&mediaId=${null}&name=${campaignName}&cid=${null}`
        );
      }
    }
  };
  useEffect(() => {
    dispatch(userMediasList(userInfo));
  }, []);
  useEffect(() => {
    if (loadingScreens === false && screens?.length > 0) {
      setFilteredData(screens);
    }
  }, [loadingScreens]);

  return (
    <Box color="black.500">
      <Text>Add new Campaign</Text>
      <Box bgColor="#FFFFFF">
        <Stack p="10">
          <Stack align="center">
            {errorMsg ? (
              <MessageBox variant="danger">{errorMsg}</MessageBox>
            ) : null}
          </Stack>
          <FormControl pt="5">
            <FormLabel htmlFor="share" fontSize="lg">
              Campaign Name
            </FormLabel>
            <InputGroup>
              <Input
                name="share"
                id="share"
                size="lg"
                color="#333333"
                placeholder="Puma snicks"
                value={campaignName}
                onChange={(e) => setCampaignName(e.target.value)}
              />
            </InputGroup>
          </FormControl>
          <FormControl pt="5">
            <FormLabel htmlFor="share" fontSize="lg">
              Upload content
            </FormLabel>
            <InputGroup>
              <Box
                height="90px"
                width="100%"
                border="1px"
                justifyContent="center"
                onClick={() => hiddenInput.click()}
              >
                <Text
                  align="center"
                  fontSize="lg"
                  fontWeight="semibold"
                  color="#000000"
                  p="8"
                >
                  Drag & Drop{" "}
                </Text>
                <Input
                  hidden
                  type="file"
                  ref={(el) => (hiddenInput = el)}
                  // accept="image/png, image/jpeg"
                  onDragEnter={startAnimation}
                  onDragLeave={stopAnimation}
                  onChange={(e: any) => handleFileSelect(e.target.files[0])}
                />
              </Box>
            </InputGroup>
          </FormControl>
          {selectedMedia ? (
            <Box
              as="video"
              src={selectedMedia}
              autoPlay
              loop
              muted
              display="inline-block"
              borderRadius="24px"
              //   height={{ base: "100%", lg: "1000px" }}
              width="1000px"
            ></Box>
          ) : null}

          <Text
            fontSize="lg"
            fontWeight="light"
            color="#000000"
            align="left"
            pt="5"
          >
            Or select content
          </Text>
          <Stack pt="5">
            <SimpleGrid columns={[1, null, 6]} spacing={3}>
              {medias?.map((media: any) => (
                <Stack
                  key={media._id}
                  borderRadius="lg"
                  onClick={() => handleSelectMedia(media)}
                >
                  <Box
                    as="video"
                    src={media.media}
                    autoPlay
                    loop
                    muted
                    display="inline-block"
                    borderRadius="24px"
                    height={{ base: "100%", lg: "100%" }}
                  ></Box>
                </Stack>
              ))}
            </SimpleGrid>
          </Stack>
        </Stack>
      </Box>
      <Center>
        <Stack
          zIndex="1"
          align="center"
          fontFamily="sans"
          width={{ base: "90%", lg: "65%" }}
        >
          <InputGroup size="lg" width="100%" mt={{ base: "1px", lg: "10px" }}>
            <Input
              type="text"
              py={{ base: "1", lg: "5" }}
              bgColor="#FCFCFC"
              borderRadius="80px"
              fontSize="lg"
              value={searchText}
              onChange={(e: any) => setSearchText(e.target.value)}
              onKeyPress={(e: any) => {
                if (e.which === 13) {
                  dispatch(filteredScreenList(searchText));
                }
              }}
              placeholder="Search by place, by location, by screen names"
            />
            <InputRightElement
              width="4.5rem"
              pr={{ base: "0", lg: "1" }}
              pt={{ base: "0", lg: "2.5" }}
            >
              <IconButton
                bg="none"
                icon={
                  <IoSearchCircleSharp
                    size="45px"
                    color="#D7380E"
                    onClick={() => dispatch(filteredScreenList(searchText))}
                  />
                }
                aria-label="Edit user details"
              />
            </InputRightElement>
          </InputGroup>
        </Stack>
      </Center>
      <Stack>{selectedScreens}</Stack>
      <Stack px="20" py="5">
        <SimpleGrid columns={[1, 2, 3]} spacing="4">
          {filteredData.length > 0 ? (
            filteredData?.map((screen: any) => (
              <MotionFlex
                key={screen._id}
                flexDir="column"
                w="100%"
                role="group"
                rounded="md"
                // shadow="card"
                whileHover={{
                  translateY: -3,
                }}
                pos="relative"
                zIndex="0"
              >
                <Box
                  bgColor={
                    selectedScreens.includes(screen._id) ? "#D6FFFF" : "#F7F7F7"
                  }
                  borderColor="#DFDFDF"
                  border="1.5px"
                  width="100%"
                  height="100%"
                  borderRadius="lg"
                  boxShadow="2xl"
                  key={screen._id}
                  onClick={() => addOrRemoveScreens(screen._id)}
                >
                  {/* image */}
                  <Box p="2" height={{ height: 50, lg: "200px" }}>
                    <Image
                      width="100%"
                      height="240px"
                      borderRadius="10px"
                      src={screen?.image}
                      // onLoad={() => triggerPort(screen?.image?.split("/").slice(-1)[0])}
                    />
                  </Box>
                  {/* details of screem */}
                  <Stack p="2" pb="4">
                    {/* Name */}
                    <Flex>
                      <Text
                        color="#403F49"
                        fontSize="lg"
                        fontWeight="bold"
                        align="left"
                        width="85%"
                      >
                        {screen.name}
                      </Text>
                      <IconButton
                        bg="none"
                        icon={<AiOutlinePlus size="25px" color="#403F49" />}
                        aria-label="Edit user details"
                      />
                    </Flex>
                    {/* Ratings */}
                    <Flex align="center" justifyContent="space-between" p="">
                      <Flex>
                        <IconButton
                          bg="none"
                          icon={<AiFillStar size="16px" color="#403F49" />}
                          aria-label="Star"
                        ></IconButton>
                        <Text
                          pl="1"
                          color="#403F49"
                          fontSize="sm"
                          fontWeight="semibold"
                          align="left"
                        >
                          {screen.rating}
                        </Text>
                      </Flex>
                      <Flex>
                        <IconButton
                          bg="none"
                          icon={<BsDot size="16px" color="#403F49" />}
                          aria-label="Star"
                        ></IconButton>
                        <Text
                          color="#403F49"
                          fontSize="xs"
                          fontWeight="semibold"
                          align="left"
                        >
                          2120 slots available
                        </Text>
                      </Flex>
                    </Flex>
                    {/* Cost */}
                    <Flex align="center" justify="space-between">
                      <Flex align="center">
                        <IconButton
                          bg="none"
                          icon={<BiRupee size="16px" color="#403F49" />}
                          aria-label="Star"
                        ></IconButton>
                        <Text
                          color="#403F49"
                          fontSize="xs"
                          fontWeight="semibold"
                          align="left"
                        >
                          {`${screen.rentPerSlot}/slot`}
                        </Text>
                      </Flex>
                      <Flex align="center" as="s">
                        <Text
                          color="#787878"
                          fontSize="xs"
                          fontWeight="semibold"
                          align="left"
                        >
                          ₹
                          {screen.rentPerSlot - screen.rentOffInPercent
                            ? (screen.rentPerSlot * 100) /
                              screen.rentOffInPercent
                            : 0}{" "}
                          per slot
                        </Text>
                      </Flex>
                      <Text
                        pl="1"
                        color="#F86E6E"
                        fontSize="xs"
                        fontWeight="semibold"
                        align="left"
                      >
                        ( {screen.rentOffInPercent}% OFF)
                      </Text>
                    </Flex>
                    {/* Address */}
                    <Tooltip
                      label={`${screen.screenAddress}, ${screen.districtCity}, ${screen.country}`}
                      aria-label="A tooltip"
                    >
                      <Stack align="left">
                        <Text
                          color="#666666"
                          fontSize="xs"
                          fontWeight="semibold"
                          align="left"
                          isTruncated
                        >
                          {`${screen.screenAddress}`}
                        </Text>
                        <Text
                          color="#666666"
                          fontSize="xs"
                          fontWeight="semibold"
                          align="left"
                        >
                          {`${screen.districtCity}, ${screen.country}`}
                        </Text>
                      </Stack>
                    </Tooltip>
                  </Stack>
                </Box>
              </MotionFlex>
            ))
          ) : (
            <Text color="red" fontSize="md">
              No result Found
            </Text>
          )}
        </SimpleGrid>
      </Stack>
      <Stack align="center">
        <Button
          width="350px"
          height="54px"
          color="#FFFFFF"
          bgColor="#D7380E"
          fontWeight="semibold"
          // isLoading={!(!loadingMedia && success)}
          fontSize="xl"
          onClick={handleContinue}
        >
          Continue
        </Button>
      </Stack>
    </Box>
  );
}
