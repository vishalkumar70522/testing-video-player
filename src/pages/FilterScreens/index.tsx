import {
  Stack,
  Input,
  Center,
  InputRightElement,
  IconButton,
  InputGroup,
  SimpleGrid,
  Flex,
  Text,
  Button,
  HStack,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { IoSearchCircleSharp } from "react-icons/io5";
// import Axios from "axios";
import { motion } from "framer-motion";
import { Screen } from "../../components/common";
import { useDispatch, useSelector } from "react-redux";
import HLoading from "../../components/atoms/HLoading";
import MessageBox from "../../components/atoms/MessageBox";
import { filteredScreenList } from "../../Actions/screenActions";

export function FilterScreens() {
  const MotionFlex = motion(Flex);
  const dispatch = useDispatch<any>();
  const [searchText, setSearchText] = useState<any>("");
  const [filteredData, setFilteredData] = useState<any>([]);
  const [filterConditions, setFilterConditions] = useState<any>([]);
  const [result, setResult] = useState<any>([]);

  const filterScreenList = useSelector((state: any) => state.filterScreenList);
  const {
    loading: loadingScreens,
    error: errorScreens,
    screens,
  } = filterScreenList;

  const filterData = () => {
    const result = filteredData.filter((data: any) => {
      let flag = true;
      // console.log("insize filter : ", filterConditions);
      for (let level of filterConditions) {
        if (data[level].toLowerCase().includes(searchText.toLowerCase())) {
          // console.log("search : ", data[level]);
        } else {
          flag = false;
        }
      }
      if (flag) {
        return data;
      }
    });
    // console.log("result1 : ", result);
    setResult([...result]);
  };

  const getScreenListByFilter = async () => {
    // try {
    //   console.log("getScreenListByFilter called! : ", searchText);
    //   if (searchText) {
    //     const { data } = await Axios.get(
    //       `${process.env.REACT_APP_BLINDS_SERVER}/api/screens/getFilterScreenList/${searchText}`
    //     );
    //     if (data.length > 0) setFilteredData([...data]);
    //     else alert("No result found!");
    //     console.log("Date : ", JSON.stringify(data));
    //   } else {
    //     alert("First enter your text for search");
    //   }
    // } catch (error) {
    //   console.log("Error : ", error);
    // }
    // console.log("search text  getScreenListByFilter : ", searchText);
    dispatch(filteredScreenList(searchText));
  };
  const addOrRemoveConditions = (lable: any) => {
    if (filterConditions.includes(lable)) {
      // console.log("removing........", lable);
      setFilterConditions([
        ...filterConditions.filter((data: any) => data !== lable),
      ]);
    } else {
      // console.log("adding........", lable);
      setFilterConditions([...filterConditions, lable]);
    }
  };
  useEffect(() => {
    const text = window.location.pathname.split("/")[2];
    setSearchText(text);
  }, []);
  useEffect(() => {
    if (loadingScreens === false && screens?.length > 0) {
      setFilteredData(screens);
    }
  }, [loadingScreens]);
  return (
    <Stack color="black.500" px={{ base: "2", lg: "20" }}>
      <Center>
        <Stack
          zIndex="1"
          align="center"
          fontFamily="sans"
          width={{ base: "90%", lg: "65%" }}
        >
          <InputGroup size="lg" width="100%" mt={{ base: "1px", lg: "10px" }}>
            <Input
              type="text"
              py={{ base: "1", lg: "5" }}
              bgColor="#FCFCFC"
              borderRadius="80px"
              fontSize="lg"
              value={searchText}
              onChange={(e: any) => setSearchText(e.target.value)}
              onKeyPress={(e: any) => {
                if (e.which === 13) {
                  getScreenListByFilter();
                }
              }}
              placeholder="Search by place, by location, by screen names"
            />
            <InputRightElement
              width="4.5rem"
              pr={{ base: "0", lg: "1" }}
              pt={{ base: "0", lg: "2.5" }}
            >
              <IconButton
                bg="none"
                icon={
                  <IoSearchCircleSharp
                    size="45px"
                    color="#D7380E"
                    onClick={getScreenListByFilter}
                  />
                }
                aria-label="Edit user details"
              />
            </InputRightElement>
          </InputGroup>
        </Stack>
      </Center>

      <Stack>
        <HStack>
          <Button
            py="3"
            variant="outline"
            onClick={() => {
              addOrRemoveConditions("name");
              //   setSearchByName(!searchByName);
            }}
            bgColor={filterConditions.includes("name") ? "#D6FFFF" : "#FAFAFA"}
          >
            By screen name
          </Button>
          <Button
            py="3"
            variant="outline"
            onClick={() => {
              addOrRemoveConditions("districtCity");
              //   setSearchByCity(!searchByCity);
            }}
            bgColor={
              filterConditions.includes("districtCity") ? "#D6FFFF" : "#FAFAFA"
            }
          >
            By city
          </Button>
          <Button
            py="3"
            variant="outline"
            onClick={() => {
              addOrRemoveConditions("stateUT");
              //   setSearchByState(!searchByState);
            }}
            bgColor={
              filterConditions.includes("stateUT") ? "#D6FFFF" : "#FAFAFA"
            }
          >
            By state
          </Button>
          <Button
            py="3"
            variant="outline"
            onClick={() => {
              addOrRemoveConditions("screenAddress");
              //   setSearchByAddress(!searchByAddress);
            }}
            bgColor={
              filterConditions.includes("screenAddress") ? "#D6FFFF" : "#FAFAFA"
            }
          >
            By Address
          </Button>
          <Button
            py="3"
            variant="outline"
            onClick={() => {
              addOrRemoveConditions("country");
              //   setSearchByCountry(!searchByCountry);
            }}
            bgColor={
              filterConditions.includes("country") ? "#D6FFFF" : "#FAFAFA"
            }
          >
            By Country
          </Button>
          <Button py="3" variant="outline" onClick={filterData} color="#F23232">
            Filter
          </Button>
        </HStack>
      </Stack>
      {loadingScreens ? (
        <HLoading loading={loadingScreens} />
      ) : errorScreens ? (
        <MessageBox variant="danger">{errorScreens}</MessageBox>
      ) : (
        <SimpleGrid columns={[1, 2, 3]} spacing="4">
          {filterConditions.length > 0 && result.length > 0 ? (
            result?.map((eachScreen: any) => (
              <MotionFlex
                key={eachScreen._id}
                flexDir="column"
                w="100%"
                role="group"
                rounded="md"
                // shadow="card"
                whileHover={{
                  translateY: -3,
                }}
                pos="relative"
                zIndex="0"
              >
                <Screen eachScreen={eachScreen} key={eachScreen._id} />
              </MotionFlex>
            ))
          ) : filterConditions.length > 0 && result.length == 0 ? (
            <Text color="red" fontSize="md">
              No result Found
            </Text>
          ) : filteredData.length > 0 ? (
            filteredData?.map((eachScreen: any) => (
              <MotionFlex
                key={eachScreen._id}
                flexDir="column"
                w="100%"
                role="group"
                rounded="md"
                // shadow="card"
                whileHover={{
                  translateY: -3,
                }}
                pos="relative"
                zIndex="0"
              >
                <Screen eachScreen={eachScreen} key={eachScreen._id} />
              </MotionFlex>
            ))
          ) : (
            <Text color="red" fontSize="md">
              No result Found
            </Text>
          )}
        </SimpleGrid>
      )}
    </Stack>
  );
}
