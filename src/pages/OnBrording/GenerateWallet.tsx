import { Box, Button } from "@chakra-ui/react";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import { useWallet } from "../../components/contexts";
export function GenerateWallet(props: any) {
  const navigate = useNavigate();
  const userSignin = useSelector((state: any) => state.userSignin);

  const {
    loading: loadingUserInfo,
    error: errorUserInfo,
    userInfo,
  } = userSignin;
  console.log("userInfo : ", userInfo);
  const {
    getTempSavedPin,
    getArweavePublicAddress,
    tempSavePin,
    wipeTempSavedPin,
    generateAndSave,
    setupPin,
  } = useWallet();
  const handleCreateWallet = async () => {
    //save pin as temprory
    const createPincode = "222222";
    await tempSavePin(createPincode);
    await getTempSavedPin();

    // start createing wallet
    getTempSavedPin().then((pin: string | null) => {
      if (pin) {
        setupPin(pin)
          .then(() => wipeTempSavedPin())
          .then(() => generateAndSave(pin, {}));
        const defWallet = getArweavePublicAddress();
      }
    });
  };

  useEffect(() => {
    if (userInfo && !userInfo.defaultWallet) {
      // console.log("userInfo  page : ", userInfo);
      navigate("/welcome");
    } else if (!userInfo) {
      navigate("/signin");
    }
  }, [userInfo]);

  return (
    <Box p="20">
      <Button onClick={handleCreateWallet}>Start Creating wallet</Button>
    </Box>
  );
}
