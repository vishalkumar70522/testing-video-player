import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { Stack, IconButton, Text, Box, Button, Flex } from "@chakra-ui/react";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { useNavigate } from "react-router-dom";
import { SingleRewardCouponDetail } from "../../components/common/SingleRewardCouponDetail";

export function ConformationModel(props: any) {
  const navigate = useNavigate();
  const reward = props.reward;
  const [isViewModel, setIsViewModel] = useState<any>(false);

  useEffect(() => {
    // console.log("reward data : ", props.rewardprogram);
  }, []);

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      backdrop="static"
      keyboard={false}
      dialogClassName="modal-90w"
    >
      <Modal.Body>
        <Box bgColor="#FFFFFF">
          <Stack align="end" justifyContent="flex-end">
            <IconButton
              bg="none"
              icon={
                <AiOutlineCloseCircle
                  size="40px"
                  fontWeight="10"
                  color="black"
                  onClick={() => props.onHide()}
                />
              }
              aria-label="Close"
            />
          </Stack>
          <Stack pl="20" pr="20">
            <Text color="#000000" fontSize="lg" fontWeight="bold">
              {" "}
              Chose your action
            </Text>
            {isViewModel ? (
              <Stack>
                <SingleRewardCouponDetail reward={reward} />
              </Stack>
            ) : null}
            <Flex gap={4} pb="10" justifyContent="space-around">
              <Button p="3" onClick={() => setIsViewModel(true)}>
                View Details
              </Button>
              <Button
                p="3"
                onClick={() => navigate(`/editRewardAndOffer/${reward?._id}`)}
              >
                Edit
              </Button>
            </Flex>
          </Stack>
        </Box>
      </Modal.Body>
    </Modal>
  );
}
