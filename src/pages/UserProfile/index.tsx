import { Box, Stack, Image, Text, Button, SimpleGrid } from "@chakra-ui/react";
import HLoading from "../../components/atoms/HLoading";
import { UserCollections } from "../../components/common/UserCollections";
import React, { useEffect, useState } from "react";
import { RiFileUploadLine } from "react-icons/ri";
import { useDispatch, useSelector } from "react-redux";
import { FcNext, FcPrevious } from "react-icons/fc";
import Axios from "axios";
import { useNavigate } from "react-router-dom";
import { userCampaignsList, userScreensList } from "../../Actions/userActions";
import MessageBox from "../../components/atoms/MessageBox";
import { MySingleCampaign, Screen } from "../../components/common";
import {
  createRewardProgram,
  myCreatedRewardPrograms,
} from "../../Actions/rewardAndOffer";
// import { SingleRewardCouponDetail } from "../../components/common/SingleRewardCouponDetail";
import { SingleRewardData } from "../../components/common/SingleRewardData";
import ScratchCard from "react-scratchcard-v3";
import Coupon from "../../assets/couponimage.jpg";
import { ConformationModel } from "./ConformationModel";

export function UserProfile() {
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();
  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;
  const [loadingMyMedis, setLoadingMyMedia] = useState<any>(true);
  const [errorMyMedias, setErrorMyMedias] = useState<any>("");
  const [startValue, setStartValue] = useState<any>(0);
  const [endValue, setEndValue] = useState<any>(6);
  const [allMedias, setAllMedias] = useState<any>([]);
  // const [screens, setScreens] = useState<any>([]);
  // const [campaigns, setCampaigns] = useState<any>([]);
  const [activeState, setActiveState] = useState<any>(0); // 0 for default 1 for master 2 for ally
  const [ConformationModelShow, setConformationModelShow] =
    useState<boolean>(false);
  const [selectedReward, setSelectedReward] = useState<any>({});

  //console.log("userInfo 423432423", JSON.stringify(userInfo));
  const userScreens = useSelector((state: any) => state.userScreens);
  const { loading: loadingScreens, error: errorScreens, screens } = userScreens;
  const mediaUpload = useSelector((state: any) => state.mediaUpload);
  const {
    loading: loadingmediaUpload,
    error: errormediaUpload,
    media,
  } = mediaUpload;
  const userCampaign = useSelector((state: any) => state.userCampaign);
  const {
    loading: loadingMyVideos,
    error: errorMyVideos,
    campaign,
  } = userCampaign;
  const myRewardProgramsCreated = useSelector(
    (state: any) => state.myRewardProgramsCreated
  );
  const {
    loading: loadingMyRewardPrograms,
    error: errorMyRewardPrograms,
    myRewardPrograms,
  } = myRewardProgramsCreated;
  const rewardProgramCreate = useSelector(
    (state: any) => state.rewardProgramCreate
  );
  const {
    loading: loadingRewardProgramCreate,
    error: errorRewardProgramCreate,
    success: successRewardCreated,
    rewardProgram,
  } = rewardProgramCreate;

  const handelMaster = () => {
    setActiveState(1);
    setStartValue(0);
    setEndValue(6);
    dispatch(userScreensList(userInfo));
  };

  const handelAlly = () => {
    setActiveState(2);
    setStartValue(0);
    setEndValue(5);
    dispatch(userCampaignsList(userInfo));
  };
  const handelRewardsList = () => {
    setActiveState(3);
    setStartValue(0);
    setEndValue(8);
    dispatch(myCreatedRewardPrograms());
  };
  const handleUserViewRewardList = () => {
    setActiveState(4);
    setStartValue(0);
    setEndValue(8);
    dispatch(myCreatedRewardPrograms());
  };

  const handleShowConformation = async (reward: any) => {
    console.log("handleShowConformation called!");
    await setSelectedReward(reward);
    setConformationModelShow(true);
  };

  const getMediaList = async () => {
    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/users/${userInfo._id}/${userInfo.defaultWallet}/myMedias`,
        {
          headers: { Authorization: "Bearer " + userInfo.token },
        }
      );
      setAllMedias(data);
      setLoadingMyMedia(false);
      //console.log("all medias 323232: ", data);
    } catch (error: any) {
      setErrorMyMedias(
        error.response && error.response.data.message
          ? error.response.data.messages
          : error.message
      );
      setLoadingMyMedia(false);
    }
  };
  useEffect(() => {
    if (successRewardCreated) {
      console.log("reward created successfull! : ", rewardProgram);
      navigate(`/editRewardAndOffer/${rewardProgram._id}`);
    }
  }, [successRewardCreated]);

  useEffect(() => {
    if (userInfo) getMediaList();
    else navigate("/signin");
  }, []);
  return (
    <Box pt={{ base: "3", lg: "5" }} height="150%">
      {/* <CreateNewCampaign
        show={campaignModal}
        onHide={() => setCampaignModal(false)}
        openUploadCampaignModal={() => setUploadCampaignModal(true)}
        setCampaignName={(e: any) => setCampaignName(e.target.value)}
        campaignName={campaignName}
        openUploadCampaignThrougnImages={() => {}}
      />
      <UploadCampaign
        show={uploadCampaignModal}
        onHide={() => setUploadCampaignModal(false)}
        setFileUrl={(value: any) => setFileUrl(value)}
        medias={[]}
        setIsOldMedia={(value: any) => setIsOldMedia(value)}
        isCameFromUserProfile={true}
        uploadMediaFromUserProfile={uploadMediaFromUserProfile}
      /> */}
      <ConformationModel
        show={ConformationModelShow}
        onHide={() => setConformationModelShow(false)}
        user={userInfo}
        reward={selectedReward}
      />
      <Box
        width="100%"
        height="350px"
        display="inline-block"
        backgroundImage="url(https://bafybeicbr46w37z6eoazs2eokarvx53dmqcsutvbryiu3jbygiee5fhq5a.ipfs.w3s.link/Rectangle.png)"
        backgroundRepeat="no-repeat"
        // backgroundAttachment="fixed"
        backgroundSize="cover"
        position="relative"
      >
        <SimpleGrid columns={[1, null, 2]} spacing="" pt="5">
          <Box
            pt="200"
            width={{ base: "100%", lg: "60%" }}
            height=""
            pl={{ base: "3", lg: "20" }}
            pr={{ base: "3" }}
          >
            <Stack
              bgColor="#FEFEFE"
              p="5"
              boxShadow="2xl"
              position="relative"
              borderRadius="10px"
            >
              <Stack align="center" pt="5">
                <Image
                  alt="user Image"
                  borderRadius="full"
                  boxSize="150px"
                  //src="https://bafybeifp4ffz7qj5fhfeejtnuofbke77rh5tsqxctbnsxxg4uzko6yxwhq.ipfs.w3s.link/userprofile.png"
                  src={userInfo.avatar}
                ></Image>
              </Stack>
              <Stack>
                <Text
                  color="#313131"
                  fontSize="xl"
                  fontWeight="semibold"
                  align="left"
                >
                  {userInfo.name}
                </Text>
                <Text color="#593FFC" fontSize="lg" align="left">
                  {userInfo.email}
                </Text>
                <Text color="#313131" fontSize="lg" align="left">
                  +91-{userInfo.phone}
                </Text>
                <Text color="#747474" fontSize="lg" align="left">
                  {`${userInfo.districtCity}, Pincode-${userInfo.pincode}`}
                </Text>
              </Stack>
              <Stack align="cetner" justifyContent="center" pt="5">
                <Button
                  color="#FFFFFF"
                  fontSize="xl"
                  fontWeight="semibold"
                  bgColor="#D7380E"
                  // width="100%"
                  p="3"
                  onClick={() => navigate(`/editProfile`)}
                >
                  Edit profile
                </Button>
              </Stack>
              <Stack align="cetner" justifyContent="center" pt="5">
                <Button
                  color="#313131"
                  fontSize="xl"
                  fontWeight="semibold"
                  bgColor="#F1F1F1"
                  // width="100%"
                  p="3"
                  alignItems="center"
                  //onClick={() => setCampaignModal(true)}
                >
                  Upload NFT{" "}
                  <Stack pl="3">
                    <RiFileUploadLine size="20px" color="black" />
                  </Stack>
                </Button>
              </Stack>
              <Stack align="cetner" justifyContent="center" pt="5">
                <Button
                  color="#313131"
                  fontSize="xl"
                  fontWeight="semibold"
                  bgColor="#F1F1F1"
                  // width="100%"
                  p="3"
                  onClick={handelMaster}
                >
                  Master
                </Button>
              </Stack>
              <Stack align="cetner" justifyContent="center" pt="5">
                <Button
                  color="#313131"
                  fontSize="xl"
                  fontWeight="semibold"
                  bgColor="#F1F1F1"
                  // width="100%"
                  p="3"
                  onClick={handelAlly}
                >
                  Ally
                </Button>
              </Stack>
              <Stack align="cetner" justifyContent="center" pt="5">
                <Button
                  color="#313131"
                  fontSize="xl"
                  fontWeight="semibold"
                  bgColor="#F1F1F1"
                  // width="100%"
                  p="3"
                  onClick={() => dispatch(createRewardProgram())}
                >
                  Create Reward and offer
                </Button>
              </Stack>
              <Stack align="cetner" justifyContent="center" pt="5">
                <Button
                  color="#313131"
                  fontSize="xl"
                  fontWeight="semibold"
                  bgColor="#F1F1F1"
                  // width="100%"
                  p="3"
                  onClick={handelRewardsList}
                >
                  Brand View created rewards
                </Button>
              </Stack>
              <Stack align="cetner" justifyContent="center" pt="5">
                <Button
                  color="#313131"
                  fontSize="xl"
                  fontWeight="semibold"
                  bgColor="#F1F1F1"
                  // width="100%"
                  p="3"
                  onClick={handleUserViewRewardList}
                >
                  User View created rewards
                </Button>
              </Stack>
              <Stack pt="20"></Stack>
            </Stack>
          </Box>
          <Box
            pt={{ base: "100%", lg: "350" }}
            width={{ base: "100%", lg: "100%" }}
            height=""
            pr={{ base: "3", lg: "20" }}
          >
            {loadingMyMedis ||
            loadingScreens ||
            loadingMyVideos ||
            loadingMyRewardPrograms ? (
              <HLoading
                loading={
                  loadingMyMedis ||
                  loadingScreens ||
                  loadingMyVideos ||
                  loadingMyRewardPrograms
                }
              />
            ) : errorScreens || errorMyVideos || errorMyRewardPrograms ? (
              <MessageBox variant="danger">
                {errorScreens || loadingMyVideos || errorMyRewardPrograms}
              </MessageBox>
            ) : (
              <Box>
                {activeState === 0 ? (
                  <Stack align="left">
                    <Text
                      color="#000000"
                      fontSize="2xl"
                      fontWeight="semibold"
                      align="left"
                    >
                      Collections
                    </Text>
                    {allMedias?.length === 0 ? (
                      <Stack>
                        <Text color="#3A3A3A" fontSize="xl" align="left">
                          Upload your first collection
                        </Text>
                        <Image
                          src="https://s3-alpha-sig.figma.com/img/ca29/f93f/58db42ff92848405ab0da2be110fd228?Expires=1678665600&Signature=TVT1uDTDsAjPuSIDQ2wNMp-KUGgX6DdL5UwXT2BhPB6b6gRVfUlaUSatCC2hReLXKIHp9ABYmAHK7JXW3qm5FSqWCfUUqQC3OFgfSqcwLY7x1Di3ezJtAyX9pWcE6Vg7jVCP8bS31vnDt00rXnxGPGbah3hz7CZ5dYnpRoUyTv-Ca6AVSP9UFmiN5jkUj9rp6CIVwwHlrxUc4UnqfOpWk9evHI~IH0CGDbQ~3zdL-m8FZa5fdde1dxdCdFcpEsMPOaee7fdG1QqJrc5ojF5-KeC4oEAcBmb~taMT3Kx7FhycohDTaWlzzADsbxRCMdmdlPNljGv8x1VR6ekUBrOCrg__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4"
                          alt=""
                          height="40px"
                          width="40px"
                        />{" "}
                      </Stack>
                    ) : (
                      <Box>
                        <SimpleGrid columns={[1, null, 2]} spacing="5" pt="5">
                          {allMedias
                            ?.slice(startValue, endValue)
                            .map((media: any) => (
                              <Stack key={media._id}>
                                <UserCollections props={media} />
                              </Stack>
                            ))}
                        </SimpleGrid>
                        <Box alignItems="left" pt="10">
                          <Button
                            p="3"
                            disabled={startValue > 0 ? false : true}
                            variant="outline"
                            borderColor="black"
                            color="#D7380E"
                            fontSize="xl"
                            fontWeight="semibold"
                            mr="10"
                            onClick={() => {
                              setStartValue((privious: any) => privious - 6);
                              setEndValue((privious: any) => privious - 6);
                            }}
                          >
                            <FcPrevious size="20px" color="#000000" />
                          </Button>
                          <Button
                            p="3"
                            disabled={
                              allMedias.length > endValue ? false : true
                            }
                            variant="outline"
                            borderColor="black"
                            color="#D7380E"
                            fontSize="xl"
                            fontWeight="semibold"
                            onClick={() => {
                              setStartValue(startValue + 6);
                              setEndValue(endValue + 6);
                            }}
                          >
                            <FcNext size="20px" color="#000000" />
                          </Button>
                        </Box>
                      </Box>
                    )}
                  </Stack>
                ) : activeState === 1 ? (
                  <Stack align="left">
                    <Text
                      color="#000000"
                      fontSize="2xl"
                      fontWeight="semibold"
                      align="left"
                    >
                      User Screens List
                    </Text>
                    {screens?.length === 0 ? (
                      <Stack>
                        <Text color="#3A3A3A" fontSize="xl" align="left">
                          No Screen Found!
                        </Text>
                        <Image
                          src="https://s3-alpha-sig.figma.com/img/ca29/f93f/58db42ff92848405ab0da2be110fd228?Expires=1678665600&Signature=TVT1uDTDsAjPuSIDQ2wNMp-KUGgX6DdL5UwXT2BhPB6b6gRVfUlaUSatCC2hReLXKIHp9ABYmAHK7JXW3qm5FSqWCfUUqQC3OFgfSqcwLY7x1Di3ezJtAyX9pWcE6Vg7jVCP8bS31vnDt00rXnxGPGbah3hz7CZ5dYnpRoUyTv-Ca6AVSP9UFmiN5jkUj9rp6CIVwwHlrxUc4UnqfOpWk9evHI~IH0CGDbQ~3zdL-m8FZa5fdde1dxdCdFcpEsMPOaee7fdG1QqJrc5ojF5-KeC4oEAcBmb~taMT3Kx7FhycohDTaWlzzADsbxRCMdmdlPNljGv8x1VR6ekUBrOCrg__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4"
                          alt=""
                          height="40px"
                          width="40px"
                        />{" "}
                      </Stack>
                    ) : (
                      <Box>
                        <SimpleGrid columns={[1, null, 2]} spacing="5" pt="5">
                          {screens
                            ?.slice(startValue, endValue)
                            .map((screen: any) => (
                              <Stack key={screen._id}>
                                <Screen eachScreen={screen} />
                              </Stack>
                            ))}
                        </SimpleGrid>
                        <Box alignItems="left" pt="10">
                          <Button
                            p="3"
                            disabled={startValue > 0 ? false : true}
                            variant="outline"
                            borderColor="black"
                            color="#D7380E"
                            fontSize="xl"
                            fontWeight="semibold"
                            mr="10"
                            onClick={() => {
                              setStartValue((privious: any) => privious - 6);
                              setEndValue((privious: any) => privious - 6);
                            }}
                          >
                            <FcPrevious size="20px" color="#000000" />
                          </Button>
                          <Button
                            p="3"
                            disabled={screens.length > endValue ? false : true}
                            variant="outline"
                            borderColor="black"
                            color="#D7380E"
                            fontSize="xl"
                            fontWeight="semibold"
                            onClick={() => {
                              setStartValue(startValue + 6);
                              setEndValue(endValue + 6);
                            }}
                          >
                            <FcNext size="20px" color="#000000" />
                          </Button>
                        </Box>
                      </Box>
                    )}
                  </Stack>
                ) : activeState === 2 ? (
                  <Stack align="left">
                    <Text
                      color="#000000"
                      fontSize="2xl"
                      fontWeight="semibold"
                      align="left"
                    >
                      User Campaign List
                    </Text>
                    {campaign?.length === 0 ? (
                      <Stack>
                        <Text color="#3A3A3A" fontSize="xl" align="left">
                          No Campaign Found!
                        </Text>
                        <Image
                          src="https://s3-alpha-sig.figma.com/img/ca29/f93f/58db42ff92848405ab0da2be110fd228?Expires=1678665600&Signature=TVT1uDTDsAjPuSIDQ2wNMp-KUGgX6DdL5UwXT2BhPB6b6gRVfUlaUSatCC2hReLXKIHp9ABYmAHK7JXW3qm5FSqWCfUUqQC3OFgfSqcwLY7x1Di3ezJtAyX9pWcE6Vg7jVCP8bS31vnDt00rXnxGPGbah3hz7CZ5dYnpRoUyTv-Ca6AVSP9UFmiN5jkUj9rp6CIVwwHlrxUc4UnqfOpWk9evHI~IH0CGDbQ~3zdL-m8FZa5fdde1dxdCdFcpEsMPOaee7fdG1QqJrc5ojF5-KeC4oEAcBmb~taMT3Kx7FhycohDTaWlzzADsbxRCMdmdlPNljGv8x1VR6ekUBrOCrg__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4"
                          alt=""
                          height="40px"
                          width="40px"
                        />{" "}
                      </Stack>
                    ) : (
                      <Box>
                        <SimpleGrid columns={[1, null, 1]} spacing="5" pt="5">
                          {campaign
                            ?.slice(startValue, endValue)
                            .map((eachCampaign: any) => (
                              <Stack key={eachCampaign._id}>
                                <MySingleCampaign campaign={eachCampaign} />
                              </Stack>
                            ))}
                        </SimpleGrid>
                        <Box alignItems="left" pt="10">
                          <Button
                            p="3"
                            disabled={startValue > 0 ? false : true}
                            variant="outline"
                            borderColor="black"
                            color="#D7380E"
                            fontSize="xl"
                            fontWeight="semibold"
                            mr="10"
                            onClick={() => {
                              setStartValue((privious: any) => privious - 5);
                              setEndValue((privious: any) => privious - 5);
                            }}
                          >
                            <FcPrevious size="20px" color="#000000" />
                          </Button>
                          <Button
                            p="3"
                            disabled={campaign.length > endValue ? false : true}
                            variant="outline"
                            borderColor="black"
                            color="#D7380E"
                            fontSize="xl"
                            fontWeight="semibold"
                            onClick={() => {
                              setStartValue(startValue + 5);
                              setEndValue(endValue + 5);
                            }}
                          >
                            <FcNext size="20px" color="#000000" />
                          </Button>
                        </Box>
                      </Box>
                    )}
                  </Stack>
                ) : activeState === 3 ? (
                  <Stack align="left">
                    <Text
                      color="#000000"
                      fontSize="2xl"
                      fontWeight="semibold"
                      align="left"
                    >
                      Brand rewards List
                    </Text>
                    {myRewardPrograms?.length === 0 ? (
                      <Stack>
                        <Text color="#3A3A3A" fontSize="xl" align="left">
                          No Campaign Found!
                        </Text>
                        <Image
                          src="https://s3-alpha-sig.figma.com/img/ca29/f93f/58db42ff92848405ab0da2be110fd228?Expires=1678665600&Signature=TVT1uDTDsAjPuSIDQ2wNMp-KUGgX6DdL5UwXT2BhPB6b6gRVfUlaUSatCC2hReLXKIHp9ABYmAHK7JXW3qm5FSqWCfUUqQC3OFgfSqcwLY7x1Di3ezJtAyX9pWcE6Vg7jVCP8bS31vnDt00rXnxGPGbah3hz7CZ5dYnpRoUyTv-Ca6AVSP9UFmiN5jkUj9rp6CIVwwHlrxUc4UnqfOpWk9evHI~IH0CGDbQ~3zdL-m8FZa5fdde1dxdCdFcpEsMPOaee7fdG1QqJrc5ojF5-KeC4oEAcBmb~taMT3Kx7FhycohDTaWlzzADsbxRCMdmdlPNljGv8x1VR6ekUBrOCrg__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4"
                          alt=""
                          height="40px"
                          width="40px"
                        />{" "}
                      </Stack>
                    ) : (
                      <Box width="100%">
                        <SimpleGrid columns={[1, null, 2]} spacing="5" pt="5">
                          {myRewardPrograms
                            ?.slice(startValue, endValue)
                            .map((reward: any) => (
                              <Stack key={reward._id}>
                                <SingleRewardData
                                  reward={reward}
                                  onClick={handleShowConformation}
                                />
                              </Stack>
                            ))}
                        </SimpleGrid>
                        <Box alignItems="left" pt="10">
                          <Button
                            p="3"
                            disabled={startValue > 0 ? false : true}
                            variant="outline"
                            borderColor="black"
                            color="#D7380E"
                            fontSize="xl"
                            fontWeight="semibold"
                            mr="10"
                            onClick={() => {
                              setStartValue((privious: any) => privious - 8);
                              setEndValue((privious: any) => privious - 8);
                            }}
                          >
                            <FcPrevious size="20px" color="#000000" />
                          </Button>
                          <Button
                            p="3"
                            disabled={campaign.length > endValue ? false : true}
                            variant="outline"
                            borderColor="black"
                            color="#D7380E"
                            fontSize="xl"
                            fontWeight="semibold"
                            onClick={() => {
                              setStartValue(startValue + 8);
                              setEndValue(endValue + 8);
                            }}
                          >
                            <FcNext size="20px" color="#000000" />
                          </Button>
                        </Box>
                      </Box>
                    )}
                  </Stack>
                ) : activeState === 4 ? (
                  <Stack align="left">
                    <Text
                      color="#000000"
                      fontSize="2xl"
                      fontWeight="semibold"
                      align="left"
                    >
                      User rewards List
                    </Text>
                    {myRewardPrograms?.length === 0 ? (
                      <Stack>
                        <Text color="#3A3A3A" fontSize="xl" align="left">
                          No Campaign Found!
                        </Text>
                        <Image
                          src="https://s3-alpha-sig.figma.com/img/ca29/f93f/58db42ff92848405ab0da2be110fd228?Expires=1678665600&Signature=TVT1uDTDsAjPuSIDQ2wNMp-KUGgX6DdL5UwXT2BhPB6b6gRVfUlaUSatCC2hReLXKIHp9ABYmAHK7JXW3qm5FSqWCfUUqQC3OFgfSqcwLY7x1Di3ezJtAyX9pWcE6Vg7jVCP8bS31vnDt00rXnxGPGbah3hz7CZ5dYnpRoUyTv-Ca6AVSP9UFmiN5jkUj9rp6CIVwwHlrxUc4UnqfOpWk9evHI~IH0CGDbQ~3zdL-m8FZa5fdde1dxdCdFcpEsMPOaee7fdG1QqJrc5ojF5-KeC4oEAcBmb~taMT3Kx7FhycohDTaWlzzADsbxRCMdmdlPNljGv8x1VR6ekUBrOCrg__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4"
                          alt=""
                          height="40px"
                          width="40px"
                        />{" "}
                      </Stack>
                    ) : (
                      <Box width="100%">
                        <SimpleGrid columns={[1, null, 2]} spacing="5" pt="5">
                          {myRewardPrograms
                            ?.slice(startValue, endValue)
                            .map((reward: any) => (
                              <Stack key={reward._id}>
                                <ScratchCard
                                  width={320}
                                  height={226}
                                  image={Coupon}
                                  finishPercent={80}
                                  onComplete={() => console.log("complete")}
                                >
                                  <SingleRewardData reward={reward} />
                                </ScratchCard>
                              </Stack>
                            ))}
                        </SimpleGrid>
                        <Box alignItems="left" pt="10">
                          <Button
                            p="3"
                            disabled={startValue > 0 ? false : true}
                            variant="outline"
                            borderColor="black"
                            color="#D7380E"
                            fontSize="xl"
                            fontWeight="semibold"
                            mr="10"
                            onClick={() => {
                              setStartValue((privious: any) => privious - 8);
                              setEndValue((privious: any) => privious - 8);
                            }}
                          >
                            <FcPrevious size="20px" color="#000000" />
                          </Button>
                          <Button
                            p="3"
                            disabled={campaign.length > endValue ? false : true}
                            variant="outline"
                            borderColor="black"
                            color="#D7380E"
                            fontSize="xl"
                            fontWeight="semibold"
                            onClick={() => {
                              setStartValue(startValue + 8);
                              setEndValue(endValue + 8);
                            }}
                          >
                            <FcNext size="20px" color="#000000" />
                          </Button>
                        </Box>
                      </Box>
                    )}
                  </Stack>
                ) : null}
              </Box>
            )}
          </Box>
        </SimpleGrid>
      </Box>
    </Box>
  );
}
