import { useState, useEffect } from "react";
import {
  Box,
  Text,
  Button,
  Stack,
  Flex,
  FormControl,
  FormLabel,
  Input,
  FormHelperText,
  FormErrorMessage,
  // InputGroup,
  // InputRightElement,
} from "@chakra-ui/react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import { BRAND_UPDATE_PROFILE_RESET } from "../../Constants/brandConstants";
import {
  getBrandDetails,
  updateBrandProfile,
} from "../../Actions/brandActions";

export function BrandProfile() {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const brandId = window.location.pathname.split("/")[2];

  const [activity, setActivity] = useState<any>("Profile");
  const [name, setName] = useState<any>();
  const [nameError, setNameError] = useState<any>("");
  const [address, setAddress] = useState<any>();

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo: user } = userSignin;

  const brandUpdateProfile = useSelector(
    (state: any) => state.brandUpdateProfile
  );
  const {
    loading: loadingBrandUpdateProfile,
    error: errorBrandUpdateProfile,
    success,
  } = brandUpdateProfile;

  const brandDetailsGet = useSelector((state: any) => state.brandDetailsGet);
  const {
    loading: loadingBrandDetails,
    error: errorBrandDetails,
    brandDetails,
  } = brandDetailsGet;

  useEffect(() => {
    if (brandDetails) {
      setName(brandDetails.brandName);
    }
    if (success) {
      dispatch({ type: BRAND_UPDATE_PROFILE_RESET });
      alert("User updated successFull!");
      navigate("/userProfile");
    }
    dispatch(getBrandDetails(user, brandId));
  }, [dispatch, success, user, brandId, activity]);

  const updateBrandInfo = () => {
    const brandInfo = {
      brandName: name,
      brandId: brandId,
    };
    dispatch(updateBrandProfile(user, brandInfo));
  };

  return (
    <>
      {loadingBrandDetails ? (
        <>dfssdfdsfsdf</>
      ) : errorBrandDetails ? (
        <>sdffdgsgfsgg</>
      ) : (
        <Box px={{ base: "2", lg: "20" }} py={{ base: "2", lg: "30" }}>
          <Text color="#000000" fontSize="3xl" align="left" fontWeight="bold">
            Brand Profile
          </Text>
          <Text color="#4E4E4E" fontSize="sm" align="left">
            Set your brand profile below
          </Text>
          <Stack pt="10">
            <Flex
              color="#000000"
              fontSize="xl"
              fontWeight="semibold"
              bg="#F2F2F2"
              justify="space-around"
              align="center"
            >
              <Button
                py="3"
                px="14"
                bg={activity === "Profile" ? "#FFFFFF" : "#F2F2F2"}
                color="#000000"
                boxShadow={activity === "Profile" ? "xl" : ""}
                onClick={() => setActivity("Profile")}
              >
                Profile
              </Button>
              <Button
                py="3"
                px="14"
                bg={activity === "Address" ? "#FFFFFF" : "#F2F2F2"}
                color="#000000"
                boxShadow={activity === "Address" ? "xl" : ""}
                onClick={() => setActivity("Address")}
              >
                Address
              </Button>
            </Flex>
          </Stack>

          <Stack pt="20" width={{ base: "100%", lg: "30%" }}>
            {activity === "Profile" ? (
              <Stack>
                <FormControl id="name" isInvalid={nameError ? true : false}>
                  <FormLabel fontSize="sm" color="#333333">
                    Brand Name
                  </FormLabel>
                  <Stack direction="column" align="left">
                    <Input
                      color="#333333"
                      id="name"
                      onChange={(e) => setName(e?.target?.value)}
                      placeholder="Enter name"
                      value={name}
                      required
                      type="text"
                      py="2"
                      rounded="md"
                      size="md"
                      borderColor="#888888"
                    />
                    {!nameError ? (
                      <FormHelperText></FormHelperText>
                    ) : (
                      <FormErrorMessage>{nameError}</FormErrorMessage>
                    )}
                  </Stack>
                </FormControl>
              </Stack>
            ) : (
              activity === "Address" && (
                <Stack>
                  <FormControl id="name" isInvalid={nameError ? true : false}>
                    <FormLabel fontSize="sm" color="#333333">
                      Home/Office
                    </FormLabel>
                    <Stack direction="column" align="left">
                      <Input
                        color="#333333"
                        id="name"
                        onChange={(e) => setAddress(e?.target?.value)}
                        placeholder="Home/office"
                        value={address}
                        required
                        type="text"
                        py="2"
                        rounded="md"
                        size="md"
                        borderColor="#888888"
                      />
                      {!nameError ? (
                        <FormHelperText></FormHelperText>
                      ) : (
                        <FormErrorMessage>{nameError}</FormErrorMessage>
                      )}
                    </Stack>
                  </FormControl>
                </Stack>
              )
            )}
            <Stack align="left">
              {activity !== "Password" ? (
                <Button
                  variant="outline"
                  borderColor="#403F49"
                  color="#D7380E"
                  py="3"
                  fontSize="lg"
                  onClick={updateBrandInfo}
                >
                  Save
                </Button>
              ) : (
                <Button
                  variant="outline"
                  borderColor="#403F49"
                  color="#D7380E"
                  py="3"
                  fontSize="lg"
                  // onClick={updatePassword}
                >
                  Change Password
                </Button>
              )}
            </Stack>
          </Stack>
        </Box>
      )}
    </>
  );
}
