import React, { useEffect, useState } from "react";
import "./videoStyle.css";
import Axios from "axios";
import ReactPlayer from "react-player";

export function MyVideoPlayList() {
  const screenId = window.location.pathname.split("/")[2];
  const [videos, setVideos] = useState<any>([]);
  const [index, setIndex] = useState<any>(0);

  const looping = (e: any) => {
    console.log("looping...............");
    if (videos.length > 0) {
      if (index === videos.length) {
        setIndex(1);
      } else {
        setIndex(index + 1);
      }
      e.target.src = videos.map((video: any) => video.video)[index - 1];
      e.target.play();
    }
  };
  const getCampaignList = async () => {
    if (videos.length == 0) {
      try {
        const { data } = await Axios.get(
          `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/${screenId}/screen`
        );
        setVideos([...data]);
        console.log("getCampaignList : ", data);
      } catch (error) {
        console.log("error");
      }
    }
  };
  function openFullscreen(elem: any) {
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.webkitRequestFullscreen) {
      /* Safari */
      elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) {
      /* IE11 */
      elem.msRequestFullscreen();
    }
  }
  /**
   url={
            "https://ipfs.io/ipfs/bafybeibzvah3xixmv5cncmayi3jyzvc367d2jmmfntyi47qjewsmxemxie"
          }
   */
  useEffect(() => {
    console.log("useEffect called!");
    getCampaignList();
    var elem = document.getElementById("myVideo");
    openFullscreen(elem);
  }, []);
  return (
    <div className="content" id="myVideo">
      {videos.length > 0 ? (
        <>
          <ReactPlayer
            playing={true}
            url={videos[index].video}
            width="100%"
            height="100%"
            onEnded={() => {
              console.log("videos index : ", index);
              if (index < videos.length) {
                setIndex(index + 1);
              } else {
                console.log("else part");
                setIndex(0);
              }
            }}
          />
        </>
      ) : null}
    </div>
  );
}
