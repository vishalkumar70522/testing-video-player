import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import "./videoStyle.css";
import axios from "axios";
import { getCampaignListByScreenId } from "../../Actions/campaignAction";
import { checkPlaylist } from "../../Actions/screenActions";
import { detailsScreen } from "../../Actions/screenActions";

export function PlayListByScreen() {
  const dispatch = useDispatch<any>();

  const screenId = window.location.pathname.split("/")[2];
  // const [videos, setVideos] = useState<any>([]);
  const [index, setIndex] = useState<any>(1);
  const [trigger, setTrigger] = useState<boolean>(false);
  const [verify, setVerify] = useState<boolean>(true);
  const [isPlaying, setIsPlaying] = useState<boolean>(true);

  const [deviceId, setDeviceId] = useState<any>(null);
  const [deviceIp, setDeviceIp] = useState<any>(null);
  const [deviceMaac, setDeviceMaac] = useState<any>(null);
  const [deviceDisplay, setDeviceDisplay] = useState<any>(null);

  const screenDetails = useSelector((state: any) => state.screenDetails);
  const { loading: loadingScreen, error: errorScreen, screen } = screenDetails;

  const activeCampaignListByScreenID = useSelector(
    (state: any) => state.activeCampaignListByScreenID
  );
  const {
    loading: loadingCampaign,
    error: errorCampaign,
    campaigns: videos,
  } = activeCampaignListByScreenID;

  const playlistCheck = useSelector((state: any) => state.playlistCheck);
  const {
    data: checkScreenData,
    // loading: loadingCheckScreen,
    error: errorCheckScreen,
  } = playlistCheck;

  function openFullscreen() {
    var elem = document.getElementById("myvideo");
    // var fullscreen =
    //   elem?.webkitRequestFullscreen ||
    //   elem?.mozRequestFullScreen ||
    //   elem?.msRequestFullscreen;
    // fullscreen.call(elem);
    if (elem?.requestFullscreen) {
      elem?.requestFullscreen();
      // } else if (elem.webkitRequestFullscreen) {
      //   /* Safari */
      //   elem?.webkitRequestFullscreen();
      // } else if (elem?.msRequestFullscreen) {
      //   /* IE11 */
      //   elem?.msRequestFullscreen();
    }
  }

  const getData = async () => {
    const res = await axios.get("https://geolocation-db.com/json/ ");
    console.log(res.data);
    setDeviceIp(res.data.IPv4);
    setDeviceId("null");
    setDeviceDisplay("Wilyer_Screen");
    setDeviceMaac("");
  };

  useEffect(() => {
    // if (videosList) {
    //   setVideos(videosList);
    //   setTrigger(!trigger);
    //   // } else {
    // }
    // if (videos.length === 0) {
    //   console.log("video.lenth  : ", videos.length);
    // getCampaignList();
    // }
    dispatch(detailsScreen(screenId));
    dispatch(getCampaignListByScreenId(screenId));
    setTimeout(() => {
      if (!verify) {
        setIsPlaying(false);
      }
    }, 120000);
  }, [dispatch, screenId, verify]);

  // const looping = (e: any) => {
  //   // console.log("looping...............");
  //   if (videos.length > 0) {
  //     if (index === videos.length) {
  //       setIndex(1);
  //     } else {
  //       setIndex(index + 1);
  //     }
  //     e.target.src = videos?.map((video: any) => video.video)[index - 1];
  //     e.target.play();
  //   }
  // };

  const onLoad = (e: any) => {
    console.log("jkljkkjjkk");
    if (errorCheckScreen) {
      setVerify(false);
    }

    getData();
    // e.target.play();
  };

  const onLoadedData = (e: any) => {
    console.log("nmnmnmmmnmnm");
    // e.target.play();
  };

  const onCanPlay = (e: any) => {
    // e.target.play();
    // elemVideo.autoplay = true;
    // elemVideo.play();
  };

  const onPlay = () => {
    openFullscreen();
  };

  const onEnd = (e: any) => {
    const currentVid = e.target.src.split("/").slice(-1);
    const timeNow = new Date().toISOString();
    const deviceInfo = {
      deviceId: deviceId,
      deviceIp: deviceIp,
      deviceMaac: deviceMaac,
      deviceDisplay: deviceDisplay,
    };

    if (videos.length > 0) {
      if (index === videos.length) {
        setIndex(1);
      } else {
        setIndex(index + 1);
      }
      e.target.src = videos?.map((video: any) => video.video)[index - 1];
      e.target.play();
    }

    if (screen) {
      dispatch(
        checkPlaylist({
          screenName: screen.name,
          timeNow,
          currentVid,
          deviceInfo,
        })
      );
    }
    if (checkScreenData) {
      verifyPlaylist();
      console.log("DONE PLAYING");
    }
  };

  const verifyPlaylist = () => {
    setVerify(true);
  };
  return (
    <div className="content" id="myvideo">
      {loadingCampaign ? (
        <h1>Loading</h1>
      ) : errorCampaign ? (
        <h1>{errorCampaign}</h1>
      ) : (
        <>
          {videos.length > 0 ? (
            <video
              autoPlay={true}
              // controls
              playsInline
              // loop={true}
              muted={true}
              onLoadStart={(e) => onLoad(e)}
              onLoadedData={(e) => onLoadedData(e)}
              onCanPlay={(e) => onCanPlay(e)}
              onPlay={onPlay}
              // onProgress={() => {}}
              onEnded={(e) => onEnd(e)}
              poster="https://arweave.net/pziELbF_OhcQUgJbn_d1j_o_3ASHHHXA3_GoTdJSnlg"
              width="100%"
              preload="auto"
            >
              {videos.map((video: any) => (
                <source key={video._id} src={video.video} />
              ))}
            </video>
          ) : null}
        </>
      )}
    </div>
  );
}
