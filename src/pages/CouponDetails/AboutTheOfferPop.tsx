import { Box, ListItem, Text, UnorderedList } from "@chakra-ui/react";
import { Modal } from "antd";

export function AboutTheOfferPop(props: any) {
  return (
    <Modal
      style={{ top: window.screen.height - 150 }}
      footer={[]}
      open={props?.aboutTheOfferPopupOpen}
      onOk={() => props?.setAboutTheOfferPopupOpen(false)}
      onCancel={() => props?.setAboutTheOfferPopupOpen(false)}
    >
      <Box>
        <Text color="#969696" m="0" fontSize="lg" fontWeight="650" align="left">
          About the offer
        </Text>
        <UnorderedList>
          <ListItem>Lorem ipsum dolor sit amet </ListItem>
          <ListItem>Consectetur adipiscing elit</ListItem>
          <ListItem>Integer molestie lorem at massa</ListItem>
          <ListItem>Facilisis in pretium nisl aliquet</ListItem>
        </UnorderedList>
      </Box>
    </Modal>
  );
}
