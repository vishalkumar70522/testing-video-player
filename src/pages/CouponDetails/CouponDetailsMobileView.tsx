import {
  Box,
  Flex,
  Stack,
  Image,
  Text,
  Button,
  Input,
  Link,
  Divider,
  Fade,
  Center,
} from "@chakra-ui/react";
import { FiChevronDown } from "react-icons/fi";
import { useState } from "react";
import { BsFillStarFill } from "react-icons/bs";
import { HiOutlineClock } from "react-icons/hi";
import { BiArrowBack } from "react-icons/bi";
import { BrandDetailsPopup } from "./BrandDetailsPopup";
import { AboutTheOfferPop } from "./AboutTheOfferPop";
import { useLocation, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";

export function CouponDetailsMobileView(props: any) {
  const [userScreenRating, setUserScreenRating] = useState<any>(2);
  const [modal1Open, setModal1Open] = useState<any>(false);
  const [aboutTheOfferPopupOpen, setAboutTheOfferPopupOpen] =
    useState<any>(false);
  const navigate = useNavigate();
  const currentPath = window.location.pathname;
  const location = useLocation();
  const couponData = props?.couponData || location?.state?.data;

  const userSignin = useSelector((state: any) => state.userSignin);
  const {
    loading: loadingUserInfo,
    error: errorUserInfo,
    userInfo,
  } = userSignin;

  if (!userInfo) {
    navigate("/signin", {
      state: {
        path: currentPath,
        data: couponData,
      },
    });
  }

  const [isCopied, setIsCopied] = useState<any>(false);

  const onCopy = (value: any) => {
    setIsCopied(true);
    navigator.clipboard.writeText(value);
  };

  return (
    <Box>
      <BrandDetailsPopup
        modal1Open={modal1Open}
        setModal1Open={(value: any) => setModal1Open(value)}
        couponData={couponData}
      />
      <AboutTheOfferPop
        aboutTheOfferPopupOpen={aboutTheOfferPopupOpen}
        setAboutTheOfferPopupOpen={(value: any) =>
          setAboutTheOfferPopupOpen(value)
        }
      />
      <Box
        bgImage={couponData?.image}
        height="237px"
        backgroundAttachment="fixed"
        backgroundRepeat="no-repeat"
      >
        <Flex justifyContent="space-between" px="5" pt="5">
          <Center
            borderRadius="100%"
            bgColor="#F1F1F1"
            p="1"
            onClick={() => navigate("/")}
          >
            <BiArrowBack size="30px" color="#292929" />
          </Center>
          <Button
            color="#484848"
            fontSize="sm"
            variant="outline"
            py="6px"
            px="12px"
            fontWeight="511"
            leftIcon={<HiOutlineClock />}
            bgColor="#FFFFFF"
          >
            Expires in 7 days{" "}
          </Button>
        </Flex>
      </Box>
      Modal
      <Box px="5" bgColor="#EBEBEB">
        <Box
          boxShadow="md"
          borderRadius={"4px"}
          bg="#FFFFFF"
          px="2"
          py="3"
          width="100%"
          bgSize="cover"
        >
          <Flex height="88.45px" gap="3">
            <Image
              src={couponData?.image}
              alt="berger"
              height="88.45px"
              width="88.45px"
              borderRadius="8px"
            ></Image>
            <Stack>
              <Text fontSize="lg" fontWeight="500" color="#000000" align="left">
                {couponData?.brand}
              </Text>
              <Text
                fontSize="16px"
                fontWeight="400"
                color="#008E06"
                align="left"
              >
                {couponData?.offer}{" "}
              </Text>
            </Stack>
          </Flex>
          <Flex>
            <Input
              color="#000000"
              m="0"
              fontSize="md"
              px="5"
              py="1"
              fontWeight="400"
              value={couponData?.couponCode}
              border="1px"
              borderColor="#D9D9D9"
              borderLeftRadius="48px"
              borderStyle="dashed"
              disabled={true}
            ></Input>
            <Stack pt="5">
              <Button
                borderEndRadius="48px"
                color="#FFFFFF"
                bgColor="#D7380E"
                py="1"
                fontSize={{ base: "", lg: "md" }}
                fontWeight="638"
                onClick={() => onCopy(couponData?.couponCode)}
              >
                {isCopied ? "Copied!" : "Copy"}
              </Button>
            </Stack>
          </Flex>

          <Text color="#484848" m="0" fontSize="sm" fontWeight="511" pt="5">
            Visit{" "}
            <Link color="teal.500" href={couponData?.brandLink}>
              {couponData?.brand}
            </Link>{" "}
            store and paste your code
          </Text>
        </Box>
        <Box
          mt="3"
          bg="#FFFFFF"
          boxShadow="md"
          borderRadius={"4px"}
          px="3"
          py="3"
          width="100%"
          bgSize="cover"
        >
          <Flex justifyContent="space-between" align="center">
            <Text color="#4D4D4D" fontSize="md" fontWeight="503">
              How to redeem offer
            </Text>
            <FiChevronDown color="#000000" size="20px" />
          </Flex>
          <Divider color="#E4E4E4" py="1" />
          <Flex
            justifyContent="space-between"
            align="center"
            pt="2"
            onClick={() => setAboutTheOfferPopupOpen(true)}
          >
            <Text color="#4D4D4D" fontSize="md" fontWeight="503">
              About the offer
            </Text>
            <FiChevronDown color="#000000" size="20px" />
          </Flex>
          <Divider color="#E4E4E4" py="1" />
          <Flex
            justifyContent="space-between"
            align="center"
            pt="2"
            onClick={() => setModal1Open(true)}
          >
            <Text color="#4D4D4D" fontSize="md" fontWeight="503">
              About brand
            </Text>
            <FiChevronDown color="#000000" size="20px" />
          </Flex>
          <Divider color="#E4E4E4" py="1" />
          <Flex justifyContent="space-between" align="center" pt="2">
            <Text color="#4D4D4D" fontSize="md" fontWeight="503">
              Term & condition
            </Text>
            <FiChevronDown color="#000000" size="20px" />
          </Flex>
        </Box>
        <Box
          my="3"
          bg="#FFFFFF"
          boxShadow="md"
          borderRadius={"4px"}
          px="2"
          py="3"
          width="100%"
          bgSize="cover"
        >
          <Text color="#202020" fontSize="md" fontWeight="503" textAlign="left">
            Rate this coupon
          </Text>
          <Flex py="3" gap="5">
            <BsFillStarFill
              size="40px"
              color={userScreenRating >= 1 ? "#FFD700" : "#E2E2E2"}
              onClick={() => setUserScreenRating(1)}
            />
            <BsFillStarFill
              size="40px"
              color={userScreenRating >= 2 ? "#FFD700" : "#E2E2E2"}
              onClick={() => setUserScreenRating(2)}
            />
            <BsFillStarFill
              size="40px"
              color={userScreenRating >= 3 ? "#FFD700" : "#E2E2E2"}
              onClick={() => setUserScreenRating(3)}
            />
            <BsFillStarFill
              size="40px"
              color={userScreenRating >= 4 ? "#FFD700" : "#E2E2E2"}
              onClick={() => setUserScreenRating(4)}
            />
            <BsFillStarFill
              size="40px"
              color={userScreenRating >= 5 ? "#FFD700" : "#E2E2E2"}
              onClick={() => setUserScreenRating(5)}
            />
          </Flex>
        </Box>
        <Box p="1"></Box>
      </Box>
      <Fade in={true}>
        <Box
          py="16px"
          color="wimporthite"
          bg="#D7380E"
          rounded="md"
          shadow="md"
          fontSize="lg"
          fontWeight="638"
        >
          Claim Now
        </Box>
      </Fade>
    </Box>
  );
}
