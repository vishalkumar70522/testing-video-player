import { Modal } from "antd";
import { Text, Button, Flex, Image, Box, SimpleGrid } from "@chakra-ui/react";
import { BsShare } from "react-icons/bs";
import { GiCircleCage } from "react-icons/gi";
import { useState } from "react";
import { ImageSliderPopup } from "./ImageSliderPopup";

export function BrandDetailsPopup(props: any) {
  const [imageShow, setImageShow] = useState<any>(false);
  const { couponData } = props;

  return (
    <Box>
      <ImageSliderPopup
        imageShow={imageShow}
        setImageShow={(value: any) => setImageShow(value)}
        images={couponData?.images}
      />

      <Modal
        style={{ top: window.screen.height - 410 }}
        footer={[]}
        open={props?.modal1Open}
        onOk={() => props?.setModal1Open(false)}
        onCancel={() => props?.setModal1Open(false)}
      >
        <Box>
          <Text
            color="#969696"
            m="0"
            fontSize="2xl"
            fontWeight="650"
            align="left"
          >
            About brand
          </Text>
          <Flex justifyContent="space-between" alignContent="center" mt="5">
            <Flex gap={{ base: "5", lg: "7" }}>
              <Image
                src={couponData?.image}
                alt="berger"
                height="82px"
                width="82px"
              ></Image>
              <Box>
                <Text
                  color="#000000"
                  m="0"
                  fontSize={{ base: "md", lg: "2xl" }}
                  fontWeight="530"
                  align="left"
                >
                  {couponData?.brand}
                </Text>
              </Box>
            </Flex>
            <Flex gap="2">
              <Text
                color="#313131"
                m="0"
                fontSize={{ base: "12px", lg: "sm" }}
                fontWeight="530"
                align="left"
              >
                Share
              </Text>
              <BsShare size="16px" color="#313131" />
            </Flex>
          </Flex>

          <Flex pt={{ base: "2", lg: "5" }} gap={{ base: "2", lg: "5" }}>
            <Button
              color="#FFFFFF"
              bgColor="#D7380E"
              py="3"
              fontSize={{ base: "", lg: "md" }}
              fontWeight="638"
              px="10"
              leftIcon={<GiCircleCage color="#FFFFFF" size="16px" />}
              onClick={() => window.open(couponData?.brandLink, "_blank")}
            >
              Visit online store
            </Button>
          </Flex>

          <Flex gap="1" direction="column">
            <Text
              color="#676767"
              m="0"
              fontSize={{ base: "12px", lg: "md" }}
              fontWeight="400"
              align="left"
              pt="5"
              noOfLines={[3, 2]}
              // isTruncated={false}
            >
              The Quirkiest Sneaker Brand Revolutionizing Footwear Trends!
              Funkfeets is a fresh burst of creativity into the world of
              sneakers, It dares to challenge conventional shoe designs and
              embraces the extraordinary. At Funkfeets, we believe that shoes
              should never be boring or predictable. We've discarded the notion
              of traditional, symmetrical designs and replaced them with
              vibrant, asymmetrical patterns that captivate the eye and spark
              conversations. Our sneakers are more than just footwear; they're
              an expression of your unique style and personality.
            </Text>

            {/* <Text
            color="#D7380E"
            m="0"
            fontSize={{ base: "12px", lg: "md" }}
            fontWeight="400"
            align="left"
            pt="5"
          >
            {" more"}
          </Text> */}
          </Flex>
          <SimpleGrid
            columns={[2, 2, 6]}
            spacing={{ base: "2", lg: "5" }}
            pt="5"
          >
            {couponData?.images?.map((image: any, index: any) => (
              <Image
                src={image}
                borderRadius="8px"
                alt="berger"
                height="135px"
                width="172px"
                onClick={() => {
                  setImageShow(true);
                }}
              />
            ))}
          </SimpleGrid>
        </Box>
      </Modal>
    </Box>
  );
}
