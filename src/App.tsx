import React from "react";
import "./App.css";
import "core-js/actual/string/pad-end";
import { BrowserRouter as Router } from "react-router-dom";
import { AppLayout } from "./components/layouts";
import { ChakraProvider } from "@chakra-ui/react";
import {
  LoginProvider,
  WalletProvider,
  IpfsProvider,
} from "./components/contexts";
import { QueryClient, QueryClientProvider } from "react-query";
import { PublicRoutes } from "./routes";
import BasicStyle from "./theme/basicStyle";
import GlobalStyle from "./theme/globalStyle";
import { theme } from "./theme/Theme.base";
import { SEO } from "./components/widgets/SEO";

const queryClient = new QueryClient();

function App() {
  return (
    <AppLayout>
      <SEO />
      <ChakraProvider theme={theme}>
        <BasicStyle />
        <GlobalStyle />
        <QueryClientProvider client={queryClient}>
          <Router>
            <WalletProvider>
              <IpfsProvider>
                <LoginProvider>
                  <PublicRoutes />
                </LoginProvider>
              </IpfsProvider>
            </WalletProvider>
          </Router>
        </QueryClientProvider>
      </ChakraProvider>
    </AppLayout>
  );
}

export default App;
