import Axios from "axios";
import {
  ADD_REDEEMER_TO_COUPON_REWARD_FAIL,
  ADD_REDEEMER_TO_COUPON_REWARD_REQUEST,
  ADD_REDEEMER_TO_COUPON_REWARD_SUCCESS,
  COUPON_REWARD_CREATE_FAIL,
  COUPON_REWARD_CREATE_REQUEST,
  COUPON_REWARD_CREATE_SUCCESS,
  COUPON_REWARD_DETAILS_FAIL,
  COUPON_REWARD_DETAILS_REQUEST,
  COUPON_REWARD_DETAILS_SUCCESS,
  COUPON_REWARD_LIST_FOR_BRAND_FAIL,
  COUPON_REWARD_LIST_FOR_BRAND_REQUEST,
  COUPON_REWARD_LIST_FOR_BRAND_SUCCESS,
  COUPON_REWARD_OFFER_PARTNER_LIST_FAIL,
  COUPON_REWARD_OFFER_PARTNER_LIST_REQUEST,
  COUPON_REWARD_OFFER_PARTNER_LIST_SUCCESS,
  INCREMENT_COUPON_REDEEMER_FREQUENCY_FAIL,
  INCREMENT_COUPON_REDEEMER_FREQUENCY_REQUEST,
  INCREMENT_COUPON_REDEEMER_FREQUENCY_SUCCESS,
  UPDATE_COUPON_REWARD_FAIL,
  UPDATE_COUPON_REWARD_REQUEST,
  UPDATE_COUPON_REWARD_SUCCESS,
} from "../Constants/couponRewardOfferConstants";

// reward coupon create
export const createCouponRewardProgram = () => async (dispatch, getState) => {
  dispatch({
    type: COUPON_REWARD_CREATE_REQUEST,
    // payload: input,
  });
  const {
    userSignin: { userInfo },
  } = getState();
  try {
    const { data } = await Axios.post(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/couponReward/create`,
      {
        user: userInfo,
        // input,
      },
      {
        headers: {
          authorization: `Bearer ${userInfo.token}`,
        },
      }
    );
    dispatch({
      type: COUPON_REWARD_CREATE_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: COUPON_REWARD_CREATE_FAIL,
      payload: message,
    });
  }
};

// my created rewards
export const getCouponRewardOfferListForBrand =
  () => async (dispatch, getState) => {
    dispatch({
      type: COUPON_REWARD_LIST_FOR_BRAND_REQUEST,
    });
    const {
      userSignin: { userInfo },
    } = getState();

    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/couponReward/couponRewardOfferListForBrand/${userInfo._id}`
      );
      dispatch({
        type: COUPON_REWARD_LIST_FOR_BRAND_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: COUPON_REWARD_LIST_FOR_BRAND_FAIL,
        payload: message,
      });
    }
  };

// get reward details
export const getCouponRewardOfferDetailForBrand =
  (CouponRewardId) => async (dispatch, getState) => {
    dispatch({
      type: COUPON_REWARD_DETAILS_REQUEST,
      payload: CouponRewardId,
    });
    const {
      userSignin: { userInfo },
    } = getState();

    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/couponReward/couponRewardOfferDetailForBrand/${CouponRewardId}`
      );
      dispatch({
        type: COUPON_REWARD_DETAILS_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: COUPON_REWARD_DETAILS_FAIL,
        payload: message,
      });
    }
  };

// update reward details
/**
 * 
 *  { "offerName" : "Dewali sell",
    "brandName" : "Flipkart",
    "quantity" : 340,
    "couponType" : "discount",
    "couponItem" : "1 large frize upto 70,000 ",
    "redeemFrequency" : "1",
    "couponInfoType" : "%",
    "couponInfoValue" : "14",
    "couponValidityTo" : "23-09-23",
    "couponValidityFrom" : "20-09-23",
  "freeItemQuantity" : "null"}  

 */
export const updateCouponRewardProgram =
  (
    CouponRewardId,
    {
      offerName,
      brandName,
      quantity,
      couponType,
      couponInfoValue,
      couponInfoType,
      couponValidityTo,
      couponValidityFrom,
      couponItem,
      freeItemQuantity,
      couponRedeemFrequency,
      reardPartner,
    }
  ) =>
  async (dispatch, getState) => {
    dispatch({
      type: UPDATE_COUPON_REWARD_REQUEST,
      payload: CouponRewardId,
    });
    const {
      userSignin: { userInfo },
    } = getState();

    try {
      const { data } = await Axios.put(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/couponReward/couponReward/${CouponRewardId}`,
        {
          offerName,
          brandName,
          quantity,
          couponType,
          couponInfoValue,
          couponInfoType,
          couponValidityTo,
          couponValidityFrom,
          couponItem,
          freeItemQuantity,
          couponRedeemFrequency,
          reardPartner,
        },
        {
          headers: {
            authorization: `Bearer ${userInfo.token}`,
          },
        }
      );
      dispatch({
        type: UPDATE_COUPON_REWARD_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: UPDATE_COUPON_REWARD_FAIL,
        payload: message,
      });
    }
  };

export const addRedeemerToCouponRewardOffer =
  (couponRewardId) => async (dispatch, getState) => {
    dispatch({
      type: ADD_REDEEMER_TO_COUPON_REWARD_REQUEST,
      payload: couponRewardId,
    });
    const {
      userSignin: { userInfo },
    } = getState();

    try {
      const { data } = await Axios.put(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/couponReward/addRedmeerToCoupon/${couponRewardId}/${userInfo._id}`,
        {
          headers: {
            authorization: `Bearer ${userInfo.token}`,
          },
        }
      );
      // console.log("addRedeemerToCouponReward : ", data);
      dispatch({
        type: ADD_REDEEMER_TO_COUPON_REWARD_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: ADD_REDEEMER_TO_COUPON_REWARD_FAIL,
        payload: message,
      });
    }
  };

// get reward details
export const getCouponRewardOfferPartnerList =
  (couponId) => async (dispatch, getState) => {
    dispatch({
      type: COUPON_REWARD_OFFER_PARTNER_LIST_REQUEST,
      payload: couponId,
    });
    const {
      userSignin: { userInfo },
    } = getState();

    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/couponReward/rewardOfferPartnerList/${couponId}`
      );
      dispatch({
        type: COUPON_REWARD_OFFER_PARTNER_LIST_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: COUPON_REWARD_OFFER_PARTNER_LIST_FAIL,
        payload: message,
      });
    }
  };

//incrementUserRedeemFrequency

export const incrementUserRedeemFrequency =
  (couponId, couponCode) => async (dispatch, getState) => {
    dispatch({
      type: INCREMENT_COUPON_REDEEMER_FREQUENCY_REQUEST,
      payload: couponId,
    });
    const {
      userSignin: { userInfo },
    } = getState();

    try {
      const { data } = await Axios.put(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/couponReward/incrementUserRedeemFrequency/${couponId}/${couponCode}`
      );
      dispatch({
        type: INCREMENT_COUPON_REDEEMER_FREQUENCY_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: INCREMENT_COUPON_REDEEMER_FREQUENCY_FAIL,
        payload: message,
      });
    }
  };
