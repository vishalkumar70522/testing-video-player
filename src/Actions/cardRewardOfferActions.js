import Axios from "axios";
import {
  CARD_REWARD_CREATE_FAIL,
  CARD_REWARD_CREATE_REQUEST,
  CARD_REWARD_CREATE_SUCCESS,
  CARD_REWARD_DETAILS_FAIL,
  CARD_REWARD_DETAILS_REQUEST,
  CARD_REWARD_DETAILS_SUCCESS,
  CARD_REWARD_LIST_FOR_BRAND_FAIL,
  CARD_REWARD_LIST_FOR_BRAND_REQUEST,
  CARD_REWARD_LIST_FOR_BRAND_SUCCESS,
  UPDATE_CARD_REWARD_FAIL,
  UPDATE_CARD_REWARD_REQUEST,
  UPDATE_CARD_REWARD_SUCCESS,
} from "../Constants/cardRewardOfferConstants";

// wallet create
export const createCardRewardProgram = () => async (dispatch, getState) => {
  dispatch({
    type: CARD_REWARD_CREATE_REQUEST,
    // payload: input,
  });
  const {
    userSignin: { userInfo },
  } = getState();
  try {
    const { data } = await Axios.post(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/cardReward/create`,
      {
        user: userInfo,
        // input,
      },
      {
        headers: {
          authorization: `Bearer ${userInfo.token}`,
        },
      }
    );
    dispatch({
      type: CARD_REWARD_CREATE_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: CARD_REWARD_CREATE_FAIL,
      payload: message,
    });
  }
};

// my created rewards
export const getCardRewardOfferListForBrand =
  () => async (dispatch, getState) => {
    dispatch({
      type: CARD_REWARD_LIST_FOR_BRAND_REQUEST,
    });
    const {
      userSignin: { userInfo },
    } = getState();

    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/cardReward/cardRewardOfferListForBrand/${userInfo._id}`
      );
      dispatch({
        type: CARD_REWARD_LIST_FOR_BRAND_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: CARD_REWARD_LIST_FOR_BRAND_FAIL,
        payload: message,
      });
    }
  };

// get reward details
export const getCardRewardOfferDetailForBrand =
  (cardRewardId) => async (dispatch, getState) => {
    dispatch({
      type: CARD_REWARD_DETAILS_REQUEST,
      payload: cardRewardId,
    });
    const {
      userSignin: { userInfo },
    } = getState();

    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/cardReward/cardRewardOfferDetailForBrand/${cardRewardId}`
      );
      dispatch({
        type: CARD_REWARD_DETAILS_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: CARD_REWARD_DETAILS_FAIL,
        payload: message,
      });
    }
  };

// update reward details
/**
 * 
 *  {  "offerName" : "Durga puja  sell",
    "brandName" : "Flipcard",
    "quantity" : 100,
    "cardType" : "loyalty",
    "cardValidityTo" : "30-9-23",
    "cardValidityFrom" : "01-09-23",}  

 */
export const updateCardRewardProgram =
  (
    cardRewardId,
    {
      offerName,
      brandName,
      quantity,
      cardValidityTo,
      cardValidityFrom,
      rewardCardType,
      cardMilestonesRewards,
    }
  ) =>
  async (dispatch, getState) => {
    dispatch({
      type: UPDATE_CARD_REWARD_REQUEST,
      payload: cardRewardId,
    });
    const {
      userSignin: { userInfo },
    } = getState();

    try {
      const { data } = await Axios.put(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/cardReward/cardReward/${cardRewardId}`,
        {
          offerName,
          brandName,
          quantity,
          cardValidityTo,
          cardValidityFrom,
          rewardCardType,
          cardMilestonesRewards,
        },
        {
          headers: {
            authorization: `Bearer ${userInfo.token}`,
          },
        }
      );
      dispatch({
        type: UPDATE_CARD_REWARD_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: UPDATE_CARD_REWARD_FAIL,
        payload: message,
      });
    }
  };
