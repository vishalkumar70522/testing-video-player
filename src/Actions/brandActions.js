import Axios from "axios";
import {
  BRAND_CREATE_PROFILE_FAIL,
  BRAND_CREATE_PROFILE_REQUEST,
  BRAND_CREATE_PROFILE_SUCCESS,
  BRAND_UPDATE_PROFILE_FAIL,
  BRAND_UPDATE_PROFILE_REQUEST,
  BRAND_UPDATE_PROFILE_SUCCESS,
  BRAND_DETAILS_REQUEST,
  BRAND_DETAILS_SUCCESS,
  BRAND_DETAILS_FAIL,
} from "../Constants/brandConstants";

export const createBrandProfile =
  (user, brandInfo) => async (dispatch, getState) => {
    dispatch({
      type: BRAND_CREATE_PROFILE_REQUEST,
      payload: user,
    });
    const {
      userSignin: { userInfo },
    } = getState();
    try {
      const { data } = await Axios.post(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/brands/${userInfo._id}/create`,
        { user, brandInfo },
        {
          headers: {
            Authorization: `Bearer ${userInfo.token}`,
          },
        }
      );
      dispatch({
        type: BRAND_CREATE_PROFILE_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: BRAND_CREATE_PROFILE_FAIL,
        payload: message,
      });
    }
  };

export const updateBrandProfile =
  (user, brandInfo) => async (dispatch, getState) => {
    dispatch({
      type: BRAND_UPDATE_PROFILE_REQUEST,
      payload: user,
    });
    const {
      userSignin: { userInfo },
    } = getState();
    try {
      const { data } = await Axios.put(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/brands/update/${brandInfo.brandId}`,
        {
          user,
          brandInfo,
        },
        {
          headers: {
            Authorization: `Bearer ${userInfo.token}`,
          },
        }
      );
      dispatch({
        type: BRAND_UPDATE_PROFILE_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: BRAND_UPDATE_PROFILE_FAIL,
        payload: message,
      });
    }
  };

export const getBrandDetails =
  (user, brandId) => async (dispatch, getState) => {
    dispatch({
      type: BRAND_DETAILS_REQUEST,
      payload: user,
    });
    const {
      userSignin: { userInfo },
    } = getState();
    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/brands/details/${brandId}`
      );
      dispatch({
        type: BRAND_DETAILS_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: BRAND_DETAILS_FAIL,
        payload: message,
      });
    }
  };
