import {
  CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_FAIL,
  CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_REQUEST,
  CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_SUCCESS,
} from "../Constants/campaignForMultipleScreen";
import Axios from "axios";

export const createCamapaignForMultipleScreens =
  (requestBody) => async (dispatch, getState) => {
    dispatch({
      type: CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_REQUEST,
      payload: requestBody,
    });
    const {
      userSignin: { userInfo },
    } = getState();
    const {
      campaignName,
      screens,
      mediaId,
      totalSlotBooked,
      startDate,
      endDate,
      startTime,
      endTime,
    } = requestBody;
    console.log("requestBody : ", requestBody);

    try {
      const { data } = await Axios.post(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/campaignForMultipleScreens/create`,
        {
          user: userInfo,
          campaignName,
          screens,
          mediaId,
          totalSlotBooked,
          startDate,
          endDate,
          startTime,
          endTime,
        },
        {
          headers: {
            Authorization: `Bearer ${userInfo.token}`,
          },
        }
      );
      dispatch({
        type: CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_FAIL,
        payload: message,
      });
    }
  };
