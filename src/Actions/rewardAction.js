import Axios from "axios";
import {
  REWARD_CREATE_FAIL,
  REWARD_CREATE_REQUEST,
  REWARD_CREATE_SUCCESS,
  MY_CREATED_REWARD_REQUEST,
  MY_CREATED_REWARD_SUCCESS,
  MY_CREATED_REWARD_FAIL,
  REWARD_DETAILS_REQUEST,
  REWARD_DETAILS_SUCCESS,
  REWARD_DETAILS_FAIL,
  UPDATE_REWARD_REQUEST,
  UPDATE_REWARD_SUCCESS,
  UPDATE_REWARD_FAIL,
} from "../Constants/rewardConstants";

// wallet create
export const createRewardProgram = () => async (dispatch, getState) => {
  dispatch({
    type: REWARD_CREATE_REQUEST,
    // payload: input,
  });
  const {
    userSignin: { userInfo },
  } = getState();
  const user = userInfo;
  try {
    const { data } = await Axios.post(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/rewardPrograms/createReward`,
      {
        user,
        // input,
      },
      {
        headers: {
          authorization: `Bearer ${userInfo.token}`,
        },
      }
    );
    dispatch({
      type: REWARD_CREATE_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: REWARD_CREATE_FAIL,
      payload: message,
    });
  }
};

// my created rewards
export const myCreatedRewardPrograms = () => async (dispatch, getState) => {
  dispatch({
    type: MY_CREATED_REWARD_REQUEST,
  });
  const {
    userSignin: { userInfo },
  } = getState();

  try {
    const { data } = await Axios.get(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/rewardPrograms/myCreatedRewards/${userInfo._id}`
    );
    dispatch({
      type: MY_CREATED_REWARD_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: MY_CREATED_REWARD_FAIL,
      payload: message,
    });
  }
};

// get reward details
export const getRewardProgramDetails =
  (rewardId) => async (dispatch, getState) => {
    dispatch({
      type: REWARD_DETAILS_REQUEST,
      payload: rewardId,
    });
    const {
      userSignin: { userInfo },
    } = getState();

    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/rewardPrograms/reward/${rewardId}`
      );
      dispatch({
        type: REWARD_DETAILS_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: REWARD_DETAILS_FAIL,
        payload: message,
      });
    }
  };

// update reward details
export const updateRewardProgram =
  (
    rewardId,
    {
      name,
      brand,
      quantity,
      rewardProgramType,
      cardValidityTo,
      cardValidityFrom,
      rewardCardType,
      milestone,
      cardMilestones,
      couponValidityTo,
      couponValidityFrom,
      rewardCouponType,
      couponItem,
      couponRedeemFrequency,
      discountCouponType,
      discountCouponValue,
      freeItemQuantity,
    }
  ) =>
  async (dispatch, getState) => {
    dispatch({
      type: UPDATE_REWARD_REQUEST,
      payload: rewardId,
    });
    const {
      userSignin: { userInfo },
    } = getState();

    try {
      const { data } = await Axios.put(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/rewardPrograms/reward/${rewardId}`,
        {
          name,
          brand,
          quantity,
          rewardProgramType,
          cardValidityTo,
          cardValidityFrom,
          rewardCardType,
          milestone,
          cardMilestones,
          couponValidityTo,
          couponValidityFrom,
          rewardCouponType,
          couponItem,
          couponRedeemFrequency,
          discountCouponType,
          discountCouponValue,
          freeItemQuantity,
        },
        {
          headers: {
            authorization: `Bearer ${userInfo.token}`,
          },
        }
      );
      dispatch({
        type: UPDATE_REWARD_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: UPDATE_REWARD_FAIL,
        payload: message,
      });
    }
  };
