export const BRAND_CREATE_PROFILE_REQUEST = "BRAND_CREATE_PROFILE_REQUEST";
export const BRAND_CREATE_PROFILE_SUCCESS = "BRAND_CREATE_PROFILE_SUCCESS";
export const BRAND_CREATE_PROFILE_FAIL = "BRAND_CREATE_PROFILE_FAIL";
export const BRAND_CREATE_PROFILE_RESET = "BRAND_CREATE_PROFILE_RESET";

export const BRAND_UPDATE_PROFILE_REQUEST = "BRAND_UPDATE_PROFILE_REQUEST";
export const BRAND_UPDATE_PROFILE_SUCCESS = "BRAND_UPDATE_PROFILE_SUCCESS";
export const BRAND_UPDATE_PROFILE_FAIL = "BRAND_UPDATE_PROFILE_FAIL";
export const BRAND_UPDATE_PROFILE_RESET = "BRAND_UPDATE_PROFILE_RESET";

export const BRAND_DETAILS_REQUEST = "BRAND_DETAILS_REQUEST";
export const BRAND_DETAILS_SUCCESS = "BRAND_DETAILS_SUCCESS";
export const BRAND_DETAILS_FAIL = "BRAND_DETAILS_FAIL";
