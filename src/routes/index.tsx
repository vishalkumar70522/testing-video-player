import { Navigate, Route, Routes } from "react-router-dom";
import { FooterPage, Nav } from "../components/common";
import {
  AllAds,
  AllScreens,
  CampaignDetails,
  CampaignListOfUser,
  CartAndSummary,
  CreateResetPassword,
  EditScreen,
  GenerateWallet,
  HomePage,
  KeyConfirm,
  KeyManagement,
  KeyPhraseSave,
  KeyRecovery,
  Login,
  Logout,
  Page404,
  PaymentReceipt,
  PinCreate,
  PinSuccess,
  RequestMoney,
  // ScreenDetail,
  ScreenDetailPage,
  ScreenOwner,
  SendMoney,
  Signin,
  Signup,
  ViewSecrateKey,
  WalletPage,
  Welcome,
  UserProfile,
  EditUserProfile,
  FilterScreens,
  PlayListByScreen,
  EmailVerificationForForgetPassword,
  MyVideoPlayList,
  AddNewCampaign,
  PleaRequestListByUser,
  CartAndSummaryForMultipleScreen,
  AddNewCampaignOnAudianceProfile,
  BrandRewards,
  CouponRewardPageForBrand,
  EditCouponRewardOfferDetails,
  ClameCouponReward,
  MyRewardsList,
  ScreenStatus,
  CouponDetailsWithRedeemer,
  CouponDetails,
  CouponHomePage,
} from "../pages";
import { EditRewardDetails } from "../pages/EditRewardDetails";
import { EditCardRewardOfferDetails } from "../pages/EditCardRewardOfferDetails";

export const PublicRoutes = () => {
  return (
    <>
      <Nav />
      <Routes>
        <Route path="/create-wallet" element={<GenerateWallet />} />
        <Route path="/" element={<CouponHomePage />} />
        <Route path="/welcome" element={<Welcome />} />
        <Route path="/key-management" element={<KeyManagement />} />
        <Route path="/key-phrase-save" element={<KeyPhraseSave />} />
        <Route path="/key-confirm" element={<KeyConfirm />} />
        <Route path="/key-recovery" element={<KeyRecovery />} />
        <Route path="/pin-create" element={<PinCreate />} />
        <Route path="/pin-success" element={<PinSuccess />} />
        <Route path="/view-secrate-key" element={<ViewSecrateKey />} />
        {/* auth */}
        <Route path="/login" element={<Login />} />
        <Route path="/logout" element={<Logout />} />
        <Route path="/signup" element={<Signup />} />
        <Route path="/signin" element={<Signin />} />
        <Route
          path="/create-reset-password/:email/:name"
          element={<CreateResetPassword />}
        />
        <Route
          path="/forgetPassword"
          element={<EmailVerificationForForgetPassword />}
        />
        <Route path="/home" element={<Navigate to="/" />} />
        <Route path="/homepage" element={<HomePage />} />
        <Route path="/allads" element={<AllAds />} />
        <Route path="/all-screens" element={<AllScreens />} />
        <Route path="/screenstatus" element={<ScreenStatus />} />
        <Route path="/screen/:id" element={<ScreenDetailPage />} />
        <Route path="/screen-owner" element={<ScreenOwner />} />
        <Route path="/edit-screen/:id" element={<EditScreen />} />
        <Route
          path="/carts/:mediaId/:screenId/:name/:url"
          element={<CartAndSummary />}
        />
        <Route
          path="/cartsForMultipleScreens"
          element={<CartAndSummaryForMultipleScreen />}
        />
        <Route path="/myCampaignList" element={<CampaignListOfUser />} />
        <Route path="/campaignDetails/:id" element={<CampaignDetails />} />
        <Route path="/walletpage" element={<WalletPage />} />
        <Route path="/paymentreceipt/:id" element={<PaymentReceipt />} />
        <Route path="/send-money" element={<SendMoney />} />
        <Route path="/request-money" element={<RequestMoney />} />
        <Route path="/userprofile" element={<UserProfile />} />
        <Route path="/editProfile" element={<EditUserProfile />} />
        <Route path="/playlist/:id" element={<PlayListByScreen />} />
        <Route path="/MyPlayList/:id" element={<MyVideoPlayList />} />
        <Route path="/filterScreens/:text" element={<FilterScreens />} />
        <Route path="/addNewCampaign" element={<AddNewCampaign />} />
        <Route
          path="/addNewCampaignOnAudianceProfile"
          element={<AddNewCampaignOnAudianceProfile />}
        />
        <Route path="/pleaRequestList" element={<PleaRequestListByUser />} />
        {/* <Route path="/rewardAndOffer" element={<RewardPage />} /> */}
        <Route path="/editRewardAndOffer/:id" element={<EditRewardDetails />} />
        <Route
          path="/editCouponRewardAndOffer/:id"
          element={<EditCouponRewardOfferDetails />}
        />
        <Route
          path="/editCardRewardAndOffer/:id"
          element={<EditCardRewardOfferDetails />}
        />
        <Route path="/brandRewards" element={<BrandRewards />} />
        <Route
          path="/CouponRewardsForBrand"
          element={<CouponRewardPageForBrand />}
        />
        <Route path="/clameCouponReward" element={<ClameCouponReward />} />
        <Route path="/myRewards" element={<MyRewardsList />} />
        <Route
          path="/couponDetailsWithRedeemer/:couponId/:couponCode"
          element={<CouponDetailsWithRedeemer />}
        />
        <Route path="/couponDetils" element={<CouponDetails />} />

        <Route path="*" element={<Page404 />} />
      </Routes>
      <FooterPage />
    </>
  );
};
